Books
-----

Currently Reading
=================

* :doc:`books/lex-rex`:  The Law is King (1644), by Samuel Rutherford
* :doc:`books/angels-in-the-architecture` (1998), by Douglas Wilson & Douglas
  M. Jones
* :doc:`books/city-of-god` (426), by Augustine of Hippo
* :doc:`books/historical-and-theological-foundations-of-law`, Volume 1 (2011),
  by John Eidsmoe
* :doc:`books/institutes-of-the-christian-religion` (1536), by John Calvin

.. toctree::
   :titlesonly:
   :hidden:

   books/lex-rex
   books/angels-in-the-architecture
   books/city-of-god
   books/historical-and-theological-foundations-of-law
   books/institutes-of-the-christian-religion

Past Reads
==========

2025
^^^^

* :doc:`books/33-theses-on-culture-shaping` (2020), by New Saint Andrews College
* :doc:`books/lectures-on-calvinism` (1898), by Abraham Kuyper

.. toctree::
   :titlesonly:
   :hidden:

   books/33-theses-on-culture-shaping
   books/lectures-on-calvinism

2024
^^^^

* :doc:`books/on-the-principles-of-political-economy-and-taxation` (1817), by
  David Ricardo
* :doc:`books/the-quest-for-cosmic-justice` (2002), by Thomas Sowell
* :doc:`books/democracy-in-america` (1835--1840), by Alexis De Tocqueville
* :doc:`books/in-six-days`:  Why Fifty Scientists Choose to Believe in Creation
  (2001), edited by John F. Ashton
* :doc:`books/the-reformed-doctrine-of-predestination` (1932), by Loraine
  Boettner
* :doc:`books/shepherds-for-sale`:  How Evangelical Leaders Traded the Truth
  for a Leftist Agenda (2024), by Megan Basham
* :doc:`books/fallen`:  The Sons of God and the Nephilim (2019), by Tim Chaffey
* :doc:`books/rhetoric` (ca. 350 BC), by Aristotle
* :doc:`books/reflections-on-the-revolution-in-france` (1790), by Edmund Burke
* :doc:`books/its-not-like-being-black`:  How Sexual Activists Hijacked the
  Civil Rights Movement (2024), by Voddie Baucham Jr
* :doc:`books/black-rednecks-and-white-liberals` (2006), by Thomas Sowell
* :doc:`books/the-christian-sabbath`:  Its nature, design, and proper
  observance (1882), by R.L. Dabney
* :doc:`books/the-roman-catholic-controversy`:  Catholics & Protestants---Do
  the Differences Still Matter? (1996), by James R. White
* :doc:`books/human-action`:  A Treatise on Economics (1940), by Ludwig von
  Mises
* :doc:`books/the-toxic-war-on-masculinity`:  How Christianity Reconciles the
  Sexes (2023), by Nancy Pearcey
* :doc:`books/the-vision-of-the-anointed`:  Self-Congratulation as a Basis for
  Social Policy (1996), by Thomas Sowell
* :doc:`books/vindiciae-contra-tyrannos`:  A Defense of Liberty Against Tyrants
  (1581), by Junius Brutus

.. toctree::
   :titlesonly:
   :hidden:

   books/on-the-principles-of-political-economy-and-taxation
   books/the-quest-for-cosmic-justice
   books/democracy-in-america
   books/in-six-days
   books/the-reformed-doctrine-of-predestination
   books/shepherds-for-sale
   books/fallen
   books/rhetoric
   books/reflections-on-the-revolution-in-france
   books/its-not-like-being-black
   books/black-rednecks-and-white-liberals
   books/the-christian-sabbath
   books/the-roman-catholic-controversy
   books/human-action
   books/the-toxic-war-on-masculinity
   books/vindiciae-contra-tyrannos
   books/the-vision-of-the-anointed

2023
^^^^

* :doc:`books/refuting-the-new-atheists`:  A Christian Response to Sam Harris,
  Christopher Hitchens, and Richard Dawkins (2021), by Douglas Wilson
* :doc:`books/the-republic` (381 BC), by Plato
* :doc:`books/church-music-and-the-other-kinds`:  A Musical Manifesto of Sorts
  (2014), by Douglas Wilson
* :doc:`books/christianity-and-the-constitution`:  The Faith of Our Founding
  Fathers (1987), by John Eidsmoe
* :doc:`books/fight-by-flight`:  Why Leaving Godless Places is Loving Godless
  Places (2023), by Joel Webbon
* :doc:`books/the-church-impotent`:  The Feminization of Christianity (1999),
  by Leon J. Podles
* :doc:`books/christian-political-action-in-an-age-of-revolution` (1860), by
  Guillaume Groen van Prinsterer
* :doc:`books/men-and-marriage` (1986), by George Gilder
* :doc:`books/by-this-standard`:  The Authority of God's Law Today (1991), by
  Greg Bahnsen
* :doc:`books/mere-christendom` (2023), by Douglas Wilson
* :doc:`books/how-to-read-a-book`:  The Classic Guide to Intelligent Reading
  (1940), by Mortimer J. Adler and Charles van Doren
* :doc:`books/the-case-for-christian-nationalism` (2022), by Stephen Wolfe
* :doc:`books/in-the-house-of-tom-bombadil` (2021), by C.R. Wiley
* :doc:`books/how-to-be-an-atheist`:  Why Many Skeptics Aren't Skeptical Enough
  (2016), by Mitch Stokes
* :doc:`books/recovering-the-lost-tools-of-learning`:  An Approach to
  Distinctively Christian Education (1991), by Douglas Wilson
* :doc:`books/skin-and-blood`:  A Gospel Approach to Race & Racial Animosity
  (2017), by Douglas Wilson
* :doc:`books/family-shepherds`:  Calling and Equipping Men to Lead Their Homes
  (2011), by Voddie Baucham Jr
* :doc:`books/wordsmithy`:  Hot Tips for the Writing Life (2011), by Douglas
  Wilson
* :doc:`books/my-life-for-yours`:  A Walk through the Christian Home (2004), by
  Douglas Wilson
* :doc:`books/the-seven-laws-of-teaching` (1954), by John Milton Gregory
* :doc:`books/rules-for-reformers` (2014), by Douglas Wilson
* :doc:`books/the-case-for-the-christian-family`:  The Covenantal Solution for
  the Dissolving American Family (2022), by Jared Longshore
* :doc:`books/future-men`:  Raising Boys to Fight Giants (2001), by Douglas
  Wilson
* :doc:`books/sinners-in-the-hands-of-an-angry-god` (1741), by Jonathan Edwards
* :doc:`books/mis-inflation`:  The Truth about Inflation, Pricing, and the
  Creation of Wealth (2022), by David L. Bahsen & Douglas Wilson
* :doc:`books/rigged`:  How the Media, Big Tech, and the Democrats Seized Our
  Elections (2021), by Mollie Hemingway
* :doc:`books/classical-education-and-the-homeschool` (1995), by Wesley
  Callihan *et al*
* :doc:`books/principles-of-war`:  Thoughts on Strategic Evangelism (1961), by
  Jim Wilson
* :doc:`books/on-secular-education` (1879), by R.L. Dabney
* :doc:`books/black-and-tan`:  A Collection of Essays and Excursions on
  Slavery, Culture War, and Scripture in America (2005), by Douglas Wilson
* :doc:`books/eggs-are-expensive-sperm-is-cheap`:  50 Politically Incorrect
  Thoughts for Men (2014), by Greg Krehbiel
* :doc:`books/masculine-christianity` (2020), by Zachary Garris
* :doc:`books/gashmu-saith-it`:  How to Build Christian Communities that Save
  the World (2021), by Douglas Wilson

.. toctree::
   :titlesonly:
   :hidden:

   books/refuting-the-new-atheists
   books/the-republic
   books/church-music-and-the-other-kinds
   books/christianity-and-the-constitution
   books/fight-by-flight
   books/the-church-impotent
   books/christian-political-action-in-an-age-of-revolution
   books/men-and-marriage
   books/by-this-standard
   books/mere-christendom
   books/how-to-read-a-book
   books/the-case-for-christian-nationalism
   books/in-the-house-of-tom-bombadil
   books/how-to-be-an-atheist
   books/recovering-the-lost-tools-of-learning
   books/skin-and-blood
   books/family-shepherds
   books/wordsmithy
   books/my-life-for-yours
   books/the-seven-laws-of-teaching
   books/rules-for-reformers
   books/the-case-for-the-christian-family
   books/future-men
   books/sinners-in-the-hands-of-an-angry-god
   books/mis-inflation
   books/rigged
   books/classical-education-and-the-homeschool
   books/principles-of-war
   books/on-secular-education
   books/black-and-tan
   books/eggs-are-expensive-sperm-is-cheap
   books/masculine-christianity
   books/gashmu-saith-it

2022
^^^^

* :doc:`books/critique-of-pure-reason` (1781), by Immanuel Kant
* :doc:`books/law-legislation-and-liberty` (1973--1979), by F.A.  Hayek
* :doc:`books/slaying-leviathan`:  Limited Government and Resistance in the
  Christian Tradition (2020), by Glenn Sunshine
* :doc:`books/federal-husband` (1999), by Douglas Wilson
* :doc:`books/critique-of-modern-youth-ministry` (1995), by Chris Schlect
* :doc:`books/the-wealth-of-nations` (1776), by Adam Smith
* :doc:`books/pandemia`:  How Coronavirus Hysteria Took Over our Government,
  Rights, and Lives (2021), by Alex Berenson
* :doc:`books/people-styles-at-work-and-beyond` (1999), by Robert Bolton &
  Dorothy Grover Bolton
* :doc:`books/making-sense-of-your-world`:  A Biblical Worldview (2008), by W.
  Gary Phillips, William E. Brown, and John Stonestreet
* :doc:`books/knowing-god` (1973), by J.I. Packer
* :doc:`books/another-gospel`  A Lifelong Christian Seeks Truth in Response to
  Progressive Christianity (2020), by Alisa Childers
* :doc:`books/seeking-allah-finding-jesus`:  A Devout Muslim Encounters
  Christianity (2018), by Nabeel Qureshi

.. toctree::
   :titlesonly:
   :hidden:

   books/critique-of-pure-reason
   books/law-legislation-and-liberty
   books/slaying-leviathan
   books/federal-husband
   books/critique-of-modern-youth-ministry
   books/the-wealth-of-nations
   books/pandemia
   books/people-styles-at-work-and-beyond
   books/making-sense-of-your-world
   books/knowing-god
   books/another-gospel
   books/seeking-allah-finding-jesus

2021
^^^^

* :doc:`books/a-practical-guide-to-culture`:  Helping the Next Generation
  Navigate Today's World (2019), by John Stonestreet and Brett Kunkle
* :doc:`books/tactics`: A Game Plan for Discussing Your Christian Convictions
  (2019), by Greg Koukl
* :doc:`books/how-now-shall-we-live` (1999), by Charles Colson and Nancy
  Pearcey
* :doc:`books/the-prince` (1532), by Niccolò Machiavelli
* :doc:`books/five-dialogues` (ca. 350 BC), by Plato
* :doc:`books/cultural-apologetics` (2019), by Paul Gould
* :doc:`books/meditations-on-first-philosophy` (1641), by René Descartes
* :doc:`books/an-enquiry-concerning-human-understanding` (1748), by David Hume
* :doc:`books/vaccines-truth-lies-and-controversy` (2021), by Peter C.
  Gøtzsche
* :doc:`books/confronting-injustice-without-compromising-truth`: 12 Questions
  Christians Should Ask (2020), by Thaddeus Williams
* :doc:`books/why-you-think-the-way-you-do`:  The Story of Western Worldviews
  from Rome to Home (2009), by Glenn Sunshine
* :doc:`books/covid-why-most-of-what-you-know-is-wrong` (2021), by Sebastian
  Rushworth
* :doc:`books/the-managers-path`:  A Guide for Tech Leaders Navigating Growth &
  Change (2017), by Camille Fournier
* :doc:`books/mere-christianity` (1952), by C.S. Lewis
* :doc:`books/the-social-contract` (1762), by Jean-Jaques Rousseau
* :doc:`books/two-treatises-of-government` (1689), by John Locke
* :doc:`books/the-constitution-of-the-united-states-of-america` (1787)
* :doc:`books/the-unicorn-project`:  A Novel about Developers, Digital
  Disruption, and Thriving in the Age of Data (2019), by Gene Kim
* :doc:`books/leviathan` (1651), by Thomas Hobbes
* :doc:`books/fault-lines`:  The Social Justice Movement and Evangelicalism's
  Looming Catastrophe (2021), by Voddie Baucham Jr
* :doc:`books/the-federalist-papers` (1787--1788), by Alexander Hamilton, James
  Madison, and John Jay
* :doc:`books/strong-and-courageous`:  Following Jesus Amid the Rise of
  America's New Religion (2021), by Jared Longshore and Tom Ascol
* :doc:`books/political-visions-and-illusions`:  A Survey & Christian Critique
  of Contemporary Ideologies (2003), by David Koyzis
* :doc:`books/the-law` (1850), by Frédéric Bastiat
* :doc:`books/total-truth`:  Liberating Christianity from Its Cultural
  Captivity (2005), by Nancy Pearcey
* :doc:`books/the-communist-manifesto` (1888), by Karl Marx and Friedrich
  Engels

.. toctree::
   :titlesonly:
   :hidden:

   books/a-practical-guide-to-culture
   books/tactics
   books/how-now-shall-we-live
   books/the-prince
   books/five-dialogues
   books/cultural-apologetics
   books/meditations-on-first-philosophy
   books/an-enquiry-concerning-human-understanding
   books/vaccines-truth-lies-and-controversy
   books/confronting-injustice-without-compromising-truth
   books/why-you-think-the-way-you-do
   books/covid-why-most-of-what-you-know-is-wrong
   books/the-managers-path
   books/mere-christianity
   books/the-social-contract
   books/two-treatises-of-government
   books/the-constitution-of-the-united-states-of-america
   books/the-unicorn-project
   books/leviathan
   books/fault-lines
   books/the-federalist-papers
   books/strong-and-courageous
   books/political-visions-and-illusions
   books/the-law
   books/total-truth
   books/the-communist-manifesto

2020
^^^^

* :doc:`books/the-consequences-of-ideas`:  Understanding the Concepts that
  Shaped Our World (2000), by R.C. Sproul
* :doc:`books/on-liberty` (1859), by John Stuart Mill
* :doc:`books/the-road-to-serfdom` (1944), by F.A.  Hayek
* :doc:`books/a-conflict-of-visions`:  Ideological Origins of Political
  Struggles (1987), by Thomas Sowell
* :doc:`books/live-not-by-lies`:  A Manual for Christian Dissidents (2020), by
  Rod Dreher
* :doc:`books/expository-apologetics`:  Answering Objections with the Power of
  the Word (2015), by Voddie Baucham Jr
* :doc:`books/how-to-speak-how-to-listen` (1983), by Mortimer J. Adler
* :doc:`books/the-twelve-days-of-the-aspen-executive-seminar` (1972), by
  Mortimer J. Adler

.. toctree::
   :titlesonly:
   :hidden:

   books/the-consequences-of-ideas
   books/on-liberty
   books/the-road-to-serfdom
   books/a-conflict-of-visions
   books/live-not-by-lies
   books/expository-apologetics
   books/how-to-speak-how-to-listen
   books/the-twelve-days-of-the-aspen-executive-seminar

