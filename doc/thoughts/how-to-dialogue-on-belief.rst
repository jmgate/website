How to Effectively Dialogue on Matters of Belief
------------------------------------------------

.. raw:: html

   <p style="text-align:left;">
       <i>February 10, 2020</i>
       <span style="float:right;">
           🕑 30 min.
       </span>
   </p>

.. note::

   The following was written as a proposal to the leadership of my church for
   how to go about addressing some potentially contentious doctrinal issues
   within the congregation.  It has been adapted for wider distribution.

.. contents::  Contents
   :local:
   :depth: 2

Preface
=======

.. epigraph::

   "'For where two or three have gathered together in My name, I am there in
   their midst.'  (Matthew 18:20, NASB), but then you have the problem of
   having two or three gathered."

   -- Jason M. Gates

Life in community is hard.  Conversation in community is hard.  Effective
dialogue about beliefs in community is hard.  So how do we go about it?  How
can we discuss deeply held beliefs and convictions, where others likely
disagree, all the while extending love, grace, and understanding to our
brothers and sisters in Christ?  Really good questions.  Short answer:
carefully.  Long answer:  read on.

With a local body of believers with more than say a dozen or so families, there
are bound to be significant differences in viewpoint across the congregation.
Such diversity of belief on secondary matters is to be expected, perhaps even
encouraged.  However, when such an issue is cause for significant consternation
within the congregation, what should we do?  Here are some example issues that
may be floating around under the surface within the congregation:

What is the purpose of the local church?
  Is it to be a "service oriented architecture" of sorts (sorry if my software
  analogy doesn't make sense), where we can plug in a "service" (ministry) for
  any given demographic group (young adults, senior adults, newly married
  folks, gardening enthusiasts, the argyle sock community, etc.)?  Is it to
  glorify God by multiplying transformational churches among all people?  What
  is it?  Our answer to this question has significant implications for who we
  are and what we do.
What are the roles and responsibilities of deacons, directors, pastors, elders, overseers, etc.?
  How do these positions differ?  In what areas is there overlap?  What are our
  expectations of people in these positions?  How should these different roles
  interact?  Our answers here have implications for our church polity and how
  our church functions.
What are appropriate roles for women in ministry?
  This is a question that church leaders are often unwilling to touch for fear
  of "poking the hornets' nest".  What do we believe here?  How should women be
  involved in the life of the church?  Can they be deaconesses?  Can a woman
  lead the deacon board?  Can women speak from the stage during Sunday
  services?  Can a woman preach the sermon?  Can women lead ministry areas?
  Can we have female pastors?  Questions abound---what do we believe and why?

How do we go about answering any of these questions?  This document is a
walkthrough of how I'd go about it if it were up to me.  I've tried to map this
out as generally as possible, such that you can insert anything as the question
for consideration.  Is this the only way to try answering such questions in a
congregational setting?  No.  Is it a good way?  I think so.  Is the
methodology set in stone?  Nope.  I'd encourage tweaking things depending on
the topic you're tackling and the people involved in the conversations.

But who on earth am I, such that I have so many thoughts and opinions on how to
approach topics such as these?  My name is Jason Gates, and I'm a teacher.  Now
when I say "teacher", I don't mean that I'm the subject matter expert who has
all the answers, which I will then impart to you---far from it.  When I say
"teacher", I mean one who comes alongside a group of learners to aid in the
learning process.  It's about helping people investigate, ruminate, discuss,
learn from one another, consider multiple viewpoints, and arrive at
conclusions.  When I'm teaching I'm operating in the intersection of my skills
and passions, where I'm most naturally motivated.  It's who I am; who I'm meant
to be.

What experience do I have?  In terms of official teaching experience, I taught
first- and second-graders in the `Bible Study Fellowship International`_
children's program for two years.  After that I taught everything from College
Algebra through Advanced Engineering Mathematics at the university level for
five years.  (Turns out college students aren't terribly different from first-
and second-graders.)  During that time I devoured any book on pedagogy and
andragogy I could get my hands on, and took part in whatever classes or focus
groups on education were available.  After transitioning to software
engineering, I still look for opportunities to help others learn to
improve.  (Turns out adults aren't terribly different from college students,
except I can't motivate them with grades or pizza any more.)  I also coach
Eldorado's speech and debate team, where all of these same skills come into
play.

.. _Bible Study Fellowship International:  https://www.bsfinternational.org/

What do I know about learning?  For that you can check out the appendices.
|app_philosophy_of_learning|_ has a write-up of my philosophy of learning---how
I think it works best.  |app_scaffolding|_ contains some background information
on the concept of scaffolding, which undergirds how some things are structured
in |sec_laying_the_foundation|_.

Looking forward to fruitful conversations together,

Jason M. Gates

.. |sec_summary| replace::  Section 1
.. _sec_summary:

1 Summary
=========

Time is precious and you likely don't have time to read through this whole
document right now.  No worries---here are the highlights:

* Invite anyone from the congregation who wants to dive into the subject to
  take part in a study group.  (|sec_forming_the_group|_)
* Make sure they know there will be significant work required to take part (at
  least an hour of discussion, an hour of reading, and an hour of writing per
  week).  (|sec_the_group_rhythm|_)
* Establish some ground rules:  (|sec_establishing_ground_rules|_)

  * Be punctual.
  * Prepare in advance.
  * Love it for a minute.
  * Others agreed upon by the group.

* Start the practice of quarterly fellowships for the sake of growing together
  in relationship.
* Determine the starting point:  (|sec_laying_the_foundation|_)

  * What do you currently believe?
    (|sec_what_do_you_currently_believe_about_this_topic|_)
  * Why?  (|sec_why_do_you_believe_what_you_believe|_)
  * What questions do you have?  (|sec_what_questions_do_you_have|_)
  * What concerns do you have?  (|sec_what_concerns_do_you_have|_)
  * What are your motivations?  (|sec_what_are_your_motivations|_)
  * Have you reached a place of indifference?
    (|sec_have_you_reached_a_place_of_indifference|_)

* Study the Bible.  (|sec_looking_to_scripture|_)

  * Have the group suggest passages.
  * Ask questions of the book:

    * Who's the author?
    * Who's the audience?
    * What was the cultural context?
    * What was going on in the church?
    * What was the author's purpose?
    * How does it fit in context?

  * Ask questions of the passage:

    * What does it say?
    * What does it not say?
    * How does it fit in context?
    * What's the particular meaning?
    * Does it address the question at hand directly?

  * Take your time.
  * Consult commentaries only after personal study and group discussion.

* Study history using the same principles used for Bible study.
  (|sec_looking_to_history|_)
* If needed to draw conclusions in your current study, take some time to answer
  related questions.  (|sec_answering_related_questions|_)
* Determine the ending point:  (|sec_drawing_conclusions|_)

  * What do you currently believe?
    (|sec_what_do_you_currently_believe_about_this_topic_2|_)
  * Why?  (|sec_why_do_you_believe_what_you_believe_2|_)
  * What questions remain unanswered?
    (|sec_what_questions_remain_unanswered|_)
  * Where do we agree?  (|sec_where_do_we_agree|_)
  * Where do we disagree?  (|sec_where_do_we_disagree|_)
  * Where do we go from here?  (|sec_where_do_we_go_from_here|_)

.. |sec_preliminaries| replace::  Section 2
.. _sec_preliminaries:

2 Preliminaries
===============

.. |sec_forming_the_group| replace::  Section 2.1
.. _sec_forming_the_group:

2.1 Forming the Group
~~~~~~~~~~~~~~~~~~~~~

A first step in having a conversation about what we believe will be determining
who will be involved in that conversation.  What voices do we need to hear
from?  Who should be doing the investigative leg-work?

My personal preference would be to open up the group to anyone in the
congregation who would like to be involved.  If you have the Holy Spirit of God
in you, I figure I'd better listen to what he might be saying through you.  If
we're trying to figure out what we believe as a local body of believers, the
membership of that body should be involved.

Beyond the open invitation to the congregation, I'd also strongly encourage
church leadership to participate.  I don't think we can make it a requirement,
just due to scheduling logistics, but if you're available, it'd be good for you
to be involved.  Does that mean the leaders do all the digging and everyone
else is along for the ride?  "May it never be!"  We're all in this together.

.. |sec_the_group_rhythm| replace::  Section 2.2
.. _sec_the_group_rhythm:

2.2 The Group Rhythm
~~~~~~~~~~~~~~~~~~~~

I'm picturing the group having a weekly gathering for about an hour at a time.
Between meetings participants would be expected to spend about an hour doing
some reading or other investigation, and about an hour doing some writing, in
preparation for the next get-together.  Three hours a week should be
manageable, but as with most learning, the more you put into it, the more
you'll get out of it.

A natural time to hold our weekly meetings might be during the Sunday school
hour, but I get the feeling meeting some time other than Sunday mornings will
require some extra committment from folks from the get-go, and that'd be
worthwhile.  What day and time would be ideal?  That bit's negotiable.

In addition to the weekly rhythm, it'd be worthwhile to have at least a
quarterly fellowship for folks to get to know each other better.  It's hard to
trust people if you don't really know them, and it's hard to get to know people
if you're only ever studying something together.  You need opportunities to
build friendships in other contexts, chances to just kick back and relax and
enjoy life together.  Picnic at the park?  Cook-out at someone's house?
Bowling night?  The sky's the limit.

.. |sec_establishing_ground_rules| replace::  Section 2.3
.. _sec_establishing_ground_rules:

2.3 Establishing Ground Rules
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

There are a handful of ground rules for participation that I'd like to impose
from the beginning:

Be punctual.
  In order to make the best use of our limited time together, please arrive and
  be prepared to start on time.
Prepare in advance.
  If in any given week you are unable to do the requisite reading/writing ahead
  of our gathering (life happens; no judgment here), you are welcome to join
  us, but please remain silent.  We want to ensure the conversation is driven
  by those who have prayerfully prepared for it ahead of time.
Love it for a minute.
  When someone throws out an idea, you may be tempted, in the moment, to react
  strongly against it and start tearing it apart.  Before doing so, you must
  first "love it for a minute"---try to see and understand things from the
  other perspective.

Beyond this initial set, it will be beneficial for the group to define any
additional ground rules they feel will be important.  Some suggestions:

* Listen respectfully, without interrupting.
* Listen actively and with an ear to understanding others' views.  Don’t just
  think about what you are going to say while someone else is talking.
* Criticize ideas, not individuals.
* Commit to learning, not debating.  Comment in order to share information, not
  to persuade.
* Avoid blame and inflammatory language.
* Allow everyone the chance to speak.

The list of ground rules can be revisited and amended throughout the
investigation if necessary.

.. |sec_laying_the_foundation| replace::  Section 3
.. _sec_laying_the_foundation:

3 Laying the Foundation
=======================

.. sidebar::  Side Note

   See |app_scaffolding|_ for some of the motivation behind questions such
   as the ones suggested in this section.

Before diving into a significant discussion, it's important to know where
everyone is coming from.  Often we may not even know where exactly we currently
stand or why we believe what we do on a particular topic, so it's important to
have an intentional time of introspection.  The following questions would be
homework assignments, one per week, for the participants to ask of and answer
for themselves in writing.

.. |sec_what_do_you_currently_believe_about_this_topic| replace::  Section 3.1
.. _sec_what_do_you_currently_believe_about_this_topic:

3.1 What do you currently believe about this topic?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Don't talk to anyone else about this (yet).  Don't refer to any books you may
have read.  Don't even crack open the Bible.  (Gasp!  Who let this guy teach in
church?  Wait till we get to |sec_looking_to_scripture|_.)  I just want
whatever information is in your head translated onto a piece of paper.  What
are your thoughts today?  Be as specific as possible.  Generalities can often
be unproductive in dialogue.  The more specificity, the more fruitful the
discussion can be.

Note that this can just be a brain-dump---getting your thoughts out on paper.
If you'd like to take the extra time to rearrange your thoughts into a
well-crafted essay, that would be appreciated, but isn't necessary.  The
importance is in writing things down so we have something concrete to talk
through.

.. |sec_why_do_you_believe_what_you_believe| replace::  Section 3.2
.. _sec_why_do_you_believe_what_you_believe:

3.2 Why do you believe what you believe?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Once you've identified what you believe, the next step is figuring out where
those beliefs came from.  They didn't just pop into your head out of nowhere.
Were they informed by your parents?  Perhaps by your peers?  Is a past
situation closely tied to a particular facet of your beliefs?  How have your
thoughts developed over time?  How have past communities you've been a part of
viewed the same topic?  Does what you believe in another context impact your
thinking here?  If so, how?

Again, don't talk to anyone before you jot down your thoughts.  No books, no
scripture---just whatever's in your head.  I realize books/scriptures/community
inform why you believe what you believe, but at any given moment you don't have
access to all of them instantaneously.  What you do have is what's in your
head, and that's what I want you to put down on paper.

.. |sec_what_questions_do_you_have| replace::  Section 3.3
.. _sec_what_questions_do_you_have:

3.3 What questions do you have?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

We should already be on the same page in terms of tackling the general
question---what do we believe about [fill in the blank]?  However to answer the
general question it will be helpful to dive into a number of specifics.  What
particular questions do you have in mind that need answering?  Write down as
few or as many as you feel are important.  Are certain questions more or less
important to you?  Again, remember that specificity will be our friend.

.. |sec_what_concerns_do_you_have| replace::  Section 3.4
.. _sec_what_concerns_do_you_have:

3.4 What concerns do you have?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

As we wade into a conversation about our beliefs, it will be good to keep in
mind that examining those beliefs can be a bit unnerving.  What worries you as
you're beginning on this journey with us?  Are you concerned we'll wind up in a
particular spot?  Are you worried we'll go about this the wrong way?  Will
relationships be marred by coming to different conclusions?  Jot down your
concerns, and ask God to help you trust him as he helps us through this.

.. |sec_what_are_your_motivations| replace::  Section 3.5
.. _sec_what_are_your_motivations:

3.5 What are your motivations?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

It's likely the case, to a greater or lesser extent, that we all walk into this
investigation with some sort of personal agenda in mind.  What are you hoping
for?  Do you want us to wind up somewhere particular?  Are you hoping we do
things a certain way?  Why did you decide to take part?  If things don't turn
out the way you hope they will, what will that mean for you?

.. |sec_have_you_reached_a_place_of_indifference| replace::  Section 3.6
.. _sec_have_you_reached_a_place_of_indifference:

3.6 Have you reached a place of indifference?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

At this point we've tried, individually, to identify what we currently believe
and why.  We've sketched out numerous specific questions we hope to find
answers to in our investigation, and we've admitted that we are burdened with a
variety of concerns.  We've tried to be honest with ourselves and identify our
motivations as we approach the topic.  It will be easy, natural even, to read
our beliefs and agendas into our study.  Are you in a place where you are
willing and able to hear with fresh ears what God would reveal to you?  Are you
indifferent to everything but the will of God?

If not, that's nothing to be ashamed of.  Are you still attached to a
particular outcome?  Are you unsure if you'll be able to reconsider a certain
belief?  Is there anything that might prevent you from seeing clearly as we
begin our study?  This may be an assignment you'll need to revisit a handful of
times, and that's okay.

.. |sec_the_study_itself| replace::  Section 4
.. _sec_the_study_itself:

4 The Study Itself
==================

.. |sec_looking_to_scripture| replace::  Section 4.1
.. _sec_looking_to_scripture:

4.1 Looking to Scripture
~~~~~~~~~~~~~~~~~~~~~~~~

At this point, we're roughly six weeks into our investigation and we're finally
ready to starting digging into source material.  The primary source for an
investigation such as this should of course be the Bible itself, but how should
we go about structuring our study?  Here are some suggestions.

Have the group suggest passages
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In the preceding six weeks, be collecting suggested passages from members of
the group.  Any bits of scripture that folks think might be relevant, jot them
down.  This helps in developing buy-in; that is, the members of the group will
know experientially that they are a part of determining what we believe and
why.  It's not a matter of some leader telling you what passages of scripture
to look at---it's all of us within the body looking at scripture with the Holy
Spirit's leading.

Setting the order
^^^^^^^^^^^^^^^^^

Once you've collected a first cut of passages to look into, how do you go about
setting the order for when you dig into which passage?  This part can be
totally flexible.  I'd suggest you group passages within a given book of the
Bible together, but that's not absolutely necessary.  It might also be
worthwhile to hit things in a more-or-less chronological order, but again, feel
free to shake things up as needed.  It may be the case that you can tell ahead
of time that certain passages will be more contentious than others.  In those
cases, it might be worthwhile to slate those passages for later in the
conversation after the group has had more time to learn how to interact well
with each other.  The flip-side of that is if you know there are certain
passages that will prompt significant agreement, those might be good for
earlier in the investigation.  I don't want to be prescriptive here.  Every
group is different and the instructor needs to be able to adapt to the dynamics
within.

What questions should we ask?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Once an order has been set, how do you go about studying any given passage of
scripture?  Here are some common questions we should ask and attempt to answer,
though this list isn't meant to be exhaustive.

At the book level:
++++++++++++++++++

The following questions help us to establish the overall context of the book in
which the passage sits.

Who is the author?
  What do we know about him?  What's his background?  Where was he in his life
  when he sat down to write the book?
Who is the audience?
  Who was the author writing to?  Who were those people?  What do we know about
  them?  What was their background?  Keep in mind, there might be multiple
  audiences intended, e.g., a book written to a particular person, but then
  intended to be shared with a local church or churches.
What was the cultural context?
  What was going on at the time?  What was life like where the audience lived?
  How does that context differ from our own?  Are there aspects of the cultural
  context that are hard for us to wrap our heads around today?
What was going on in the church?
  If applicable, what did the current situation in the church look like?  How
  did the church context differ from the cultural context?  How did it not?
What was the author's purpose?
  Why did he write the book to the audience?  Was he intending to inform?
  Persuade?  Rebuke?  Encourage?  Note that in some cases you may not be able
  to come to concrete answers, but you should be able to get a general idea.
How does it fit in context?
  Given the overall context of the Bible as a whole, how does this particular
  book fit in?

At the passage level:
+++++++++++++++++++++

Once the overall context has been established, we can dive into the specifics
of the passage itself.

What does the passage say?
  Try not to impart you own meaning to the passage.  What does it actually say?
  What did it say to the original audience?
What does it not say?
  Perhaps more important than the last question, let's be clear about what a
  passage does not specifically say.
How does it fit in context?
  Given the context established at the book level, how does the particular
  passage fit?  How does it contribute to the overall message?
What is its particular meaning?
  If this passage were omitted, how would that affect the overall meaning or
  purpose of the book?
Does this speak to the question directly?
  Or does the current passage just touch the question you're looking into
  tangentially?

How long do we spend?
^^^^^^^^^^^^^^^^^^^^^

Hang on a second.  If the group is coming up with all the source material we'll
look into, and for any given passage we're going to try to answer all the
questions above, how long is this study going to take?  As long as it takes.
I'd be very hesitant to attempt to time-box the study, becuse it feels like
that would be telling God, "I trust you to reveal your will to us, but I'm
requiring you to do it within the next three months."  Sounds just a wee bit
arrogant to me.

What about commentaries?
^^^^^^^^^^^^^^^^^^^^^^^^

Bible commentaries are great, but can I make a request?  Don't go to them right
away.  Take time first with just you, the passage of scripture, and the Holy
Spirit.  See what he reveals to you with just the Bible itself.  Bring all your
insights with you to our discussion group and then let's hear from one another
what God has been showing us through his word.  After that, if you want to go
back to see what a handful of commentaries say about a passage, I heartily
encourage you to do so.  My caution here is that if reading something puts a
certain interpretation in your mind ahead of time, it can be hard to see
something other than that interpretation in the passage.

.. |sec_looking_to_history| replace::  Section 4.2
.. _sec_looking_to_history:

4.2 Looking to History
~~~~~~~~~~~~~~~~~~~~~~

While the Bible should be our primary source when investigating questions of
belief, we want to make sure we're not attempting to interpret it in a vacuum.
Chances are the church universal has been thinking through the same questions
you're pondering for hundreds or thousands of years.  It'd be worthwhile to
take into consideration what the church has thought over time.  How should we
go about doing that?

I'd suggest following the same principles outlined in
|sec_looking_to_scripture|_:

* Have folks suggest sources to look into so it's very much the group doing the
  investigation and not just the leader.
* How should you sequence the material?  Chronologically may be worthwhile so
  you can trace the progression of the church's beliefs through time.  It may
  also be beneficial to look to earlier sources first, because they will be
  closer to the cultural context of the Mediterranean region in the first
  century.  Keep in mind that the original readers may have likely read and
  understood things very differently from how you do initially.  All that being
  said, a thematic organization of the content might make sense too.  Again,
  you can be flexible here.
* Ask and attempt to answer many of the same questions we intend to ask of
  scripture, with the understanding that you're not necessarily looking at a
  Holy Spirit-inspired text this time around.
* And take your time.  If the church has been ruminating on your question for
  generations, you probably don't need an answer by tomorrow.

Though the organization of this section makes it look like you should do all
your Bible study before you get to any study of church history, that's not
necessarily the case.  It may be worthwhile to interweave some of the
historical study with the scriptural study.  Back and forth between the two
might be natural in some cases.  If you do, though, it will be important to
differentiate that which is scripture from that which isn't.  The Bible is our
ultimate authority---if something doesn't jive with it, it's *no bueno*.

.. |sec_answering_related_questions| replace::  Section 4.3
.. _sec_answering_related_questions:

4.3 Answering Related Questions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In the course of our investigation, we may find the questions we're asking are
intimately intertwined with other questions that we don't necessarily have
answers to yet.  It may be the case they're related at the big picture
level---e.g., to determine what the roles, responsibilities, etc., of church
offices are, do we first need to determine what the purpose of the local church
is?  Alternatively they may be related at a more specific level---e.g., to
answer the question of whether it's appropriate for a woman to be a pastor, do
we first need to have a discussion on a wife and mother's role in the home?
When such questions come up, how do we deal with them?  I would recommend one
of two options.

On the one hand we could set them aside for another time.  We could continue
our current study as-is, and when we get to the end, acknowledge as a group
that there are questions that remain unanswered because we chose not to dig
into them at this time (see |sec_what_questions_remain_unanswered|_).
That's completely fine.  I'm not expecting us to answer every single question
we could possibly come up with.  However, when we get to the point of drawing
conclusions, we must acknowledge that there are still gaps in our
understanding, and perhaps hold more loosely any conclusions we know to be
intimately tied to any such gaps.

On the other hand, it might be worthwile to consider this process inside a
recursive loop.  If we come up against a figurative brick wall in our
understanding, we can say, "Before we go any further in our investigation of X,
it appears we need to spend some time figuring out what we believe about Y."
Push pause on the current study, and head back to the beginning to start afresh
on the new question.  Once you've figured out what you think about Y, return to
where you left things off in the study of X.

Well hang on a second, we can't spend the next twenty years trying to answer
this question!  (Why not?  Would that not be a fruitful time?)  I understand
the hesitancy.  Perhaps you abbreviate the process.  Maybe you go through the
various questions we kicked things off with (see
|sec_laying_the_foundation|_) and realize that the group is already more
or less in alignment.  Perhaps you realize the main points you all agree on are
all that is necessary to contribute back to the original conversation, and the
handful of places where you disagree are inconsequential.  The goal is for you
to understand corporately what you believe and why.  How you get from here to
there can vary from question to question.

.. |sec_drawing_conclusions| replace::  Section 5
.. _sec_drawing_conclusions:

5 Drawing Conclusions
=====================

When it seems the investigation is drawing to a close, it's time to wrap things
up by asking and answering a number of questions.  These assignments parallel
those in |sec_laying_the_foundation|_, but here they serve as a means of
assessing what progress has been made throughout the study.

.. |sec_what_do_you_currently_believe_about_this_topic_2| replace::  Section 5.1
.. _sec_what_do_you_currently_believe_about_this_topic_2:

5.1 What do you currently believe about this topic?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This is the same question we asked to kick things off.  It may be the case that
your thinking hasn't changed much.  Then again, what you believe may have
changed significantly.  Perhaps the changes in your understanding were broad
and sweeping, or perhaps they were slight tweaks to small particulars.
Whatever the case, please jot down your current thoughts.

.. |sec_why_do_you_believe_what_you_believe_2| replace::  Section 5.2
.. _sec_why_do_you_believe_what_you_believe_2:

5.2 Why do you believe what you believe?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

At this point we've spent months digging down into the details so you should
have a pretty good idea of the foundations supporting your beliefs.  This
assignment, along with the last one, is kind of like your "final exam".  Tell
me what you think and why.  This time please do refer to any source material
you like.  I realize you only have what's in your head at the spur of the
moment, but here we'd like to summarize all the details of the last many months
of dialogue.

.. |sec_what_questions_remain_unanswered| replace::  Section 5.3
.. _sec_what_questions_remain_unanswered:

5.3 What questions remain unanswered?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

It may be the case, even after months of study, that there are some questions
you don't feel you have solid answers to yet.  That's completely fine---it's
okay to not have all the answers.  What are those remaining questions?  What is
your thinking on them currently?  What interpretations are you weighing?  Why
don't you feel you have a solid answer?

.. |sec_where_do_we_agree| replace::  Section 5.4
.. _sec_where_do_we_agree:

5.4 Where do we agree?
~~~~~~~~~~~~~~~~~~~~~~

In discussions about beliefs it can be easy to get bogged down in our
disagreements.  We don't want to do that.  It's likely the case that the group
you've dug into this topic with agrees on the vast majority of its beliefs, and
that the areas of disagreement are relatively small in scope.  (That is not to
say they are likely small in importance, though they may be.)  From your
perspective, what are all the facets of this topic that your group agrees on?
What is the common ground on which you all stand?  Please be thorough and
specific.

.. |sec_where_do_we_disagree| replace::  Section 5.5
.. _sec_where_do_we_disagree:

5.5 Where do we disagree?
~~~~~~~~~~~~~~~~~~~~~~~~~

Now that we have an understanding of what our common ground is, and that it is
substantial, what are those areas in which the group has not come to consensus?
What are the differing viewpoints, and why do folks hold to one way of seeing
things versus another?  For any given point of contention, is it a major issue,
rising to the importance of our statement of faith?  Or is it a minor issue,
where brothers and sisters in Christ can lovingly and peacably hold differing
viewpoints within the same local body?  Is it perhaps somewhere in between?

.. |sec_where_do_we_go_from_here| replace::  Section 5.6
.. _sec_where_do_we_go_from_here:

5.6 Where do we go from here?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Given the lengthy journey we've been through together, what needs to happen at
this point?  Are there any changes that we need to make?  Are there
conversations we need to continue?  Should someone sit down and write *There
and Back Again:  A Congregant's Tale* to record our sojournings for future
generations?  This assignment is entirely is entirely open-ended, but I'd
suggest you break it down into two parts:  What's next for you, personally?
And then:  What's next for us as a church?

.. |app_philosophy_of_learning| replace:: Appendix 1
.. _app_philosophy_of_learning:

Appendix 1:  Philosophy of Learning
===================================

The driving passion of my life is to help people learn how to learn.  I use the
phrase "how to learn" for two reasons.  First, I believe learning is a
life-long activity, and if we are to do it all our lives, we should learn to do
it effectively.  Second, it's likely the case that most people have gone
through their lives being taught *what* to learn, but not necessarily *how*.

.. sidebar::  Side Note

   This list is a generalization of a four-fold study approached used by `Bible
   Study Fellowship International`_, with which I studied for seven years, and
   for which I taught 1st and 2nd graders for two years.

Allow me to tell you how I believe learning works.  I consider the learning
process to be a combination of the following activities:

Investigation
  is the process of seeking to learn new material by yourself.  In our current
  educational framework, this is generally accomplished by reading material
  written by experts in an attempt to understand the necessary concepts,
  relationships, etc.  However, not all people learn best through books.  The
  process of investigation can incorporate other methods, such as
  experimentation for those who learn best by doing, or observation for those
  who learn best by seeing something done.
Meditation
  is a means of study often neglected. When investigating new material, you
  must continually ask certain questions:  What does this actually mean?  How
  might it apply to my life?  Does it fit with my current understanding of
  life, the universe, and everything?  If so, how and where does it fit in?  If
  not, is this utter nonsense, or do I need to reformulate the way I view the
  world?  By seeking to understand how new material fits with the vast wealth
  of knowledge and experience we already have (see |app_scaffolding|_ on
  the topic of scaffolding) we internalize it, thereby strengthening our
  understanding.
Conversation
  is what naturally happens when multiple people are learning together.  Since
  everyone has a different set of prior knowledge, students will be able to
  grasp new concepts with differing levels of certainty.  It is beneficial to
  be able to discuss with others any thoughts or questions that arise during
  investigation of and meditation on new material.  What may seem confusing for
  one may be straightforward for another.  Even if you have a flawless
  understanding of a concept, articulating your understanding to others further
  reinforces that understanding.
Consultation
  is the act of seeking help from a professional.  Generally this happens
  through students listening to lectures given by their instructors.  However,
  the lecture format is not the only way to accomplish this.  Teachers can
  impart their knowledge in a small group setting, or even one-on-one.  Experts
  have years of experience in their field, during which they have formulated
  and reformulated their understanding of the material. The crucial part of
  consultation, then, is for the instructor to aid the students in developing
  the structure of their knowledge.  (Again, see |app_scaffolding|_.)
Imagination
  is when students extend their knowledge beyond the bounds of the box in which
  it was first presented.  Instead of just reading the textbook, they find out
  where the material has been applied in the real world.  Instead of the
  examples discussed in the classroom, they question what might happen if they
  omit their initial assumptions.  Instead of assuming what any given text says
  is true, they set up counter-examples and attempt to prove or disprove them.
  Without being able to transfer our knowledge to new situations, our
  understanding of the material is still incomplete.

Though these five learning activities have been presented as if they come one
after the other, we must realize that learning is not a linear process---it is
inherently organic.  These various learning activities happen, to an extent,
simultaneously.  We can think of this process occurring within a feedback loop,
such that we are constantly reevaluating our understanding of the material
based on the inputs received.

Whether you like it or not, you will be learning for the rest of your life.  A
wise man once said, "Some people learn the easy way; some people learn the hard
way; and some people never learn."  My desire is to help you learn to learn the
easy way, and to relish every minute of it.

.. |app_scaffolding| replace::  Appendix 2
.. _app_scaffolding:

Appendix 2:  Scaffolding
========================

When learning new material, you're never starting from an empty slate.  Your
understanding of a new concept is built up on top of the sum total of all your
prior knowledge.  The task of the educator is to assess what a pupil's prior
knowledge is, and then provide sufficient intermediary steps to take them from
where they are to where they need to be in understanding the new subject.  This
is a concept known in education as *scaffolding*, as the new knowledge and
understanding is built up, bit by bit, in a manner similar to a scaffold
erected outside a building.

If all an educator needed to do was provide adequate scaffolding on top of an
already sound base, life would be relatively easy.  Unfortunately reality is a
little more complicated.  It may be the case that in assessing a student's
understanding of a subject you find certain inconsistencies---the structure of
the scaffold is unsound.  In such cases you still need to get an accurate
picture of the structure of a learner's knowledge, but you do so in order to
determine what needs to be deconstructed in order to rebuild a sound structure
on top.

This is one of the harder parts of aiding someone in learning.  The structure
of their current understanding may be long-established.  It can be quite
difficult to let go of preconceived notions in order to arrive at a fuller
understanding of a subject.  This part of the learning process must be handled
with extreme care and sensitivity.  The questions in
|sec_laying_the_foundation|_ are intended for learners to map out what their
scaffolding looks like so the instructor knows where the starting point is.

