Critical Social Justice
~~~~~~~~~~~~~~~~~~~~~~~

.. role:: raw-html(raw)
   :format: html

.. raw:: html

   <p style="text-align:left;">
       <i>August 9, 2022</i>
       <span style="float:right;">
           🕑 31 min.
       </span>
   </p>

.. note::

   This piece is not a finished work in an of itself.  Instead it is a section
   from the first draft of :doc:`counterfeit-worldviews` that was removed
   before the publication of that piece.  In that work, it served as the third
   and final worldview under consideration, but it was ripped out because

   1. initial reviews indicated that it was too hard to understand, given the
      deep dive into philosophy, and to do it justice (hah!) would've required
      substantially more space, making the intended piece unbalanced,
   2. I realized that the worldviews of emotionalism and authoritarianism are
      actually more fundamental, and contribute to the development and
      promulgation of a whole host of others (feminism, environmentalism,
      scientism, etc.), including this one, and
   3. with this section out of the way, that left space to further develop the
      remaining sections by detailing how the worldviews tend to play
      themselves out in the life of the church.

   However, in the time since the counterfeit worldviews piece was published,
   I've often enough found myself wanting to refer back to things I'd written,
   or resources I'd linked to, here.  I grew tired of rewinding the repository
   history to find what I was looking for, so instead I've reproduced the
   section, unedited, here, for my own reference.

   With all that said, here are some final disclaimers:

   * This work is incomplete.
   * It's not nearly as refined as the piece from whence it came.
   * Pieces of it were integrated into that finished work, so you may recognize
     bits if you've read the other one.
   * My understanding of the historical developments that have contributed to
     critical social justice as we see it today has grown since this was
     written, but those details are not reflected here.
   * It would probably benefit you to read :doc:`counterfeit-worldviews` first
     to have an understanding of both the tools being employed and the overall
     structure into which this fits.

.. contents::  Contents
   :local:

Introduction
^^^^^^^^^^^^

The final worldview we'll discuss today is that of *critical social justice*
(CSJ).  This is a hard one to talk about at this particular time, because (1)
people think they know what it is, and (2) we think we have it on the retreat.
At this point, unless you've been living under a rock for the past few years,
we're all familiar with organizations like `Black Lives Matter`_ and `Antifa`_
and their `"mostly peaceful" protests`_.  We've heard of how the CSJ propaganda
is being pushed on `various branches of the U.S. government`_ and `military`_.
We've been told of various corporate leadership teams being put through `white
privilege training`_.  However, we've also seen `parents pushing back against
this`_ in school board meetings all over the country, and occasionally we see
`church leaders speaking out against it`_ too.  Some are inclined to think
Satan pushed too far, too fast on this one, such that we wizened up to what
he's been up to, and now we can effectively deal with him.  Such thinking is
optimistic, at best.  I'm much more inclined to suspect he pushed so hard, so
fast to fool us into thinking we'd foiled his plans.

.. _Black Lives Matter:  https://en.wikipedia.org/wiki/Black_Lives_Matter
.. _Antifa:  https://en.wikipedia.org/wiki/Antifa_(United_States)
.. _"mostly peaceful" protests:  https://www.youtube.com/watch?v=yziRK7j0Zpw
.. _various branches of the U.S. government:  https://christopherrufo.com/summary-of-critical-race-theory-investigations/
.. _military:  https://smile.amazon.com/Irresistible-Revolution-Marxisms-Conquest-Unmaking/dp/1737067307/ref=sr_1_1?crid=1PC65FX9JNZ00&keywords=irresistible+revolution+marxism&qid=1658517708&sprefix=irresistible+revolution+marxism%2Caps%2C131&sr=8-1
.. _white privilege training:  https://www.racialequitytools.org/curricula/transforming-white-privilege/introduction-to-the-training
.. _parents pushing back against this:  https://www.aei.org/op-eds/parents-against-critical-race-theory/
.. _church leaders speaking out against it:  https://www.baptistmessage.com/john-macarthur-rebukes-sbc-stance-on-critical-race-theory/

.. sidebar::  Definition

   |hegemony|_ is a term indicating the political supremacy one state has over
   another.  *Cultural hegemony* refers to the societal supremacy one people
   group has over all the others.  Today, if you are rich, white, male,
   heterosexual, `cisgendered`_, able-bodied, and a native-born citizen, you're
   a member of the cultural hegemony.

.. _hegemony:  https://en.wikipedia.org/wiki/Hegemony
.. |hegemony| replace::  *Hegemony*
.. _cisgendered:  https://en.wikipedia.org/wiki/Cisgender

It's easy to think of CSJ as the new kid on the block, as far as worldviews are
concerned.  After all, I hadn't heard of terms like critical race theory or
intersectionality until a few years ago.  However, to think of it as a novelty
is one of its many dangers.  You may be surprised to know that some of the
ideas underpinning CSJ (e.g., equality vs equity) have actually been `hotly
debated`_ in North America since long before the United States was even
founded.  Then in the mid-1800s you have the development of `conflict theory`_
with folks like `Marx`_ and `Engels`_.  When their ideas of economic class
revolution never really played out as they'd prophesied, that led folks at `the
Frankfurt School`_ in the early 1900s to posit that Marx was right in principle
but had simply misunderstood the `cultural hegemony`_, and thus `critical
theory`_ was born.  They then hopped the pond, and this infection began to
fester in the American academy to the point where ~30 years ago we added
`intersectionality`_ to the mix.  You and I might've been caught off guard by
CSJ, but I'm afraid the water's been heating for a good long time, and at this
point the frog is about cooked.

.. _hotly debated:  http://cyberspacei.com/greatbooks/h2/speak_listen/sp_listen_016.htm
.. _conflict theory:  https://en.wikipedia.org/wiki/Conflict_theories
.. _Marx:  https://en.wikipedia.org/wiki/Karl_Marx
.. _Engels:  https://en.wikipedia.org/wiki/Friedrich_Engels
.. _the Frankfurt School:  https://en.wikipedia.org/wiki/Frankfurt_School
.. _cultural hegemony:  https://www.youtube.com/watch?v=GRMFBdDDTkI
.. _critical theory:  https://en.wikipedia.org/wiki/Critical_theory
.. _intersectionality:  https://en.wikipedia.org/wiki/Intersectionality

"But wait," you say, "that's not what I mean.  God commands us to work for
justice (|MIC.6.8|), and he created us as social beings, so any working out of
justice in the world will be social in nature.  Therefore:  'social justice'."
While it's true that `when the term was coined`_ ~200 years ago, it meant
something closer to what you have in mind, the term was redefined ~100 years
ago to be the nice, warm, fuzzy synonym for the more accurate term
"distributive justice".  When you hear "social justice" in use today, the
chances that the speaker is referring to the original definition are somewhere
between slim and nil.  It reminds me of this line from Iñigo Montoya:

  "You keep using that [term].  `I do not think it means what you think it
  means`_." :raw-html:`<br>`
  ~ *The Princess Bride*

You wouldn't use the term "gay" today to mean simply characterized by
cheerfulness, would you?  Many who speak of "social justice" today without
actually knowing what it means do so simply out of ignorance; however, there
are many others, even within the church, who confuse the term intentionally
because it's part of a disingenuous rhetorical tactic known as `the motte and
bailey`_.  Using the term "social justice" loosely and without definition is
dangerous---stop it.

.. |MIC.6.8| replace:: :raw-html:`<a data-gb-link="MIC.6.8">Micah 6:8</a>`
.. _when the term was coined:  https://www.heritage.org/poverty-and-inequality/report/social-justice-not-what-you-think-it
.. _I do not think it means what you think it means:  https://www.youtube.com/watch?v=jhmONchRuC8
.. _the motte and bailey:  https://www.youtube.com/watch?v=_kiLJQGft_8

.. sidebar::  Book Recommendation

   I link to a number of books and other resources throughout this piece, but
   if there's one book you read on critical social justice, it should be
   `Voddie Baucham Jr`_'s |fault_lines|_.  It's the most comprehensive, yet
   concise and approachable, resource I've yet found on the subject.

"How did this happen, though?" you may wonder.  "Was no one standing guard to
fend off this incursion?"  Actually, yes, but generally speaking no one
listened to them.  In the early 1900s you had folks like `Ludwig von Mises`_
with his |human_action|_, among other works, arguing that free market economics
are not only essential to a healthy capitalism, but are indeed the bedrock of
human civilization.  To begin to engineer society toward producing particular
outcomes, then, is to being to chip away at the foundation of society until one
day it collapses.  In the 1970s you had `Friedrich Hayek`_ and what is likely
the most comprehensive refutation of the arguments in support of social justice
in his |law_legislation_and_liberty_vol_2|_.  A decade later `Thomas Sowell`_'s
|a_conflict_of_visions|_ describes the paradigmatic disconnect between
diametrically opposed visions of human nature in society, illustrating why it's
so difficult to talk about this subject and have all parties accurately
understand each other.  Most recently `Voddie Baucham Jr`_ has demonstrated the
|fault_lines|_ that have been building over the past few centuries, to warn you
such that when the tectonic plates finally slip, you'll be standing on the
right side.  People have been fighting the good fight---we've just been unaware
of it.

.. _Ludwig von Mises:  https://en.wikipedia.org/wiki/Ludwig_von_Mises
.. _human_action:  https://smile.amazon.com/Human-Action-Ludwig-Von-Mises/dp/1614273545/ref=sr_1_1?crid=3LKBTND5V3SBN&keywords=mises+human+action&qid=1658523289&sprefix=mises+human+action%2Caps%2C258&sr=8-1
.. |human_action| replace::  *Human Action*
.. _Friedrich Hayek:  https://en.wikipedia.org/wiki/Friedrich_Hayek
.. _law_legislation_and_liberty_vol_2:  https://smile.amazon.com/Friedrich-Legislation-Liberty-Mirage-Social/dp/B08P7LLLBT/ref=sr_1_1?crid=3TKHWRUR60PV0&keywords=hayek+law+legislation+liberty+vol+2&qid=1658523496&sprefix=hayek+law+legislation+liberty+vol+2%2Caps%2C118&sr=8-1
.. |law_legislation_and_liberty_vol_2| replace::  *Law, Legislation and Liberty, Volume 2:  The Mirage of Social Justice*
.. _Thomas Sowell:  https://en.wikipedia.org/wiki/Thomas_Sowell
.. _a_conflict_of_visions:  https://smile.amazon.com/Conflict-Visions-Ideological-Political-Struggles/dp/0465002056/ref=sr_1_1?keywords=sowell+a+conflict+of+visions&qid=1658524124&sprefix=sowell+a+con%2Caps%2C170&sr=8-1
.. |a_conflict_of_visions| replace::  *A Conflict of Visions*
.. _fault_lines:  https://www.voddiebaucham.org/product/fault-lines-hardback/
.. |fault_lines| replace::  *Fault Lines*
.. _Voddie Baucham Jr:  https://www.voddiebaucham.org/about/

This worldview is often easy to see, even within the church:

* If your church is sending out reading lists or organizing small groups to
  read works by authors like `Ibram X. Kendi`_ or `Robin DiAngelo`_, make for
  the door, posthaste.  I'm one to almost never say, "Don't read this
  book"---indeed, I recommend voracious reading from a wide variety of
  sources---but if your church is doing this it probably indicates your church
  leadership is compromised.
* If your church leadership preached sermons, authored blog posts, or recorded
  videos in support of George Floyd in the weeks after his death, and they have
  not yet repented of being led astray by and propagating lies, that's another
  reason to look for leadership who are more faithful to truth.  I'm not saying
  there's no room for forgiveness, but when there's no repentance, what wisdom
  is there in staying under a commanding officer who refuses to admit and learn
  from his mistakes?  In battle, that's practically a death sentence.
* If your church starts doing anything even remotely resembling corporate
  diversity, inclusion, and equity initiatives, get out of Dodge.

.. _Ibram X. Kendi:  https://en.wikipedia.org/wiki/Ibram_X._Kendi
.. _Robin DiAngelo:  https://en.wikipedia.org/wiki/Robin_DiAngelo

.. note::

   Please don't mistake me here.  In the statements above, I'm not advocating
   for leaving a church over some minor quibble.  Rather, this worldview is so
   corrosive, and so completely antithetical to the gospel, that I don't think
   it wise for you to waste another minute in such a community.  Life is too
   short, and our mission too urgent.

   You may think it worthwhile to stay and fight, that you might win back your
   brother (|MAT.18.15|; |JAS.5.19-JAS.5.20|; |2TI.2.24-2TI.2.26|), and you
   might be right.  Keep in mind that I've fought this particular fight three
   times in the last three years and I'm batting 0 for 3.  My opinion here is
   definitely colored by my experience, so take it with a grain of salt.

.. |MAT.18.15| replace:: :raw-html:`<a data-gb-link="MAT.18.15">Matthew 18:15</a>`
.. |JAS.5.19-JAS.5.20| replace:: :raw-html:`<a data-gb-link="JAS.5.19-JAS.5.20">James 5:19&ndash;20</a>`
.. |2TI.2.24-2TI.2.26| replace:: :raw-html:`<a data-gb-link="2TI.2.24-2TI.2.26">2 Timothy 2:24&ndash;26</a>`

On the other hand, there are times when it's much better-camouflaged:

* We believe that engaging the inner journey of spiritual transformation leads
  to the outward growth of global community health and reconciliation.
* We're going to be creating a co-ed advisory board that will be in close
  communication with the elder board, because we want to include more diverse
  perspectives in the decision-making of the church.
* We'd like the congregation to prayerfully consider how we might be able to
  reach out to those in our community, particularly those that don't look like
  us.
* We want to make sure we're not allowing pride to blind us to truths that the
  broader culture has figured out but historically the church has turned a deaf
  ear to.
* "WHEREAS, Critical race theory is a set of analytical tools that explain how
  race has and continues to function in society, and intersectionality is the
  study of how different personal characteristics overlap and inform one's
  experience;" (from `SBC Resolution 9`_).

.. _SBC Resolution 9:  https://www.youtube.com/watch?v=pFHfa0s1XLM

What's Wrong with This?
^^^^^^^^^^^^^^^^^^^^^^^

If your only exposure to CSJ has been the outlandish things that hit the news,
it may be difficult to see what the prior examples have to do with it.  We know
God has given us the ministry of reconciliation (|2CO.5.19|).  We know it's
good to get input from a variety of sources.  We know we're supposed to reach
out to those who aren't in our same situation (|MAT.25.35-MAT.25.40|).  We know
`all truth is God's truth`_.  We know tools for analyzing things aren't
inherently good or bad.  It seems like these are just a handful of good
reminders---what's wrong with that?  To answer that question, we need to
realize that CSJ is built on a number of false assumptions:  that ideas must be
refined over time, that who you are impacts what you can know and how you can
know it, and that we can't know the whole truth without listening to
perspectives other than our own, among a whole host of others.  Let's dive in
and see what these have to say.

.. |2CO.5.19| replace:: :raw-html:`<a data-gb-link="2CO.5.19">2 Corinthians 5:19</a>`
.. |MAT.25.35-MAT.25.40| replace:: :raw-html:`<a data-gb-link="MAT.25.35-MAT.25.40">Matthew 25:35&ndash;40</a>`
.. _all truth is God's truth:  https://www.ligonier.org/learn/articles/all-truth-gods-truth-sproul

Ideas Must be Refined Over Time
+++++++++++++++++++++++++++++++

This first notion makes some intuitive sense.  As fallen, finite, and fallible
human beings, it's not possible for us to be entirely correct in our thinking
at all times, so some refinement is obviously necessary.  Before we accept this
claim without question, though, let's figure out what exactly is being said
here, where the idea comes from, and what its purpose is.  We've intimated
before that it's important to examine `the consequences of ideas`_ when
evaluating them, and now we'll add that the provenance and purposes of ideas
must be factored into our evaluation as well.

.. _the consequences of ideas:  https://smile.amazon.com/Consequences-Ideas-Redesign-Understanding-Concepts/dp/1433563770/ref=sr_1_1?crid=15RN66K4P9GXZ&keywords=the+consequences+of+ideas&qid=1658700346&sprefix=the+consequences+of+ideas%2Caps%2C206&sr=8-1

First let's add some specificity.  Are we saying that *some* ideas need
refinement, or *all* ideas?  We know from experience that some ideas are
incorrect and could use some improvement, but the assumption here is that's
true for all ideas.  We know that isn't right.  After all, :math:`2+2=4`, and
that's always been true and always will be true, anywhere in the universe---no
refinement needed.  Unless of course your local school is one teaching that
`things like objectivity are racist characteristics of white supremacy`_.
Wait, what?  Where on earth did that come from?  Well, it's a bit of a long
story.

.. _things like objectivity are racist characteristics of white supremacy:  https://summit.news/2020/10/07/elementary-school-kids-taught-that-objectivity-and-perfectionism-are-racist-traits-of-white-supremacy/

If we take a trip back to antiquity, the classical Greek philosophers used a
form of reasoning known as |dialectic|_, which consisted of putting forth an
argument (or *thesis*) and then rebutting it with a counter-argument (or
*antithesis*).  Depending on how the argumentation went, you either showed one
side or the other, or perhaps a combination of the two (a *synthesis*), to be
correct.  Fast-forward about two millennia, during which a handful of other
philosophers used the methodology, and we get to `Immanuel Kant`_, who
`rediscovers it`_ and introduces it into modern philosophy in the late 1700s.
From there it gets picked up by a fellow named `Georg Wilhelm Friedrich
Hegel`_, who not only puts a different spin on it in his
|phenomenology_of_spirit|_, among other works, but also makes it the
fundamental driver of his metaphysical understanding of life, the universe, and
everything.

.. _dialectic:  https://en.wikipedia.org/wiki/Dialectic
.. |dialectic| replace::  *dialectic*
.. _Immanuel Kant:  https://en.wikipedia.org/wiki/Immanuel_Kant
.. _rediscovers it:  https://fortnightlyreview.co.uk/2020/11/immanuel-kant-origin-dialectic/
.. _Georg Wilhelm Friedrich Hegel:  https://en.wikipedia.org/wiki/Georg_Wilhelm_Friedrich_Hegel
.. _phenomenology_of_spirit:  https://www.gutenberg.org/cache/epub/6698/pg6698.html
.. |phenomenology_of_spirit| replace::  *Phenomenology of Spirit*

In terms of form, `the Hegelian dialectic`_ differs from the classical one in
three fundamental ways:  a thesis *necessarily* gives rise to an antithesis,
and the two *must* produce a synthesis, which is *better* than what came
before.  These may seem like subtle distinctions, but their inclusion
transforms dialectic from a mere method of reasoning into the foundational
principle upon which a comprehensive worldview is constructed.  Whereas in the
classical form, the antithesis is any argument that can potentially counter the
thesis, for Hegel it's something that's constructed from the thesis itself
through a process of negation.  Whereas classically the synthesis is one of
three possible outcomes, and may not necessarily be an improvement on the two
arguments that generated it, for Hegel it's assumed that it's the only possible
outcome and is indeed progress in the right direction.  Let's unpack these
differences a bit.

.. _the Hegelian dialectic:  https://www.youtube.com/watch?v=6Yh-J2ABC3c

The process of generating the antithesis from the thesis through negation is
known by the German word |aufheben|_.  Though this literally means "to lift
up," Hegel extends the definition to mean both to preserve and contradict, and
thereby transform.  His thought was that every idea contains its own
contradictions, and you can arrive at the antithesis by deconstructing the
idea.  If the original idea is racism, then its antithesis is `anti-racism`_.
If you start with the idea of Christianity, then the antithesis is whatever set
of beliefs you arrive at after deconstructing the faith you've known from
childhood.

.. _aufheben:  https://en.wikipedia.org/wiki/Aufheben
.. |aufheben| replace::  *aufheben*
.. _anti-racism:  https://en.wikipedia.org/wiki/Anti-racism

Hegel's assertion is when you contemplate these ideas of thesis and antithesis
in the right way, you end up with a broader understanding of the greater total,
which is the synthesis.  When this is done at the societal level, this
synthesis you arrive at is known as the |weltgeist|_, which literally means
"world spirit," but figuratively was used to indicate "the way in which the
world is currently moving."  The process doesn't stop there, though.  After
ideas and their contradictions give rise to the world spirit, this then gives
rise to new ideas, and the process starts all over again.  Baked into this
meta-narrative of history is the assumption that each step of the way we are
progressing in the right direction, perfecting ideas as we go.  Eventually
we'll reach a point where we finally have all the contradictions exposed and
synthesized, where ideas are perfected, and then the world spirit becomes
self-aware, and is actualized as the absolute spirit.  Now I know that last
line may sound way out there, but Hegel was very intentionally building a
systematic philosophy to underpin a transcendental religion.

.. _weltgeist:  https://de.wikipedia.org/wiki/Weltgeist
.. |weltgeist| replace::  *weltgeist*

Okay, what on earth does this mind-bending trip through 18th-century German
idealist philosophy have to do with anything?  Let's trace the influence of
these ideas through history and see.  A prominent thinker strongly influenced
by Hegel's dialectic and metaphysics was `Ludwig Feuerbach`_, and he in turn
influenced a great number of people, including Darwin, Freud, Engels, and Marx.
Whereas Hegel argued that to save the world we must perfect *ideas* through his
dialectical process driven by *aufheben*, Marx argued that we do it by
perfecting the *economy*.  Marx's ideas are eventually appropriated and
extended by the critical theorists at the Frankfurt School, but then their
argument was we need to perfect *culture* to save the world.  For them, your
life is the thesis, making you miserable about it (e.g., making you despise the
"privilege" you now enjoy that past generations worked so hard to give you) is
the antithesis, and the liberation you find on the other side is the synthesis.
In order to achieve that liberation, we must deconstruct and supplant all
ingrained ideas from our current world spirit, ideas like objectivity and right
answers in math class.  If we don't, we're getting in the way of the progress
of history, and are therefore on the wrong side of it.

.. _Ludwig Feuerbach:  https://en.wikipedia.org/wiki/Ludwig_Feuerbach

This is starting to sound all too familiar now, isn't it?  Though it's true
that sometimes ideas are wrong and need to be corrected through a process of
examination and contradiction (classical dialectic), over the last 200 years
this notion has, more often than not, referred to the Hegelian understanding,
particularly in conversations at the societal level.  This isn't just a neat
way of thinking about things that someone cooked up recently.  It has a
particular past and a particular purpose.  In service of that purpose, we are
to use any means necessary, because the utopian `end goal`_ is infinitely
better than any finite sacrifices we might need to make in the near term.  Such
a mindset has wrought incalculable havoc on mankind in recent centuries, and it
will continue to do so if we don't stop it.

.. _end goal:  https://en.wikipedia.org/wiki/Telos

Who You Are Impacts Your Determination of Truth
+++++++++++++++++++++++++++++++++++++++++++++++

This next assumption supporting CSJ is another one that makes some intuitive
sense.  What I know currently depends on the sum total of my past experience,
and that necessarily informs what I'm able to figure out next.  If I haven't
mastered arithmetic, chances are I'm not going to be able to understand
calculus, right?  Sure, no argument there.  Unfortunately that's not what's
meant by this assumption, though this misconception is one of the reasons it
endures.

The problem boils down to what exactly we mean by the word *are* when we say
who you *are* influences what you can know and how you can know it.  If we mean
how you think, speak, and act is influenced by all the experiences you've had
throughout life, then there's no issue.  If, on the other hand, we mean that
your identity, in terms of your membership in a variety of demographic
categories (gender, ethnicity, nationality, religion, etc.), dictates what
information you have access to and how you respond to it, that's a very
different matter.

This latter meaning, which is the prevalent one in recent decades, assumes that
all members of a group think, and therefore speak and act, in the same way.
There's some truth to this---it may be the case that *most* members of a
particular group tend to think similarly in *most* situations---but it's yet
another example of taking what's true of a subset and contending that it's true
of the whole as well, without any qualifications.  In
|intellectuals_and_race|_, `Thomas Sowell`_ tells the story of `children of
black American servicemen`_ in post-WWII Germany.  Growing up in the absence of
the black American subculture, they saw completely different outcomes in terms
of common success markers (educational achievement, financial prosperity,
family stability, etc.).  In |black_rednecks_and_white_liberals|_, he shows
that a common southern subculture tends to produce unsuccessful outcomes
regardless of ethnicity.  The culture in which you're immersed contributes to
your experiences, and they play into how you think, speak, and act.  Your
identity in terms of demographic categories does not.

.. _intellectuals_and_race:  https://smile.amazon.com/Intellectuals-Race-Thomas-Sowell/dp/0465058728/ref=sr_1_1?crid=SKYC2XQAVG6T&keywords=sowell+intellectuals+and+race&qid=1658838108&sprefix=sowell+intellectuals+and+race%2Caps%2C163&sr=8-1
.. |intellectuals_and_race| replace::  *Intellectuals and Race*
.. _children of black American servicemen:  https://www.youtube.com/watch?v=9ESlS2jrhXY
.. _black_rednecks_and_white_liberals:  https://smile.amazon.com/Black-Rednecks-Liberals-Thomas-Sowell/dp/1594031436/ref=sr_1_1?keywords=black+rednecks+and+white+liberals+thomas+sowell&qid=1658838225&sprefix=black+rednecks+%2Caps%2C237&sr=8-1
.. |black_rednecks_and_white_liberals| replace::  *Black Rednecks and White Liberals*

Given that this notion is so pervasive in society, though, where on earth did
it come from?  If we turn back the clock, we actually find that the first seeds
of this idea can be seen in the master-slave dialectic developed by none other
than Hegel in his |phenomenology_of_spirit|_, which we referenced earlier.  The
struggle between lord and bondsman would inform the thinking of Marx in his
analysis of class conflict between the |bourgeoisie|_ and |proletariat|_, where
the ruling class couldn't possibly understand the plight of the working class,
because they hadn't lived it.  Hegel's and Marx's ideas were then inspiration
for `Nancy Hartsock`_'s essay |the_feminist_standpoint|_, which then catalyzed
the formalization of these ideas into what's now known as `standpoint
epistemology`_.  The purpose in each instance is to galvanize the oppressed
group into cultural revolution:  for Hegel, to achieve the liberation to be
found in the synthesis and drive history toward its inevitable conclusion; for
Marx, to overthrow the ruling class and establish the communist utopia; for
feminism, to uproot the established patriarchy and supplant it with an
egalitarian paradise.

.. _bourgeoisie:  https://en.wikipedia.org/wiki/Bourgeoisie
.. |bourgeoisie| replace::  *bourgeoisie*
.. _proletariat:  https://en.wikipedia.org/wiki/Proletariat
.. |proletariat| replace::  *proletariat*
.. _Nancy Hartsock:  https://en.wikipedia.org/wiki/Nancy_Hartsock
.. _the_feminist_standpoint:  https://www.amazon.com/Feminist-Standpoint-Revisited-Essays-Politics/dp/0813315581
.. |the_feminist_standpoint| replace::  *The Feminist Standpoint*
.. _standpoint epistemology:  https://en.wikipedia.org/wiki/Standpoint_theory

The thought that knowledge is derived from your lived experience as a member of
various identity groups has most recently been extended to the concept of
`positionality`_.  This latest iteration, which functions effectively as the
engine driving `intersectionality`_, formalizes that each of your
intersectional identities has a particular relationship to the supposed power
dynamics that govern reality.  As this idea has permeated society in recent
decades, it's given rise to what's been dubbed `ethnic gnosticism`_.  Whereas
`gnosticism`_, which was a heresy battled by the early church, posited that
there was special personal spiritual knowledge (*gnosis*) that could trump
orthodox teachings, ethnic gnosticism claims you have access to special
personal knowledge by virtue of your lived experience via your intersectional
identities, and those truths can't be contradicted.  When it comes to your
ability to determine truth, conflating past personal experience and categorical
identities is both wrong and dangerous.

.. _positionality:  https://newdiscourses.com/tftw-position-positionality/
.. _ethnic gnosticism:  https://www.youtube.com/watch?v=Ip3nV6S_fYU&t=29s
.. _gnosticism:  https://en.wikipedia.org/wiki/Gnosticism

We Can't Know the Whole Truth Without a Plurality of Voices
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

This final assumption we'll tackle today under CSJ is really just the logical
implication of the prior two.  If I'm not right all the time and my ideas need
refinement, and if who I am determines what information I have access to, then
in order to work my way to the truth, the whole truth, and nothing but the
truth, I'll need to engage with other people in conversation.  If I cut myself
off from other perspectives, I could wind up in an echo chamber and be dead
wrong.

As we've noted before, really effective lies have a whole lot of truth baked
into them, and this one is no different.  Listening to other people's
perspectives, particularly if they disagree with you, can be a very good thing,
as they can help you to refine your ideas (|PRO.27.17|).  Indeed, whenever I
write something like this, I run it by a half-dozen trusted believers with the
express directive, "Tell me where I'm being an idiot."  To think yourself right
all the time and not consider the counsel of others is foolishness
(|PRO.12.15|).  So where's the problem here?

.. |PRO.27.17| replace:: :raw-html:`<a data-gb-link="PRO.27.17">Proverbs 27:17</a>`
.. |PRO.12.15| replace:: :raw-html:`<a data-gb-link="PRO.12.15">Proverbs 12:15</a>`

The problem is this assumption goes far beyond, "Make sure you're open to
correction from others (|PRO.12.1|)."  Instead, the claim is that it is
impossible to arrive at the complete truth of a matter without first
synthesizing all other contradictory viewpoints.  Did you catch Hegel in there?
I can't know if I'm right or not, and I have no way to evaluate whether what
you say is true, but we need to put our heads together.  That doesn't mean what
we have at the end of our interaction will actually be true, but rather we're
slowly progressing toward the end of history where wrongs will no longer exist
(|REV.21.4|) and we'll know all truth (|1CO.13.12|).  It's no wonder this
thinking is so seductive---Satan's game all along has been to steal ideas from
God and then to distort them to serve his own purposes.

.. |PRO.12.1| replace:: :raw-html:`<a data-gb-link="PRO.12.1">Proverbs 12:1</a>`
.. |REV.21.4| replace:: :raw-html:`<a data-gb-link="REV.21.4">Revelation 21:4</a>`
.. |1CO.13.12| replace:: :raw-html:`<a data-gb-link="1CO.13.12">1 Corinthians 13:12</a>`

This actually goes further than requiring that you work with someone to develop
your ideas, though.  No, instead we are required to "elevate the voices" of the
marginalized and oppressed.  If you recall our definition of cultural hegemony
and discussion of intersectionality from earlier, you need the input of all
those various people groups before you can really get at the truth of a matter.
For expediency's sake, it seems like it'd be nice if everyone had one poor,
black, female, lesbian, transgender, handicapped, immigrant friend to consult
so you'd get all the necessary perspectives in one shot, but even that's
insufficient (though it seems to be how the Biden administration is going about
hiring these days).  After all, your diversity, inclusion, and equity
poster-child friend only comes from one country.  What about the perspective of
someone living half-way across the world?  It's simply not possible, nor is it
reasonable, to fulfill this requirement to incorporate a plurality of voices
when attempting to ascertain truth.

Let's Pull All This Together
++++++++++++++++++++++++++++

Now that we've examined some of the major assumptions underpinning CSJ, let's
return to the concept of it being a comprehensive worldview.  In the prior
cases, we've focused solely on the five-fold breakdown, but in this case it's
easy to see how this contradicts the biblical worldview no matter how you slice
it.  In terms of the four fundamental questions:

1. **Who am I?**  I am a representative of a number of intersectional people
   groups, all members of which think, speak, and act identically.
2. **Why am I here?**  I'm here to do the work of *aufheben*, of elevating the
   voices of the oppressed, of `racial reconciliation`_ and anti-racism, of
   overthrowing the cultural hegemony, of revolution.
3. **What's wrong with the world?**  Our ideas are not yet perfect, and people
   are clinging to old ideas and preventing the progress of history.
4. **How can what's wrong be made right?**  Everyone needs to embrace why we're
   here, such that we can drive history toward it's inevitable utopian
   conclusion.

.. _racial reconciliation:  https://www.youtube.com/watch?v=FoJGYCc7EUg

In terms of the historical meta-narrative:

1. **Creation:**  In the beginning, everything was perfect, as postulated in
   `Jean-Jacques Rousseau`_'s "state of nature."
2. **Fall:**  Certain people groups began oppressing other people groups.
3. **Redemption:**  There's no true fix to the fall, except to continually do
   the work of *aufheben*, etc., in a perpetual pursuit of improvement.
4. **Consummation:**  Eventually---and we have no idea when we'll get there, or
   how we'll know we're there when we do---we'll get to the place where all
   ideas are right and the world spirit has fully realized itself.

.. _Jean-Jacques Rousseau:  https://en.wikipedia.org/wiki/Jean-Jacques_Rousseau

In terms of the five-fold breakdown:

1. **God:**  God is defined not in a theistic, but rather in a transcendental,
   sense, being this concept of the world spirit, which grows in self-awareness
   and actualization as time progresses.
2. **Man:**  Man is necessary to the refinement of ideas, and therefore
   contributes to the creation of the god-concept through his driving the
   progress of history.
3. **Truth:**  Your truth depends on your intersectional standpoint, meaning it
   varies from person to person and throughout time.
4. **Knowledge:**  You can come to a fuller understanding of truth by
   contradicting what you know with the lived experience of others.
5. **Ethics:**  Right and wrong are whatever the oppressed say they are.  The
   more oppressed you are, the more authority you have in determining right and
   wrong.

Now let's go back to those more subtle examples from earlier and see if we can
pick out where things are going wrong:

* For the community health folks, what kind of reconciliation is being pursued,
  and for what purpose?  Is it possible to engage in the inner journey of
  spiritual transformation without the inclusion of diverse perspectives?
* For the co-ed advisory board, is the purpose to elevate women's voices?  If
  so, why?  Is it possible for an all-male elder board to accurately discern
  God's will for the church without this?
* For the congregation reaching out to its community, what's the motivation?
  Is it that members don't feel any burden for evangelism and need to be
  encouraged, or is there a felt need for diversifying the viewpoints within
  the body?  If the latter, what motivates that?
* For the church wanting to listen to truths from the broader culture, is it
  possible that culture has hit on something that God has not already revealed
  to you?  Is your motivation to be seen as righting the wrongs of historical
  injustices?
* For Resolution 9, is critical race theory merely a set of analytical tools,
  or is it a comprehensive worldview?  Is intersectionality only the study of
  overlapping characteristics, or is there a particular end goal in mind?

What are the Consequences?
^^^^^^^^^^^^^^^^^^^^^^^^^^

I don't know if you've realized it yet, but this last worldview is one that can
only thrive in a society in which the first two have had substantial influence
for many years.  Emotionalism set the stage for lived experience being a
primary conduit of truth, and authoritarianism hammers home that if you don't
agree with any of this, then you're on the wrong side of history.  We can
therefore cross-apply all the consequences of the first two here as well, as
these days CSJ is often the context in which they act.  In addition, though, I
see this worldview negatively impacting the body of Christ in at least three
distinct ways.

In the first case, we've traded in classical biblical interpretation for `woke
hermeneutics`_.  What on earth is that?  First, |hermeneutics|_ is the science
of biblical interpretation, or how to handle the word of God accurately
(|2TI.2.15|).  When studying a passage of scripture, there's lots to do,
including:

.. _woke hermeneutics:  https://www.youtube.com/watch?v=Mf_GfIZI46wA
.. _hermeneutics:  https://en.wikipedia.org/wiki/Hermeneutics
.. |hermeneutics| replace::  *hermeneutics*
.. |2TI.2.15| replace:: :raw-html:`<a data-gb-link="2TI.2.15">2 Timothy 2:15</a>`

* Figuring out who the author was and learning about his life.
* Determining when and where the book was written, and what the overall context
  of that period in history was.
* Ascertaining the author's purpose in writing to his original audience, and
  understanding how they would have interpreted his message.
* Understanding what kind of passage it is:  narrative, poetry, wisdom
  literature, prophecy, etc.
* Seeing how the passage fits into the book as a whole, and how the book fits
  into the Bible as a whole.
* Figuring out how the truths communicated apply to our lives today.

Woke hermeneutics, on the other hand, instead of starting with scripture and
historical evidence, starts with you and what you think and feel.  You read the
Bible in your own particular way, influenced by your cultural standpoint.
That's going to be different from someone else's understanding of the Bible, so
to reach a fuller understanding of a passage, we need to engage a multiplicity
of perspectives.  However, if we were to raise the alarm bells regarding where
such thinking comes from, we'd be rebuffed with, "This isn't any of that; `this
is just`_ hermeneutical/epistemic/interpretive humility."  To be humble, as we
are commanded (|EPH.4.1-EPH.4.3|), we must rigorously study scripture with
people of different ethnicities, etc.  We call their unfounded assertion,
though, and then we wind up accused of epistemic injustice, oppression, or
violence.

.. _this is just:  https://www.youtube.com/watch?v=cq-Rngb6zfo
.. |EPH.4.1-EPH.4.3| replace:: :raw-html:`<a data-gb-link="EPH.4.1-EPH.4.3">Ephesians 4:1&ndash;3</a>`

Part of the problem here is just plain cluelessness.  Ideas aren't
independent---they have particular histories and purposes attached to them.
Language isn't neutral either.  To think that you can use the language and
ideas of CSJ and not be influence by the history and philosophy that spawned it
is folly.  The other part of the problem here is that words like humility,
unity, and love are being weaponized to silence the Holy Spirit speaking
through believers, and to push a very intentional anti-biblical agenda.  What
does this lead to?  It means not being able to understand Abraham and Sarah's
sojourn in Egypt `without a modern Latin American immigrant's perspective`_.
It means considering `polygamy as a remedy for divorce`_ in the African
context.  It means twisting the immutable word of God to mean whatever we want
it to mean.  Far from humility, this is hubris.

.. _without a modern Latin American immigrant's perspective:  https://www.youtube.com/watch?v=cSfRr2mNDJc
.. _polygamy as a remedy for divorce:  https://www.amazon.com/Polygamy-Reconsidered-Eugene-Hillman/dp/0883443929

"Wait a second, though.  I've not seen anything near this egregious."  Perhaps
not, but when was the last time you were in a Bible study where a handful of
people got together, someone read a passage of scripture, and then the
conversation was kicked off with something like, "What does this passage mean
to you?"  For a long time we've often been approaching Bible study by starting
with the reader as opposed to the text.

I was a part of just such a group this past year where our study materials were
the `Bible Project videos`_, which are short, well-animated, informational
videos that help you understand that the Bible is a unified story that leads
people to Jesus.  In terms of what they attempt to achieve, they are both
excellent and unparalleled; however, problems became apparent very early on in
the series.  In some cases the concerns raised were along the lines of, "I
don't think I would've said it quite that way."  In others, "I disagree with
your interpretation, but I understand how you got there."  In still others,
"This is just plain wrong.  The Bible says one thing, the video says another,
and the two do not agree."  If our starting point had been the videos instead
of the scripture they summarize, we wouldn't have caught these discrepancies.
We were eventually counseled by the leadership to take what was good from the
videos and comment on the positive for the edification of others, but to keep
our concerns to ourselves, as they were negatively impacting some members of
the group (emotionalism).

.. _Bible Project videos:  https://bibleproject.com/explore/

More pertinent to our current discussion, though, was the subtle presence of
CSJ infused throughout.  Though it wasn't present in every video, it showed up
originally in their `summary of Exodus`_, and then continued to make an
appearance, sometimes fairly hidden, other times more blatant, and is perhaps
epitomized in their `thematic video on justice`_.  Perhaps this was just a
fluke, though.  After all, it's been very easy to get caught up in the language
of social justice in recent years.  Maybe these were just small mistakes of
saying something that wasn't intended.  That may be the case in other
scenarios, but in this case it's possible to track down `a sermon series on
justice`_ from a decade ago by `Tim Mackie`_, who's the voice behind the Bible
Project videos, and you can see the trajectory of his thinking coming more and
more in line with the CSJ narrative over time.  As we continued to pursue the
issue with leadership, we eventually reached the pointed where they just kicked
some of us out, and then defended their actions to the remaining members of the
group with unsupported claims that we were being prideful, divisive, unloving,
discouraging, etc. (authoritarianism).  When we abandon biblical hermeneutics,
the consequence is we are unable to stand firm for truth (|EPH.6.13-EPH.6.18|),
and are instead blown about by every wind of false doctrine
(|EPH.4.11-EPH.4.16|).

.. _summary of Exodus:  https://bibleproject.com/explore/video/exodus-19-40/
.. _thematic video on justice:  https://bibleproject.com/explore/video/justice/
.. _a sermon series on justice:  https://www.youtube.com/watch?v=c0rs3KL9wxg
.. _Tim Mackie:  https://bibleproject.com/tim-mackie/
.. |EPH.6.13-EPH.6.18| replace:: :raw-html:`<a data-gb-link="EPH.6.13-EPH.6.18">Ephesians 6:13&ndash;18</a>`
.. |EPH.4.11-EPH.4.16| replace:: :raw-html:`<a data-gb-link="EPH.4.11-EPH.4.16">Ephesians 4:11&ndash;16</a>`

Finally since we have abandoned revelation, reason, and reality as the bedrock
of knowledge, and since we regularly employ emotionalism and authoritarianism
to cow our brothers and sisters into submission, the church has been helping to
set the stage for, and is now helping to usher in, a worldwide
"soft" totalitarianism.  Back in 2015, people from around the world started
reaching out to author and editor `Rod Dreher`_ with concerns that the changes
they were seeing in the culture closely mirrored what they had seen decades
prior immediately before the people of their countries embraced totalitarian
rule with open arms.  His curiosity piqued, Dreher dug in and researched the
matter and eventually published |live_not_by_lies|_, in which he tells stories
of the horrors of life under totalitarian regimes, and shares practices that
helped the faithful preserve Christianity throughout.

.. _Rod Dreher:  https://en.wikipedia.org/wiki/Rod_Dreher
.. _live_not_by_lies:  https://smile.amazon.com/Live-Not-Lies-Christian-Dissidents/dp/0593087399/ref=sr_1_1?crid=1YFNXM22VEZWT&keywords=live+not+by+lies&qid=1659014944&sprefix=live+not+by+lie%2Caps%2C268&sr=8-1
.. |live_not_by_lies| replace::  *Live Not by Lies*

In the past, these counterfeit worldviews have often resulted in a "hard"
totalitarianism, in which dissenters are dealt with by force.  Within the 20th
century alone, estimates of the death associated range from `about 100`_ to
`170 million`_.  These days, however, the force is softer in nature, though no
less real, as dissenting opinions are unable to be voiced, let alone heard.
Most of the time you're not even able to ask the question.  Do you question the
validity of critical theory as an analytical tool?  You're both arrogant and a
racist.  Do you question the danger of COVID-19?  You're a science denier, and
you're trying to kill grandma.  Do you question the integrity of the 2020
election?  You're anti-democracy at best and an insurrectionist at worst.  Do
you question whether man-made climate change poses an existential threat to
mankind?  You're a conspiracy theorist who's out to destroy the planet.  It
seems `liberty of conscience`_ has left the building.

.. _about 100:  https://reason.com/2013/03/13/communism-killed-94m-in-20th-century/
.. _170 million:  https://www.providentialhistory.org/?p=295
.. _liberty of conscience:  https://founders.org/2021/08/13/vaccine-mandates-and-the-christians-liberty-of-conscience-from-2021-to-1721-and-back-again/

Where are we today?  I’ll borrow and augment a phrase from `Michael O'Fallon`_
and say the war for epistemology is well underway, what’s at stake is the
future of the Christian church and civilization as we know it, and much of the
church unknowingly finds itself on the wrong side.  We are now reaping the
consequences of having been asleep on the watch for centuries.  Christians,
it's time to pull ourselves up, dust ourselves off, and get back in the game.
Faithfulness to our Lord demands it.

.. _Michael O'Fallon:  https://sovereignnations.com/profile/mike/

.. raw:: html

   <script src="https://bibles.org/static/widget/v2/widget.js"></script>
   <script>
       GLOBALBIBLE.init({
           url: "https://bibles.org",
           bible: "f421fe261da7624f-01",
           autolink: false,
       });
   </script>

