Why I Believe the Bible
-----------------------

.. raw:: html

   <p style="text-align:left;">
       <i>May 6, 2021</i>
       <span style="float:right;">
           🕑 22 min.
       </span>
   </p>

As you read anything I've written, you may notice that I always seem to be
working from the assumptions that the Bible is true, that all truth is God's
truth, and that God's truth is true for all of life.  The assumptions are made
for expediency's sake, because to give sufficient evidence to support them as
reasonable in everything I write would require substantial detours from
whatever my subject is.  However, if you're curious to know my reasons for
believing the Bible to be true, here is a high-level overview I wrote for our
homeschool speech and debate club:  |why_i_choose_to_believe_the_bible|_.

.. _why_i_choose_to_believe_the_bible:  https://newmexicoforensicsclub.readthedocs.io/en/latest/training/worldview-analysis/the-bible.html
.. |why_i_choose_to_believe_the_bible| replace::  *Why I Choose to Believe the Bible*

