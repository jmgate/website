Pandemic Response
-----------------

.. raw:: html

   <table style="width: 100%;">
     <tr>
       <td>
         <i>
           Originally published August 29, 2021; last updated September 20,
           2021.  See <a
           href="https://gitlab.com/jmgate/website/-/commits/master">here</a>
           for the complete revision history.
         </i>
       </td>
       <td style="float: right; white-space: nowrap;">
         🕑 70 min.
       </td>
     </tr>
   </table>
   <br>

.. contents::  Contents
   :local:

Motivation
==========

At this point it seems necessary for me to formalize our family's response to
the ongoing pandemic countermeasures.  At various points in the past year and a
half it seemed reasonable to wait for more data, and in recent months it looked
like things would get back to some semblance of normal once everyone who wanted
the protection of one of the vaccines had it.  However, that no longer appears
to be the case, as cities, states, and countries are starting to reimpose
various levels of lockdown measures and vaccine mandates.

Before we dive in, if you'd like a concise and approachable book on the subject
of COVID-19 and the worldwide response, I highly recommend `COVID:  Why Most of
What You Know is Wrong <https://smile.amazon.com/Covid-most-what-know-wrong/dp/9188729834/ref=sr_1_1?crid=3SXM6XNCSA2EN&dchild=1&keywords=covid+why+most+of+what+you+know+is+wrong&qid=1629757216&sprefix=covid+why+most+%2Caps%2C271&sr=8-1>`_,
by `Sebastian Rushworth <https://sebastianrushworth.com/about/>`_.  The author
is a Swedish doctor, and he gives a remarkably level-headed analysis of the
data that is easily understandable by a non-expert.  I will note the book was
finalized in December of 2020, so any data that became available this year were
unavailable while the book was being written, and as such the author has
changed his take on a few minor points since its publication.  Another good
resource is `Unreported Truths About COVID-19 and Lockdowns
<https://smile.amazon.com/gp/product/B08QND25GL/ref=ppx_yo_dt_b_search_asin_title?ie=UTF8&psc=1>`_,
by `Alex Berenson <http://www.alexberenson.com/about-the-author/>`_, but if
you're only going to read one book on the subject, prefer the former.

A final aside before getting started:  It can be easy to dismiss concerns such
as those that follow as uninformed, unscientific, paranoid, delusional, etc.;
however, researchers at MIT `conducted a study
<https://arxiv.org/pdf/2101.07993.pdf>`_ to determine how coronavirus skeptics
spread information on social media, and they were able to determine that such
communities employed high degrees of scientific rigor and rhetoric in their
data analysis and argumentation.  We encourage you, as they do, to follow the
data and see where it leads, and to be willing to re-evaluate any preconceived
notions you have.  We will endeavor to do the same, and we invite you to
correct us where we are mistaken.  Our goal here is to educate and encourage a
dialogue on any of these points.

Where Do We Stand Today?
========================

Our thinking today was informed by trying to tackle the following questions:

1. How severe is the pandemic, in terms of hospitalizations, mortality, and any
   long-term effects of the virus?
2. How effective have we been in countering it, in terms of measures like
   masks, lockdowns, and vaccines?
3. Have there been any negative consequences to adopting the various
   countermeasures we've employed?

Let's walk through these one at a time.

The Danger of COVID-19
~~~~~~~~~~~~~~~~~~~~~~

.. include::  pandemic/details-on-the-danger-of-covid-19.rst

The Effectiveness of Countermeasures
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. include::  pandemic/details-on-the-effectiveness-of-countermeasures.rst

The Consequences of Countermeasures
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. include::  pandemic/details-on-the-consequences-of-countermeasures.rst

What Does All This Mean?
========================

At this point, we can answer the questions that launched our investigation with
reasonable certainty:

1. The danger posed by the disease has been grossly exaggerated.
2. The various countermeasures we have employed---specifically masks,
   lockdowns, and vaccines---have proven themselves ineffective.
3. The consequences of those countermeasures have been too costly, and will
   continue to be if we don't do something.

What does all this mean?  I'm afraid it means you're being lied to.  I'm not
going to speculate here as to the "why", and I'm going to try to assume the
best of intentions in any given individual, but the near-constant stream of
reporting you've been fed over the last year and a half speaking to the grave
dangers of the pandemic:  those are lies.  The woman at the store who gets
irate with you because you not wearing a mask puts her in danger:  that's a
lie.  The community organization that switches to online-only meetings because
it will keep everyone safe:  that's a lie.  The employer requiring you to be
vaccinated to keep your job because that's the only way we make it through this
pandemic:  that's a lie.  The implicit assumption that any negative side
effects that come from our decisions don't matter, because the danger of the
virus is definitely worse:  that's a lie.

How should you respond in light of all this?  I won't prescribe anything;
that's for you to decide.  As for me and my household, we will no longer be
complying with any government or business requests to wear masks or socially
distance, and we will not be getting any of the available COVID-19 vaccines.
To do so would be to tacitly agree that the situation is as dangerous as
reported, that the countermeasures work to slow the spread and prevent death,
and that the consequences of the countermeasures don't matter.  *That we cannot
do.*

We do not do this for the sake of being troublemakers, but because our
consciences no longer allow us to comply, given all that we have learned in the
last year and a half.  In hindsight, I think we should have taken this stand
much sooner.  We are aware that taking such a stand will likely lead to
confrontation, and we will endeavor to handle such situations with grace and
compassion.  We hope they will be opportunities to begin an open dialogue on
all of these points.

Last spring someone pointed me to this excerpt from "On Living in an Atomic
Age" (1948), by `C.S. Lewis <https://en.wikipedia.org/wiki/C._S._Lewis>`_, in
`Present Concerns: Journalistic Essays
<https://www.amazon.com/Present-Concerns-Journalistic-C-Lewis/dp/0062643592/?tag=thegospcoal-20>`_,
and told me to replace any references to the atomic bomb with the coronavirus,
as I've done below.  I find it a fitting way to conclude.

  In one way we think a great deal too much of the [virus].  "How are we to
  live in [a pandemic] age?"  I am tempted to reply:  "Why, as you would have
  lived in the sixteenth century when the plague visited London almost every
  year, or as you would have lived in a Viking age when raiders from
  Scandinavia might land and cut your throat any night; or indeed, as you are
  already living in an age of cancer, an age of syphilis, an age of paralysis,
  an age of air raids, an age of railway accidents, an age of motor accidents."

  In other words, do not let us begin by exaggerating the novelty of our
  situation.  Believe me, dear sir or madam, you and all whom you love were
  already sentenced to death before the [coronavirus] was invented:  and quite
  a high percentage of us were going to die in unpleasant ways.  We had,
  indeed, one very great advantage over our ancestors---anesthetics; but we
  have that still.  It is perfectly ridiculous to go about whimpering and
  drawing long faces because the scientists have added one more chance of
  painful and premature death to a world which already bristled with such
  chances and in which death itself was not a chance at all, but a certainty.

  This is the first point to be made:  and the first action to be taken is to
  pull ourselves together.  If we are all going to be destroyed by [a
  pandemic], let that [virus] when it comes find us doing sensible and human
  things---praying, working, teaching, reading, listening to music, bathing the
  children, playing tennis, chatting to our friends over a pint and a game of
  darts---not huddled together like frightened sheep and thinking about
  [viruses].  They may break our bodies (a microbe can do that) but they need
  not dominate our minds.

We are not currently living as we are meant to.  As `Aleksandr Solzhenitsyn
<https://en.wikipedia.org/wiki/Aleksandr_Solzhenitsyn>`_ said the day before he
was exiled to the West, `"Live not by lies."
<https://www.solzhenitsyncenter.org/live-not-by-lies>`_  Live instead in
accordance with the truth of reality.

.. admonition::  Disclaimer

   These pages represent about 55 hours of research and writing on my part.  I
   think that should be more than sufficient for a project such as this, but it
   does mean I didn't have time to find and read every piece of evidence that
   may have bearing on these issues.  If I've missed something, or if new
   results come to light after this piece is published, that may call into
   question one or more of the conclusions I've drawn, and I'll be happy to
   revisit them with the new data.

References
==========

#. https://adc.bmj.com/content/106/3/e14
#. https://advance.sagepub.com/articles/preprint/Comment_on_Flaxman_et_al_2020_The_illusory_effects_of_non-pharmaceutical_interventions_on_COVID-19_in_Europe/12479987
#. https://agupubs.onlinelibrary.wiley.com/doi/10.1029/2021GH000413
#. https://ajph.aphapublications.org/doi/abs/10.2105/AJPH.2020.306095
#. https://apnews.com/article/coronavirus-pandemic-health-941fcf43d9731c76c16e7354f5d5e187
#. https://apps.who.int/iris/rest/bitstreams/1291156/retrieve
#. https://beyond.britannica.com/what-is-excess-mortality-and-how-is-it-calculated
#. https://bmjopen.bmj.com/content/bmjopen/5/4/e006577.full.pdf
#. https://childrenshealthdefense.org/news/if-covid-fatalities-were-90-2-lower-how-would-you-feel-about-schools-reopening/
#. https://covid.cdc.gov/covid-data-tracker/#datatracker-home
#. https://dailynews.ascopubs.org/do/10.1200/ADN.20.200416/full/
#. https://dailysceptic.org/2021/02/27/latest-news-298/#the-more-stringent-the-lockdown-the-higher-the-covid-death-toll
#. https://dailysceptic.org/replying-to-christopher-snowdon-again/
#. https://deadline.com/2021/06/99-p4rcent-covid-deaths-los-angeles-unvaccinated-1234781402/
#. https://elemental.medium.com/i-see-you-but-i-dont-how-masks-alter-human-connection-fbf2e21dd748
#. https://en.wikipedia.org/wiki/N95_respirator
#. https://en.wikipedia.org/wiki/Transverse_myelitis
#. https://evidence.nihr.ac.uk/themedreview/living-with-covid19/
#. https://factcheck.afp.com/doctor-expired-license-falsely-claims-masks-dont-work
#. https://fallacyinlogic.com/anecdotal-fallacy-definition-and-examples/
#. https://founders.org/2021/08/13/vaccine-mandates-and-the-christians-liberty-of-conscience-from-2021-to-1721-and-back-again/
#. https://gbdeclaration.org/
#. https://github.com/TheEconomist/covid-19-excess-deaths-tracker
#. https://globalnews.ca/news/8122807/canada-election-covid-19-madatory-vaccination/
#. https://heart.bmj.com/content/107/2/113
#. https://jamanetwork.com/journals/jamacardiology/fullarticle/2769293?guestAccessKey=4425a07e-573b-45ec-9a16-82e32ecf762f&utm_source=For_The_Media&utm_medium=referral&utm_campaign=ftm_links&utm_content=tfl&utm_term=080720
#. https://jamanetwork.com/journals/jamanetworkopen/fullarticle/2770975
#. https://jamanetwork.com/journals/jamanetworkopen/fullarticle/2772834?utm_source=For_The_Media&utm_medium=referral&utm_campaign=ftm_links&utm_term=111220
#. https://journals.healio.com/doi/10.3928/00485713-20201105-01
#. https://onlinelibrary.wiley.com/doi/10.1111/eci.13423
#. https://onlinelibrary.wiley.com/doi/full/10.1111/eci.13484
#. https://ourworldindata.org/coronavirus
#. https://ourworldindata.org/grapher/biweekly-confirmed-covid-19-cases?tab=chart&time=earliest..latest
#. https://ourworldindata.org/grapher/biweekly-covid-deaths?tab=chart&time=earliest..latest
#. https://ourworldindata.org/grapher/covid-stringency-index?tab=chart&country=AUT~BEL~CZE~DNK~FIN~FRA~DEU~ITA~LVA~LTU~NOR~POL~PRT~SVN~ESP~SWE~CHE~GBR~USA
#. https://ourworldindata.org/grapher/excess-mortality-p-scores-by-age?country=SWE~NOR~USA~FIN~GBR~DNK
#. https://oxfamilibrary.openrepository.com/bitstream/handle/10546/621023/mb-the-hunger-virus-090720-en.pdf
#. https://paulyowell.substack.com/p/the-nordics-and-the-baltics
#. https://rightsfreedoms.wordpress.com/2021/08/07/dr-malone-cdcs-own-data-shows-masks-vaccines-dont-stop-covid-junk-science-is-driving-authoritarianism/
#. https://sebastianrushworth.com/2020/11/06/how-accurate-are-the-covid-tests/
#. https://sebastianrushworth.com/2020/11/17/what-is-long-covid/
#. https://sebastianrushworth.com/2020/11/29/how-many-years-of-life-are-lost-to-covid/
#. https://sebastianrushworth.com/2021/01/10/are-the-covid-vaccines-safe-and-effective/
#. https://sebastianrushworth.com/2021/04/17/is-the-astra-zeneca-vaccine-killing-people/
#. https://sebastianrushworth.com/2021/05/09/update-on-ivermectin-for-covid-19/
#. https://sebastianrushworth.com/2021/07/13/does-it-make-sense-to-vaccinate-those-who-have-had-covid/
#. https://sebastianrushworth.com/2021/07/19/do-drug-trials-underestimate-side-effects/
#. https://sma.org/post-viral-syndrome/
#. https://storage.googleapis.com/plos-corpus-prod/10.1371/journal.pone.0240287/1/pone.0240287.pdf?X-Goog-Algorithm=GOOG4-RSA-SHA256&X-Goog-Credential=wombat-sa%40plos-prod.iam.gserviceaccount.com%2F20210827%2Fauto%2Fstorage%2Fgoog4_request&X-Goog-Date=20210827T202406Z&X-Goog-Expires=86400&X-Goog-SignedHeaders=host&X-Goog-Signature=528479d38b2b0438a5f241712c08b2eb4429b88e58af75b447a006dc5ae56b6c8f1013bcfbedfa64ded9b4c9c6071c2b63413b44a22fa9c4fca624f0653fbb0b8359c75575f7749ec06312406034485504ef225528ed985a4e11de8565eecd109e6e52d10a95411c6afa46c4166cf11a54de66345d12bce6bcb7298d16a5748802f57c3789b9579b4abe3d47176c9241a90ebdadd2874cb1ba0c1569926f7014b14f8a29527510a470c02f17387066c76e6b8f3e7f21bd481c3baf7a7f79205b608d986113e4f553e64c4e310a411983afa0d6ebbe5106fd784567f27bc5d5877160e238bb74b91c9c200ba7d1adc42face0e157c25ce2f442c31ff8a71d6d3f
#. https://stovouno.org/2020/11/12/there-is-no-covid-test-and-the-casedemic-is-a-shameless-scam/
#. https://swprs.org/covid19-lethality-how-not-to-do-it
#. https://swprs.org/covid-just-a-casedemic/
#. https://swprs.org/face-masks-evidence/
#. https://swprs.org/on-the-treatment-of-covid-19/
#. https://tamhunt.medium.com/lockdown-policies-have-vastly-exacerbated-global-hunger-b878b25e85d
#. https://thefatemperor.com/scientific-analyses-and-papers-on-lockdown-effectiveness/
#. https://thefatemperor.com/wp-content/uploads/2020/11/WHO-Pandemic-Guidelines-2019.pdf
#. https://undercurrents723949620.wordpress.com/2021/05/13/why-were-not-hearing-about-covid-vaccine-side-effects/
#. https://vaersanalysis.info/2021/06/23/what-is-the-backlog-in-terms-of-time-for-the-publishing-of-vaers-records-for-covid-19-vaccines/
#. https://vaers.hhs.gov/
#. https://videopress.com/embed/4egEyh2b?hd=1&amp;loop=0&amp;autoPlay=0&amp;permalink=1
#. https://wallethub.com/edu/states-coronavirus-restrictions/73818
#. https://watermark.silverchair.com/ciaa939.pdf?token=AQECAHi208BE49Ooan9kkhW_Ercy7Dm3ZL_9Cf3qfKAc485ysgAAAsAwggK8BgkqhkiG9w0BBwagggKtMIICqQIBADCCAqIGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMJ8N9Iqq39Si2OXZeAgEQgIICcyryY8OT3U5SwIqAF4NqwieFQ0hGJKCQPws0ayakFJ6QWacOvbDNCacKO25bI8R-qdDltbJYAzTa9DQ1MNOytgoMWdnfC5wniKdb7BKja4sP-T2koSw-GAhGIUmfvt8FpNOAIr3c_yGgGX7IOtWhBFcTGse43cynw6TsEbB5AToRjlJcDKyzCg-vtBbc7CGfCH62cuBkn3gomxnboGIsXTcFElX5ePGDcQVqqPmtwbvNFCs6pBDeOgrYT48d_UUZ3OXFBaBhVQqBP80a-Z1aIhnBhn4CUJbiDeArl5x0GS44R0gEpvhWvcUaU4Z8GckEgiiEXELb9hrj-FelhEpbRhCRkmf9aoLLaMFIghfsKzoPu0LAOecHq1l3U_4qylxkIvGAUEktcG5Z9HZUIOu2i8fqMeZzGsNayJKFD6_eKNc5vFsqooU0gHC6wTQ_9oMsuXgWNt2jS36sDk5aH1n9PYTC_IsHj9oA2nF2H0Sm5ukahbXI2GLG8RRpKMmUxamQc_huCx5SenPyCh1LnoYRn36vOqYsSTv3CbahnrOEyH_cumGVvYt6Zv9_zy5QA7yeRwY_3EmFwxXz15VYHAy7YduFvA8_Zwg4eH97dcKGYYIyLEjRTo1W5i7KIu6ho8BYZy5F_eUzseks3QIfQiJKyMD0B5lsknNRLMi9ews49wxV4FtBHkWzmrBf5rerFDsD2ra5jVULGZ9KldyvOv4zURM0FJgubnym576FZoAZmLWwsA_1vrRyTtw3WPAJ23vI3hFfKPb5HrILbbpEzb6w7yCjbTJNckKdTPCB6YgtR0Z4RyoSZ2XMshACh11JuMsrxFn0iQ
#. https://wellbeingtrust.org/areas-of-focus/policy-and-advocacy/reports/projected-deaths-of-despair-during-covid-19/
#. https://www150.statcan.gc.ca/n1/en/daily-quotidien/210208/dq210208c-eng.pdf?st=FUoSbNT4
#. https://www.acpjournals.org/doi/full/10.7326/M20-6817
#. https://www.aier.org/article/lockdowns-do-not-control-the-coronavirus-the-evidence/
#. https://www.americanthinker.com/articles/2020/09/its_a_covid_casedemic_not_a_pandemic.html
#. https://www.caranddriver.com/news/a34240145/2019-2020-traffic-deaths-coronavirus/
#. https://www.cdc.gov/media/releases/2020/p1218-overdose-deaths-covid-19.html
#. https://www.cdc.gov/mmwr/volumes/69/wr/mm6942e2.htm
#. https://www.cdc.gov/mmwr/volumes/70/wr/pdfs/mm7031e2-H.pdf
#. https://www.cdc.gov/nchs/data/misc/hb_cod.pdf
#. https://www.cdc.gov/nchs/data/misc/hb_me.pdf
#. https://www.cdc.gov/nchs/data/nvss/coronavirus/Alert-2-New-ICD-code-introduced-for-COVID-19-deaths.pdf
#. https://www.cdc.gov/nchs/nvss/vsrr/COVID19/index.htm
#. https://www.cdc.gov/nchs/nvss/vsrr/covid_weekly/index.htm#Comorbidities
#. https://www.cebm.net/covid-19/infectious-positive-pcr-test-result-covid-19
#. https://www.cebm.net/covid-19/masking-lack-of-evidence-with-politics/
#. https://www.cidrap.umn.edu/news-perspective/2020/04/commentary-masks-all-covid-19-not-based-sound-data
#. https://www.city-journal.org/death-and-lockdowns?wallit_nosession=1
#. https://www.city-journal.org/do-masks-work-a-review-of-the-evidence?wallit_nosession=1
#. https://www.cochranelibrary.com/cdsr/doi/10.1002/14651858.CD006207.pub5/epdf/abstract
#. https://www.cochranelibrary.com/cdsr/doi/10.1002/14651858.CD013574.pub2/full
#. https://www.ecdc.europa.eu/sites/default/files/documents/covid-19-face-masks-community-first-update.pdf
#. https://www.frontiersin.org/articles/10.3389/fpubh.2020.604339/full
#. https://www.ijidonline.com/action/showPdf?pii=S1201-9712%2820%2932180-9
#. https://www.imperial.ac.uk/media/imperial-college/medicine/sph/ide/gida-fellowships/Imperial-College-COVID19-NPI-modelling-16-03-2020.pdf
#. https://www.lifesitenews.com/news/47-studies-confirm-inefectiveness-of-masks-for-covid-and-32-more-confirm-their-negative-health-effects/
#. https://www.medrxiv.org/content/10.1101/2020.04.11.20062133v1.full.pdf
#. https://www.medrxiv.org/content/10.1101/2020.10.19.20214494v2.full.pdf
#. https://www.medrxiv.org/content/10.1101/2020.12.28.20248936v1.full.pdf
#. https://www.medrxiv.org/content/10.1101/2021.06.01.21258176v3.full.pdf
#. https://www.nber.org/system/files/working_papers/w27309/w27309.pdf
#. https://www.nber.org/system/files/working_papers/w28304/w28304.pdf
#. https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2493952/pdf/annrcse01509-0009.pdf
#. https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7338130/
#. https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7534595/pdf/main.pdf
#. https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7965847/
#. https://www.ncbi.nlm.nih.gov/pmc/articles/PMC8164734/
#. https://wwwnc.cdc.gov/eid/article/26/5/pdfs/19-0994.pdf
#. https://www.nejm.org/doi/full/10.1056/NEJMoa2029717
#. https://www.nejm.org/doi/full/10.1056/NEJMp2006372
#. https://www.nejm.org/doi/pdf/10.1056/NEJMoa2034577?articleTools=true
#. https://www.nejm.org/doi/pdf/10.1056/NEJMoa2035389?articleTools=true
#. https://www.newsweek.com/stanford-doctor-calls-lockdowns-biggest-public-health-mistake-weve-ever-made-1574540
#. https://www.nsc.org/newsroom/motor-vehicle-deaths-2020-estimated-to-be-highest
#. https://www.rt.com/op-ed/530567-cdc-fauci-covid-mask-guidance-history/
#. https://www.sciencedirect.com/science/article/abs/pii/S0033350620304467?via%3Dihub
#. https://www.scribd.com/document/490005936/The-Transmission-of-Epidemic-Influenza-by-R-Edgar-Hope-Simpson-1992-257pp-pdf
#. https://www.spiegel.de/international/world/reconstruction-of-a-mass-hysteria-the-swine-flu-panic-of-2009-a-682613-amp.html
#. https://www.theepochtimes.com/mkt_morningbrief/whos-really-being-hospitalized_3963392.html?utm_source=morningbriefnoe&utm_medium=email2&utm_campaign=mb-2021-09-02&mktids=103e4a8a3568d4bc6067c7183d607118&est=G681s9gkkIWKa0AW%2F%2BH0qtJMw0XtRAx%2BM4D08osLTy4Q8blcVSJ30sNiIb5SnZh6yH3z
#. https://www.theepochtimes.com/unvaccinated-delta-staff-to-pay-200-monthly-health-surcharge_3964980.html
#. https://www.theguardian.com/global-development/2020/nov/13/measles-cases-900000-worldwide-in-2019
#. https://www.theguardian.com/world/2021/jan/13/christian-persecution-rises-as-people-refused-aid-in-covid-crisis-report
#. https://www.thelancet.com/action/showPdf?pii=S0140-6736%2820%2932661-1
#. https://www.thelancet.com/journals/lancet/article/PIIS0140-6736
#. https://www.thieme-connect.com/products/ejournals/pdf/10.1055/a-1174-6591.pdf
#. https://www.usmortality.com/
#. https://www.washingtonpost.com/health/2020/09/16/coronavirus-dementia-alzheimers-deaths/?arc404=true
#. https://www.webmd.com/vaccines/covid-19-vaccine/news/20210629/almost-all-us-covid-19-deaths-now-in-the-unvaccinated
#. https://www.youtube.com/embed/_XCE1lZwASc
#. https://www.zerohedge.com/covid-19/why-cdc-quietly-abandoning-pcr-test-covid
#. https://z3news.com/w/why-masks-are-a-charade/
#. http://www.fao.org/3/ca9692en/ca9692en.pdf
#. http://www.fao.org/3/cb4474en/cb4474en.pdf

