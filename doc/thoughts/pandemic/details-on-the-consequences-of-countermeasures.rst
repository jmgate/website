At this point we've shown it's completely reasonable to conclude that the
danger of COVID-19 has been grossly exaggerated, and there's more than enough evidence to
conclude that the countermeasures we've employed over the last year and a half
haven't had their desired effect.  However, we're still being
told we must do whatever is necessary to stop the virus, and it doesn't look
like cities and countries the world over will be abandoning non-pharmaceutical
interventions (NPIs) any time soon.  Something we were deeply concerned about
in March of 2020 was what the short- and long-term consequences of any such
countermeasures would be, but it was exceedingly difficult then to get people
to try to consider the ramifications, as the world was gripped by fear of the
potential severity of the pandemic.  Now, a year and a half later, we can
attempt to assess how any such consequences have played out.

Overview
^^^^^^^^

As we examine the ramifications of the various masking, lockdown, and
vaccination policies that have been put in place around the world, we'll be
looking at the evidence in terms of both the medical and then the societal
consequences.  To begin, though, it will be worthwhile to get a broad overview
by starting with excess mortality data.  If you've been following along with
the data in the earlier parts of this analysis, `excess mortality
<https://beyond.britannica.com/what-is-excess-mortality-and-how-is-it-calculated>`_
is a concept you've been exposed to, though we haven't defined it yet.  In
brief, it is the number of deaths in a given area in a given time period over
(or under, if it turns out to be negative) what can reasonably be expected for
the number of deaths in that area in that time period, where that estimate for
expected deaths is calculated based on prior years' mortality data, accounting
for changes in the age and health of the population.  The reason we look to
excess mortality data first is it's really the only hard measure by which we
can compare the impact of the pandemic itself to the impact of our response to
it.

.. sidebar::  Side Note

   If it is the case, as we noted earlier, that death coding guidelines were
   changed to result in COVID-19 being the cause of death more often than not,
   and up to 94% of deaths attributed to COVID-19 should actually have been
   attributed to some other comorbidity, then the proportions of this excess
   mortality that are and are not directy attributed to COVID-19 would be
   changed substantially.

So what do the data show?  First it's important to realize that we don't have
all the data available yet, as it's generally not available in real time, and
we haven't returned to normal yet, but at this point the numbers are in for
2020, so we can see what they have to say.  All told, the US saw about `300,000
excess deaths in 2020 <https://www.cdc.gov/mmwr/volumes/69/wr/mm6942e2.htm>`_,
with two-thirds of them attributed to COVID-19.  That means we're looking at
about 100,000 deaths that we didn't expect for which we're trying to find an
explanation.  What's both confusing and distressing about the data from the US
is the demographics that saw the largest percentage increases in excess
mortality were those who were Hispanic or Latino, and those in the 25--44 age
bracket, meaning certain ethnicities and younger adults were impacted more
significantly than others by our response to the pandemic.  `Additional
research
<https://www.sciencedirect.com/science/article/abs/pii/S0033350620304467?via%3Dihub>`_
backs this up, and also adds that men experienced more excess death than women
in the US.  `A study in Canada
<https://www150.statcan.gc.ca/n1/en/daily-quotidien/210208/dq210208c-eng.pdf?st=FUoSbNT4>`_
noticed similar patterns.

To get an idea of whether or not this excess mortality is likely due to the
NPIs we've put in place, it's worth `comparing countries
<https://ourworldindata.org/grapher/excess-mortality-p-scores-by-age?country=SWE~NOR~USA~FIN~GBR~DNK>`_
with more restrictive NPIs (e.g., US, UK) to those with less restrictive NPIs
(e.g., the Nordic countries).  Indeed such a comparison shows countries with
less severe lockdown measures to have generally lower excess mortality on a
percentage basis than those with more significant lockdowns.  The pattern is
noticeable across all age groups, but is particularly pronounced in the younger
demographics.  `A Stanford doctor has claimed
<https://www.newsweek.com/stanford-doctor-calls-lockdowns-biggest-public-health-mistake-weve-ever-made-1574540>`_
that "lockdowns are the biggest health mistake we've ever made," and our
initial look at excess mortality data suggests he may be right.

.. note::

   If you'd like another perspective on the death count attributable to
   lockdowns, `this article
   <https://www.city-journal.org/death-and-lockdowns?wallit_nosession=1>`__ is
   well worth your time.  If you'd like to do your own data mining to draw your
   own conclusions, these resources will likely be of use to you:

   * `US mortality statistics <https://www.usmortality.com/>`_
   * `Provisional death counts for COVID-19
     <https://www.cdc.gov/nchs/nvss/vsrr/COVID19/index.htm>`_
   * `The Economist's excess death tracker
     <https://github.com/TheEconomist/covid-19-excess-deaths-tracker>`_
   * `COVID-19 stringency index
     <https://ourworldindata.org/grapher/covid-stringency-index?tab=chart&country=AUT~BEL~CZE~DNK~FIN~FRA~DEU~ITA~LVA~LTU~NOR~POL~PRT~SVN~ESP~SWE~CHE~GBR~USA>`_

Thus far we've only looked at observational data, which I strongly prefer, but
before we leave the overview I'm actually going to point you to a modeling
study for your consideration.  You've heard a number of times my skepticism
when it comes to using mathematical modeling as a basis for public policy
decision-making, particularly when the models prove themselves inaccurate as
real-world data becomes available, but that doesn't mean I discount them
altogether.  When considered appropriately within the broader context, they can
give us useful food for thought when weighing alternatives, with the
understanding that at the end of the day they are merely extrapolations.  `This
particular one
<https://jamanetwork.com/journals/jamanetworkopen/fullarticle/2772834?utm_source=For_The_Media&utm_medium=referral&utm_campaign=ftm_links&utm_term=111220>`_
was trying to estimate the impact of school closures on the years of life lost,
and concluded that keeping schools open may have resulted in fewer years of
life lost in the long run.  The excess mortality data we looked at above
indicates there are short-term consequences to our pandemic response that we've
already realized and will continue to realize if we keep more restrictive NPIs
in place.  This modeling study suggests there may be longer-term consequences
that may only unfold over the next few generations.

But where are all these additional deaths coming from?

Medical Consequences
^^^^^^^^^^^^^^^^^^^^

A first place to look is to the medical literature to see if there have been
any changes in what we typically expect for various illnesses.  We'll first
examine whatever we can in terms of death data, as that's a concrete measure,
and then wrap up with any other consequences we find in the medical arena.

Deaths
++++++

A first category that stands out is cardiovascular diseases.  `One study
<https://heart.bmj.com/content/107/2/113>`_ reports an 8% increase in overall
cardiovascular mortality, with 35% more deaths occurring in the home, and 32%
more occurring in extended care homes, than would be expected.  The most
frequent causes were stroke, acute coronary syndrome, and heart failure.
`Another study
<https://jamanetwork.com/journals/jamacardiology/fullarticle/2769293?guestAccessKey=4425a07e-573b-45ec-9a16-82e32ecf762f&utm_source=For_The_Media&utm_medium=referral&utm_campaign=ftm_links&utm_content=tfl&utm_term=080720>`_
noted a peculiar decrease in the number of hospitalizations for acute
myocardial infarction (AMI, or heart attack), along with a substantial increase
in the risk-adjusted mortality rate.  `Another study specific to stroke victims
<https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7338130/>`_ detected the median
time from the onset of symptoms to arrival in the hospital more than doubled
from about two and a half hours to more than six.  In the case of stroke,
getting someone to a hospital as soon as possible is absolutely critical to
the chances of recovery.  Though the studies don't give a direct cause for
these changes in behavior and outcomes, it seems we can reasonably infer that
individuals were waiting longer after the onset of symptoms to seek medical
help, resulting in worse outcomes overall, including excess deaths.

Leaving cardiovascular diseases behind, we see similar patterns `when it comes
to cancer <https://dailynews.ascopubs.org/do/10.1200/ADN.20.200416/full/>`_.
In March, 2020, the CDC issued guidance to health care providers around the
country for non-COVID-19-related care, with the goals of slowing the spread,
appropriately allocating PPE, etc.  These guidelines contributed to the number
of routine in-person cancer screenings plummeting, which in turn contributed to
the number of new diagnoses of cancer dropping nearly 50% from early March to
mid-April, 2020.  This sharp decline is indicative of cases that would've been
caught earlier, were it not for our reaction to the pandemic.  Since with
cancer the earlier the diagnosis, the better the prognosis, our countermeasures
have led to degraded outcomes for cancer patients.

The next diseases we'll turn to are `Alzheimer's and dementia
<https://www.washingtonpost.com/health/2020/09/16/coronavirus-dementia-alzheimers-deaths/?arc404=true>`_,
which saw a 10% increase in excess mortality between March and August of 2020.
The degrading conditions of patients stem from the strategy of isolation we've
adopted, ostensibly for their own safety.  "Doctors have reported increased
falls, pulmonary infections, depression and sudden frailty in patients who have
been stable for years."  This shouldn't come as a surprise to us, as we've
known for ages that social and mental stimulation are some of the few tools we
have available to slow the progression of the disease; taking them away will
naturally lead to rapid degeneration and a hastened death.  Throughout this
analysis I've attempted to stick dispassionately to the data and medical and
other reporting, and to steer clear of making value judgements, but in this
case I cannot.  In addition to the raw data, the article linked above also
gives you an idea of what the human component looks like in all our efforts to
protect the most vulnerable.  The way we as a society have treated the elderly
is downright evil, and constitutes elder abuse of the most heinous variety.

In addition to all of the above, we noted earlier that `the vaccines are
resulting in death
<https://sebastianrushworth.com/2021/04/17/is-the-astra-zeneca-vaccine-killing-people/>`_
more often than is reasonable.  And while in our last area of analysis we noted
the ineffectiveness of face masks, it turns out there are also `a number of
risks <https://swprs.org/face-masks-evidence/#risks>`_ associated with them as
well.  In some cases that means mild difficulty breathing or skin irritation,
in others there are severe psychosocial consequences (which we'll revisit down
below), and in a small handful of cases mask wearing seems to have resulted in
death.

.. note::

   It may be worth your time to read over the `Great Barrington Declaration
   <https://gbdeclaration.org/>`_, which was written by infectious disease
   epidemiologists and public health scientists, recommending an approach of
   "focused protection".  It has garnered over 850,000 signatures.

Other Impacts
+++++++++++++

In addition to the immediate death toll that has already been realized (and
will likely continue to be, if we don't do anything), there are likely also a
handful of other ramifications that will play themselves out over the coming
years.  One of those is it appears in our efforts to prepare for and combat
COVID-19, `measles vaccination programs were halted
<https://www.theguardian.com/global-development/2020/nov/13/measles-cases-900000-worldwide-in-2019>`_
in 26 countries around the world, meaning 94 million children were at risk of
missing their vaccinations for a disease that largely kills children under the
age of five.  In 2019 we saw just shy of 900,000 cases of the measles around
the world.  Using cohort life tables, which insurance companies use to predict
how long someone will live, given their age, `this analysis
<https://sebastianrushworth.com/2020/11/29/how-many-years-of-life-are-lost-to-covid/>`_
of years of life lost due to COVID-19 estimates that if a mere 0.1% of those
who have been denied their measles vaccine wind up dying, that will equal the
estimated number of years of life lost to COVID-19 itself.  Granted, that's
speculation, but since we know the danger of measles and have a safe and
effective preventative for it, why should we gamble in such a way, particularly
since COVID-19 does not pose near the same threat, and our protective measures
that led to the halt of the measles vaccinations have proven themselves
ineffective?

Societal Consequences
^^^^^^^^^^^^^^^^^^^^^

Now that we've looked at the problems caused with a variety of medical issues,
let's turn our attention to any other factors that might be negatively
impacting the world.  Again, we'll first examine whatever might be contributing
directly to the overall excess death we looked at in the beginning, and then
follow that up with any other bad side effects our response to the pandemic may
have caused.

Deaths
++++++

Before we get into how things played out, let's take a look at some projections
first.  `One study
<https://ajph.aphapublications.org/doi/abs/10.2105/AJPH.2020.306095>`__
estimated between 8,000 and 200,000 all-cause excess deaths attributed to
COVID-19-related unemployment during the first year of the pandemic, with the
most likely estimate being 30,000.  `Another study
<https://wellbeingtrust.org/areas-of-focus/policy-and-advocacy/reports/projected-deaths-of-despair-during-covid-19/>`__
predicted between 27,000 and 154,000 deaths of despair (alcohol and drug
misuse, and suicide) based on unemployment and economic projections, with
75,000 being the most likely estimate.  It did note, however, that "when
considering the negative impact of isolation and uncertainty, a higher estimate
may be more accurate."  I mention these modeling studies not to try to give an
accurate prediction of how things would turn out, but rather to point out that
people tried to warn us of the consequences if we continued on the course we
were headed on early in the pandemic.

Let's see what kind of studies are available.  One paper indicates there's been
an `increase in suicide
<https://journals.healio.com/doi/10.3928/00485713-20201105-01>`_ during the
pandemic, spurred by "feelings of uncertainty, sleep disturbances, anxiety,
distress, and depression."  The authors also caution that the psychological and
financial costs may result in long-term psychological conditions.  Another
indicates a `substantial increase in alcohol use
<https://jamanetwork.com/journals/jamanetworkopen/fullarticle/2770975>`_, which
likely contributes to the worsening case rates for anxiety, depression, and
suicide.  The most substantial change was in heavy drinking, with a 41%
increase over the baseline.  Yet another report indicates the `drug overdose
deaths
<https://www.cdc.gov/media/releases/2020/p1218-overdose-deaths-covid-19.html>`_
were rapidly accelerating in the early days of the pandemic.

Perhaps one of the more surprising results is 2020 saw the `highest number of
motor vehicle deaths in 13 years
<https://www.nsc.org/newsroom/motor-vehicle-deaths-2020-estimated-to-be-highest>`_,
and the year-to-year spike of 24% was the highest we've seen in almost a
century.  "That doesn't make sense," you might think.  "Nobody was driving last
year."  Indeed, the overall number of miles driven was down by an estimated
13--16%, but somehow with emptier roads we wound up with more fatalities.  It
appears this is due to a substantial increase in `riskier driving behavior
<https://www.caranddriver.com/news/a34240145/2019-2020-traffic-deaths-coronavirus/>`_,
with drugs, alcohol, and excessive speed leading to more collisions and more
deadly ones.  These changes in driver behavior seem to correlate with the
lockdown measures that went into effect in the second quarter of 2020.

Other Impacts
+++++++++++++

In addition to the side effects of our decisions that contributed directly to
the excess death toll, there are also a number of other issues that arise that
contribute to the overall deterioration of society.  One of our concerns when
the world decided to shut down was what that would do to children living in
unsafe home environments.  If the home is already unsafe, due to substance
abuse or any number of things, and you add to that the stress and anxiety
brought on by the pandemic, business closures and job losses, and mandatory
stay-at-home orders, how much more dangerous does that make the home
environment?  While I wasn't able to find a direct answer to that question,
there was a study that was tangentially related, which found a `fifteen-fold
increase <https://adc.bmj.com/content/106/3/e14>`_ in abusive head trauma (AHT,
which includes "shaken baby syndrome") in infants during the first month of
lockdown measures.  I hope to see more studies on the correlation between
lockdowns and the safety of the home environment in the future.

Another question we had early on was what masks do to communication within
society.  Humans have enough issues communicating effectively as it is; what
happens when everyone hides half their faces?  The answer is `it gets much
harder
<https://elemental.medium.com/i-see-you-but-i-dont-how-masks-alter-human-connection-fbf2e21dd748>`_,
but I'm sure I didn't need to tell you that.  Masks hide a good deal of
non-verbal communication, which can make it more difficult to understand tone
and intent.  When the little gestures like "social smiling" disappear, that has
a significant impact on the health of society.  While the authors suggest using
more gestures can do something to make up for the deficit, given what we know
about the danger of the virus and the efficacy of masks, you're probably better
off just removing them.

Now let's turn for a moment to economic impacts.  In March and April of 2020,
the number of active business owners in the US `plummeted by 3.3 million
<https://www.nber.org/system/files/working_papers/w27309/w27309.pdf>`_ (22%).
The US has never seen a drop that significant in a twelve-month period, let
alone two.  This in turn contributed to the substantial `unemployment shock
<https://www.nber.org/system/files/working_papers/w28304/w28304.pdf>`_, which
then contributed to the various problems listed above.  The unemployment shock
was predicted to result in ~800,000 additional deaths over the next 15 years,
but keep in mind, that's an extrapolation; we'll have to wait and see what
really happens.

On the religious front, lockdown measures in particular, and the stress of the
pandemic and our response to it in general, have led to `increased persecution
of Christians
<https://www.theguardian.com/world/2021/jan/13/christian-persecution-rises-as-people-refused-aid-in-covid-crisis-report>`_
in various parts of the world.  There was a 60% increase in the number of
Christians killed for their faith compared to 2019.  Though not all of that
increase is attributable to our pandemic response, Christians being denied
COVID-19-related aid in numerous countries in Africa and Asia is.  In
sub-Saharan Africa, Islamist militant groups took advantage of lockdowns and
governments weakened by the crisis to ratchet up the level of violence.  This
sort of persecution of religious minorities was already happening around the
world, but the way we responded to the pandemic gave aggressors an opening to
"kick it up a notch."

And now the final societal side effect from our response to the pandemic:
world hunger.  Last year, from late spring through the summer, there were a
handful of reports predicting significant increases in world hunger throughout
the pandemic.  The `2020 State of Food Security and Nutrition in the World
<http://www.fao.org/3/ca9692en/ca9692en.pdf>`_ report from the United Nations
estimated an additional 120 million people would be facing food scarcity, who
would not have otherwise were it not for the NPIs we'd put in place.  Other
reporting predicted `12,000 deaths per day
<https://oxfamilibrary.openrepository.com/bitstream/handle/10546/621023/mb-the-hunger-virus-090720-en.pdf>`_
from starvation, more than we were expected to see from the virus.  At the time
this was generally seen as alarmist reporting, and the developing story saw
surprisingly little coverage through the rest of 2020 and early 2021.  At this
point, though, we have the data on hand to see that `some of the worst fears
have indeed come to pass
<https://tamhunt.medium.com/lockdown-policies-have-vastly-exacerbated-global-hunger-b878b25e85d>`_.
The UN's `2021 report <http://www.fao.org/3/cb4474en/cb4474en.pdf>`_ has just
been released, and it shows the single biggest year-to-year increase in global
hunger in decades, with 161 million more people experiencing it than in 2019.
It's estimated that about three-quarters of that increase (118 million) is due
to the hard policies of lockdowns and related measures and the single-minded
focus on the virus.

What Can We Conclude?
^^^^^^^^^^^^^^^^^^^^^

This has probably been a lot to absorb.  Let's take a moment to summarize what
we've learned.

A sizeable portion of the excess deaths (at least a third, probably more) are
not directly attributable to COVID-19, but are instead attributable to our
response to it.  The unaccounted for excess deaths seem to be particularly
prevalent in the younger demographics.  A comparison across countries indicates
less restrictive NPIs correlate with lower excess mortality and more
restrictive NPIs correlate with higher excess mortality; that is, the more
we've tried to forcibly stamp out COVID-19 (and failed miserably, as our prior
analysis suggests), the more we've contributed to the deaths of others.

In trying to determine where all the excess deaths are coming from, we find
that cardiovascular mortality is up overall, with significantly more deaths
happening in homes and assisted living facilities.  Stroke victims were taking
more than twice as long to make it to the hospital, significantly impacting the
chances of recovery.  All this means people have been waiting longer than they
otherwise would have before seeking treatment, leading to disastrous
consequences.  Cancer screenings plummeted, due to new guidance for
non-COVID-19-related care, leading to substantial increases in missed
diagnoses.  We saw a 10% rise in excess mortality for Alzheimer's and dementia
patients, as all the forced isolation measures put in place for the patients'
safety directly contributed to their rapid deterioration while their loved ones
watched from a distance.  It seems our attempts to protect the elderly and
frail have instead contributed to their demise.

The COVID-19 vaccines themselves have contributed to a significant number of
deaths, and at this point it looks like masks have contributed to a handful as
well.  Measles vaccination programs that were halted so countries could prepare
for and respond to COVID-19 could easily wind up contributing to more years of
life lost than this pandemic does.

In addition to the medical issues, we've seen increases in suicide, heavy
drinking, and drug overdoses.  Counter-intuitively, 2020 saw the highest number
of motor vehicle deaths in 13 years.  Though there were fewer people on the
road, those who were driving were engaging in riskier behavior, likely
motivated by the stress of the pandemic response, and compounded by the
substance abuse problems.

In addition to the direct contributions to the non-COVID-19 death toll, there
have also been a number of less tangible impacts on society.  Shaken baby
syndrome saw a significant increase, likely due to the various stresses added
to life by our response to the pandemic.  Communication was impaired by masks,
leading to a degradation of social graces and an increased tension in social
interactions.  Small business were hit harder than they ever have been before,
contributing to an unemployment shock that we'll likely still be feeling the
aftermath of over a decade later.  All of the instability around the world set
the stage for an increased persecution of Christians, both through execution,
and through COVID-19 aid distribution programs.

Finally, an additional 118 million people faced food scarcity last year than
would have otherwise were it not for our self-imposed pandemic countermeasures.
It's hard for people in rich, Western nations to wrap our heads around what's
meant by global hunger.  Try going without food for the next three
days---pretend you can't simply open the refrigerator to sate your hunger, and
you're not sure when your next meal will be---and then imagine that for a third
of the population of the United States.

**Bottom line:**  For decision-makers to set policy, whether they're in local
or national government or private businesses or other organizations, without a
balanced consideration of the consequences such policies may have, is
unethical.  We knew the majority of these consequences were coming ahead of
time, and we ignored them.  Instead we decided what's best for the world was a
single-minded focus on stamping out the virus, no matter what it takes, and no
matter who suffers in the process.  Our behavior has been reprehensible.

If you're anything like me, wading through all of the above has likely left you
with a profound sense of grief.  Don't squander that.  Wrestle with the
following questions:

* Why is all of the above so troubling?
* Given the myriad ways things have gone so wrong, is there something at the
  root of what's wrong with the world?
* Is there any way to fix all this?

