While our prior analysis indicates
it's reasonable to conclude that COVID-19 does not pose near the threat that's
been reported, let's suppose for the sake of argument that it does.  If the
danger is as great as reported---if the initial mathematical modeling is
representative of reality---then it's entirely reasonable for governments to
consider encouraging various countermeasures to slow the spread of the disease
and thereby decrease the overall death toll.  The countermeasures used have
generally fallen into the categories of mask mandates, lockdowns, and vaccines,
and with the last 18 months' worth of data in hand, we can now ask the
question:  Are such countermeasures effective?

Mask Mandates
^^^^^^^^^^^^^

Let's look first to the concept of mask mandates.  Intuition tells us that if a
virus is spread via droplets expelled from the nose and mouth, then covering
the nose and mouth should slow the spread of the virus through the population.
This is why you're always told to cough into your elbow, for instance.  The
question, then, is whether this intuition is empirically verifiable.

Before we get into the evidence, it's important to make a distinction between
two different hypotheses being put forward:

1. Masks protect *me* from *your* infection; that is, they function as
   *personal protective equipment* (PPE).
2. Masks protect *you* from *my* infection; that is, they function as *source
   control*.

It may be the case that masks are effective for one but not the other.

It's also worth noting, before diving into the data, that the `pandemic
guidelines from the WHO
<https://thefatemperor.com/wp-content/uploads/2020/11/WHO-Pandemic-Guidelines-2019.pdf>`_,
which were in effect before COVID-19 came on the scene, do recommend masks for
the general public if a pandemic's severity is determined to be high; for
moderate or low severity, they are not recommended.  If this pandemic is as
severe as is typically claimed, then recommending masks for the general public
is reasonable according to prior guidance.  However, if this pandemic is not as
severe as we've been led to believe, as our prior analysis suggests, then prior guidance indicates
the masking of the general public is unnecessary.

Now let's dig into the evidence.  Thankfully, the folks at the `Swiss Policy
Research <https://swprs.org/contact/>`_ organization have done a fantastic job
`compiling all the latest studies <https://swprs.org/face-masks-evidence/>`_,
and I'll happily point you in their direction rather than replicating their
work here.  The analysis that follows comes from their collection.

A first question we can ask of the data is whether or not the imposition of
mask mandates has any noticeable effect on the case or death rates.
Intuitively we would expect masking of the general public to significantly
alter the curves in daily case and death rates, though we might expect any
change in the curves to be lagged by some amount of time to account for the
incubation period of the virus.  Unfortunately the data do not bear out this
hypothesis, instead displaying the standard Gompertz curve we expect from,
e.g., seasonal epidemic influenza, as `explained by Ivor Cummins
<https://www.youtube.com/watch?v=_XCE1lZwASc>`_ in the video embedded above.
Note that this even
holds for Bavaria in Germany, which mandated the use of `N95 respirators
<https://en.wikipedia.org/wiki/N95_respirator>`_ in public in January, 2021.

Since our intuition turns out to be dead wrong, a natural next question is why
on earth that is.  It turns out there has been concern, at least since last
summer, that the virus is spread not only through expelled droplets, `but also
aerosols
<https://watermark.silverchair.com/ciaa939.pdf?token=AQECAHi208BE49Ooan9kkhW_Ercy7Dm3ZL_9Cf3qfKAc485ysgAAAsAwggK8BgkqhkiG9w0BBwagggKtMIICqQIBADCCAqIGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMJ8N9Iqq39Si2OXZeAgEQgIICcyryY8OT3U5SwIqAF4NqwieFQ0hGJKCQPws0ayakFJ6QWacOvbDNCacKO25bI8R-qdDltbJYAzTa9DQ1MNOytgoMWdnfC5wniKdb7BKja4sP-T2koSw-GAhGIUmfvt8FpNOAIr3c_yGgGX7IOtWhBFcTGse43cynw6TsEbB5AToRjlJcDKyzCg-vtBbc7CGfCH62cuBkn3gomxnboGIsXTcFElX5ePGDcQVqqPmtwbvNFCs6pBDeOgrYT48d_UUZ3OXFBaBhVQqBP80a-Z1aIhnBhn4CUJbiDeArl5x0GS44R0gEpvhWvcUaU4Z8GckEgiiEXELb9hrj-FelhEpbRhCRkmf9aoLLaMFIghfsKzoPu0LAOecHq1l3U_4qylxkIvGAUEktcG5Z9HZUIOu2i8fqMeZzGsNayJKFD6_eKNc5vFsqooU0gHC6wTQ_9oMsuXgWNt2jS36sDk5aH1n9PYTC_IsHj9oA2nF2H0Sm5ukahbXI2GLG8RRpKMmUxamQc_huCx5SenPyCh1LnoYRn36vOqYsSTv3CbahnrOEyH_cumGVvYt6Zv9_zy5QA7yeRwY_3EmFwxXz15VYHAy7YduFvA8_Zwg4eH97dcKGYYIyLEjRTo1W5i7KIu6ho8BYZy5F_eUzseks3QIfQiJKyMD0B5lsknNRLMi9ews49wxV4FtBHkWzmrBf5rerFDsD2ra5jVULGZ9KldyvOv4zURM0FJgubnym576FZoAZmLWwsA_1vrRyTtw3WPAJ23vI3hFfKPb5HrILbbpEzb6w7yCjbTJNckKdTPCB6YgtR0Z4RyoSZ2XMshACh11JuMsrxFn0iQ>`_.
If that's the case, it poses a problem, as the vast majority of masks are
`unable to block or filter aerosols
<https://bmjopen.bmj.com/content/bmjopen/5/4/e006577.full.pdf>`_, the majority
of which "fill a medium-sized room within minutes."  The potential problem is
well-illustrated in this video:

.. raw:: html

    <div style="position: relative;
                padding-bottom: 56.25%;
                height: 0;
                overflow: hidden;
                max-width: 100%;
                height: auto;">
        <iframe src="https://videopress.com/embed/4egEyh2b?hd=1&amp;loop=0&amp;autoPlay=0&amp;permalink=1"
                frameborder="0"
                allowfullscreen
                style="position: absolute;
                       top: 0;
                       left: 0;
                       width: 100%;
                       height: 100%;
                       padding: 10px;">
        </iframe>
    </div>

.. note::

   There's been `a good deal of backlash
   <https://factcheck.afp.com/doctor-expired-license-falsely-claims-masks-dont-work>`_
   against this video, so don't take it as your sole source that masks don't
   work; instead consider it in the broader context of the mounting evidence
   that supports that claim.  When evaluating critiques, try to discern whether
   those arguing on the other side are giving you empirical evidence, or if
   they're simply making assertions without giving you grounds to back them up.

Now *if* it's the case that this pandemic spreads via expelled aerosols, and
*if* the vast majority of masks are unable to stop the spread of such aerosols,
then we should be able to find ample evidence that masks are not effective for
what we think they should be.  And indeed we can:

* `Commentary:  Masks-for-all for COVID-19 not based on sound data
  <https://www.cidrap.umn.edu/news-perspective/2020/04/commentary-masks-all-covid-19-not-based-sound-data>`_
  (April, 2020)
* `Nonpharmaceutical Measures for Pandemic Influenza in Nonhealthcare
  Settings---Personal Protective and Environmental Measures
  <https://wwwnc.cdc.gov/eid/article/26/5/pdfs/19-0994.pdf>`_ (May, 2020)
* `Perspective:  Universal Masking in Hospitals in the COVID-19 Era
  <https://www.nejm.org/doi/full/10.1056/NEJMp2006372>`_ (May, 2020)
* `Masking lack of evidence with politics
  <https://www.cebm.net/covid-19/masking-lack-of-evidence-with-politics/>`_
  (July, 2020)
* `Facemask against viral respiratory infections among Hajj pilgrims: A
  challenging cluster-randomized trial
  <https://storage.googleapis.com/plos-corpus-prod/10.1371/journal.pone.0240287/1/pone.0240287.pdf?X-Goog-Algorithm=GOOG4-RSA-SHA256&X-Goog-Credential=wombat-sa%40plos-prod.iam.gserviceaccount.com%2F20210827%2Fauto%2Fstorage%2Fgoog4_request&X-Goog-Date=20210827T202406Z&X-Goog-Expires=86400&X-Goog-SignedHeaders=host&X-Goog-Signature=528479d38b2b0438a5f241712c08b2eb4429b88e58af75b447a006dc5ae56b6c8f1013bcfbedfa64ded9b4c9c6071c2b63413b44a22fa9c4fca624f0653fbb0b8359c75575f7749ec06312406034485504ef225528ed985a4e11de8565eecd109e6e52d10a95411c6afa46c4166cf11a54de66345d12bce6bcb7298d16a5748802f57c3789b9579b4abe3d47176c9241a90ebdadd2874cb1ba0c1569926f7014b14f8a29527510a470c02f17387066c76e6b8f3e7f21bd481c3baf7a7f79205b608d986113e4f553e64c4e310a411983afa0d6ebbe5106fd784567f27bc5d5877160e238bb74b91c9c200ba7d1adc42face0e157c25ce2f442c31ff8a71d6d3f>`_
  (October, 2020)
* `Physical interventions to interrupt or reduce the spread of respiratory
  viruses (Review)
  <https://www.cochranelibrary.com/cdsr/doi/10.1002/14651858.CD006207.pub5/epdf/abstract>`_
  (November, 2020)
* `Mund-Nasen-Schutz in der Öffentlichkeit:  Keine Hinweise für eine
  Wirksamkeit
  <https://www.thieme-connect.com/products/ejournals/pdf/10.1055/a-1174-6591.pdf>`_
  (2020, unknown month)
* `Using face masks in the community:  first update
  <https://www.ecdc.europa.eu/sites/default/files/documents/covid-19-face-masks-community-first-update.pdf>`_
  (February, 2021)
* `Effectiveness of Adding a Mask Recommendation to Other Public Health
  Measures to Prevent SARS-CoV-2 Infection in Danish Mask Wearers
  <https://www.acpjournals.org/doi/full/10.7326/M20-6817>`_ (March, 2021)

Some of these sources are specific to COVID-19; others pertain to aerosol
viruses in general.  Some focus on the efficacy of masks serving as PPE, others
as source control, and others consider both.  What they all have in common is
they show "little to no evidence for the effectiveness of face masks in the
general population, neither as PPE nor as source control."  Apparently this
even holds true `in hospital settings
<https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2493952/pdf/annrcse01509-0009.pdf>`_,
which was a surprise to me.

Before we leave the subject of face masks, we should take a moment to address
the various studies that have been completed showing that masks are indeed
effective for combatting COVID-19.  A number of them are based on mathematical
modeling, and, as a :doc:`former computational mathematician </resume>`, I'm
suspicious of such studies because of how they contributed to the fear and
public health policy decisions at the advent of the pandemic.  The remainder
exhibit either poor methodology or counter-intuitive conclusions drawn from the
data presented.  `From the Swiss Policy Research organization
<https://swprs.org/face-masks-evidence/>`_:

  Typically, these studies ignore the effect of other measures, the natural
  development of infection rates, changes in test activity, or they compare
  places with different epidemiological conditions. Studies performed in a lab
  or as a computer simulation often aren't applicable to the real world.

I encourage you to `dig through all the sources
<https://swprs.org/face-masks-evidence/>`_ and draw your own conclusions.

.. note::

   If you'd like another analysis of all of the above from someone more
   qualified than myself to evaluate the medical evidence, consider reading
   `Why Masks Are a Charade <https://z3news.com/w/why-masks-are-a-charade/>`_,
   by `Dr Joseph Mercola <https://www.mercola.com/forms/background.htm>`_.  For
   more of a historical perspective, consider `Do Masks Work?
   <https://www.city-journal.org/do-masks-work-a-review-of-the-evidence?wallit_nosession=1>`_,
   by `Jeffrey H. Anderson
   <https://www.ojp.gov/archives/about/jeffrey-anderson>`_.  Both of these
   articles were published in August, 2021.

   If you'd like to do more research on the effectiveness and potential side
   effects of masks, `this page
   <https://www.lifesitenews.com/news/47-studies-confirm-inefectiveness-of-masks-for-covid-and-32-more-confirm-their-negative-health-effects/>`_
   contains links to 79 studies.  I recommend skipping over the introductory
   material and going straight to the sources.

Lockdowns
^^^^^^^^^

Another bit of human intuition says that if viral particles are spread via
either droplets or aerosols, then simply staying away from other humans should
slow the spread of the virus, and thereby decrease the overall death toll (not
to mention, keep me healthy).  Thus we have the concept of locking down cities
and countries to hopefully decrease the total impact of the pandemic.

Before we look into whether or not such measures achieve their desired aim,
it's worth noting that the `WHO pandemic guidelines from 2019
<https://thefatemperor.com/wp-content/uploads/2020/11/WHO-Pandemic-Guidelines-2019.pdf>`_
stipulate that the following measures are not recommended in any circumstances:

* Contact tracing
* Quarantine of exposed individuals
* Entry and exit screening
* Border closure

So historically the medical and public health communities thought lockdowns to
have no appreciable effect on the length or severity of a pandemic, but we
tried them anyway.  The question, then, is whether or not they worked.

.. sidebar::  Caveat

   This author exhibits a good deal of frustration in his writing, but his
   treatment of the data appears to be sound.

As we did with masks, we can first ask if the imposition of lockdowns had any
noticeable effect on case or death rates.  You likely already know the answer,
if you've been looking at the data as we've walked through prior arguments, but
here's `a fresh take on it
<https://dailysceptic.org/replying-to-christopher-snowdon-again/>`_ specific to
lockdown measures.  Given the dates of imposition of lockdown measures, the
author finds no subsequent impact on the case or death data.  Of particular
note is a comparison of the states within the US, where the five states with
the highest number of deaths per million all issued stay-at-home orders.

If they appear to have no positive effect on the global scale, perhaps it's the
case they can be effective on a smaller scale, if adhered to strictly.  After
all, how many people really obeyed 100% of the time?  This is a question worth
asking, and the US Marine Corps `sought to answer it
<https://www.nejm.org/doi/full/10.1056/NEJMoa2029717>`_ in the midst of boot
camp last year.  Recruits had to quarantine for two weeks before arrival, and
then an additional two weeks after arriving at a closed college campus set
aside for the experiment.  During the experiment masks were worn at all times
except while eating and sleeping, six-foot distancing was maintained at all
times, all buildings followed single direction traffic flow patterns, and
recruits spent most of their time outdoors.  If these recruits stayed clear of
COVID-19, this should have been an ideal setup to show that lockdown measures
work.

Unfortunately, there was no such luck, as 2.8% of the 1800+ recruits taking
part in the experiment wound up testing positive via a PCR test, compared to
1.7% of the 1500+ recruits who elected not to take part.  (It would have been
better for the study if those who elected not to participate had not followed
any of the lockdown measures 100% of the time, such that we could have a better
comparison, but that was not the case.)  Though recruits shared common grounds,
bathrooms, and the mess hall, analysis of the particular genomes in those who
tested positive showed transmission to only take place within platoons and
sleeping quarters.  The analysis also seems to lend support to the notion that
most individuals are mildly infectious, while others function as "super
spreaders", as the majority of the genomes could be traced back to two of the
51 recruits who tested positive.

.. note::

   Given what we learned about PCR tests, it might have been the case that
   there were really only two infected individuals who wound up spreading the
   disease, and those who tested positive but didn't trace back to those two
   were false positives.  Unfortunately there's no way to know for sure.

Okay, so lockdowns don't seem to impact the case and death rate data, and this
one specific instance shows that COVID-19 broke through anyway, but have there
been any systematic studies on the effects of lockdowns?  Indeed, yes.

`One study from last November
<https://www.frontiersin.org/articles/10.3389/fpubh.2020.604339/full>`_ sought
to measure the impact a variety of non-medical factors had on COVID-19
mortality; while they found correlations in some areas, they concluded the
"stringency of the measures settled to fight pandemic, including lockdown, did
not appear to be linked with death rate."  Then in December `a study
<https://onlinelibrary.wiley.com/doi/full/10.1111/eci.13484>`_ found that more
restrictive non-pharmaceutical interventions (NPIs) like stay-at-home orders
and forced business closures had no effect on case growth.  At the same time
the American Institute of Economic Research pulled together `a couple dozen
studies
<https://www.aier.org/article/lockdowns-do-not-control-the-coronavirus-the-evidence/>`_
providing evidence against the efficacy of lockdowns.  At the end of the year,
`a study from Denmark
<https://www.medrxiv.org/content/10.1101/2020.12.28.20248936v1.full.pdf>`_
showed the infection decline to occur before any mandated lockdown measures
went into effect, not after.  More recently, `an analysis from this April
<https://wallethub.com/edu/states-coronavirus-restrictions/73818>`_ sought to
examine the impacts of coronavirus restrictions across the US; they found the
measures correlated well with unemployment, but not with mortality.

On top of all that, there is even some evidence to suggest that `harder
lockdowns increase COVID-19 mortality
<https://dailysceptic.org/2021/02/27/latest-news-298/#the-more-stringent-the-lockdown-the-higher-the-covid-death-toll>`_.
Given what we learned of the importance of vitamin D in combatting the
infection, and what the medical community has known for decades about how your
mental and emotional state contributes to your ability to overcome sickness,
this makes some intuitive sense, but more investigation is likely required.

"Well wait," you might ask, "why have I heard so much about Sweden doing so
poorly without a lockdown compared to the surrounding countries?"  That's a
great question, and there's `a good write-up on it
<https://paulyowell.substack.com/p/the-nordics-and-the-baltics>`_ from March of
this year.  The answer involves a variety of factors like:

* the age and health breakdown of the population
* the percentage of the population in extended care homes
* how many people traveled to the Alps and brought the virus home with them
* whether or not the virus had reached neighboring countries yet
* etc.

Before we leave the subject of lockdowns, we should, as we did with mask
mandates, examine the evidence that suggest they are indeed effective in
controlling the spread of the virus.  Unfortunately, we find much the same
thing as we did with masks:  that such studies are largely based on modeling
(e.g., `this rapid review
<https://www.cochranelibrary.com/cdsr/doi/10.1002/14651858.CD013574.pub2/full>`_
consists primarily of modeling studies, though the authors sought to include
other kinds as well), and they involve `poor methodology and conclusions that
contradict the data
<https://advance.sagepub.com/articles/preprint/Comment_on_Flaxman_et_al_2020_The_illusory_effects_of_non-pharmaceutical_interventions_on_COVID-19_in_Europe/12479987>`_.
Again, feel free to do your own digging and come to your own conclusions.

.. note::

   If you'd like to do more digging on the effectiveness of various lockdown
   measures, `here's a thorough collection of papers
   <https://thefatemperor.com/scientific-analyses-and-papers-on-lockdown-effectiveness/>`_
   on the subject.

.. note::

   I also endeavored to find empirical evidence on the efficacy of social
   distancing policies in particular, in terms of slowing viral spread,
   decreasing overall death toll, etc., but was unable to.  There are a number
   of modeling studies out there predicting the impact of distancing, but given
   the track record mathematical modeling has had in accurately predicting the
   course of this pandemic, I'm inclined to disregard them.  It seems
   reasonable to conclude that the evidence supporting the ineffectiveness of
   both masks and lockdowns can be extended to apply to social distancing as
   well, but if any observational studies on the impact of distancing are
   published in the future, they may refute that conclusion.

Vaccines
^^^^^^^^

The final countermeasures we'll examine here are the COVID-19 vaccines.  A
first question I have when approaching the topic is how their manufacturers
were able to determine their efficacy and ensure minimal side effects on such a
short timeframe.  `Sebastian Rushworth
<https://sebastianrushworth.com/about/>`_ gives us `a good overview
<https://sebastianrushworth.com/2021/01/10/are-the-covid-vaccines-safe-and-effective/>`_
of both how the vaccines work, and what their trials show.  It appears the
trials were limited (you might say flawed) in their design:

* `The Astra-Zeneca vaccine trial
  <https://www.thelancet.com/action/showPdf?pii=S0140-6736%2820%2932661-1>`_
  exhibits a few peculiarities that are concerning:  not using double-blind
  trials throughout, sometimes administering a meningococcus vaccine in place
  of a placebo, using primarily young, healthy participants (only 4% were over
  age 70; none younger than 18; ideal body mass index; small percentage with
  underlying conditions), and being small enough to not detect rare side
  effects.
* `The Pfizer vaccine trial
  <https://www.nejm.org/doi/pdf/10.1056/NEJMoa2034577?articleTools=true>`_
  seems to be much better designed than the Astra-Zeneca one.  That said, it
  did have some limitations:  excluding the immunocompromised, those with
  severe allergic reactions to a vaccine in the past, women who were pregnant
  or breastfeeding, and people with auto-immune disease.  Only 5% of
  participants were 75 years old or older.
* The design of `the Moderna vaccine trial
  <https://www.nejm.org/doi/pdf/10.1056/NEJMoa2035389?articleTools=true>`_ was
  very similar to that of the Pfizer one.  No participants were over age 80.

All three trials had the problematic endpoint of a positive PCR test after one
or more symptoms presented.  It's therefore only possible for the trials to
give us an indication of whether or not the vaccines decrease the presence of
COVID-19 symptoms.  They cannot tell us if they prevent you from contracting
the virus, and they also can't tell us if they prevent you from spreading the
disease to others.  The trials only lasted 2--3 months beyond the second
vaccine dose, so they also can't tell us anything about long-term immunity.

Based on the design of the trials, it's also worth noting that they can't
really tell us if the vaccines are either safe or effective for the most
vulnerable.  Recall that the median age of death is in the 80s, and the average
decedent has four comorbidities.  I would assume this vulnerable demographic
would be the one targeted by the vaccines, but the trials were conducted almost
entirely on young healthy people.

While they can't tell us what we really want to know, they can tell us
something:  They do seem to decrease the incidence of symptomatic COVID-19 in
young healthy people.  If symptoms do present, they tend to be less severe.
Moderna seems to be the best bet, followed by Pfizer, with Astra-Zeneca pulling
up the rear.

A next question I have in evaluating not only the efficacy, but also the safety
of the vaccines is what the side effects look like.  Since we've established
that the virus is of little to no concern to myself and my family, I need to be
able to perform a thorough cost benefit analysis if I were to consider
receiving one or vaccinating my children.  Similarly, if I were an octogenarian
with a few underlying health conditions, I would want to know what the odds
were that a vaccine would help rather than hurt.

At first glance, the initial trial reports leave me a bit discomforted.  An
initial concern is the trials were not designed to find side effects.  The
selection criteria were such that any who were likely to have complications
after receiving the vaccines weren't included in the trials.  Apparently this
is `a common tactic
<https://sebastianrushworth.com/2021/07/19/do-drug-trials-underestimate-side-effects/>`_
in drug trials, such that the drug can be made to look as good as possible.
Even with the deck stacked against finding potential complications, the
Astra-Zeneca vaccine seemingly caused two cases of `transverse myelitis
<https://en.wikipedia.org/wiki/Transverse_myelitis>`_ (which in such a small
sample size raises a major red flag), and the Pfizer one increased the
incidence of severe adverse events, but they failed to specify what those
adverse events were.  The Moderna vaccine didn't wind up with any black eyes in
the trials, so it seems to be the one to go with in terms of both efficacy and
safety.

However, these initial trials were limited, and at this point the vaccines have
been rolled out all over the world, so we should have substantially more data
available to evaluate the potential risks.  When any new vaccine is launched,
we use the `Vaccine Adverse Event Reporting System (VAERS)
<https://vaers.hhs.gov/>`_ to catalogue any complications that arise that may
be due to the vaccine.  Ideally we should be able to query the database and get
a glimpse of what the potential side effects of the three vaccines look like
today.  Unfortunately it looks like `it takes about nine weeks
<https://vaersanalysis.info/2021/06/23/what-is-the-backlog-in-terms-of-time-for-the-publishing-of-vaers-records-for-covid-19-vaccines/>`_
between someone submitting a report of an adverse event and that data becoming
available in the database.  This means we don't have a real time picture of the
safety of the vaccines, and we'll wind up responding too late to any trends
that may arise in the data.

Despite the backlog in data processing, it looks like `the vaccines have
already accrued
<https://undercurrents723949620.wordpress.com/2021/05/13/why-were-not-hearing-about-covid-vaccine-side-effects/>`_
more than 120,000 adverse events, more than 12,000 serious adverse events, and
more than 3,500 deaths.  Typically a drug is pulled from the market after 50
unexplained deaths.  The current death count is higher than for any other
vaccine in history.  This should be significant cause for concern.

Next question:  Do the vaccines in fact stop the spread of the virus?  We saw
that the initial trials were not really designed to answer this question, and
even then they indicated that the answer might be "no," but now that we have
months of data from around the world, what can we say?  Unfortunately the
answer is a resounding "no," `from the doctor often credited for inventing mRNA
technology
<https://rightsfreedoms.wordpress.com/2021/08/07/dr-malone-cdcs-own-data-shows-masks-vaccines-dont-stop-covid-junk-science-is-driving-authoritarianism/>`_,
which is the technology behind the Pfizer and Moderna vaccines.  `A recent
study out of Massachusetts
<https://www.cdc.gov/mmwr/volumes/70/wr/pdfs/mm7031e2-H.pdf>`_ shows that 74%
of individuals testing positive for the delta variant were fully vaccinated,
either with two doses of the Pfizer or Moderna vaccine, or the single course of
the Johnson & Johnson one.  `More recent reporting
<https://www.theepochtimes.com/mkt_morningbrief/whos-really-being-hospitalized_3963392.html?utm_source=morningbriefnoe&utm_medium=email2&utm_campaign=mb-2021-09-02&mktids=103e4a8a3568d4bc6067c7183d607118&est=G681s9gkkIWKa0AW%2F%2BH0qtJMw0XtRAx%2BM4D08osLTy4Q8blcVSJ30sNiIb5SnZh6yH3z>`_
indicates 77% of new cases in Iceland are in those who are fully vaccinated,
and in Israel the number is 86%.  Honestly these vaccines are starting to sound
less and less like vaccines.

.. note::

   Just last week I heard the phrase that the "delta surge is largely a
   pandemic of the unvaccinated."  If someone can give me data to support that
   claim, I'd love to see it.

.. admonition::  Follow-Up

   On September 9th, I was pointed to `this WebMD article
   <https://www.webmd.com/vaccines/covid-19-vaccine/news/20210629/almost-all-us-covid-19-deaths-now-in-the-unvaccinated>`_
   claiming that "almost all US COVID-19 deaths [are] now in the unvaccinated."
   The article mentions some numbers to back up the claim, but doesn't give you
   sources for the raw data, instead referring you to `an Associated Press
   article
   <https://apnews.com/article/coronavirus-pandemic-health-941fcf43d9731c76c16e7354f5d5e187>`_
   as the source of its information.  The WebMD article also includes a few
   anecdotes intended to convince you that vaccination is the best thing for
   you, but they don't contain information that would be useful in assessing
   them (e.g., age at time of death, number of comorbidities, etc.).

   The WebMD article also includes a link to `a Deadline article
   <https://deadline.com/2021/06/99-p4rcent-covid-deaths-los-angeles-unvaccinated-1234781402/>`_
   making the same claim, but specific to Los Angeles county.  This article
   mentions some numbers from a statement from the LA Department of Public
   Health, speaking to cases, hospitalizations, and deaths between December,
   2020, and June, 2021, but it doesn't indicate how the data were collected
   and analyzed, and it doesn't give you access to the raw data to analyze it
   for yourself (though one commenter requests it).  It then refers to the same
   Associated Press article for the rest of its information.

   As far as I can tell, the Associated Press article doesn't cite a single
   source (neither via footnotes nor links), but says that the information
   comes from "an Associated Press analysis of available government data from
   May, [2021,]" and that they also "analyzed figures provided by the [CDC]." 
   Without any links or citations, there's no way to know what data they're
   looking at, and without any specificity, there's no way to know how they
   carried out their analysis.  Both of these complications mean there is no
   way to evaluate the trustworthiness of the information provided.  They do
   note, however, that "the CDC itself has not estimated what percentage of
   hospitalizations and deaths are in fully vaccinated people, citing
   limitations in the data."

   Aside from the complete lack of transparency, the Associated Press article
   also suffers from a number of rhetorical problems.  Like the WebMD article,
   it too includes anecdotal evidence without giving you full information to
   properly evaluate what the anecdotes do and do not show.  Amplifying the
   anecdotal evidence are strong uses of `appeals to emotion
   <https://www.logical-fallacy.com/articles/appeal-to-emotions/>`_.  The
   article also associates a dramatic drop in COVID-19-attributed deaths after
   January, 2021, to vaccinations, but offers no explanation of similar drops
   after both April and August, 2020.  Finally, it uses imprecise language to
   say one thing but imply another.  For instance, "cases, hospitalizations,
   and deaths are rising," seems to be used to imply the situation is dire and
   due to people refusing vaccination.  How much are they rising?  How does
   today compare to last year?  Are they rising based on what we'd expect from
   the seasonality of the virus?  The article fails to answer any such
   questions that could allow us to effectively evaluate the information being
   presented.

   It seems these articles are referencing people the authors consider to be
   trustworthy experts, and the authors are asking you to accept the claims
   made by these experts on faith.  However, the authors and their experts
   provide no empirical evidence to support their claims, so you, if you have a
   data-driven, scientific mind, have no way of evaluating the truth or
   falsehood of the claims made.  If you believe them, you do so solely on
   faith; if you doubt them, you have solid reason to, until any raw data is
   provided.

   For what it's worth, I went looking but couldn't find data that tracked
   cases, hospitalizations, or deaths with vaccination status, either `from the
   CDC <https://covid.cdc.gov/covid-data-tracker/#datatracker-home>`_ or `from
   Our World in Data <https://ourworldindata.org/coronavirus>`_.  Again, if
   someone can show me the data to support these claims, I'll happily take a
   look.

The next thing I'm curious about is whether or not the vaccines are necessary
if you've already recovered from COVID-19.  We're being told that everyone
needs to be vaccinated, but past experience tells us recovering from a virus
should produce a similar immunity, so is that the case here?  I'll run through
the evidence briefly, but check out `this article
<https://sebastianrushworth.com/2021/07/13/does-it-make-sense-to-vaccinate-those-who-have-had-covid/>`__
if you want a longer explanation of the results.  `A study from April, 2021
<https://www.thelancet.com/journals/lancet/article/PIIS0140-6736(21)00675-9/fulltext>`_,
shows immunity from prior infection to be at least as effective as the best
vaccines in preventing reinfection.  `Another study from June
<https://www.medrxiv.org/content/10.1101/2021.06.01.21258176v3.full.pdf>`_
shows no reinfections at all in the cohort studied that had previously
recovered from COVID-19, so it sounds like prior infection provides all the
immunity you need.

Final question, and then we'll draw some conclusions:  For anyone who hasn't
yet had COVID-19, are there any alternatives to the vaccines that offer either
comparable protection or safe and effective treatment for the disease?  In
fact, there are a few straightforward measures you can take to decrease your
risk of experiencing a severe infection.  We saw earlier the importance vitamin
D plays in your ability to fight off viruses, this one included.  It therefore
stands to reason that other preventative measures such as vitamin C, zinc,
a healthy diet, fresh air, sunshine, and exercise would help you stack the deck
in your favor.  Beyond that, though, are there any options for when you get
sick?

A first candidate that jumps out is hydroxychloroquine (HCQ), which is
typically used to treat malaria and auto-immune diseases, like lupus and
rheumatoid arthritis.  It was developed in 1946, so we have plenty of
experience with it.  `A systematic review
<https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7534595/pdf/main.pdf>`_ of HCQ
use on COVID-19 patients concluded

  HCQ was found to be consistently effective against COVID-19 when provided
  early in the outpatient setting.  It was also found to be overall effective
  in inpatient studies.  No unbiased study found worse outcomes with HCQ use.
  No mortality or serious safety adverse events were found.

Another contender is ivermectin, which is typically used to treat parasitic
infestations.  It hasn't been around as long as HCQ, but we still have 40+
years of experience with it.  In May of this year, `this meta-analysis
<https://sebastianrushworth.com/2021/05/09/update-on-ivermectin-for-covid-19/>`_
of the studies of ivermectin use on COVID-19 patients showed a substantial
reduction in the relative risk of dying, with some inconclusive indication that
it might decrease the time for symptom resolution as well.

.. note::

   If you'd like more information about COVID-19 treatment options not
   involving one of the available vaccines, see `this treatment protocol
   <https://swprs.org/on-the-treatment-of-covid-19/>`_ and the research
   supporting it.

.. note::

   As we enter into a time of public unease regarding the ethical
   considerations of vaccine mandates, `this article on liberty of conscience
   <https://founders.org/2021/08/13/vaccine-mandates-and-the-christians-liberty-of-conscience-from-2021-to-1721-and-back-again/>`_,
   though lengthy, will be well worth your time.

What Can We Conclude?
^^^^^^^^^^^^^^^^^^^^^

This has been quite the whirlwind tour of the various pandemic countermeasures
we've employed in the last year and a half.  Let's try to summarize what we've
found and draw some conclusions.

Masks were not historically recommended for the general populace for pandemics
of low to moderate severity, which our prior analysis indicates applies to
COVID-19.  Prior guidance aside, it appears the imposition of mask mandates has
had no impact on the daily case and death data.  This may be because the virus
spreads via aerosols, in addition to droplets, and the majority of aerosols are
not captured by masks, and instead wind up filling a room within minutes.
Numerous studies indicate masks have no effect on viral spread, either for
COVID-19 or in general, and some studies even indicate they don't alter the
chances of post-operative infection in surgical settings.  There are a number
of studies arguing for the efficacy of masks, but many tend to be mathematical
modeling analyses of questionable value, and they typically exhibit poor
methodology, drawing conclusions that contradict the data.

Given all this evidence, it seems clear that `the CDC's changing guidance
<https://www.rt.com/op-ed/530567-cdc-fauci-covid-mask-guidance-history/>`_ on
masking over the course of the pandemic hasn't been motivated by evidence-based
medicine.  One has to wonder why there's continued pressure to use them when
they don't do what they're supposed to do.

Historically lockdown measures such as contact tracing, quarantining exposed
individuals, entry and exit screening, and border closures were not recommended
for a pandemic, regardless of severity.  For some reason we abandoned
well-established guidance and instead imposed various levels of lockdown around
the world; however, these restrictions can be seen to have had no impact on the
daily case and death rates.  On the local scale, strict adherence to
quarantine, masking, social distancing, one way traffic in buildings, etc.,
still leads to spread of COVID-19.  A number of studies have been conducted
looking for the impact non-pharmaceutical interventions (NPIs) have had on the
spread of the disease, but the studies consistently show no correlation
whatsoever between the more restrictive NPIs and mortality.  (Washing your
hands, however, is still good
for you.)

Though it seemed justified early on in the pandemic to point to Czechia as an
example of a hard lockdown conquering the virus, you could immediately counter
by pointing to the UK with a similarly hard lockdown and some of the worst case
rates in the world at the time.  Also time has shown that the countries we
might've initially thought had beaten the virus like Czechia simply hadn't
received it yet; they were hit hard later on.  Given all the data available
from the last 18 months, one has to wonder why cities, states, and countries
the world over are now returning to various degrees of lockdown when that does
nothing to slow the spread or prevent deaths.

The rush to get COVID-19 vaccines to market as quickly as possible raises a
number of questions about the safety and efficacy of the vaccines.  The initial
trials designed to earn them the emergency use authorization suffer from poor
design, and in some cases execution.  They can really only tell us how well the
vaccines reduce COVID-19 symptoms in young healthy people, while we'd like to
know if they're safe and effective for the most vulnerable (70+ with one or
more underlying conditions).  In terms of side effects, the Astra-Zeneca
vaccine possibly causing transverse myelitis, and the Pfizer one increasing the
incidence of unspecified severe adverse events, are concerning.  Beyond that,
the Vaccine Adverse Event Reporting System (VAERS) data being lagged by nine
weeks mean we won't be able to respond promptly to trends developing in the
side effect data, and at this point we've already seen more than 3,500 deaths
associated with the vaccines, meaning they should've been pulled from
circulation ages ago.

On top of all these concerns, it appears the vaccines don't actually stop you
from either contracting COVID-19 or spreading it to others.  The good news,
though, is having recovered from the disease provides you with equal or better
immunity, so in many cases the vaccines simply aren't needed.  And if you want
to combat the virus without one of the vaccines, hydroxychloroquine (HCQ) and
ivermectim have proven themselves safe and effective treatments.  One has to
wonder why, in the context of limited vaccines and a press for global herd
immunity as soon as possible, we're pressuring everyone in the world to get
vaccinated when it appears it's largely unnecessary, and if treatment becomes
necessary, we've had safe, effective, cheap, generic treatments like HCQ and
ivermectin on hand since before the pandemic began.

.. note::

   "Coercing" might be a better word than "pressuring," given what I've heard
   in the past week of people losing their jobs, being `fined $200/month
   <https://www.theepochtimes.com/unvaccinated-delta-staff-to-pay-200-monthly-health-surcharge_3964980.html>`_,
   being `restricted from air and rail travel
   <https://globalnews.ca/news/8122807/canada-election-covid-19-madatory-vaccination/>`_,
   etc., for not getting vaccinated.

**Bottom line:**  Masks don't protect me from you or you from me.  Lockdowns
don't have any impact on case rate or mortality.  The available vaccines only
appear to be effective therapeutics, but with significant risk involved.
Alternative treatments are available with negligible risk.  Given this summary
of the efficacy of the countermeasures we've employed in combatting the
pandemic over the last 18 months, here are some questions for you to wrestle
with:

* Why do you continue to use various countermeasures if they've proven
  themselves ineffective?
* How long will you continue?
* When will we reach the point when "enough is enough"?
* If there doesn't seem to be any harm to them, should you just keep going with
  the flow?

