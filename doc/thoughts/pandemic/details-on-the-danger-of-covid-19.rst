Historical Background
^^^^^^^^^^^^^^^^^^^^^

Before assessing the danger of COVID-19, I think it's instructive to get some
historical perspective by looking back at how the world responded to the swine
flu in 2009.  After it was all over, Der Spiegel sought to reconstruct the
timeline of events and what played into the decision-making around the globe.
`Their article
<https://www.spiegel.de/international/world/reconstruction-of-a-mass-hysteria-the-swine-flu-panic-of-2009-a-682613-amp.html>`_
is worth a quick read, but allow me to sum up their analysis here:  A fear of
prior deadly pandemics, combined with a focus on worst-case scenario
predictions out of touch with mounting evidence, along with significant
pressure from both the pharmaceutical industry and political players around the
world, contributed to a worldwide hysteria for a virus that in the end killed
no more than the average flu season.

With that background in mind, let's take a look at how things have played out
this time around.

The Initial Scare
^^^^^^^^^^^^^^^^^

For the first few months as the disease spread throughout Asia, and began to
spread through the rest of the world, most countries did nothing but watch to
see how things would unfold.  Then in March of 2020, the `Imperial College of
London <https://www.imperial.ac.uk/about/>`_ released `a report
<https://www.imperial.ac.uk/media/imperial-college/medicine/sph/ide/gida-fellowships/Imperial-College-COVID19-NPI-modelling-16-03-2020.pdf>`_
that seems to have triggered the initial scare.  The team that produced the
report was led by a mathematical epidemiologist, and they used a combination of
mathematical modeling and analysis of data as it was emerging from China to
predict that an unmitigated pandemic (meaning without taking any
countermeasures) would lead to 510,000 deaths in the UK and 2.2 million deaths
in the US, not counting any deaths that would result from the healthcare
systems being overrun.  Unfortunately the Imperial College report isn't clear
on a cut-off date for those deaths; however, `a separate modeling study on
Sweden
<https://www.medrxiv.org/content/10.1101/2020.04.11.20062133v1.full.pdf>`_ was
posted to `medRxiv <https://www.medrxiv.org/>`_ in April, 2020, predicting
96,000 deaths in the country by July 1st.  Given such startling predictions,
it's understandable that much of the world responded with, "We can't just sit
here; we have to *do* something!"

However, it's important to realize that many of the initial predictions were
based on *mathematical models* trained on a limited amount of data.  As a
:doc:`former computational scientist </resume>`, I think the mathematical
modeling community needs to have a lot more humility and transparency about
what it can claim, and with what level of certainty, and what it cannot.  Now
that we have an additional 18 months of real-world data, we can evaluate both
how accurate those models were, and how dangerous the disease really is.

What Constitutes a COVID-19 Case?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Before we do that, though, we need to talk a bit about how the world has been
tracking the spread of the virus, and for that we need to understand the
details of the polymerase chain reaction (PCR) tests.  The best write-up I've
seen on these is from `Sebastian Rushworth
<https://sebastianrushworth.com/about/>`_:  `How Accurate are the COVID Tests?
<https://sebastianrushworth.com/2020/11/06/how-accurate-are-the-covid-tests/>`_
I highly recommend reading the article, as I won't be able to do it justice
here.  In a nutshell, the PCR test is trying to take a viral genome fragment
and replicate it enough times such that you have a large enough sample to
detect.  The number of times you allow the fragment to replicate is what's
known as the *cycle threshold* (CT).  Rushworth says "a positive test result
after 40 cycles is almost certainly a false positive, while a positive result
after 20 cycles is most likely a true positive."  Unfortunately, as PCR tests
were rolled out the world over to track the spread of the disease, no
mechanisms were put in place to associate test results with the CTs that
produced them, so we (the public and doctors alike) are left in the dark as to
what the results truly mean.

On top of this problem, the PCR test `has no ability to tell you whether or not
you are infectious
<https://www.cebm.net/covid-19/infectious-positive-pcr-test-result-covid-19>`_.
The reason here is the tests only detect viral genome *fragments* in the swab
that was done of your nose or mouth.  Those fragments may have come from an
active infection in your system, they could be remnants from you weathering the
disease in the past, or they could have been inhaled from your environment (and
it's not possible to tell if they will or won't infect you).  To determine
whether or not you truly had the disease, you need to follow up a PCR test with
an antibody test, though this is rarely being done.  Rushworth concludes his
article with

  And that, ladies and gentlemen, is why PCR positive cases are a very poor
  indicator of how prevalent COVID is in the population, and why we should
  instead be basing decisions on the rates of hospitalization, ICU admission,
  and death.  If we just look at the PCR tests, we will continue to believe
  that the disease is widespread in the population indefinitely, even as it
  becomes less and less common in reality.

.. sidebar::  Caveat

   The first article linked here was not written in the most level-headed
   fashion.  Try to just pay attention to the data and links provided therein.

Unfortunately it sounds like `a good number of PCR tests for COVID-19 are being
conducted with a CT of 40
<https://www.zerohedge.com/covid-19/why-cdc-quietly-abandoning-pcr-test-covid>`_
(`other sources
<https://undercurrents723949620.wordpress.com/2021/05/13/why-were-not-hearing-about-covid-vaccine-side-effects/>`_
indicate 45), so the fidelity of the results is highly questionable.  That
being said, as you hear of "cases rising" or a "resurgence of cases" in your
area or around the world, these are the cases being referenced.

.. note::

   The situation may even be worse than this, as it looks like `the WHO
   guidelines for the surveillance of COVID-19
   <https://apps.who.int/iris/rest/bitstreams/1291156/retrieve>`_ also allow
   for probable cases to be recorded without any testing.  However, my
   understanding of the medical and legal terminology in that document may be
   incorrect.

The Rise of the "Casedemic"
^^^^^^^^^^^^^^^^^^^^^^^^^^^

The loose criteria for determining COVID-19 cases in turn give rise to a
phenomenon that's been dubbed a "casedemic", where testing yields a significant
number of cases, but you don't have corresponding impacts in terms of
hospitalizations and deaths.  `Ivor Cummins
<https://thefatemperor.com/about-ivor-cummins/>`__ gives us a good look at what
the data show in the video clip below.

.. raw:: html

    <div style="position: relative;
                padding-bottom: 56.25%;
                height: 0;
                overflow: hidden;
                max-width: 100%;
                height: auto;">
        <iframe src="https://www.youtube.com/embed/_XCE1lZwASc?start=1511"
                frameborder="0"
                allowfullscreen
                style="position: absolute;
                       top: 0;
                       left: 0;
                       width: 100%;
                       height: 100%;
                       padding: 10px;">
        </iframe>
    </div>

If you have time to do some number crunching, I encourage you to compare the
time series data for `COVID-19 cases
<https://ourworldindata.org/grapher/biweekly-confirmed-covid-19-cases?tab=chart&time=earliest..latest>`_
and `COVID-19 deaths
<https://ourworldindata.org/grapher/biweekly-covid-deaths?tab=chart&time=earliest..latest>`_
and draw your own conclusions.

.. note::

   If you do look into the raw data, you'll notice a significant rise in both
   cases and deaths in late 2020 and early 2021.  This is to be expected after
   locking down much of the world's population during the summer months when we
   would normally be outside building viral immunity within the population (see
   the discussion of seasonality below).

Unfortunately searching the internet for the term "casedemic" has a tendency to
provide articles that exhibit significant bias.  I'll provide two links here,
in case you want to dig through the information and links therein, but take
them with a grain (or perhaps a tablespoon) of salt.

* `There is No COVID Test and the Casedemic is a Shameless Scam
  <https://stovouno.org/2020/11/12/there-is-no-covid-test-and-the-casedemic-is-a-shameless-scam/>`_
* `It's a COVID Casedemic; not a Pandemic
  <https://www.americanthinker.com/articles/2020/09/its_a_covid_casedemic_not_a_pandemic.html>`_

The `Swiss Policy Research <https://swprs.org/contact/>`_ organization,
however, gives us the more level-headed assessment that `we have a "casedemic"
on top of a pandemic <https://swprs.org/covid-just-a-casedemic/>`_.  We
understand that the virus is real, and that it's impacting people all over the
world; what we're trying to assess it its severity.

What Constitutes a COVID-19 Death?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Before we can understand the severity, though, we first need to understand how
COVID-19 deaths are tallied.  `As the Children's Health Defense organization
reports
<https://childrenshealthdefense.org/news/if-covid-fatalities-were-90-2-lower-how-would-you-feel-about-schools-reopening/>`_,
for some reason "COVID-19 data is collected and reported by a much different
standard than all other infectious diseases and causes of death data."  Prior
to March of 2020, causes of death in the US were reported according to the
`Medical Examiners' and Coroners' Registration and Fetal Death Reporting
<https://www.cdc.gov/nchs/data/misc/hb_me.pdf>`_ guide and the `Physicians'
Handbook on Medical Certification of Death
<https://www.cdc.gov/nchs/data/misc/hb_cod.pdf>`_ put out by the Centers for
Disease Control and Prevention (CDC).  Under these guidelines, the cause of
death would be the last problem in the chain of events that led to death, e.g.,
renal failure, and that would be listed on the first line in Part 1 of the
death certificate.  Below that would be listed the other comorbid conditions in
the chain of events that led to death, so, e.g., COVID-19 might be at the
bottom of that list.  Below that, in Part 2 of the death certificate, you would
list any other things that happened to be wrong with the patient at the time of
death that were unrelated to the chain of events that lead to death.

That was how things worked prior to March of 2020, at which point `new
guidelines were adopted
<https://www.cdc.gov/nchs/data/nvss/coronavirus/Alert-2-New-ICD-code-introduced-for-COVID-19-deaths.pdf>`_,
which state that "the rules for coding and selection of the underlying cause of
death are expected to result in COVID-19 being the underlying cause more often
than not."  The updated guidelines make two significant changes to the process
of filling out a death certificate.

1. If the decedent has COVID-19 at the time of death, that should be listed on
   the first line in Part 1, and all other comorbidities listed in Part 2.
2. COVID-19 can be listed as the cause of death without a positive lab test.

Why were these changes made?  I can't think of a logical reason, and I haven't
been able to find any justification.  Generally speaking I want to stay away
from the "why" questions in this analysis, because at this point in the game
it's exceedingly difficult to find direct answers, and one can quickly slip
into the realm of opinion and intrigue.  Regardless of the motivation, though,
we can see what impact the change in coding guidelines had.  `Data from the CDC
<https://www.cdc.gov/nchs/nvss/vsrr/covid_weekly/index.htm#Comorbidities>`_
indicate that about 94% of the deaths attributed to COVID-19 in the US would
have been attributed to other comorbidities if accounted by the rules in place
prior to March, 2020.  Hang on to this thought, as I'll come back to it in a
moment.

.. note::

   It sounds like there's disagreement on whether or not these updated
   guidelines were actually followed by doctors and coroners when filling out
   death certificates.  Without interviewing a large number of people who
   filled out death certificates in the last 18 months to get an idea (I
   couldn't find a study on this yet), I'm assuming the new guidelines that
   were put in place were followed.  However, if this turns out to not be the
   case, then my concerns about deaths being over-attributed to COVID-19
   are significantly diminished.

So How Bad Is It?
^^^^^^^^^^^^^^^^^

Before we get to how bad COVID-19 is, please note that there are many variables
that complicate such an assessment when looking at the different countries
around the world:

* differences in health care systems
* relative health of the population
* age distribution within the population
* percentage of the population in assisted living facilities
* differences in data collection and reporting
* how bad the prior year's flu season was
* etc.

The `Swiss Policy Research <https://swprs.org/contact/>`_ organization has done
an excellent job `compiling all the latest information
<https://swprs.org/covid19-lethality-how-not-to-do-it>`_, so I'll happily point
you to their work and encourage you to click through to the source materials.

An initial observation to make from their collected research is that in the
early days of the virus, the information coming out of China led to an
estimated hospitalization rate (the number of people needing hospital care
divided by the number of infections) of about 20%.  This fueled the concerns
that healthcare systems the world over would be overwhelmed, and to prevent
that we needed to do whatever was necessary to "flatten the curve" (or spread
hospitalizations out over a longer period of time).  In reality the
hospitalization rate has been closer to 2%, and relatively few localities have
had their healthcare systems overrun.  For comparison purposes, a 1--2%
hospitalization rate is what we typically see with seasonal influenza.

The next key point to note is that the median age of death globally is in the
80s, meaning half of decedents are younger and half are older.  On a related
note, a sizeable percentage of deaths occurred in assisted living facilities.
How many varies from country to country, based on the age and health breakdown
of the population, but in some countries it's as high as 80%.  Correlated with
age is also the number of comorbidites a decedent has, or the number of other
underlying conditions present at the time of death.  `The most recent CDC data
<https://www.cdc.gov/nchs/nvss/vsrr/covid_weekly/index.htm#Comorbidities>`_
indicates the average number of comorbidities for those who have died with
COVID-19 in the US is four.  This is not a virus that affects the population
equally, but instead it has greater impact on those who are in poorer health
and are nearer the end of life already.

The next key piece of information to look at when assessing the severity of the
pandemic is a measure known as the *infection fatality ratio* (IFR), or the
number of deaths divided by the number of infections.  For reference, a "bad"
IFR is about 0.3%, as exhibited in the influenza epi/pandemics of 1936, 1951,
1957, and 1968.  Last October `John Ioannidis
<https://en.wikipedia.org/wiki/John_Ioannidis>`_ provided `a low estimate
<https://onlinelibrary.wiley.com/doi/10.1111/eci.13423>`_ of 0.2% for the
COVID-19 IFR; however, an earlier paper `provided a higher estimate
<https://www.ijidonline.com/action/showPdf?pii=S1201-9712%2820%2932180-9>`_ of
0.68%.  Ioannidis' paper used substantially more underlying data, so I'm
inclined to consider his the more accurate estimate.  The Swiss Policy Research
link above gives you the full breakdown by country, so you can see how the IFR
varies from place to place.  One final tidbit before leaving IFR is that
Ioannidis showed if we restrict our consideration to individuals younger than
70, the estimate drops to 0.04%.  For comparison purposes, seasonal influenza
typically sees an IFR of 0.05--0.1%.

.. sidebar::  Side Note

   I've had a number of people give me anecdotal evidence that they've never
   known anyone to have died of the flu, but they've known X number of people
   who have died with COVID-19.  See the section on long-term effects below for
   why you shouldn't rely on such anecdotal evidence, but that aside, keep in
   mind that the last time the world saw a pandemic of this severity was 50+
   years ago.  I'm guessing the people who keep telling me this wouldn't have
   known many people in the most vulnerable population groups back then, but it
   stands to reason that they know significantly more people in those groups
   now.  Their anecdotal evidence makes complete sense when viewed in the
   larger context, and it doesn't show what they think it shows.

So how bad is COVID-19 in terms of mortality?  The data as of March, 2021,
suggest an IFR between 0.1% and 0.35%, and the overall global COVID-19
mortality (total deaths divided by the global population) is about 0.035%.  For
reference, the global mortality rate for the Spanish flu pandemic of 1918 was
about 2.3% (granted, medicine has progressed a good deal in the intervening
century).  Looking at all the data, this looks to be on par with the "bad" flu
seasons we see a handful of times per century, and `the Children's Health
Defense article
<https://childrenshealthdefense.org/news/if-covid-fatalities-were-90-2-lower-how-would-you-feel-about-schools-reopening/>`_
referenced above also shows it to be comparable to both influenza and
pneumonia.

Finally, some food for thought:  Recall at the end of the last section the
observation that a sizeable percentage of deaths attributed to COVID-19 would
have been attributed to other causes, were it not for a change in cause of
death coding procedures.  Since all the data we've looked at in this section
have been based on COVID-19 deaths as recorded, does that mean the severity of
the virus is actually substantially less than indicated here?  I'll let you
come to your own conclusions.

How Does This Compare to Other Viruses?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

At this point it's beginning to sound like COVID-19 presents like any other
virus, as far as the mortality data are concerned, but is it similar to other
viruses in other respects?  Indeed, yes, and examining how can help us make
better sense of the numbers and figures we're inundated with on a regular
basis.  Again, I'll direct you to `Ivor Cummins
<https://thefatemperor.com/about-ivor-cummins/>`_ for his balanced walk through
what the data can tell us about the seasonality of the virus.

.. raw:: html

    <div style="position: relative;
                padding-bottom: 56.25%;
                height: 0;
                overflow: hidden;
                max-width: 100%;
                height: auto;">
        <iframe src="https://www.youtube.com/embed/_XCE1lZwASc"
                frameborder="0"
                allowfullscreen
                style="position: absolute;
                       top: 0;
                       left: 0;
                       width: 100%;
                       height: 100%;
                       padding: 10px;">
        </iframe>
    </div>

.. note::

   This is the same video embedded above, just started from the beginning
   instead of part-way through.

He directs us to a book by `R. Edgar Hope-Simpson
<https://en.wikipedia.org/wiki/Robert_Edgar_Hope-Simpson>`_ from 1992:  `The
Transmission of Epidemic Influenza
<https://www.scribd.com/document/490005936/The-Transmission-of-Epidemic-Influenza-by-R-Edgar-Hope-Simpson-1992-257pp-pdf>`_.
In it the author notes the distribution of epidemic impact depends on climate;
specifically, the temperate climates will see relatively sharp peaks, while the
tropical climates will have wider distributions spread across the whole year
(see, e.g., Chapter 8, particularly Figures 8.4--8.7).  We see these same
patterns in the COVID-19 data.  The dependence on climate also explains the
so-called "second wave" the US experienced last year.  Since this US consists
of such a large area, with such diverse climate, you wind up adding the
distributions for the temperate and tropical climates to get the double-hump
curve we see every flu season.

.. note::

   The climate variety *may* also explain the rise in cases and deaths we're
   seeing associated with the delta variant, but it's too soon to know with any
   certainty.

You don't have to just take Cummins' word for it, though; at this point we have
a number of studies on the seasonality of COVID-19.  For instance, `a paper
from May, 2021
<https://agupubs.onlinelibrary.wiley.com/doi/10.1029/2021GH000413>`_, analyzed
the effects of air drying capacity and ultraviolet radiation on COVID-19
dynamics at the seasonal scale.  `Another paper from June of this year
<https://www.ncbi.nlm.nih.gov/pmc/articles/PMC8164734/>`_ discusses the impact
of sunlight on transmissibility, morbidity, and mortality, which seems to
validate `an earlier study
<https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7965847/>`_ that suggested the
importance of vitamin D in fighting the virus.

Cummins also demonstrated the impact the prior year's flu season had on how
hard a country was hit by COVID-19.  The excess mortality data show that a
country with a relatively light prior season---meaning fewer than expected
deaths, leading to a build-up of the frail within the population (e.g., Sweden,
the UK)---led to a substantial COVID-19 impact.  Similarly, a normal to hard
prior season led to a minimal COVID-19 impact (e.g., Finland, Norway).

Are There Any Long-Term Effects?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

So far our analysis has been based largely on mortality data, and to a much
lesser extent hospitalization rates.  But what about the long-term effects of
having contracted COVID-19 and then recovered from it?  There seems to be
significant anecdotal evidence supporting a host of bizarre long-term side
effects after recovering from the infection---what's come to be known as "long
COVID"---and we should evaluate that as well in assessing the danger of the
disease.

First, let me caution you strongly against arguing solely on the basis of
anecdotal evidence---this is something known as `the anecdotal fallacy
<https://fallacyinlogic.com/anecdotal-fallacy-definition-and-examples/>`_.
Beyond that, though, I'll gladly refer you to `this article
<https://sebastianrushworth.com/2020/11/17/what-is-long-covid/>`_, again from
`Sebastian Rushworth <https://sebastianrushworth.com/about/>`_, on what long
COVID is and isn't.  It's been hard to peg down, as `studies have shown
<https://evidence.nihr.ac.uk/themedreview/living-with-covid19/>`_ it to be such
a hodge-podge of unrelated conditions.  Regardless of any particular lingering
symptom, `most people recover within a month
<https://www.medrxiv.org/content/10.1101/2020.10.19.20214494v2.full.pdf>`_,
~95% are fully recovered by two months, and only ~2% continue to experience
side effects after three months.  In a nutshell, the studies conducted so far
seem to indicate that long COVID is nothing more than `post-viral syndrome
<https://sma.org/post-viral-syndrome/>`_, which is "a wide range of complex
conditions involving physical, cognitive, emotional and neurological
difficulties that vary in severity over time."  It's something that you might
suffer as your body recovers from a significant viral infection.

What Can We Conclude?
^^^^^^^^^^^^^^^^^^^^^

We've been through a lot in this section, so let's try to pull it all together to
reach some final conclusions about the danger of COVID-19.

Our initial fear of the virus looks like it was triggered by either

1. poor mathematical modeling,
2. poor communication of the results of that modeling,
3. poor reaction to the communication of the results of that modeling,

or some combination of all three.  As the virus began spreading around the
world, it seems our assessment of its severity was exaggerated by loose
definitions of both what constitutes a COVID-19 case and a COVID-19 death.
Polymerase chain reaction (PCR) tests with a cycle threshold (CT) of 25 or
greater produce too many false positives to be useful in surveilling the spread
of the disease, and lack of reporting of CTs with test results means doctors
and patients alike can't know whether they represent useful information.
Beyond that, a positive PCR result can't tell you if you have live virus in
your system, or if it merely found fragments of dead virus.  In addition to
"case" being defined so generously, it looks like new cause of death coding
guidelines were implemented that would result in COVID-19 being listed as the
cause of death more often than not.

Governments and individuals alike have routinely focused on the questionable
measure of cases, rather than the more useful measures of hospitalizations and
deaths.  When you stick to those harder measures, COVID-19 looks to be on par
with the bad epi/pandemics we see a few times per century (e.g., 1936, 1951,
1957, 1968), with an infection fatality rate (IFR) of ~0.3%, and a sizeable
portion of deaths occurring in assisted living facilities.  However, if you
look at the portion of the population below age 70, for those individuals this
pandemic is no more severe than a typical influenza season (IFR of ~0.04%).
Though the hospitalization rate was predicted to be 20%, which would have
overwhelmed healthcare systems the world over, in reality the rate has been
closer to 2%, and few hospital system have been overwhelmed.

The seasonality data we have at this point seems to support the claim that this
pandemic is behaving generally as influenza epi/pandemics do, with the
distribution of cases and deaths being largely governed by climate,
specifically air drying capacity and ultraviolet radiation.  This is good news,
as it gives us clues as to what we can do to mitigate the risks.  On the
personal level, make sure your diet is healthy, get some good exercise, and
spend plenty of time outside in the sun.  Excess mortality data show that a
light prior flu season---indicating a build up of the frail within the
population---seems to be a good predictor for the severity of COVID-19 impact,
so at a policy-making level, nations or healthcare systems can monitor the
prior season's excess deaths to get a feel for how severe the upcoming viral
season will be, and then take appropriate measures to protect the most
vulnerable.

Though there is anecdotal evidence to support the notion of long-term side
effects after recovering from the disease (what's been dubbed "long COVID"), an
analysis of such cases shows them to be nothing more than post-viral syndrome,
which is well-established in the medical literature.  Long-lasting effects are
therefore not something to worry about.

**Bottom line:**  If you are near the end of life as it is---you're over 70,
have one or more underlying health conditions, or are in an assisted living
facility---now is probably a good time to make sure you're ready to die.  (Of
course this would've been true for you before COVID-19 as well.)  If this is
you, here are some questions I'd encourage you to wrestle with:

* Why are you here?
* What is your purpose in life?
* What will you do with the time you have left, whether that's two months or
  twenty years (and we'll pray it's the latter)?
* Does anything, good or bad, await you after death?
* How do you know the answers to these questions?

If you don't fall into the category above, you likely don't have anything to
worry about in terms of your own health and well-being.  You may want to talk
through all this with those you know in the vulnerable subset of the
population, and you may wind up mourning the loss of one or more loved ones.

