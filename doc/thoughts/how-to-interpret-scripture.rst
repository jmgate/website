How to Interpret Scripture
--------------------------

.. raw:: html

   <p style="text-align:left;">
       <i>August 19, 2023</i>
       <span style="float:right;">
           🕑 42 min.
       </span>
   </p>

In recent years I've had a handful of experiences with people that have
ultimately boiled down to disagreements over what particular verses in the
Bible mean.  We're looking at the same verses, but interpreting them in ways
that are incompatible with each other.  Some situations ended with the other
person saying something along the lines of, "Well, I guess we'll just have to
agree to disagree," but without any `argumentation`_ first.  Others ended with
the other party simply cutting off communication, and still others ended in
messier ways.  Regardless of how those conversations ended, they all
highlighted for me the importance of believers knowing how to rightly interpret
scripture, that is, the discipline of biblical hermeneutics.  This past spring
I taught this subject to our homeschool speech and debate club, and now I've
finally taken the time to write up all the notes:  |biblical_hermeneutics|_.

.. _argumentation:  https://newmexicoforensicsclub.readthedocs.io/en/latest/training/crafting-an-argument.html
.. _biblical_hermeneutics:  https://newmexicoforensicsclub.readthedocs.io/en/latest/training/biblical-hermeneutics.html
.. |biblical_hermeneutics| replace::  *Biblical Hermeneutics*

