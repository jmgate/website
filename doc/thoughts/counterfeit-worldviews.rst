Counterfeit Worldviews Invading the Church
------------------------------------------

.. role:: raw-html(raw)
   :format: html

.. raw:: html

   <p style="text-align:left;">
       <i>September 12, 2022</i>
       <span style="float:right;">
           🕑 95 min.
       </span>
   </p>

.. contents::  Contents
   :local:

Motivation
==========

When my family and I moved to town six years ago, it took us quite a while to
find a church home.  Though we tried plugging into a few places, sometimes for
months at a time, we never really felt like we fit.  One weekend, though, we
had what I called the most efficient church shopping experience ever.  After
doing some research throughout the week, we set off Sunday morning to visit a
new church.  We lasted maybe 20 minutes into the service when my wife and I
looked at each other, said, "Nah," and got up and left.  We got back in the
car, found the next closest church on the map that hadn't started yet, and
headed that way.  At stop number two, we didn't even make it into the sanctuary
because they wanted us to hand over the sleeping baby in our arms to their
childcare program.  No thanks; back in the car, and on down the road to the
next closest church.  Stop number three was the winning ticket, so in less than
an hour we'd visited three churches and found the place we would call home.
Mission accomplished.

Fast-forward about three months, and the pastor announces that he'll be taking
a pastorate back home so he and his family can better care for aging parents.
Makes sense, and I don't fault him for making the switch.  Sometimes, though,
it seems like the lead pastor is what's holding things together, and after his
departure things that hadn't been addressed but really needed to be start to
surface and things start to fall apart.  In this particular case we had a rough
interim period, a sketchy pastoral search process, a contentious congregational
meeting to hire a new pastor, a difficult time under new leadership, a request
from the elder board that the new pastor resign, and then the departure of both
the new guy and a disgruntled faction within the church.  You'd think, with all
that in mind, when asked if I would consider joining the elder board, I
would've responded with, "I'm out.  No---thank you for thinking of me."
Instead, being convinced the call to eldership was from God and not solely from
the church, I agreed, and then got to see "how the sausage is made," so to
speak.  Fast-forward some more, and after my wife and I confront some of the
church leadership on lying to the congregation, and those conversations ending
effectively in, "Well, I guess we'll just need to agree to disagree," we
realize that for us to continue following where God is leading us, we need to
leave the church we'd invested our lives in over a number of years.

That was rough, but in short order we found ourselves connected to a church
that seemed like it really had a culture of devoting itself to the study of the
scriptures and to discipleship.  Good deal---at that point we needed a place
that was rock-solid founded on truth.  However, on our second Sunday there, the
lead pastor who'd been there a couple decades gives his two weeks notice.  Now
at this point I start thinking we should be able to monetize our apparent skill
set here.  Want to get rid of your pastor?  Invite us to your church, we'll
start getting plugged in, and he'll be out the door in a few weeks to a few
months, guaranteed.  Tongue in cheek aside, this ushered in a rough interim
period, a sketchy pastoral search process, and a contentious congregational
meeting to hire a new pastor.  Déjà vu.  Seeing that this congregation didn't
have a biblical understanding of the church, of church leadership, or of church
discipline, we decided not to invest any more time there.

Switching away from our church family woes, in recent years I'd also been
feeling a need to seek out more intentional training in spiritual matters.
Being raised in the middle east, a good deal of my childhood was devoted to
learning how to defend the faith, particularly in conversation with all the
other world religions out there.  However, much of my adult life was spent
largely relying on that foundation that had been built up in childhood, not
necessarily developing it further.  I therefore started looking around to see
who or what kind of program I might train under.  I looked into a few
seminaries, but didn't think I was supposed to step away from software
engineering at the time.  I came across `The Institute of Public Theology`_,
which looks like a great program, but the price tag and time commitment were
more than I thought was reasonable for us, given the rest of life right now.
Eventually I found a program that looked like it fit the bill, and a few
friends and I signed up for their year-long training program.

.. _The Institute of Public Theology:  https://www.instituteofpublictheology.org/

Given our last two church experiences, I think I'm perhaps more perceptive now
than you're average Joe when it comes to seeing things start to go sideways in
a Christian context.  As our time in the program progressed, there started to
be concerns about how some of us were engaging with the materials (e.g., you're
being too negative in your critiques, etc.).  I suspected our small group
leader was undergoing some harsh treatment at the hands of the program's
leadership.  Eventually, not being able to ignore the problems any longer, I
sank an entire day into re-reading six months' worth of online forum
discussions to see if I could figure out what exactly the problem was and
where.  At the end of that, I reached out to the program leadership and said,
"Hey folks, I'm not sure exactly what's going on, but I suspect some of us have
been accidentally sinning against one another.  I might be wrong, but here's
all my research to support that.  Can we please get together and talk things
through?"  What should have been a simple matter of, "Sorry, all.  We made some
bad assumptions, didn't do our homework, and therefore made some false
accusations.  Can you please forgive us?" turned really ugly really quickly,
and in less than two weeks a friend and I were expelled from the program.

You run into three nasty church situations like this in three years' time, and
you have to start wondering, "Am I the problem?  Why would God put us through
all of this?  Wasn't the first go-around painful enough?  What on earth is
going on?"  On the surface it might seem like all the various problems we've
experienced are unrelated---a confluence of events such that this was simply an
unfortunate and unpleasant season in our lives---however, we eventually
realized that there is something that ties all the strands together:  a
conflict of worldviews.

.. raw:: html

   <p style="text-align:left;">
       <span style="float:right;">
           🕑 8 min.
       </span>
   </p>

What Is a Worldview?
====================

Before we unpack what I mean by a conflict of worldviews, we must first
understand what the term means.  It was first coined by the German philosopher
`Immanuel Kant`_ in his |critique_of_judgment|_ in 1790 as *Weltanschauung*
(literally *view of the world*).  While for Kant the term meant simply the
perception of the world you gained empirically, the term quickly grew to
encompass all the fundamental beliefs you hold that color how you view the
world.  Often you'll hear people use the analogy that your worldview is a pair
of glasses through which you see everything else---if the lenses are blue, then
everything will have a bluish hue, etc.  Though there are multiple ways to talk
about worldviews, there are three that I find most helpful.

.. _Immanuel Kant:  https://en.wikipedia.org/wiki/Immanuel_Kant
.. _critique_of_judgment:  https://www.gutenberg.org/files/48433/48433-h/48433-h.htm
.. |critique_of_judgment| replace::  *Critique of Judgment*

The Four Fundamental Questions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The first way to have a more concrete understanding of a worldview is to say it
answers these four fundamental questions:

1. Who am I?
2. Why am I here?
3. What's wrong with the world?
4. How can what's wrong be made right?

Everyone has an answer to these questions, whether or not they've thought
through them intentionally.  The biblical worldview answers these questions as
follows:

1. I am an individual (|PSA.139.13|) made in the image of God, the crowning
   glory of his creation (|GEN.1.26-GEN.1.31|).
2. I am here to glorify God (|1CO.10.31|) and enjoy him forever (|PSA.144.15|).
3. The problem with the world is I don’t do what I was created to do
   (|LEV.5.17|; |JAS.4.17|).
4. To address the problem, I can surrender my life to the lordship of Christ
   (|ROM.10.9|) and allow God to transform me more and more each day into his
   likeness (|2CO.3.18|).  Ultimately Christ will return to judge the world and
   make all things right again (|MAT.25.31-MAT.25.46|; |REV.21.1-REV.21.8|).

.. |PSA.139.13| replace:: :raw-html:`<a data-gb-link="PSA.139.13">Psalm 139:13</a>`
.. |GEN.1.26-GEN.1.31| replace:: :raw-html:`<a data-gb-link="GEN.1.26-GEN.1.31">Genesis 1:26&ndash;31</a>`
.. |1CO.10.31| replace:: :raw-html:`<a data-gb-link="1CO.10.31">1 Corinthians 10:31</a>`
.. |PSA.144.15| replace:: :raw-html:`<a data-gb-link="PSA.144.15">Psalm 144:15</a>`
.. |LEV.5.17| replace:: :raw-html:`<a data-gb-link="LEV.5.17">Leviticus 5:17</a>`
.. |JAS.4.17| replace:: :raw-html:`<a data-gb-link="JAS.4.17">James 4:17</a>`
.. |ROM.10.9| replace:: :raw-html:`<a data-gb-link="ROM.10.9">Romans 10:9</a>`
.. |2CO.3.18| replace:: :raw-html:`<a data-gb-link="2CO.3.18">2 Corinthians 3:18</a>`
.. |MAT.25.31-MAT.25.46| replace:: :raw-html:`<a data-gb-link="MAT.25.31-MAT.25.46">Matthew 25:31&ndash;46</a>`
.. |REV.21.1-REV.21.8| replace:: :raw-html:`<a data-gb-link="REV.21.1-REV.21.8">Revelation 21:1&ndash;8</a>`

A non-biblical worldview will have different answers to one or more of these
questions.

The Historical Meta-Narrative
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The second way to understand a worldview more fully is to look at the
historical meta-narrative it tells.  History tells a story, the main components
of which are:

1. Creation
2. Fall
3. Redemption
4. Consummation

The biblical worldview tells us:

1. God created all things, both seen and unseen (|COL.1.16|), created man and
   woman in his image as the crowning glory of his creation, and deemed it all
   very good (|GEN.1.26-GEN.1.31|).
2. Mankind ruined our perfect relationship with our creator by sinning against
   him (|GEN.3.6-GEN.3.8|).  This fall from grace forever impacted God’s
   perfect creation (|ROM.5.12-ROM.5.21|; |ROM.8.18-ROM.8.22|).
3. After promising redemption for millennia (|GEN.3.15|; |DEU.18.15|), God sent
   his son Jesus (|GAL.4.4|) to become fully human (|JHN.1.14|), to live a
   perfect, sinless life (|HEB.4.15|), to die a substitutionary death on the
   cross in our place (|2CO.5.21|), that his righteousness might be credited to
   us (|ROM.4.20-ROM.4.25|) and we might be reconciled to God
   (|COL.1.19-COL.1.20|) through faith in him (|EPH.2.8-EPH.2.9|).
4. God has appointed a day for Christ to come again to judge the living and the
   dead (|ACT.10.42|) and make all things right (|2PE.3.10-2PE.3.13|).

.. |COL.1.16| replace:: :raw-html:`<a data-gb-link="COL.1.16">Colossians 1:16</a>`
.. |GEN.3.6-GEN.3.8| replace:: :raw-html:`<a data-gb-link="GEN.3.6-GEN.3.8">Genesis 3:6&ndash;8</a>`
.. |ROM.5.12-ROM.5.21| replace:: :raw-html:`<a data-gb-link="ROM.5.12-ROM.5.21">Romans 5:12&ndash;21</a>`
.. |ROM.8.18-ROM.8.22| replace:: :raw-html:`<a data-gb-link="ROM.8.18-ROM.8.22">Romans 8:18&ndash;22</a>`
.. |GEN.3.15| replace:: :raw-html:`<a data-gb-link="GEN.3.15">Genesis 3:15</a>`
.. |DEU.18.15| replace:: :raw-html:`<a data-gb-link="DEU.18.15">Deuteronomy 18:15</a>`
.. |GAL.4.4| replace:: :raw-html:`<a data-gb-link="GAL.4.4">Galatians 4:4</a>`
.. |JHN.1.14| replace:: :raw-html:`<a data-gb-link="JHN.1.14">John 1:14</a>`
.. |HEB.4.15| replace:: :raw-html:`<a data-gb-link="HEB.4.15">Hebrews 4:15</a>`
.. |2CO.5.21| replace:: :raw-html:`<a data-gb-link="2CO.5.21">2 Corinthians 5:21</a>`
.. |ROM.4.20-ROM.4.25| replace:: :raw-html:`<a data-gb-link="ROM.4.20-ROM.4.25">Romans 4:20&ndash;25</a>`
.. |COL.1.19-COL.1.20| replace:: :raw-html:`<a data-gb-link="COL.1.19-COL.1.20">Colossians 1:19&ndash;20</a>`
.. |EPH.2.8-EPH.2.9| replace:: :raw-html:`<a data-gb-link="EPH.2.8-EPH.2.9">Ephesians 2:8&ndash;9</a>`
.. |ACT.10.42| replace:: :raw-html:`<a data-gb-link="ACT.10.42">Acts 10:42</a>`
.. |2PE.3.10-2PE.3.13| replace:: :raw-html:`<a data-gb-link="2PE.3.10-2PE.3.13">2 Peter 3:10&ndash;13</a>`

A non-biblical worldview will address the same four areas of the historical
meta-narrative, but there will be differences in one or more of the stages.

.. note::

   The historical meta-narrative and the four fundamental questions line up as
   follows:  Who am I and why am I here?  Creation.  What’s wrong with the
   world?  Fall.  How can what’s wrong be made right?  Redemption and
   consummation.

The Five-Fold Breakdown
~~~~~~~~~~~~~~~~~~~~~~~

The third and final way to think about worldviews that I find useful is to
break up the fundamental beliefs into a handful of categories.  People tend to
break it down in different ways, but one way that’s helpful to remember is
your worldview consists of your views on:

1. God
2. Man
3. Truth
4. Knowledge
5. Ethics

The biblical worldview specifies:

1. There is only one true God (|DEU.6.4|), eternally existent in the three
   persons of the Father, the Son, and the Holy Spirit (|MAT.28.19|).
2. God made man in his image as the crowning glory of his creation
   (|GEN.1.26-GEN.1.31|), and gave us the responsibility to exercise dominion
   over his created order (|GEN.2.15|).
3. Truth is absolute and unchanging (|PSA.119.160|), having its source in God
   himself (|JHN.14.6|).
4. We can know truth through what God revealed to man in the special revelation
   of the Bible (|JHN.17.17|), as well as in the general revelation of his
   creation (logic, math, science) (|ROM.1.20|; |PSA.19.1-PSA.19.4|;
   |ROM.2.14-ROM.2.15|).
5. Ethics are absolute and are based on the unchanging word and will of God
   (|EXO.20.1-EXO.20.17|; |MAT.5.17-MAT.5.18|).

.. |DEU.6.4| replace:: :raw-html:`<a data-gb-link="DEU.6.4">Deuteronomy 6:4</a>`
.. |MAT.28.19| replace:: :raw-html:`<a data-gb-link="MAT.28.19">Matthew 28:19</a>`
.. |GEN.2.15| replace:: :raw-html:`<a data-gb-link="GEN.2.15">Genesis 2:15</a>`
.. |PSA.119.160| replace:: :raw-html:`<a data-gb-link="PSA.119.160">Psalm 119:160</a>`
.. |JHN.14.6| replace:: :raw-html:`<a data-gb-link="JHN.14.6">John 14:6</a>`
.. |JHN.17.17| replace:: :raw-html:`<a data-gb-link="JHN.17.17">John 17:17</a>`
.. |ROM.1.20| replace:: :raw-html:`<a data-gb-link="ROM.1.20">Romans 1:20</a>`
.. |PSA.19.1-PSA.19.4| replace:: :raw-html:`<a data-gb-link="PSA.19.1-PSA.19.4">Psalm 19:1&ndash;4</a>`
.. |ROM.2.14-ROM.2.15| replace:: :raw-html:`<a data-gb-link="ROM.2.14-ROM.2.15">Romans 2:14&ndash;15</a>`
.. |EXO.20.1-EXO.20.17| replace:: :raw-html:`<a data-gb-link="EXO.20.1-EXO.20.17">Exodus 20:1&ndash;17</a>`
.. |MAT.5.17-MAT.5.18| replace:: :raw-html:`<a data-gb-link="MAT.5.17-MAT.5.18">Matthew 5:17&ndash;18</a>`

A non-biblical worldview can be broken down into the same five categories, but
will differ in what it specifies for one or more of them.

To Sum Up
~~~~~~~~~

There is a biblical worldview---that is, a worldview completely consistent with
revelation, reason, and reality---and there are numerous counterfeit
worldviews---those that disagree with the biblical one on one or more points.
I refer to them as *counterfeit* worldviews because, like a counterfeit bill,
they resemble the real thing, but are wrong in often subtle ways.  That which
they get right, they pilfer from scripture, and that which they get wrong is a
twisting of God's truth.

Note that in all I've said above I have appealed to the Bible as a source of
truth without first establishing it as such.  Examining the veracity of
scripture is a lengthy endeavor outside the scope of our current discussion, so
I'll leave looking into is as `an exercise to the reader`_.  However, to whet
your appetite it'll be sufficient to say, "I choose to believe the Bible
because it is a reliable collection of historical documents, written down by
eyewitnesses in the lifetime of other eyewitnesses; they report supernatural
events that took place in fulfillment of specific prophecies, and claim that
their writings are divine rather than human in origin."

.. _an exercise to the reader:  https://newmexicoforensicsclub.readthedocs.io/en/latest/training/worldview-analysis/the-bible.html

.. sidebar::  Note

   Though a person's worldview is formed by the time they enter adolescence,
   that doesn't mean it can't change over time.  Rather what's meant here is
   that if you're talking with someone who's a teenager or older, they already
   have a comprehensive view of the world that answers all of life's
   fundamental questions, whether they realize it or not.

Now one final note before we leave this section:  Often enough I've heard
people use the term "worldview" to mean something more along the lines of
`systematic theology`_---a complete, orderly account of all Christian doctrine
that believers will learn over the course of years of intensive study---but to
conflate the two terms is to misunderstand both concepts significantly.
Everyone has a worldview, whether or not they've studied theology, and whether
or not they can articulate it with any clarity.  The elements of your worldview
form quite early in life, with most of them (your fundamental beliefs about
God, man, truth, and knowledge) solidifying by age nine or so, and the final
element (ethics) `settling around age thirteen`_.  To learn to identify,
analyze, and refute counterfeit worldviews is not just an academic exercise for
the theological scholars---it's necessary for each and every one of us because
we're all influenced by various worldviews on a daily basis.

.. _systematic theology:  https://en.wikipedia.org/wiki/Systematic_theology
.. _settling around age thirteen:  https://www.oshmanodyssey.com/jensblog/n6pb5opplj5kaq12ihl4gahklx7oc1

Where Is the Conflict Then?
===========================

If you've been in the church for any significant amount of time, I'd guess as
you were reading through the last section you were thinking something along the
lines of, "Okay, this seems like Christianity 101.  What's the big deal?"  The
problem that arises is we say we hold to scripture, while at the same time
mentally assenting to multiple conflicting worldviews.  This inherent
contradiction is played out in what we think, say, and do, but without the
ability to identify, analyze, and refute counterfeit worldviews, we're often
unaware of it.

Let's unpack this by examining two concrete cases of worldviews that have been
making significant inroads into the church in recent decades.  First we'll take
a look at what happens when our emotions become the barometer for reality.
After that we'll see how the tyranny of the experts unfolds within the church.
Let's dive in.

.. raw:: html

   <p style="text-align:left;">
       <span style="float:right;">
           🕑 30 min.
       </span>
   </p>

Emotionalism
~~~~~~~~~~~~

The first worldview in play is *emotionalism*.  For a fuller treatment of this
subject, I highly recommend |mama_bear_apologetics|_, edited by `Hillary Morgan
Ferrer`_, but I'll try to do it justice in brief.  Starting around the Age of
Enlightenment back in the 17th century, there was a growing belief in the
philosophy of *naturalism*, which holds (among other things) that the only
things that are knowable are what we can observe with our senses.  As this view
grew in popularity, eventually authority and divine revelation were jettisoned
as sources of knowledge, which characterized the age of *modernism* in the 19th
century.  As we continued to whittle away at sources of knowledge, we
eventually wound up at *postmodernism* in the mid-20th century, where the only
truth is that there is no truth.  However, being created in the image of God,
and subject to his reality, humans naturally crave a source of truth---perhaps
a built-in longing for our creator---so even in our insistence that no such
source existed, we still needed one.

.. _mama_bear_apologetics:  https://mamabearapologetics.com/book/
.. |mama_bear_apologetics| replace::  *Mama Bear Apologetics*
.. _Hillary Morgan Ferrer:  https://mamabearapologetics.com/about/meet-the-mama-bears/

Well if we've given up on revelation, reason, and reality, what's left over
that can fill the void?  Cue the Disney montage and crank up
|true_to_your_heart|_ from the *Mulan* soundtrack, because the answer is your
emotions.  This worldview is often easy to see out in the wild.  For instance:

.. _true_to_your_heart:  https://www.youtube.com/watch?v=W6pAiwbFQ4s
.. |true_to_your_heart| replace::  *True to Your Heart*

* Following your heart---oftentimes contrary to your better judgement and the
  advice of those who have your best interests in mind---has been one of the
  dominant themes in children's literature and entertainment over the last
  half-century, and it always works because "they all lived happily ever
  after."
* Over the last two decades or so, the concepts of trigger warnings and safe
  spaces were incubated on college campuses and are now spreading through
  society at large, all in service of ensuring no one has a negative emotional
  response to anything.
* Recently `Matt Walsh`_ released a documentary entitled |what_is_a_woman|_, in
  which he travels the world seeking answers regarding gender and transgender
  issues.  At times it's downright comical as his interviewees are reticent to
  answer simple questions directly, instead relying on emotional justifications
  for their beliefs.

.. _Matt Walsh:  https://en.wikipedia.org/wiki/Matt_Walsh_(political_commentator)
.. _what_is_a_woman:  https://whatisawoman.com/
.. |what_is_a_woman| replace::  *What is a Woman?*

Though the examples above are fairly noticeable, emotionalism can also be much
more subtle, particularly as it's been invading the church.  For instance:

* We have a family that will be joining our small group, and the husband is one
  who was raised in the church and is now an unbeliever, and we want to make
  sure we conduct ourselves in a way that is loving and welcoming and not
  off-putting.
* We want to make sure we're seen as loving our neighbor, so we'll [insert
  something here] to ensure we're not creating a wedge between us and those we
  seek to minister to.
* The way you're critiquing this Christian organization's content is overly
  negative, and is hampering the ability of some in our group to engage with
  the material, so you need to change the way you engage with the information
  and communicate with your fellow participants.

What's Wrong with This?
^^^^^^^^^^^^^^^^^^^^^^^

In the prior three examples it might be significantly harder to discern where
the problems lie.  We know we're commanded to love our neighbor as ourselves
(|MRK.12.31|).  We know that people can't be saved without first hearing the
gospel from us (|ROM.10.14|), and we don't want to create a barrier such that
they can't hear it.  We don't want to cause people to stumble in their faith
(|LUK.17.2|).  It seems like in these examples we're simply being exhorted to
be good Christians---what's wrong with that?  To answer that question, we need
to realize that emotionalism is built on a number of false assumptions:  that
positive emotions are good and negative ones are bad, that something I feel
must be true, and that I'm responsible for the feelings of those I interact
with, among others.  Let's walk through each of these in turn.

.. |MRK.12.31| replace:: :raw-html:`<a data-gb-link="MRK.12.31">Mark 12:31</a>`
.. |ROM.10.14| replace:: :raw-html:`<a data-gb-link="ROM.10.14">Romans 10:14</a>`
.. |LUK.17.2| replace:: :raw-html:`<a data-gb-link="LUK.17.2">Luke 17:2</a>`

Positive Good, Negative Bad
+++++++++++++++++++++++++++

It's natural that we think emotions indicate whether the situation that caused
them was good or bad.  For instance, when we're celebrating the wedding of dear
friends, we're overjoyed, because their marriage is a good thing.
Alternatively, when we hear of the latest mass shooting, our hearts break,
because murder is a bad thing.  However, it's relatively easy to come up with
counterexamples to show that this relationship between emotions and ethics
doesn't hold universally.

Consider the case of a man viewing a woman naked.  He naturally has a positive
emotional response to this visual stimulus.  Does that mean the situation that
caused that response is good?  It depends on the broader context.  If the woman
in question is his wife, then yes, because that's how God designed it to work.
If she isn't, then no, the situation that caused the positive emotional
response is bad, because it's adultery, which God forbids (|EXO.20.14|).  These
two possibilities are an oversimplification, to be sure, but for our purposes
here we just need to realize that the goodness or badness of any scenario
depends on the specifics of the situation and not on the emotional responses in
play.

.. |EXO.20.14| replace:: :raw-html:`<a data-gb-link="EXO.20.14">Exodus 20:14</a>`

Consider also the scenario of saying something in a conversation that causes a
negative emotional reaction in someone.  Were you wrong to say what you said?
Again, it depends on what exactly is going on.  If you were hurling insults at
them, then yes, repentance and forgiveness are necessary, and hopefully the two
of you can be restored to fellowship in short order (|MAT.5.23-MAT.5.24|).
However, if you communicated truth to them and the Holy Spirit used that truth
to convict them of unrepentant sin in their life, which then caused the
negative emotions associated with guilt, then no, what you did was both right
and necessary; indeed, scripture requires it (|TIT.2.15|).

.. |MAT.5.23-MAT.5.24| replace:: :raw-html:`<a data-gb-link="MAT.5.23-MAT.5.24">Matthew 5:23&ndash;24</a>`
.. |TIT.2.15| replace:: :raw-html:`<a data-gb-link="TIT.2.15">Titus 2:15</a>`

At its core, this foundational assumption of emotionalism is just bad logic.
We know from experience that some good situations produce positive emotions,
and some bad situations produce negative ones, but then we flip that around and
say that positive emotions imply goodness and negative ones imply badness.  I'm
afraid that's just wrong.

Feeling = Truth
+++++++++++++++

This second assumption tends to manifest in two different ways.  In the first
case, consider a newly married couple.  It's their first time celebrating the
wife's birthday together after the wedding, and the husband has arranged for a
romantic meal at a fancy restaurant.  The food is superb, the dessert
top-notch, and yet when they return home for the evening, the husband can tell
something's wrong, but hasn't the slightest idea what.  Being a loving husband,
he tries to remedy the situation with flowers, notes, etc., but to no avail.
Fast-forward to the husband's birthday and the wife prepares a cake and
candles, has some small gifts to open, and invites some friends over.  "Aha!"
realizes the husband, now fully clued in.  "*This* is how you celebrate a
birthday!"  On the wife's birthday, the celebration had left her feeling
deflated and unloved, because all the regular trappings of a birthday were
missing.  However, did her husband stop loving her, on her birthday of all
days?  Not at all---it was simply a mismatch of how birthdays were celebrated
in their respective families.  The feelings present did not match the reality
of the situation.  (And yes, I've learned my lesson, and there have been
flowers, cake, presents, etc., ever since.)

The second case is a little harder to parse through.  If you've ever been
through any instruction on communicating in the midst of conflict, chances are
you've heard it can be helpful to the conversation to make statements about
your feelings rather than to make statements of fact.  For instance, "I feel
like you're being unloving," rather than simply, "You're being unloving."  No
one can argue with your feelings, or so the saying goes.  While there's some
truth to this statement, unfortunately it has been extended well outside its
natural boundaries as it's permeated modern culture.

If you are simply expressing the emotions you're feeling, you're making a truth
claim about your emotional state (e.g., "I'm angry right now").  Someone could
try to argue that you're lying about your feelings in the moment, but in
general we can take it on faith that someone is being truthful in relaying
their emotions to us.  And doing so can indeed be helpful to the conversation
so others aren't left guessing and then operating on assumptions.

However, more often than not people tend to use the phrase "I feel" as a
substitute for phrases like "I suspect," "my gut tells me," or ultimately "I
believe."  Rather than making a truth claim about the speaker's emotional
state (which can't practically be falsified), they're actually making a truth
claim about some aspect of reality (which can).  They're couching their
statements in the vocabulary of emotions, though, which has the impact of
making them unassailable.  The problem, then, is that when making a truth claim
about reality, you should be able to support it `with a well-reasoned
argument`_ (where "argument" refers to the rhetorical device, as opposed to the
modern redefinition of the term to mean "quarrel").  Even in the cases when you
think it's just your gut talking, that's actually your brain processing
information faster than your consciousness can keep up (see, e.g., `Blink`_, by
`Malcolm Gladwell`_), and you should be able to think through the argument
after the fact.  If you can't, your truth claim has no justification, and
there's really no reason for someone to listen to it.

.. _with a well-reasoned argument:  https://newmexicoforensicsclub.readthedocs.io/en/latest/training/crafting-an-argument.html
.. _Blink:  https://www.gladwellbooks.com/titles/malcolm-gladwell/blink/9780316172325/
.. _Malcolm Gladwell:  https://www.gladwellbooks.com/landing-page/about-malcolm-gladwell/

This assumption is a pernicious one, again because we have first-hand
experience of instances in which our emotions do happen to line up with
reality; that is, they *can* indeed clue us in to the truth of the matter.
However, to take the (understandably large) subset of instances in which
emotions and truth align, and then to say that what I feel in *any* situation
*must* be true, is a `logical fallacy`_.  On the one hand your emotions may not
line up with reality, and on the other you may be using emotional language to
hide your assertions about reality from scrutiny.  Either way the assumption
falls flat.

.. _logical fallacy:  https://newmexicoforensicsclub.readthedocs.io/en/latest/training/logical-fallacies.html

I'm Responsible for Your Feelings
+++++++++++++++++++++++++++++++++

This final assumption we'll analyze under emotionalism is a case of taking a
good idea---don't be a jerk---and extending it far beyond the bounds of reason.
If you're mean, rude, abusive, etc., your actions will cause negative emotions
in others, and that's a bad thing that should be avoided.  What should be
avoided, exactly?  Being mean, rude, abusive, etc.; however, we've been trained
to think the answer is causing negative emotions in others, and when you take
that to its logical conclusion, it means you need to conduct yourself in such a
way as to never cause a negative emotional response in anyone you interact
with, ever.

To an extent, this may simply be a lack of maturity playing itself out in
society.  I don't like feeling sad, upset, depressed, hurt, etc., and when I do
I have two options:  I can take those thoughts captive and make them obedient
to Christ (|2CO.10.5|), and then deal with them appropriately, or I could blame
you, take no responsibility whatsoever, and demand that you and everyone else
on the planet never hurt me in such a way ever again.  Keep in mind that life
will throw all sorts of circumstances your way.  You're not responsible for
what life throws at you, but you absolutely are responsible for how you
respond.  |boundaries|_, by `Henry Cloud`_ and `John Townsend`_, is a great
resource in this arena.

.. |2CO.10.5| replace:: :raw-html:`<a data-gb-link="2CO.10.5">2 Corinthians 10:5</a>`
.. _boundaries:  https://smile.amazon.com/Boundaries-Updated-Expanded-When-Control/dp/0310351804/ref=sr_1_1?crid=3GLIV8L2Q463T&keywords=boundaries&qid=1657714011&sprefix=boundaries%2Caps%2C278&sr=8-1
.. |boundaries| replace::  *Boundaries*
.. _Henry Cloud:  https://en.wikipedia.org/wiki/Henry_Cloud
.. _John Townsend:  https://en.wikipedia.org/wiki/John_Townsend_(author)

I suspect something else that's been hampering the church in particular in this
area (at least in the western world) is our relatively recent poor performance
in terms of number of souls saved vs those abandoning their faith, which forces
us to ask the question, "What are we doing wrong?"  It seems we've gravitated
toward the answer---I would argue because we're far more influenced by the
cultural milieu than we'd ever care to admit---that it's the way in which you
speak that determines whether or not your words will be received well.  If only
we could speak in such a way as to be perceived as loving, unifying, humble,
encouraging (though we'll define these terms as the culture does, not as
scripture does), then we'll be able to have enough relational capital to win
our neighbors to Christ and prevent our children from abandoning the faith as
soon as they leave home.  Remember that you're responsible for delivering the
truth, not for how someone responds to it.

Counterfeit Worldview
+++++++++++++++++++++

Now that we've walked through some of the faulty assumptions underpinning
emotionalism, let's take a step back to see how it's a counterfeit worldview in
conflict with the biblical one.  The contrast is easiest to see if you recall
the latter three parts of the five-fold breakdown from earlier:

3. **Truth:**  Truth has you as its source, and is therefore fluid, both in
   time, and from person to person.
4. **Knowledge:**  We can know truth through what we happen to be feeling in
   the moment.
5. **Ethics:**  Right and wrong are determined by whether actions cause
   positive or negative emotions.

Though it's easiest to see in these three areas, these fundamental beliefs have
implications across the board.  In terms of the four fundamental questions:

1. **Who am I?**  My identity is determined by whatever I happen to be feeling
   in the moment.  This may be in accord with the biblical worldview, but I
   might feel like I'm attracted to members of the same sex, that I'm a member
   of the opposite sex stuck in the wrong body, that I'm actually a `non-human
   animal inhabiting a human body`_, etc.  My identity is effectively only
   limited by my imagination.
2. **Why am I here?**  To pursue whatever activities lead to positive emotional
   responses.  This usually only means in the short-term, though---if there are
   any long-term negative emotional consequences to my actions, I'll just
   pursue more activities that yield short-term highs to distract from that.
3. **What's wrong with the world?**  Definitely all the things everyone else is
   doing that result in me experiencing negative emotions.  Definitely not
   anything I do that does the same, and if anyone tries to point out such a
   scenario, them pointing it out is what causes my negative emotional
   reaction, so they're in the wrong.
4. **How can what's wrong be made right?**  Society must be restructured such
   that no one hurts anyone else, even accidentally, ever again.  While we're
   at it, we may as well throw in free unicorns for everyone too.

.. _non-human animal inhabiting a human body:  https://en.wikipedia.org/wiki/Therianthropy#Modern_therianthropy

In terms of the historical meta-narrative:

1. **Creation:**  Everything came into being however I feel like it did.  I
   might feel like a young-earth creationist or a big bang naturalistic
   materialist---either way's fine, or I can pick something else that makes me
   feel good.
2. **Fall:**  Things went wrong when humans started experiencing negative
   emotions.  They're uncomfortable and often hurt, and ruin the paradise of
   positive emotions we inhabited at some indeterminate point in the past.
3. **Redemption:**  We can save ourselves by ensuring we never experience
   things like hurt, anger, rejection, depression, etc., ever again.
   Theoretically that should be possible by rearchitecting society, but in the
   meantime we'll just do whatever we can to deaden our senses.
4. **Consummation:**  One day we'll get to a place where everyone's happy and
   no one's ever upset.  No telling how we'll get there, but oh well
   ¯\\_(ツ)_/¯.

Then we can return to the five-fold breakdown, the last three parts of which we
started with up above.  To finish it out:

1. **God:**  Whatever conception of God makes you feel most at peace is good
   for you.  This tends to be what's been characterized as `moralistic
   therapeutic deism`_.
2. **Man:**  I'll define myself however I feel like it, thank you very much.
   See the first two fundamental questions up above.

.. _moralistic therapeutic deism:  https://www.gotquestions.org/Moralistic-Therapeutic-Deism.html

How Does This Play Out?
^^^^^^^^^^^^^^^^^^^^^^^

With all of this in mind, let's see if we can discern where emotionalism might
be in play in the three more subtle example scenarios from earlier.

When Love Silences You
++++++++++++++++++++++

  We have a family that will be joining our small group, and the husband is one
  who was raised in the church and is now an unbeliever, and we want to make
  sure we conduct ourselves in a way that is loving and welcoming and not
  off-putting.

Many ideas you'll run into in life are neither all good nor all bad.  Rather
than simply accept them at face value, though, you can cultivate a posture of
discernment by approaching ideas with the following questions:

* What ideas here are good or bad?
* Why are they good or bad?
* Where do these good or bad ideas come from?
* What purposes do these good or bad ideas serve?

Let's try to ask some of these questions of the current scenario.

In terms of what's good, we know that treating an unbeliever with the love and
respect due to another made in the image of God is a good thing.  Scripture
tells us to love our neighbors as ourselves (|MAT.22.39|) and to be welcoming
(|MAT.18.5|).  We know such admonitions are good because they come from God and
are meant for our flourishing, both individually and in community.  We can
reason that if we were, for instance, to conduct the small group in such a way
that it seemed like the purpose was to save the unbeliever, treating him as a
sort of project, that likely wouldn't go over well.  Shouldn't we instead just
let our light shine before others (|MAT.5.16|) and see how that plays out?

.. |MAT.22.39| replace:: :raw-html:`<a data-gb-link="MAT.22.39">Matthew 22:39</a>`
.. |MAT.18.5| replace:: :raw-html:`<a data-gb-link="MAT.18.5">Matthew 18:5</a>`
.. |MAT.5.16| replace:: :raw-html:`<a data-gb-link="MAT.5.16">Matthew 5:16</a>`

It depends on what exactly you mean by "letting your light shine."  If you mean
regularly pointing people to the truth of God's word, such that it can set them
free from our slavery to sin (|JHN.8.31-JHN.8.36|), then absolutely---go for
it.  If, on the other hand, you mean just generally living as "decent" human
beings---you know, a step or two above serial killers---in the hopes that one
day they'll have an epiphany and realize there's something fundamental they've
been missing that no one's ever told them about, then no, chances are that
won't be terribly effective.  We tend toward the latter understanding, though,
due to tacitly believing a false assumption that to be loving means to not tell
someone what they're doing is wrong.

.. |JHN.8.31-JHN.8.36| replace:: :raw-html:`<a data-gb-link="JHN.8.31-JHN.8.36">John 8:31&ndash;36</a>`

I distinctly remember one evening where this influence of emotionalism was in
full force, though I wouldn't realize what was going on until years later.  Our
small group went through a season in which the couples shared their backstories
with each other.  This was a time in which we were more vulnerable with each
other than ever before.  When this particular couple shared their story of
faith, marriage, and then the husband abandoning his faith afterward, one of
the comments he made was that in all of their journey he hadn't abandoned his
marital vows.  In that moment, I was instantly on high alert as my mind
thought, "That ain't right."  I waited to see if anyone else would raise a
concern, but instead it was all hugs, thanks, "we're here for you," etc.  To my
shame, I didn't say anything.

.. sidebar::  Book Recommendation

   For a fuller treatment of these roles the husband takes on in marriage, see
   |what_he_must_be|_, by `Voddie Baucham Jr`_.

.. _what_he_must_be:  https://smile.amazon.com/What-He-Must-Be-Daughter/dp/1581349300/ref=sr_1_1?crid=18YCH80ANBILW&keywords=what+he+must+be+baucham&qid=1660603156&sprefix=what+he+must+be+baucham%2Caps%2C227&sr=8-1
.. |what_he_must_be| replace::  *What He Must Be... If He Wants to Marry My Daughter*
.. _Voddie Baucham Jr:  https://www.voddiebaucham.org/about/

What was I so concerned about, though?  When we got home, I brought the issue
up to my wife.  Fidelity to the marital covenant is more than just making sure
you don't cheat on your wife.  When a man marries a woman, he's agreeing to
take on the roles of provider, protector, prophet, and priest, both for his
wife and for any children God may bless them with.  Provider and protector are
easy enough for everyone to understand:  bring home bacon, and stand between
your family and those who would do them harm.  Prophet and priest sound
stranger to our modern ears, though.  Prophet means representing God to your
family (speaking God's truths to them), and priest means representing your
family to God (lifting them up in prayer).  When my friend abandoned his faith,
he abdicated these latter two roles.  When he shared his story, the Holy Spirit
tried to speak truth through me, but then and in the days, weeks, and months
that followed, my desire to love my friend kept me silent.

When Love Makes You Compromise
++++++++++++++++++++++++++++++

  We want to make sure we're seen as loving our neighbor, so we'll [insert
  something here] to ensure we're not creating a wedge between us and those we
  seek to minister to.

As we saw up above, we know that loving our neighbor is a good thing, because
it's the second part of the greatest commandment (|MAT.22.37-MAT.22.40|).  We
know we're to be the salt of the earth (|MAT.5.13|), and that salt only does
any good when it comes into contact with food, so we don't want to be
separating ourselves from those we're supposed to be seasoning.  Where does
this thought go wrong, then?  It depends on what kind of bounds we put on it
(if any).  For instance, in our attempts to ensure our neighbors perceive us as
loving them, do we wind up compromising truth?  As we seek to ensure we can
continue ministering to the lost, do we wind up disobeying other commands from
God in the process?

.. |MAT.22.37-MAT.22.40| replace:: :raw-html:`<a data-gb-link="MAT.22.37-MAT.22.40">Matthew 22:37&ndash;40</a>`
.. |MAT.5.13| replace:: :raw-html:`<a data-gb-link="MAT.5.13">Matthew 5:13</a>`

This scenario is perhaps easier to work through if we make it more concrete, so
let's talk about how many churches responded to the COVID-19 pandemic.  If you
think back to March of 2020, one of the commmon reasons cited for shuttering
churches was that we were doing it to show our love for our neighbors.  If you
tried to push on this to determine what exactly was meant, you might've been
answered with something along the lines of, "We don't want to take the chance
that us staying open is what causes someone to contract COVID and die."  What
motivates such a response, though?  If we do stay open, and someone does
contract the virus, and they do die, what happens?  Negative emotions---either
in us, because we feel guilty, or in those close to the deceased, because
they're mourning the loss of a loved one, or both.

Hang on, though:  Why are we worried about believers passing away, when we know
that what awaits us after death is infinitely better than anything we could
possibly have in this life (|PHP.1.21-PHP.1.23|)?  People will still be sad,
sure, but that doesn't mean death is a bad thing for us.  Maybe you're worried
about not being ready to die yet, but if that's the case, what do you need to
do to get ready?  Life is fleeting (|PSA.39.5|, |JAS.4.14|), and we never know
which day will be our last.  Are you living accordingly?

.. |PHP.1.21-PHP.1.23| replace:: :raw-html:`<a data-gb-link="PHP.1.21-PHP.1.23">Philippians 1:21&ndash;23</a>`
.. |PSA.39.5| replace:: :raw-html:`<a data-gb-link="PSA.39.5">Psalm 39:5</a>`
.. |JAS.4.14| replace:: :raw-html:`<a data-gb-link="JAS.4.14">James 4:14</a>`

Perhaps then church leaders clarified that, no, we're concerned for the
:raw-html:`<i>un</i>believer` who might die as a result of our actions.  In
that case there really is a negative outcome, because the end result is an
eternity spent in hell.  Hang on, though:  Who's responsible for someone coming
to faith?  Is it me, or is it God (|EPH.1.4-EPH.1.6|, |EPH.2.8-EPH.2.9|,
|2TI.1.9|)?  Is God indeed sovereign over all his creation and the affairs of
man (|PSA.115.3|, |PRO.19.21|, |JER.32.17|, |LAM.3.37-LAM.3.39|), or did
someone die with COVID and God's response was, "Darn it, I meant to save that
one"?  If our actions were intended to prolong the lives of unbelievers that
they might be saved, did we then do anything to ensure they came in contact
with the truth that has the power to save them, or did we just go about living
as "loving" examples in the hopes that they'd eventually get a clue?

.. |EPH.1.4-EPH.1.6| replace:: :raw-html:`<a data-gb-link="EPH.1.4-EPH.1.6">Ephesians 1:4&ndash;6</a>`
.. |2TI.1.9| replace:: :raw-html:`<a data-gb-link="2TI.1.9">2 Timothy 1:9</a>`
.. |PSA.115.3| replace:: :raw-html:`<a data-gb-link="PSA.115.3">Psalm 115:3</a>`
.. |PRO.19.21| replace:: :raw-html:`<a data-gb-link="PRO.19.21">Proverbs 19:21</a>`
.. |JER.32.17| replace:: :raw-html:`<a data-gb-link="JER.32.17">Jeremiah 32:17</a>`
.. |LAM.3.37-LAM.3.39| replace:: :raw-html:`<a data-gb-link="LAM.3.37-LAM.3.39">Lamentations 3:37&ndash;39</a>`

Okay, no, we understand that the death of a believer means, "Go directly to
heaven; do not pass 'Go'; do not collect $200," and that God is ultimately in
charge of when we kick the bucket.  Rather, we're closing down for a time
because this is how we lovingly submit to our governing authorities
(|ROM.13.1-ROM.13.7|).  First question:  What if you're being told to disobey
God's commands (|ACT.5.29|), like to not neglect gathering together
(|HEB.10.25|)?  Second question:  What authorities are we supposed to submit
to, exactly?

.. |ROM.13.1-ROM.13.7| replace:: :raw-html:`<a data-gb-link="ROM.13.1-ROM.13.7">Romans 13:1&ndash;7</a>`
.. |ACT.5.29| replace:: :raw-html:`<a data-gb-link="ACT.5.29">Acts 5:29</a>`
.. |HEB.10.25| replace:: :raw-html:`<a data-gb-link="HEB.10.25">Hebrews 10:25</a>`

.. sidebar::  Book Recommendation

   For more details on the appropriate relationship between church and state,
   and on the history of the development of Christian political thought, grab a
   copy of `Glenn Sunshine`_'s |slaying_leviathan|_---one of the best books
   I've read in the past year.

   Note that if I were still living in another country with a different form of
   government, I would not be making this argument.  Instead, I would be living
   my life in submission both to Christ and those governing authorities,
   praying and working for, and looking forward to the day in which God's truth
   would lead to a proper relationship between earthly sovereigns and subjects.

.. _Glenn Sunshine:  https://www.esquareinch.com/about/
.. _slaying_leviathan:  https://smile.amazon.com/Slaying-Leviathan-Government-Resistance-Christian/dp/195241072X/ref=sr_1_1?crid=1W0FQNKDJZBNA&keywords=slaying+leviathan&qid=1660679041&sprefix=slaying+leviathan%2Caps%2C178&sr=8-1
.. |slaying_leviathan| replace::  *Slaying Leviathan*

This latter question deserves some examination, real quick, as it's something
that often gets confused when people are throwing Romans 13 around.  When Paul
was writing his letter to the Romans, he was doing so in the midst of `the
Roman Empire`_, in which the highest authority in the land was the emperor.
Some authority was delegated to those under the emperor for the execution of
certain offices (e.g., within the military or various provincial governments),
but ultimately this period of ancient Roman history was characterized by a
government that became ever more monarchical.  In contrast, the United States
is what's known as a constitutional `republic`_, in which the highest authority
in the land isn't a person, and isn't an office, but instead is a document:
the `United States Constitution`_.  Certain authority is delegated to various
elected representatives for the execution of particular offices, but when an
individual in one of those offices seeks to appropriate for himself authority
that has not been delegated to him by the constitution, he's operating outside
the bounds of his roles both as a government official and as a minister of God
(|ROM.13.4|).  When that's the case, you're still required to submit to the
governing authority, but that's the constitution, which the particular official
has run afoul of.  When presidents, governors, and mayors start behaving like
little tyrants, submitting to their whims, instead of holding them accountable
both to the foundational documents that delineate their authority and
responsibilities, and to the God who they ultimately serve, is perhaps one of
the most unloving responses we could have.

.. _the Roman Empire:  https://en.wikipedia.org/wiki/Constitution_of_the_Roman_Empire
.. _republic:  https://en.wikipedia.org/wiki/Republic
.. _United States Constitution:  https://www.gutenberg.org/files/5/5-h/5-h.htm
.. |ROM.13.4| replace:: :raw-html:`<a data-gb-link="ROM.13.4">Romans 13:4</a>`

While we must love our neighbor, we must do so while both loving God
(|DEU.6.5|) and obeying all that he's commanded us (|JHN.14.15|).  Our actions
must be in accordance with all of scripture.  Compromise isn't an option.
Unfortunately in this instance many believers allowed fear (of death, of being
seen as doing the wrong thing, of making the wrong decision, etc.) and other
concerns to trump revelation, reason, and reality.

.. |DEU.6.5| replace:: :raw-html:`<a data-gb-link="DEU.6.5">Deuteronomy 6:5</a>`
.. |JHN.14.15| replace:: :raw-html:`<a data-gb-link="JHN.14.15">John 14:15</a>`

.. note::

   You may argue that churches closed down not based on emotional reasoning,
   but based on a sound examination of the scientific evidence available at the
   time.  In practice, I didn't see this---and I was very much on the lookout
   for it---though I'll grant that it might've happened somewhere.  What I saw
   instead was a good deal of appealing to experts without first examining
   their authority or the veracity of their claims, which we'll return to later
   on in this piece.

When Love Demands You Stop Thinking
+++++++++++++++++++++++++++++++++++

  The way you're critiquing this Christian organization's content is overly
  negative, and is hampering the ability of some in our group to engage with
  the material, so you need to change the way you engage with the information
  and communicate with your fellow participants.

First some backstory on this one.  Some of the study materials for that
year-long training program I mentioned in the introduction included the `Bible
Project videos`_, which are short, well-animated, informational videos that
help you understand that the Bible is a unified story that leads people to
Jesus.  In terms of what they attempt to achieve, they are both excellent and
unparalleled; however, problems became apparent very early on in the series.
In some cases the concerns raised were along the lines of, "I don't think I
would've said it quite that way."  In others, "I disagree with your
interpretation, but I understand how you got there."  In still others, "This is
just plain wrong.  The Bible says one thing, the video says another, and the
two do not agree."  We were eventually counseled by the program's leadership to
take what was good from the videos and comment on the positive for the
edification of others, but to keep our concerns to ourselves, as they were
negatively impacting some members of the group.

.. _Bible Project videos:  https://bibleproject.com/explore/

What's good here?  You don't want to be a Debbie Downer all the time---it's not
good for you or for those you're around.  A joyful heart is good medicine
(|PRO.17.22|).  It's good to focus on the good (|PHP.4.8|).  Uplifting speech
is a good thing, but negative talk is *no bueno* (|PRO.10.32|, |PRO.15.4|,
|EPH.4.29|).  We shouldn't judge others (|MAT.7.2|, |LUK.6.37|).  You don't
want to be around divisive people (|TIT.3.10|, |1CO.15.33|).  We shouldn't be a
stumbling block to others (|LEV.19.14|, |ROM.14.13|).  It seems like there's a
pretty solid scriptural basis for the exhortation to chill out and focus on the
good.

.. |PRO.17.22| replace:: :raw-html:`<a data-gb-link="PRO.17.22">Proverbs 17:22</a>`
.. |PHP.4.8| replace:: :raw-html:`<a data-gb-link="PHP.4.8">Philippians 4:8</a>`
.. |PRO.10.32| replace:: :raw-html:`<a data-gb-link="PRO.10.32">Proverbs 10:32</a>`
.. |PRO.15.4| replace:: :raw-html:`<a data-gb-link="PRO.15.4">Proverbs 15:4</a>`
.. |EPH.4.29| replace:: :raw-html:`<a data-gb-link="EPH.4.29">Ephesians 4:29</a>`
.. |MAT.7.2| replace:: :raw-html:`<a data-gb-link="MAT.7.2">Matthew 7:2</a>`
.. |LUK.6.37| replace:: :raw-html:`<a data-gb-link="LUK.6.37">Luke 6:37</a>`
.. |TIT.3.10| replace:: :raw-html:`<a data-gb-link="TIT.3.10">Titus 3:10</a>`
.. |1CO.15.33| replace:: :raw-html:`<a data-gb-link="1CO.15.33">1 Corinthians 15:33</a>`
.. |LEV.19.14| replace:: :raw-html:`<a data-gb-link="LEV.19.14">Leviticus 19:14</a>`
.. |ROM.14.13| replace:: :raw-html:`<a data-gb-link="ROM.14.13">Romans 14:13</a>`

What's wrong here?  Unfortunately it's that the scriptures above are either
being misinterpreted or misapplied to the current situation, or both.

* |PRO.17.22|:  While it's good to be cheerful, thinking critically about a
  teaching isn't indicative of a spirit of anxiety that weighs you down.  If
  someone else's critical thinking gets you down, that's something that we
  should work through.
* |PHP.4.8|:  Focusing on the good is worthwhile, but I'd think trying to
  ensure Christian teaching adheres to the word of God is one of the honorable
  and commendable things that fits into that bucket.
* |PRO.10.32|, |PRO.15.4|, |EPH.4.29|:  These verses don't warn against
  critiquing other people's ideas, but rather against speech that is
  unwholesome, wicked, perverse, rotten, or worthless.
* |MAT.7.2|, |LUK.6.37|:  These passages aren't saying not to judge, as Jesus
  goes on to call out the Pharisees for being blind, and says you can determine
  people to be either good or bad based on the fruit their lives produce.
  Rather, he's saying the measure we use when judging others is the measure
  that God will use when judging us, so it's a call to forgiveness and
  sacrificial love.  But in order to forgive, there first needs to be something
  that requires forgiveness, which means some action has been deemed wrong.
* |TIT.3.10|, |1CO.15.33|:  Paul's letter to Titus is an exhortation to sound
  doctrine and the godly living that accords with it.  The divisive people in
  this verse are those Paul has been warning Titus about throughout the
  letter---rebellious and deceptive false teachers and charlatans---not people
  questioning the biblical accuracy of a presentation.  The bad company in
  Paul's letter to the Corinthians is referring to those who would tempt you to
  sin, not to ensure your teaching is sound.
* |LEV.19.14|, |ROM.14.13|:  The first verse here is admonishing you to not
  take advantage of the disabled, or, more generally, to not be a jerk.  In the
  second verse, Paul is talking about the liberty of conscience we have in
  matters of differing personal conviction.  Neither verse is making the
  general statement that you must avoid doing something that has the potential
  to upset somebody.

To sum up, when someone's being critical, exhorting them to not be a jerk is
just fine.  Beyond that, though, there are some questions you should ask to get
to the heart of the matter:  Are the criticisms mean-spirited and rude, or are
they simply pointing out where something doesn't seem to align with scripture?
Are you uncomfortable with the critique because of the content, tone, or way in
which it was put forth, or just because you're uncomfortable with any kind of
confrontation?  Has the person leveling the criticism actually sinned, or is
someone just offended that their leadership has been called into question?  By
all means think critically about your critical thinking, but by no means stop
thinking critically.

What are the Consequences?
^^^^^^^^^^^^^^^^^^^^^^^^^^

Emotionalism is a worldview that is inconsistent with how God designed the
world.  When your thoughts, speech, and actions don't mesh with reality, that
causes problems.  Other than the specific instances we've looked into above,
what sort of problems manifest when this worldview infiltrates the church?

.. sidebar::  Definition

   *Epistemology* is just a fancy word for beliefs about what we can know and
   how we can know it.

On the most basic level, our thinking is simply conflicted and erratic.  We
have one epistemology that applies to spiritual or religious matters (however
you'd care to define those terms), and another one that applies everywhere
else.  `Nancy Pearcey`_ does a fantastic job in |total_truth|_ chronicling
where this sacred/secular divide came from, and what havoc it's wrought both on
the church and on society at large.  The problem is worse than this, though, as
we hold to the biblical worldview only when discussing matters of doctrine or
theology within our walls, but then when those beliefs are to be lived out,
even within the church, a different and incompatible ethic informs our actions.
We've been so effectively discipled in this way of thinking that we don't even
realize the disconnect.  What does this mean practically?  I've typically seen
it work itself out in two ways.

.. _Nancy Pearcey:  http://www.nancypearcey.com/about.html
.. _total_truth:  http://www.nancypearcey.com/total-truth.html
.. |total_truth| replace::  *Total Truth*

In the first case we wind up stifling the Holy Spirit's attempts to speak
through us.  We think, "Gosh, I really feel like I should say something right
now, but if I do the other person might take it the wrong way, it might come
across as sounding harsh, it might push them further away from the faith, I
might be seen as unloving and judgmental, etc."  This we must not do
(|1TH.5.19|).

.. |1TH.5.19| replace:: :raw-html:`<a data-gb-link="1TH.5.19">1 Thessalonians 5:19</a>`

Consider the various interactions Jesus had with the religious elite of his
day.  At one time or another people took things the wrong way, he was
(appropriately) harsh, people wound up walking away, and he would definitely
have been considered unloving and judgmental by today's standards.  We assume
these are bad things, and yet Jesus, who is God---the source and definition of
love, goodness, rightness, etc.---did them.  It's completely reasonable for us
to approach such passages of scripture and say, "Hang on, this looks wrong to
me, but I know it must be right, so Lord help me to understand how it is."  In
practice, though, our actions say, "Lord, I think you were wrong."  How's that
going to go over when you stand before him in judgment?

"Well hang on a minute," you say.  "That's Jesus we're talking about!  Who am I
to do something like that?"  Let me get this straight:  We're called to imitate
Christ (|1CO.11.1|), and are to be sanctified (|JHN.17.17|) more and more into
his likeness (|1TH.4.1-1TH.4.8|), but you don't want me to do what Jesus did
because he's God and I'm not?  What are the boundaries of your assertion?

.. |1CO.11.1| replace:: :raw-html:`<a data-gb-link="1CO.11.1">1 Corinthians 11:1</a>`
.. |1TH.4.1-1TH.4.8| replace:: :raw-html:`<a data-gb-link="1TH.4.1-1TH.4.8">1 Thessalonians 4:1&ndash;8</a>`

On top of the fact that our thinking here is nonsensical, we also deprive the
body of believers from hearing God speak through us.  Though we all
intellectually assent to the fact that God can communicate to his church
through the leading of the Holy Spirit in believers' lives, more often than not
we do a poor job corporately discerning what he's trying to say to us.  If you
wish to improve in this arena, |pursuing_gods_will_together|_, by `Ruth Haley
Barton`_, is a good resource with practices to integrate into the life of your
community to increase the likelihood that you'll discern God's will accurately,
and decrease your chances of accidentally quenching the Holy Spirit.

.. _pursuing_gods_will_together:  https://www.amazon.com/Pursuing-Gods-Will-Together-Transforming/dp/0830835660/ref=asap_bc?ie=UTF8
.. |pursuing_gods_will_together| replace::  *Pursuing God's Will Together*
.. _Ruth Haley Barton:  https://www.ruthhaleybarton.com/

The second way emotionalism works itself out is actually an extension of the
first.  Since we've made it a habit to ignore the Holy Spirit when he prompts
us to say something, we abandon our responsibilities both to exhort fellow
believers to more Christ-like behavior, and to rebuke those in unrepentant sin
(|2TI.4.2|).  For those who are actually bold enough to speak up, you often run
into the "nice police" trying to silence you so no one gets hurt or is made
uncomfortable.  If that happens to you---keeping yourself open to correction,
and making sure you conduct yourself in a manner that is above reproach
(|1TI.3.2|)---humbly and politely ask them what exactly you did wrong, when and
where, why they believe it was wrong, and what they think an appropriate action
in its place would have been (basically a more detailed version of
|JHN.18.23|).  In practice no one has ever answered these questions for me, and
like the officials questioning Jesus, my accusers stand condemned by their
silence.

.. |2TI.4.2| replace:: :raw-html:`<a data-gb-link="2TI.4.2">2 Timothy 4:2</a>`
.. |1TI.3.2| replace:: :raw-html:`<a data-gb-link="1TI.3.2">1 Timothy 3:2</a>`
.. |JHN.18.23| replace:: :raw-html:`<a data-gb-link="JHN.18.23">John 18:23</a>`

The end result of all of this is we lovingly strong-arm the church into an
artificial unanimity so we can present our best face to the world---a world
that desperately needs the truth we have but won't share for fear of offending
someone.  `Greg Koukl`_ contends in |tactics|_---another one of the best books
I've read in the past year---that we must combat such a fabricated unity
through well-reasoned argumentation in our pursuit of knowledge.  What is it
that changes lives?  Living lovingly as good examples, or truth convicting one
of sin and of the need for a savior and sanctification?  Again, don't be a
jerk, but truth is absolutely essential, and Satan is doing everything he can
to suppress it with this counterfeit worldview.

.. _Greg Koukl:  https://www.str.org/greg-koukl
.. _tactics:  https://smile.amazon.com/Tactics-10th-Anniversary-Discussing-Convictions/dp/0310101468/ref=sr_1_1?crid=2QIE4OBW0YIQI&keywords=tactics+koukl&qid=1647201612&sprefix=tactics+koukl%2Caps%2C224&sr=8-1
.. |tactics| replace::  *Tactics*

.. raw:: html

   <p style="text-align:left;">
       <span style="float:right;">
           🕑 30 min.
       </span>
   </p>

Authoritarianism
~~~~~~~~~~~~~~~~

The next worldview in play is *authoritarianism*.  If you recall our brief
history of emotionalism, over the past couple hundred years we worked our way
to the point where we had thrown out all our sources of knowledge and then were
left grasping for something, anything, to fill the void.  Where some thought to
fill the void with our emotions, others said, "Gosh, I don't know.  This is so
hard to figure out.  Tell you what, you just tell me what to think, mister
public authority figure of some sort, and I'll go with that."

The nasty thing about lies is the best ones have a good deal of truth baked
into them.  Whereas emotionalism plays off the fact that our feelings often do
provide information regarding the truth of a situation, authoritarianism
exploits the fact that people in positions of authority often should be good
sources of information.  And indeed, this was largely true for much of history:
children should listen to parents (|PRO.4.1-PRO.4.2|), elders should be
respected (|1PE.5.5|), and so on, and this was a good thing.  The interesting
thing is we abandoned authority as a valid source of knowledge in the age of
modernism, but then less than two centuries later we brought it back with a
vengeance.  What's different between then and now?  Historically authority
figures functioned as sources of knowledge, because the knowledge they were
imparting was anchored in revelation, reason, and reality.  Now they're
attempting to function as sources of knowledge completely untethered from that
foundation.

.. |PRO.4.1-PRO.4.2| replace:: :raw-html:`<a data-gb-link="PRO.4.1-PRO.4.2">Proverbs 4:1&ndash;2</a>`
.. |1PE.5.5| replace:: :raw-html:`<a data-gb-link="1PE.5.5">1 Peter 5:5</a>`

Many examples of this worldview are easy enough to spot:

* Various forms of government are built on it, e.g., dictatorships,
  oligarchies, autocracies, etc.  When the power to determine right and wrong
  thinking, speech, and behavior lies in the hands of a select few, the average
  citizen is simply to be grateful that those in power know what they're doing
  and are benevolent enough to bless the population with their oversight and
  control, all in service of the public good.
* Something that often comes out in the midst of a scandal---whether we're
  talking about something like `Watergate`_ or `the fall of Mars Hill
  Church`_---is that the culture of an organization was such that the one thing
  you absolutely could not do was question the person in charge.
* In recent years we've all become well-acquainted with various talking heads
  claiming to represent "the science," though they never seem to present any
  scientific evidence to back up what they have to say.  Their message is
  simply, "Agree with me, or else..."  If you don't, there's an army of fact
  checkers policing the internet to cow you into submission.

.. _Watergate:  https://en.wikipedia.org/wiki/Watergate_scandal
.. _the fall of Mars Hill Church:  https://www.seattletimes.com/seattle-news/the-rise-and-fall-of-mars-hill-church/

Though the examples above are fairly easy to recognize, authoritarianism can
also rear its head in much more subtle ways.  For instance:

* Your research really flies in the face of what we're hearing from everywhere
  else.  Perhaps you got something wrong somewhere, and we just haven't figured
  out what it is yet.  Regardless, we don't feel comfortable publishing this.
* If you don't like this decision, then you really need to think and pray about
  it.  Perhaps there's some sin in your life that's preventing you from
  agreeing, or perhaps you need to mature some before it'll make sense.
* If you'll continue to be tempted to be critical of this organization's
  leadership, then perhaps a different program would better meet your needs at
  this time.

What's Wrong with This?
^^^^^^^^^^^^^^^^^^^^^^^

These last three examples might be a good deal harder to parse through.
There's definitely value in listening to what everyone else is my field is
saying, particularly if my own conclusions disagree significantly with theirs.
It may be the case that either I'm lacking in maturity or being blinded by sin
in my life.  I can imagine it being awkward and unpleasant if I'm continually
questioning the leadership, so perhaps it's best to just walk away.  It seems
like in these examples we're simply being encouraged to be prudent---what's
wrong with that?  Before we answer that question, we need to realize that
authoritarianism is built on a number of false assumptions:  that authority
figures are right, that I don't have the ability to ascertain truth for myself,
and that to question an authority figure is wrong, to name a few.  Let's tackle
each of these one at a time.

Authority Figures are Right
+++++++++++++++++++++++++++

As mentioned previously, this assumption held true a good deal of the time
throughout history, and indeed it continues to hold true in a great many
scenarios today.  That's where much of its staying power comes from.  After
all, to become an authority figure in whatever the context, you've likely put
in a good deal of work over a number of years, building up experience that
contributes to your ability to be right more often than not.  Who am I to
question you?  It's not difficult, though, to come up with some counterexamples
to show that the assumption doesn't hold in all circumstances.

Consider an example from history.  Prior to the 19th century, well before we
had an understanding of things like virology and microbiology, the medical
authorities of the time regularly prescribed `bloodletting`_ as the treatment
of choice for a wide variety of ailments.  Sicknesses were due, or so the
thinking went, to an imbalance in the *humours* in the body, and the only
solution was to get them back in balance by draining some blood.  If the
patient doesn't show signs of improvement, just keep draining.  Now with our
modern understanding of medical science, we look back in time and think, "What
a bunch of fools."  At the time, though, this wasn't just one expert, but near
the entirety of the medical community saying, "This is truth," and they were
dead wrong (macabre pun intended).

.. _bloodletting:  https://en.wikipedia.org/wiki/Bloodletting

Another example from the last century or so is the growing
|tyranny_of_the_experts|_ in modern society.  This phrase refers to the various
social and economic planning activities that have been attempted by governments
or philanthropic organizations that have failed miserably.  This `has been
chronicled recently`_ by economist `William Easterly`_, and `50 years ago`_ by
lawyer and political scientist `Jethro K. Lieberman`_.  Though such
activities---be they economic development, medical interventionist, or
otherwise---are designed to produce the ideal society, they fail to achieve
their desired outcomes.  Why?  Because it is wholly impossible for one person,
or even a group of people, to have access to and process sufficient information
to make the right decisions every step of the way, as `Friedrich Hayek`_
explains in his |law_legislation_and_liberty_vol_1|_.

.. _tyranny_of_the_experts:  https://mises.org/wire/tyranny-enlightened-experts
.. |tyranny_of_the_experts| replace::  *tyranny of the experts*
.. _has been chronicled recently:  https://smile.amazon.com/Tyranny-Experts-Economists-Dictators-Forgotten-ebook/dp/B00ET7IZF2/ref=tmm_kin_swatch_0?_encoding=UTF8&qid=1658405679&sr=8-1
.. _William Easterly:  https://en.wikipedia.org/wiki/William_Easterly
.. _50 years ago:  https://smile.amazon.com/tyranny-experts-professionals-closing-society/dp/080270249X/ref=sr_1_1?crid=3IK3SRSI2FZGB&keywords=tyranny+of+the+experts+lieberman&qid=1658405356&sprefix=tyranny+of+the+experts+lieberman%2Caps%2C151&sr=8-1
.. _Jethro K. Lieberman:  https://www.jethrolieberman.com/bio.htm
.. _Friedrich Hayek:  https://en.wikipedia.org/wiki/Friedrich_Hayek
.. _law_legislation_and_liberty_vol_1:  https://smile.amazon.com/Law-Legislation-Liberty-Rules-Order/dp/0226320863/ref=sr_1_1?crid=1OH1LB7H0CG1Z&keywords=law+legislation+and+liberty+vol+1&qid=1658406368&sprefix=law+legislation+and+liberty+vol+1%2Caps%2C148&sr=8-1
.. |law_legislation_and_liberty_vol_1| replace::  *Law, Legislation and Liberty, Volume 1:  Rules and Order*

As we've seen in other places, this assumption boils down to yet another
example of a `logical fallacy`_, where we know *some* authority figures are
right *some* of the time, but then we extend that and say that *all* authority
figures are right *all* of the time.  If you want to impress your friends at a
cocktail party with your Latin skills, that's what we call a *non sequitur*,
meaning "it doesn't follow;" that is, the conclusion (that authority figures
are right, without any qualifications) doesn't follow from the premise (that I
know of some examples in which authority figures were right).  I suspect `C.S.
Lewis`_ was on to something 70+ years ago:

  "Logic!" said the Professor half to himself.  "Why don't they teach logic at
  these schools?" :raw-html:`<br>`
  ~ *The Lion, the Witch and the Wardrobe*

.. _C.S. Lewis:  https://en.wikipedia.org/wiki/C._S._Lewis

I Can't Ascertain Truth for Myself
++++++++++++++++++++++++++++++++++

This second assumption tends to be an outgrowth of the first, particularly for
the average Joe.  After all, if the experts are the ones that can figure out
what's true, and they've put in years, sometimes decades, of intense work and
study to develop their expertise, who am I to try to figure out what's correct?
I don't have their background, and I don't have the time to develop the
experience, so shouldn't I just take their word for it?

There are two misconceptions at play here.  The first is that we have to become
an expert in a field in order to determine truth in that field.  If that were
the case, we simply wouldn't function:

* I don't know how to cook dinner tonight; I'm not a chef.
* I don't know how to take care of my yard; I'm not a landscaper.
* I don't know how to exercise; I'm not a professional athlete.
* I don't know how to raise my kids; I'm not a certified teacher.
* I don't know what a woman is; I'm not a biologist.

I hope it's clear from personal experience that such thoughts are nonsensical.
It should also be obvious that to whatever extent you lack expertise in a
field, you are capable of learning.

The second misconception is that since I don't have time to become an expert in
something, I shouldn't do any research at all.  The premise is understandable.
I'm not going to spend the next decade earning degrees and certifications in
communicable diseases to determine how my family should respond to the
pandemic.  However, the conclusion, again, doesn't follow; that is, it is
possible for me to `do enough research to come to well-informed decisions`_
without being an expert.

.. _do enough research to come to well-informed decisions:  https://jmgate.readthedocs.io/en/latest/thoughts/pandemic.html

I suspect, if we're honest with ourselves, this assumption really just boils
down to good old fashioned laziness.  We're too comfortable, and the thought of
giving up some of our many pleasures to put time and effort into determining
whether or not something is true is too much for us to bear.  We'd much rather
return to the never-ending onslaught of Disney+ series and think about hard
questions later.  We're like Scarlett O'Hara:

  "I can't think about that right now.  If I do, I'll go crazy.  I'll think
  about it tomorrow." :raw-html:`<br>`
  ~ *Gone with the Wind*

The problem is tomorrow never comes.

Questioning Authority Figures is Wrong
++++++++++++++++++++++++++++++++++++++

This final assumption we'll analyze under authoritarianism is really just the
logical conclusion of the last two.  If authority figures are right, and I'm
not currently capable of determining right behavior on my own, then my role is
to believe and obey.  This should sound familiar, because it accurately
describes a particular phase of life:  when I'm a child, and Mom and Dad are
doing everything they can to prevent me from accidentally maiming myself.
"Don't run out in the street!  Don't touch the hot stove!  Don't stick the fork
into the power outlet!"  Then of course those admonitions are met with the
never-ending chorus of "Why?"s, which are inevitably countered with, "Because I
said so!"  I wonder if this is part of where we've gone wrong.

In |parenting_by_the_book|_, author `John Rosemond`_ walks the reader through
the four seasons of parents' relationships with their children:  service,
leadership, mentoring, and friendship.  In the season of service, which is from
birth to roughly age two, "Do what I said" is appropriate, because the kids
won't be able to understand more yet, and they need to learn to obey Mom and
Dad for their own safety.  The season of leadership, which lasts till the
pre-teen years, is characterized by training your kiddos to understand the
reasoning behind thoughts and actions.  Obedience is still required, but obey
first, and then we can tackle all the questions.  The mentoring season, which
lasts till the late teens, assumes you've already trained the kids to discern
truth from falsehood, right from wrong.  You're there to assist, but not to lay
down the law, as they should understand the law and its foundation for
themselves at this point.

.. _parenting_by_the_book:  https://smile.amazon.com/Parenting-Book-Biblical-Wisdom-Raising/dp/1476718717/ref=sr_1_1?crid=35ENM4GX0BBBU&keywords=parenting+by+the+book&qid=1658453245&sprefix=parenting+by+the+book%2Caps%2C164&sr=8-1
.. |parenting_by_the_book| replace::  *Parenting by the Book*
.. _John Rosemond:  https://en.wikipedia.org/wiki/John_Rosemond

There's much more that can be said about the abysmal state of parenting in
modern society, but I suspect one of the consequences is our culture is
effectively stuck in the transition between the service and leadership stages.
Authority figures will tell us what to think, say, and do, but then we're not
allowed to question them because [insert grave consequences here].  For some
reason we don't realize this is a phase we're supposed to grow out of.  In
short, we're behaving like toddlers.

I suspect something else that plays into the prevalence of this assumption is a
dangerous combination of pride and incompetence.  If someone never really
learned how to ascertain truth, and if at some point they wind up in a position
of authority, then they find themselves in a precarious place where they can't
actually justify their thoughts, speech, and actions.  That's a scary place to
be.  What's easier:  putting in the hard work to support your beliefs, or
saying, "How dare you question me!"?

Counterfeit Worldview
+++++++++++++++++++++

Now that we've dismantled some of the assumptions undergirding
authoritarianism, let's circle back to examining how it qualifies as a
counterfeit worldview.  As with emotionalism, this is easiest to see by
returning to the last three points of the five-fold breakdown from earlier:

3. **Truth:**  Truth has authority figures as its source, and therefore
   fluctuates, depending on who's in power and what they're thinking at the
   time.
4. **Knowledge:**  We can know truth by listening to whomever the authorities
   happen to be currently.
5. **Ethics:**  Right and wrong are determined by whomever is in charge,
   however they care to determine it.

Though it's easiest to see in these three areas, these fundamental beliefs have
implications across the board.  In terms of the four fundamental questions:

1. **Who am I?**  My identity is determined by whatever authority figures I
   happen to be listening to at the moment.  If they're saying I'm just a
   random collection of atoms, then I guess I'm no more than a cosmic accident.
   If they're saying I'm just the product of neurons firing in response to
   environmental stimuli, then I guess I'm not really responsible for my
   actions.
2. **Why am I here?**  To do whatever the `grand poobahs`_ of the world tell me
   to do.  If they tell me to go green to save the planet, then I better go
   `buy a Tesla`_.  If they tell me I need to be working for `racial
   reconciliation`_, then point me to the bandwagon so I can jump on.  If they
   say "two weeks to flatten the curve," I'll lock myself in my house until
   they say it's safe to come out again.
3. **What's wrong with the world?**  People are standing on the wrong side of
   history, not obeying the directives the elites are doling out, which are
   designed to usher us into a utopian paradise.  If you point out that any
   time that's been tried throughout history it's resulted in complete and
   total failure, you're part of the problem, because it'll work this time.
4. **How can what's wrong be made right?**  Just obey, for crying out loud.
   Failing that, wait for those in charge to eventually eliminate all their
   opposition, first through indoctrination via propaganda, then through
   coercion, and eventually and ultimately through lethal force.  Enjoy the
   ride.

.. _grand poobahs:  https://en.wikipedia.org/wiki/Grand_Poobah
.. _buy a Tesla:  https://www.youtube.com/watch?v=PLxPAwIeL0w
.. _racial reconciliation:  https://www.youtube.com/watch?v=FoJGYCc7EUg

In terms of the historical meta-narrative:

1. **Creation:**  Everything came into being the way "the science" tells us it
   did:  nothing plus time plus chance plus more nothing equals everything.
   The fact that the experts disagree on the age of the universe `to the tune
   of billions of years`_, and that `we don't yet have a working theory of
   evolution`_, shouldn't bother us.
2. **Fall:**  The ultimate problem that we need to be trying to solve is
   whatever happens to be fashionable for the elites at the moment.  Depending
   on where you are in history, it bounces around between things like economic
   inequity, racism, human-induced climate change, overpopulation, pandemics,
   threats to our democracy, etc.
3. **Redemption:**  Whatever the problem, and whatever its enormity, we
   definitely have it in us to solve it ourselves, thanks to the special
   knowledge somehow attained by those in charge.
4. **Consummation:**  If we do what the authorities say, they'll usher us into
   a brave new world where all our problems are solved.

.. _to the tune of billions of years:  https://phys.org/news/2019-09-universe-billion-years-younger.html
.. _we don't yet have a working theory of evolution:  https://answersingenesis.org/natural-selection/natural-selection-vs-evolution/

And then to finish out the five-fold breakdown, the last three points of which
we saw up above:

1. **God:**  The concept of God is defined by whomever happens to be in charge.
   Depending on whom you listen to, this may match the biblical worldview, or
   God might be an outdated notion from prior unenlightened times.  It might
   even be some nonsensical combination of the two.
2. **Man:**  Man is at the same time both an insignificant chance combination
   of space dust, and supremely and infinitely capable of bending nature to our
   will to remove any of its imperfections.

How Does This Play Out?
^^^^^^^^^^^^^^^^^^^^^^^

Now let's revisit the three more subtle examples from earlier to see if we can
discern where non-biblical thinking might be in play.

When Research Controverts the Narrative
+++++++++++++++++++++++++++++++++++++++

In our discussion of emotionalism, one of the scenarios we examined was how
various churches were influenced by that worldview when responding to the
COVID-19 pandemic.  As you read through that section, you might've been
thinking, "Actually, no, we consulted with a number of people in the medical
field as we made our decisions."  In general, that's a good thing, but then the
question is how you went about doing it.  For the sources consulted, were their
backgrounds examined to determine what level of authority they had to speak to
the unfolding situation, or was being a doctor or nurse sufficient?  Did they
provide data and rational argumentation to back up their recommendations, or
were there only opinions?  For any data provided, were the sources also
available such that you could examine them for yourself, or was it just, "I'm a
professional; trust me"?  Hang on, am I saying that church leaders should have
dug that deep into the science before coming to their decisions?  Yes, but
that's actually beside the point here.

What I want to talk about now is the scientific community.  I know a fellow who
is a virologist, working for an organization to promote biological security and
incident response around the world.  You would think those would be the people
you'd want weighing in on an emerging global pandemic, right?

.. sidebar::  Book Recommendation

   If you'd like a thorough and balanced examination of the quest for the
   origin of COVID-19, check out |viral|_, by Alina Chan and Matt Ridley.

.. _viral:  https://smile.amazon.com/Viral-Search-COVID-19-Matt-Ridley/dp/006313912X/ref=monarch_sidesheet
.. |viral| replace::  *Viral*

If you can remember the early months of 2020, you'll recall that the WHO
declared the outbreak a `public health emergency of international concern`_ at
the end of January.  By mid-March we had the first publication to definitively
say that the virus was `not a laboratory construct`_ or purposefully
manipulated; that is, it jumped to humans from some animal population.  Once
that paper was published, the "definitely not man-made" origin story was
absolutely everywhere almost overnight.  The strange thing about it, though,
was that the paper basically argued that the unique features seen in this
particular virus, such as the presence of a `furin cleavage site`_, for
example, could be explained away as evolutionary changes, and that since, as
far as we knew, nobody had been doing the kind of gain of function research
that might lead to it, it must not be man-made.  I say all this not to begin a
debate about the origins of the virus, but rather so you have the backstory for
what unfolded next.

.. _public health emergency of international concern:  https://www.who.int/news/item/27-04-2020-who-timeline---covid-19
.. _not a laboratory construct:  https://www.nature.com/articles/s41591-020-0820-9
.. _furin cleavage site:  https://palaceintrigueblog.com/2022/01/14/its-all-about-the-furin-cleavage-sites/

Now if you know anything about furin cleavage sites (which I don't, but I know
people who do), you knew that argument was at the very least sketchy, and
warranted some closer examination.  The virologist I mentioned earlier found
the evidence and argument presented both weak and disturbing, so he and a small
handful of collaborators painstakingly researched all the data that was
available at the time.  Within five weeks, they had prepared a paper arguing
that the preponderance of evidence pointed to some sort of artificial
intervention in the development of the virus.  However, the paper needed to
first make it through an internal review process before being published, and
that's where the lead researcher ran into feedback along these lines:

  Your research really flies in the face of what we're hearing from everywhere
  else.  Perhaps you got something wrong somewhere, and we just haven't figured
  out what it is yet.  Regardless, we don't feel comfortable publishing this.

What's good here?  It's wise to consider the opinions of others, particularly
when your own ideas differ significantly.  Perhaps there were unwarranted
claims within the paper, so just to be safe, the researcher pulled out any kind
of editorializing and ensured it was focused solely on the science to see if
that would help.  No dice.  Perhaps there was something wrong in the analysis,
so they ran the research by a number of others to see if anyone could find
anything wrong with it.  No luck there either.  True, the situation in early
2020 was volatile, to say the least, and the paper had the potential to add to
the volatility, but what you need more than anything in the midst of such a
scenario is truth on which to operate.  Regardless, the higher ups in this
particular organization feared publishing the research of some experts, because
it controverted the opinions of other experts who had the power to set the
narrative.

Let me give you an example from my own place of employment, which is one of our
`national laboratories`_.  In September of 2021, an announcement went out in
the company's daily news email that made the claim that almost all U.S.
COVID-19 deaths were in unvaccinated people.  The announcement included a link
to a `WebMD article`_ for more details, which then in turn linked to two other
articles from the `Associated Press`_ and `Deadline`_.  Of the three, none
provided sources for raw data or indicated how their analyses were carried out,
so there was no means of assessing the accuracy of the analysis and conclusions
for yourself.  Additionally, the articles employed bad rhetoric and a number of
logical fallacies in attempting to convince you of their points.  The
announcement included a point of contact for any questions, so I wrote in with
my concerns and a summary of the research I'd done trying to understand the
science before contacting them.  Here's the response I got:

.. _national laboratories:  https://nationallabs.org/
.. _WebMD article:  https://www.webmd.com/vaccines/covid-19-vaccine/news/20210629/almost-all-us-covid-19-deaths-now-in-the-unvaccinated
.. _Associated Press:  https://apnews.com/article/coronavirus-pandemic-health-941fcf43d9731c76c16e7354f5d5e187
.. _Deadline:  https://deadline.com/2021/06/99-p4rcent-covid-deaths-los-angeles-unvaccinated-1234781402/
..

  Jason, thank you for your email.  This sounds like the beginning of a debate
  than a question [*sic*], and I have asked the staff not to get into debates
  on this issue.  With the executive order announced last week, sounds like we
  all will be vaccinated soon, which is good news.

Hang on, at one of our nation's premier scientific institutions, we're not
allowed to debate the science?  After our organization spent about ten million
taxpayer dollars in various research and development efforts supporting the
dominant narrative?  Really?

What's wrong here?  This isn't how science is supposed to work.  Regardless of
the results, if the research has been conducted with integrity, then you
publish and allow the scientific community to debate the issue in public.  To
fail to do so, or to put the kibosh on such debate, is to hamper the community
in its quest for truth.  Two and a half years into this thing, and the world is
slowly coming around to the realization that human intervention is actually the
more likely origin for the virus, and that at this point the "pandemic of the
unvaccinated" line was questionable at best, if not downright manipulative.
How might things have played out differently if the scientific community had
actually allowed for a free interchange of ideas, as it's supposed to?

The story of COVID-19 is one in which there's only one way you're allowed to
think, speak, and act, and that's defined by those in charge.  To search for
truth outside the acceptable answer is to risk ostracization.  Most of the
time, you're not even allowed to ask a question.  But why do I bring all this
up when we're talking about counterfeit worldviews invading the church?
Because the leadership of many churches conducted themselves in the exact same
way.  We're in charge, we made our decisions based on what the experts (at
least the ones who are allowed to speak) told us to do, and the issue is not up
for debate (but of course all of this was said in the most loving way
possible).

When Church Decisions Seem Sketchy
++++++++++++++++++++++++++++++++++

This next example comes from one of the pastoral search scenarios mentioned in
the introduction.  As the process progressed, the lead of the search team
became very quickly convinced that the candidate they'd found was the right man
for the job; however, one of the other members of the team was not so sure.
Not quite knowing what was wrong, but having a gut feeling that something was
off, they tried to talk it through with the team lead, but eventually ran into
a response along these lines:

  If you don't like this decision, then you really need to think and pray about
  it.  Perhaps there's some sin in your life that's preventing you from
  agreeing, or perhaps you need to mature some before it'll make sense.

What's good here?  We should definitely seek the Lord in prayer in the midst of
our decision making (|PHP.4.6-PHP.4.7|, |JAS.1.5|).  We definitely want to be
examining ourselves to ensure we don't have something blinding us
(|LUK.6.41-LUK.6.42|).  Perhaps our thinking in this matter is still worldly,
and our mind needs some Spirit-led transformation (|ROM.12.2|).  Additionally,
the lead of the search team had more experience, both in church leadership in
general, and in pastoral search processes in particular.  Perhaps the person
questioning the decision was just put in a role for which they were ill-suited
or ill-prepared.

.. |PHP.4.6-PHP.4.7| replace:: :raw-html:`<a data-gb-link="PHP.4.6-PHP.4.7">Philippians 4:6&ndash;7</a>`
.. |JAS.1.5| replace:: :raw-html:`<a data-gb-link="JAS.1.5">James 1:5</a>`
.. |LUK.6.41-LUK.6.42| replace:: :raw-html:`<a data-gb-link="LUK.6.41-LUK.6.42">Luke 6:41&ndash;42</a>`
.. |ROM.12.2| replace:: :raw-html:`<a data-gb-link="ROM.12.2">Romans 12:2</a>`

What's wrong here?  A first problem is that concerns aren't being considered,
let alone addressed.  One would think that if this search team is comprised of
believers, each of whom has the Holy Spirit within them, then someone sensing a
potential problem might be God trying to communicate to the group.  One would
also think that at the very least each member of the group would be coming with
a different background and set of skills and spiritual gifts, so it'd be
worthwhile to consider whatever's brought up, because the group as a whole
should be better suited to fulfill its task than any one member individually.
Unfortunately, concerns were brushed aside as unimportant or irrelevant,
without any investigation, and with little discussion.

Beyond the fact that people simply weren't listening to each other, any
concerns raised were also being hidden.  When it came time for the
congregational meeting to vote on the pastoral candidate, we were told that the
search team was unanimous in their recommendation.  Not so.  The team lead knew
there were misgivings within the group, but that information was buried.  Dig a
little deeper, and come to find out that it wasn't just one member of the team
who thought the applicant wasn't right for the job, it was three.
Unfortunately concerns were typically raised in one-on-one conversations with
the person in charge, who then had the ability to quash them.

A final problem that we'll discuss here is that someone in a position of
authority, real or perceived, was using that authority to manipulate people
into submission.  When concerns were brought to the team lead, the response was
essentially, "You don't know what you're talking about."  Now it's one thing to
make such a claim and then back it up with well-reasoned argumentation to
educate the other person and help clear up their confusion.  It's quite another
to make the claim and then give no justification, which communicates that the
speaker isn't required to justify what they say---their word is truth.  I'm
afraid the only one who gets to play ball that way is God.  To make sure the
speaker isn't questioned, though, they then follow that up with a series of
|ad_hominem_attacks|_ (e.g., "You're an immature, unrepentant sinner") to
silence the questioner.  Note that the team lead's initial response up above
sounds very polite and spiritual, but I suspect for too long we've been duped
into thinking that the tone we use is the primary factor in determining how
communication is received.  If what's communicated is essentially, "You're a
fool, and an awful human being to boot," it doesn't matter how nicely you say
it.  You've essentially told someone to shut up, and you've slapped them in the
face to hammer home your point.

When Study Materials Shouldn't be Critiqued
+++++++++++++++++++++++++++++++++++++++++++

As you read through the prior section, you might've thought, "Goodness, what a
mess.  Surely this sort of thing is a rarity, though, right?"  Years ago I
would've thought the same, but having personally observed three instances in
three organizations in three years, and having heard numerous similar stories
either from acquaintances or online, it seems the problem is far more prevalent
than we'd like to believe.  For a final example, let's return now to that
year-long training program and the critiques of some of the content of the
`Bible Project videos`_.  "But wait," you may say, "wasn't that an example of
emotionalism?"  Indeed it was, and it's interesting to note how often these two
worldviews actually surface in conjunction with one another.

Months before I accidentally kicked the proverbial hornets' nest by emailing
the program's leadership and requesting that we get together for an open
discussion, one of the friends I'd invited to participate in the program with
me wound up chatting one-on-one with a member of the leadership over some
initial concerns.  They had what seemed to both to be a productive
conversation, and came to a place of understanding (or so it seemed at the
time).  One of the things that stood out to me, though, as my friend recounted
the interaction, was that very early into the conversation he ran into the
following:

  If you'll continue to be tempted to be critical of this organization's
  leadership, then perhaps a different program would better meet your needs at
  this time.

Perhaps it was the case that the program was ill-suited to him---that is, maybe
its purpose wasn't what we originally thought it to be---and it would've been
beneficial to look for a different training opportunity elsewhere.  However, it
seemed fishy that the leader would so quickly jump to, "If you question our
content, you should go away."  I would think it would be beneficial for
participants to question the content, because (1) that shows they're really
engaging with the material, and (2) that gives you feedback necessary for
continuous improvement.

Fast-forward a few months, and two from the leadership team joined our monthly
small group meeting to give a presentation and then host a discussion, because
some in our group were being inappropriate in our online forum posts.  During
the presentation, they made and implied a number of claims without giving us
any grounds to support them.  As we've seen before, when no grounds are given
in support of a claim, the implied grounds are, "because I said so." Now on the
one hand, it may have been the case that some of the online forum posts were
inappropriate, and some certain individuals needed correction.  However, the
way they went about addressing it---without identifying any problematic
behavior specifically, or suggesting appropriate alternate behavior in its
place, and without addressing the concerns with the individuals in question
directly---indicated that the desired outcome was not to correct bad behavior,
but to just make the problem go away by getting people to stop critiquing the
materials.

Fast-forward again, and we reach the point where I've caused enough of a ruckus
that the director of the program reaches out to me directly to say that he and
another member of their leadership team would be "more than happy to speak with
[me] personally and address [my] concerns."  Seems like a step in the right
direction, right?  Except my past experiences, some of which I've related
above, have me on high alert, because in response to my request for an open,
honest conversation with all parties involved, I've been invited to a
closed-door, two-on-one meeting with individuals who look like they've been
demonstrating significant influence of authoritarianism in their actions.
Now momma didn't raise no dummy, so I immediately reached out to a small
handful of elders and spiritual advisors to discern with them the wisdom of
accepting such an offer.  The week it took to have those conversations was too
much, and I was then notified that the offer for a conversation with leadership
had actually been a demand, and was ejected from the program.  Friends who
stuck it out a while longer informed me that I was later vilified to the rest
of the group for having inserted myself into a matter that didn't concern me,
causing division, and being unresponsive to leadership.

What on earth just happened?  No need to speculate on the motives involved, or
anything else that might've been going on behind the scenes.  What's important
for you is to understand how this worldview tends to play out.  The examples
above have demonstrated a pattern:

1. Someone raises a concern.
2. The authorities get involved.
3. The concern isn't addressed.
4. The authorities do whatever they can to just make the problem (the fact that
   someone's raised a concern) disappear.
5. The authorities either strongly imply or say directly that the real problem
   is with the one who brought forth the concern in good faith.
6. The authorities do whatever they can to discredit the one who raised the
   concern.

Put that way, have you seen this pattern in play anywhere else?  This is how
totalitarian regimes behave.

What are the Consequences?
^^^^^^^^^^^^^^^^^^^^^^^^^^

One of my favorite quotes from `Chuck Colson`_---special counsel to President
Nixon before his imprisonment, and renowned Christian apologist after---is,
"Ideas have consequences; bad ideas have victims."  We've already seen a number
of bad ideas and their consequences up to this point.  The question now is,
"Where are the victims?"

.. _Chuck Colson:  https://en.wikipedia.org/wiki/Charles_Colson

First let's cross-apply all the harms of emotionalism here.  Whereas that
worldview quenches the Holy Spirit speaking through believers because we fear
being perceived as unloving, prideful, divisive, etc., this worldview does the
same because we fear that to question an authority figure would be sin.
Whereas the former causes us to abandon our responsibilities to encourage and
rebuke our fellow believers, this one manipulates us into abandoning those same
responsibilities to exhort and admonish Christians in positions of authority,
real or perceived.  Though these worldviews are quite different in their
foundational beliefs on truth, knowledge, and ethics, the impact they have on
society and on the church is remarkably similar.  It's also interesting to note
how often they tend to work in tandem:  in a single situation my indoctrination
in emotionalism is telling me to shut up, while at the same time well-respected
leaders are telling me the same.  It's no wonder we find it so difficult to
stand for truth.

The workings out of these two worldviews can be referred to as "soft" and
"hard" bullying, respectively, but I prefer to call them by the more
appropriate term of *abuse*, as `Alisa Childers`_ notes in her
|another_gospel|_---yet another one of the best books I've read in the past
year.  We recoil at using the term "abuse," and, to an extent, for good reason.
It's a serious matter, and the term should not be thrown about willy-nilly.  In
these situations, though, its use is entirely justified.

.. _Alisa Childers:  https://www.alisachilders.com/about-me.html
.. _another_gospel:  https://www.alisachilders.com/
.. |another_gospel| replace::  *Another Gospel?*

It's easy enough to see that abuse is the appropriate term in high-profile
cases like `that of Ravi Zacharias`_.  Such cases are the outlier rather than
the norm, though.  Harder to identify are the cases where the abuse is
spiritual or emotional, rather than physical, in nature.  For instance, the
case of the pastoral search committee chair using |ad_hominem_attacks|_ to
manipulate into submission any who question the candidate's fit.  Or consider
the case of the congregant, politely requesting more than a mere 24 hours to
think and pray about a pastoral candidate before voting, only to be shouted
down into silence.  Or maybe it's the discussion group leader being berated by
the para-church ministry's leadership for having lost control of the group,
though they don't provide any details as to what members of the group have done
wrong and the only solution is to start kicking people out.  Perhaps you know
someone who rebuked church leadership on lockdown or reopening policies, only
to be rebuffed with, "We're not operating out of fear; this is how we love our
neighbor and submit to the governing authorities," with no further
justification or dialogue.  Though the abuse is not physical, this quote from
|the_subtle_power_of_spiritual_abuse|_ holds true:

  "The results of spiritual abuse are usually the same:  The individual is left
  bearing a weight of guilt, judgement or condemnation and confusion about
  their worth and standing as a Christian."

.. _that of Ravi Zacharias:  https://s3-us-west-2.amazonaws.com/rzimmedia.rzim.org/assets/downloads/Report-of-Investigation.pdf
.. _ad_hominem_attacks:  https://newmexicoforensicsclub.readthedocs.io/en/latest/training/logical-fallacies.html#ad-hominem
.. |ad_hominem_attacks| replace::  *ad hominem* attacks
.. _the_subtle_power_of_spiritual_abuse:  https://smile.amazon.com/Subtle-Power-Spiritual-Abuse-Manipulation/dp/0764201379/ref=sr_1_1?keywords=the+subtle+power+of+spiritual+abuse&qid=1658499896&sprefix=the+subtle+power+%2Caps%2C185&sr=8-1
.. |the_subtle_power_of_spiritual_abuse| replace::  *The Subtle Power of Spiritual Abuse*

For those who have experienced this, such feelings can hang on for a long time,
as they're hard to work through.  You feel like you can't talk about it,
because you've been manipulated into thinking you're the one in the wrong.  If
you do get up the courage to share your experience with a fellow believer,
often the influence of emotionalism and authoritarianism in their life leads to
them downplaying the wrongs that were committed.

The end result of all of this is our army is being ravaged by friendly fire.  A
spiritual war is being waged all around us every day, and we're either laying
down our weapons (truth, scripture, the Holy Spirit), or we're inadvertently
turning them on our own team, wounding one another with our misguided piety.
Brethren, we don't have time for this.

Finally since we have abandoned revelation, reason, and reality as the bedrock
of knowledge, and since we regularly employ emotionalism and authoritarianism
to cow our brothers and sisters into submission, the church has been helping to
set the stage for, and is now helping to usher in, a worldwide
"soft" totalitarianism.  Back in 2015, people from around the world started
reaching out to author and editor `Rod Dreher`_ with concerns that the changes
they were seeing in the culture closely mirrored what they had seen decades
prior immediately before the people of their countries embraced totalitarian
rule with open arms.  His curiosity piqued, Dreher dug in and researched the
matter and eventually published |live_not_by_lies|_, in which he tells stories
of the horrors of life under totalitarian regimes, and shares practices that
helped the faithful preserve Christianity throughout.

.. _Rod Dreher:  https://en.wikipedia.org/wiki/Rod_Dreher
.. _live_not_by_lies:  https://smile.amazon.com/Live-Not-Lies-Christian-Dissidents/dp/0593087399/ref=sr_1_1?crid=1YFNXM22VEZWT&keywords=live+not+by+lies&qid=1659014944&sprefix=live+not+by+lie%2Caps%2C268&sr=8-1
.. |live_not_by_lies| replace::  *Live Not by Lies*

In the past, these counterfeit worldviews have often resulted in a "hard"
totalitarianism, in which dissenters are dealt with by force.  Within the 20th
century alone, estimates of the death associated range from `about 100`_ to
`170 million`_.  These days, however, the force is softer in nature, though no
less real, as dissenting opinions are unable to be voiced, let alone heard.
Most of the time you're not even able to ask the question.  Do you question the
validity of critical theory as an analytical tool?  You're both arrogant and a
racist.  Do you question the danger of COVID-19?  You're a science denier, and
you're trying to kill grandma.  Do you question the integrity of the 2020
election?  You're anti-democracy at best and an insurrectionist at worst.  Do
you question whether man-made climate change poses an existential threat to
mankind?  You're a conspiracy theorist who's out to destroy the planet.  It
seems `liberty of conscience`_ has left the building.

.. _about 100:  https://reason.com/2013/03/13/communism-killed-94m-in-20th-century/
.. _170 million:  https://www.providentialhistory.org/?p=295
.. _liberty of conscience:  https://founders.org/2021/08/13/vaccine-mandates-and-the-christians-liberty-of-conscience-from-2021-to-1721-and-back-again/

Where are we today?  I’ll borrow and augment a phrase from `Michael O'Fallon`_
and say the war for epistemology is well underway, what’s at stake is the
future of the Christian church and civilization as we know it, and much of the
church unknowingly finds itself on the wrong side.  We are now reaping the
consequences of having been asleep on the watch for centuries.  Christians,
it's time to pull ourselves up, dust ourselves off, and get back in the game.
Faithfulness to our Lord demands it.

.. _Michael O'Fallon:  https://sovereignnations.com/profile/mike/

.. raw:: html

   <p style="text-align:left;">
       <span style="float:right;">
           🕑 20 min.
       </span>
   </p>

What Can We Do About All This?
==============================

I've heard a number of people saying things like, "We just need to have faith
and focus on the gospel."  The mental picture that comes to mind when I hear
that is of a squad of swordsmen in the midst of battle.  Beleaguered by enemy
forces (though they don't realize it), their squadmaster encourages them,
shouting, "Hold on to your swords, men!"  You look at the squad and realize
that's not the most useful advice.  One guy has his hand on the hilt, but
hasn't drawn it yet.  Another's resting his hand on the pommel, using it as a
walking stick.  A third, the most eager of the group, is holding it like a
baseball bat, but he's holding it backwards, grasping the blade near the point,
as blood runs down his hands from self-inflicted wounds.  A fourth has faith in
the countless hours he's logged as a swordsman in a video game, though he
doesn't even know how to grip his weapon correctly yet.  None of these men have
been trained in swordsmanship, let alone battle, and their squadmaster doesn't
realize the precarious spot they're in.

I hope at this point your eyes have been thoroughly opened to our current
predicament.  Counterfeit worldviews have been invading the church for
generations, and we've largely been ill-equipped to notice the incursion, let
alone fend it off.  We've only talked through two of them today---and even
then, not in much detail---but there are plenty more where they came from.  The
pit of hell has been churning out tantalizing distortions of God's truth almost
since the beginning of time.  Satan and his minions are remarkably
well-practiced, and are well-aware of what's at stake (more so than we are, or
so it seems).  With all that in mind, how on earth do we fight back?

Understand Our Training Regimen
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The first step is to begin training for the battle in which we are already
engaged.  A successful warrior is not one who just wakes up one day, decides to
head off into battle, and then returns victorious.  Rather, a successful
warrior is one who spends the vast majority of his time training, such that
when he's in battle, he can fall back on the skills and muscle memory he's
built over all those hours of training, and be victorious.  What does such
training look like for us, though?  You've heard me mention a number of times
that what we think, say, and do must be in accordance with revelation, reason,
and reality.  The justification for this particular epistemological breakdown
I'll save for another time, but for now suffice it to say these "three Rs" are
umbrella terms for how God communicates truth to us:

Revelation
  God's direct communication to us through his written word, through the
  indwelling Holy Spirit, and through his miraculous intervention in the world
  he created and superintends.
Reason
  God's way of thinking via rational thought processes, which we think after
  him by virtue of being made in his image.
Reality
  God's indirect communication to us through his created order, which we can
  study through the observational and historical sciences.

Under each of these umbrellas, then, are a number of skills in which we can
develop our competency over time.

.. note::

   Don't think of these three categories as hard and fast subdivisions.
   There's some overlap, and developing skills in one area improves your skills
   in another.

Revelation
^^^^^^^^^^

A primary habit to be cultivated under revelation is that of reading the
scriptures.  There are some who contend that we are where we are today simply
because Christians are largely ignorant of what the Bible says.  If the only
Bible reading we do is in conjunction with the sermons on Sunday, and if those
slowly work their way through scripture a few verses at a time, it can take a
lifetime to finally make it through the whole counsel of God.  That's simply
too slow a pace, and it means we're just not familiar enough with everything
God has to say.  Pick one of the many "read the Bible in a year" plans and go
with it.  Don't get discouraged if you get behind a few days---if it takes you
two years to make it through, that's better than twenty.  When you make it
through, celebrate, and then start back in again. |the_daily_bible|_ is a good
resource in this endeavor.

.. _the_daily_bible:  https://smile.amazon.com/Daily-Bible%C2%AE-NIV-LaGard-Smith/dp/0736980296/ref=sr_1_1?keywords=the+daily+bible+in+chronological+order+365+daily+readings&qid=1659354637&sprefix=the+daily+bible%2Caps%2C165&sr=8-1
.. |the_daily_bible| replace::  *The Daily Bible*

Beyond simply reading scripture every day, we must commit ourselves to
memorization, that we might hide truth in our heart (|PSA.119.11|).  Part of
this is memorizing scripture itself, whether that means individual verses, or
whole books of the Bible (start with the shortest books to build your
confidence).  A program like `Awana`_ can be a big help here.  In addition to
memorizing scripture, consider also memorizing one or more of the early
Christian statements of faith, such as the `Apostles'`_ or `Nicene`_ creeds.  A
final practice in the realm of memorization that has been largely lost in
evangelicalism is that of *catechism*, which is just a fancy word for a summary
of doctrine compiled in a question and answer format.  Consider using
|a_catechism_for_girls_and_boys|_, though there are a number of others to
choose from.

.. |PSA.119.11| replace:: :raw-html:`<a data-gb-link="PSA.119.11">Psalm 119:11</a>`
.. _Awana:  https://www.awana.org/about/
.. _Apostles':  https://www.crcna.org/welcome/beliefs/creeds/apostles-creed
.. _Nicene:  https://www.crcna.org/welcome/beliefs/creeds/nicene-creed
.. _a_catechism_for_girls_and_boys:  http://www.reformedreader.org/ccc/acbg.htm
.. |a_catechism_for_girls_and_boys| replace::  *A Catechism for Girls and Boys*

.. note::

   As you're memorizing these extra-biblical resources, keep in mind that they
   are not infallible as the Bible itself is.

In addition to knowing what's in the Bible, we also need to hone our skills
when it comes to interpreting what it says.  The practice of understanding the
meaning of scripture is known as `biblical hermeneutics`_.  To an extent, it's
simply an application of `how to analyze literature`_, where the literature in
question is the word of God.  If you get good at one, you get good at the
other, so a resource like |how_to_read_a_book|_, by `Mortimer J. Adler`_, can
be beneficial.  More specific to scripture, though, you might consider learning
and practicing the `inductive Bible study method`_, though other methods
abound.  A significant difference between interpreting scripture versus other
works is you have the author on hand to help you.  As such, books like `Richard
Foster`_'s |celebration_of_discipline|_ can be helpful, in that spiritual
disciplines like prayer and fasting help to put us in a place where God can
better inform and correct us.

.. _biblical hermeneutics:  https://www.logos.com/grow/biblical-hermeneutics-guide/
.. _how to analyze literature:  https://www.youtube.com/watch?v=zY3A5LiW5bk
.. _how_to_read_a_book:  https://smile.amazon.com/How-Read-Book-Classic-Intelligent/dp/0671212095/ref=sr_1_1?crid=32L4Y35SEN3W2&keywords=how+to+read+a+book&qid=1659390686&sprefix=how+to+read+a+boo%2Caps%2C272&sr=8-1
.. |how_to_read_a_book| replace::  *How to Read a Book*
.. _Mortimer J. Adler:  https://en.wikipedia.org/wiki/Mortimer_J._Adler
.. _inductive Bible study method:  https://biblestudy.tips/inductive-bible-study/
.. _Richard Foster:  https://renovare.org/people/richard-foster/bio
.. _celebration_of_discipline:  https://smile.amazon.com/Celebration-Discipline-Special-Anniversary-Spiritual/dp/0062803883/ref=sr_1_1?crid=3LE8K4IBA4D9R&keywords=celebration+of+discipline&qid=1659392012&sprefix=celebration+of+discipline%2Caps%2C282&sr=8-1
.. |celebration_of_discipline| replace::  *Celebration of Discipline*

Reason
^^^^^^

Another way we can discern between that which is true and that which is
*almost* true is by using the rules of logic.  This is a practice that is often
overlooked these days, so training ourselves in the `fundamentals of logic`_,
and in `more advanced logical argumentation`_, can go a long way in improving
our ability to ascertain truth.  Once you have the fundamentals down, a fun way
to continue to sharpen your skills is to keep your eyes peeled for `logical
fallacies`_ throughout the day and talk through them come dinner time.

.. _fundamentals of logic:  https://canonpress.com/products/intrologic/
.. _more advanced logical argumentation:  https://canonpress.com/products/new-intermediate-logic-complete-program-with-dvd-course/
.. _logical fallacies:  https://newmexicoforensicsclub.readthedocs.io/en/latest/training/logical-fallacies.html

In addition to thinking logically, we can also improve our skills in the realm
of communicating effectively, known as |rhetoric|_.  While you speaking
persuasively doesn't directly improve your ability to determine truth, knowing
how people communicate effectively, in terms of both honest and deceitful
tactics, can help you sense when something's amiss.  `Aristotle`_'s
|aristotles_rhetoric|_ is the classic text on the subject, but
|how_to_speak_how_to_listen|_, by `Mortimer J. Adler`_, is a good one as well.

.. _rhetoric:  https://en.wikipedia.org/wiki/Rhetoric
.. |rhetoric| replace::  *rhetoric*
.. _Aristotle:  https://en.wikipedia.org/wiki/Aristotle
.. _aristotles_rhetoric:  https://smile.amazon.com/Rhetoric-Dover-Thrift-Editions-Aristotle/dp/0486437930/ref=sr_1_4?crid=3KROPNSO7BJXS&keywords=rhetoric+aristotle&qid=1659394008&sprefix=rhetoric+aristotle%2Caps%2C179&sr=8-4
.. |aristotles_rhetoric| replace::  *Rhetoric*
.. _how_to_speak_how_to_listen:  https://smile.amazon.com/How-Speak-Listen-Mortimer-Adler/dp/0684846470/ref=sr_1_1?crid=2YCJRWMR1411G&keywords=how+to+speak+how+to+listen&qid=1659394100&sprefix=how+to+speak+how+to+listen%2Caps%2C181&sr=8-1
.. |how_to_speak_how_to_listen| replace::  *How to Speak, How to Listen*

Reality
^^^^^^^

A first arena under the umbrella of reality is that of empirical science, which
consists of using our senses to better understand the world around us.  We can
`improve our observational abilities`_ over time with practice.  Such skills
are foundational to the `scientific method`_, which allows us to verify whether
our current understanding of how the natural world works is correct, and adjust
our thinking if not.  Additional skills that play into the process are the
practices of `deductive and inductive reasoning`_, which come from logic,
mentioned earlier.  "Well hang on now.  Are you saying I need to switch careers
and become a scientist?"  No---indeed, doing so may decrease, rather than
increase, your odds of being able to see truth clearly---but I am saying you
need to be able to think critically about what you see in the world around you.

.. _improve our observational abilities:  https://www.sciencelearn.org.nz/resources/605-observation-and-science
.. _scientific method:  https://en.wikipedia.org/wiki/Scientific_method
.. _deductive and inductive reasoning:  https://www.masterclass.com/articles/what-is-deductive-reasoning

A second arena is that of `historical science`_.  This one is often overlooked
these days, because the prevalence of naturalism has duped us into thinking the
only things we can know are those we can know "scientifically," by which is
meant "by the methods of empirical science."  Consider this question, though:
What did you have for lunch last Tuesday?  I'm afraid no amount of
observational work is going to answer that question for you.  Instead you need
`historical thinking skills`_ like examining sources, determining context,
finding corroboration, and careful reading, to name a few.  A great resource
for training you in these techniques is |cold_case_christianity|_, by `J.
Warner Wallace`_.

.. _historical science:  https://answersingenesis.org/what-is-science/historical-science-useful/
.. _historical thinking skills:  https://www.waterford.org/education/historical-thinking-skills-for-students/
.. _cold_case_christianity:  https://smile.amazon.com/Cold-Case-Christianity-Homicide-Detective-Investigates/dp/1434704696/ref=sr_1_1?keywords=cold+case+christianity&qid=1659442454&sprefix=cold+case+chri%2Caps%2C179&sr=8-1
.. |cold_case_christianity| replace::  *Cold Case Christianity*
.. _J. Warner Wallace:  https://coldcasechristianity.com/j-warner-wallace-christian-apologist-and-author/

Whether you're dealing with empirical or historical sciences, a skill set
useful to both is that of `evaluating evidence`_.  This involves evaluating the
`credibility of sources`_:  how close they are to the information they're
reporting, whether or not they're biased, how reliable they've been in the
past, etc.  Instead of simply accepting information because it comes from an
expert, you need to be able to determine the likelihood that the expert is
giving you accurate information in a particular situation.

.. _evaluating evidence:  https://courses.lumenlearning.com/olemiss-writ250/chapter/evaluating-evidence/
.. _credibility of sources:  https://www.scribbr.com/working-with-sources/credible-sources/

How Am I Supposed to Do All This?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

At this point I've introduced you to a dozen or so skills that need improving
over the course of your life.  I doubt this list is complete; it was just
intended to give you an idea of the depth and breadth of the training necessary
to be able to stand firm for truth.  That said, do you need to drop everything
and start training in each of these areas simultaneously?  No, don't do that.
Instead, figure out one or two areas where you happen to be weak at the moment,
and focus there.  On the flip side, if you know you're particularly strong in
an area, try to figure out how you might teach others.  I don't want you to
walk away thinking, "Gosh, there's so much to do, it just seems hopeless;"
rather, I want you to say, "Here's one thing I can tackle, so let me get after
it."

Parents, understand that you have the responsibility to ensure your kids are
well-practiced in the martial arts of discerning truth from falsehood before
they leave the home.  Once they're out in the wild, the vast majority of the
influences they'll run into will be peddling counterfeit epistemologies.  The
good news is training your kids in most, if not all, of these skills can be
incorporated into the regular rhythms of family life:  Bible reading,
memorization, and catechism around the dinner table; a homeschool curriculum
that includes things like critical thinking, persuasive writing and speaking,
scientific experimentation, investigative journalism; etc.  There's no silver
bullet solution to this training regimen---use your creativity and tailor it to
each of your kids' personalities.

Church leaders, understand that you have the responsibility to ensure your
congregants are growing in these areas, as it's part of equipping them for the
work of ministry (|EPH.4.11-EPH.4.16|).  Now I'm not saying here that we
shouldn't be focused on loving our neighbors, sharing the gospel, being Jesus'
hands and feet in our community, etc.  What I am saying, though, is that all
the various things we are called to as followers of Christ must have truth as
their foundation.  Without it, everything falls apart.  Please don't walk away
from this thinking, "Great, I just added another dozen things to my already
overloaded plate."  When executing this responsibility you can and should be
leveraging those in your congregation for what they can contribute, which we'll
come back to momentarily.

.. |EPH.4.11-EPH.4.16| replace:: :raw-html:`<a data-gb-link="EPH.4.11-EPH.4.16">Ephesians 4:11&ndash;16</a>`

Identify, Analyze, and Refute
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

With our training regimen in place, the next question is what to do when we
encounter one of these faulty worldviews out in the wild.  The first step is
simply to learn to recognize it, which is why we started off with the
introduction to worldview analysis up above.  Memorize the four fundamental
questions, the historical meta-narrative, and the five-fold breakdown, along
with what the biblical worldview has to say about each area.  Once those are
lodged in the back of your head, thinking "worldviewishly" is simply a matter
of testing things against them.  This can actually be a lot of fun when you're
practicing.  Pick something from popular culture---a movie, song, book, piece
of artwork---and analyze it.  What does this have to say that's good, and why
is it good?  What does it have to say that's bad, and why is it bad?  For that
which differs from the biblical worldview, brownie points if you dig in and
figure out where the bad ideas came from or what they're trying to achieve.

It's one thing to see counterfeit worldviews crop up in popular culture,
though; what do you do when you see one exerting influence in the church?  A
common mantra is, "If you see something, say something."  Usually that comes up
in various corporate trainings around safety, security, harassment, etc., but
it applies here as well.  If you think you see the influence of an
anti-biblical worldview in the life of a fellow believer, call it out
appropriately (|MAT.18.15-MAT.18.17|).  There's a chance you might be mistaken,
but you owe it to your brother to talk it through with him.  We can't afford to
let brothers and sisters accidentally lead one another astray.

.. |MAT.18.15-MAT.18.17| replace:: :raw-html:`<a data-gb-link="MAT.18.15-MAT.18.17">Matthew 18:15&ndash;17</a>`

Hopefully such situations result in the errant believer being reconciled both
to truth and to whomever they may have harmed along the way.  That's not always
the case, though, as pride and confusion can very easily cause us to dig in our
heels.  When that happens, the one in error will often resort to tactics from
emotionalism or authoritarianism (or both) to try to get you to give up.  If
that happens to you, politely say something along the lines of, "This is
manipulation, and it doesn't work on me."  You put them on notice that you know
that they know their position is both wrong and indefensible, and that their
response to it is unethical.

Require Specificity
~~~~~~~~~~~~~~~~~~~

The one thing that likely most hampers our pursuit of truth, other than the
sinful human heart, is ambiguity.  Let me give you an example from history.
One of the most influential philosophers in the last 250 years was a German
fellow by the name of `Georg Wilhelm Friedrich Hegel`_ (and with a name like
that, how could he not be German?).  We'll leave what he had to say and the
impact it's had as a story for another time, but for now it'll be sufficient to
hear what some of his detractors had to say of his writing, in which he used
such imprecise language, often nobody had any clue what he was talking about.
`Arthur Schopenhauer`_ was a fierce critic of his, denouncing him as a clumsy
charlatan, saying that his awful writing was an embarrassment to the German
people and to philosophy.  `Ludwig von Mises`_ devotes some space in
|human_action|_ to rail against Hegel for the same problem, pointing out that
shortly after his death there were two camps of adherents that sprung up:  the
`Old Hegelians`_, who were devoted to maintaining protestant orthodoxy, and the
`Young Hegelians`_, who were supporting revolutionary atheism.  Hard to get
further apart than that, yet both groups pointed back to the same works and the
same words to support their radically different interpretations of what he'd
meant.  Imprecise language is dangerous.

.. _Georg Wilhelm Friedrich Hegel:  https://en.wikipedia.org/wiki/Georg_Wilhelm_Friedrich_Hegel
.. _Ludwig von Mises:  https://en.wikipedia.org/wiki/Ludwig_von_Mises
.. _human_action:  https://smile.amazon.com/Human-Action-Ludwig-Von-Mises/dp/1614273545/ref=sr_1_1?crid=3LKBTND5V3SBN&keywords=mises+human+action&qid=1658523289&sprefix=mises+human+action%2Caps%2C258&sr=8-1
.. |human_action| replace::  *Human Action*

These days I often run across authors or speakers who use words and phrases
that sound really good, enticing even---words that make you react with, "Ooh, I
like that"---but have no concrete meaning.  The deceptive power of such
communication is you allow the reader or listener to define the terms however
they like, such that they agree with you.  Though you may be using the same
vocabulary, you might not be using the same dictionary.  Your best defense
against this will be to cultivate the habit of asking, "What do you mean by
that?"  If the response you get doesn't really clarify things, keep asking.  If
the person you're speaking with is actually interested in a meeting of the
minds, it's in their best interest to be as specific as possible such that the
two of you actually understand each other.  If not, then perhaps it's worth
pointing out that they're not really saying anything.

.. _Arthur Schopenhauer:  https://en.wikipedia.org/wiki/Arthur_Schopenhauer
.. _Young Hegelians:  https://en.wikipedia.org/wiki/Young_Hegelians
.. _Old Hegelians:  https://en.wikipedia.org/wiki/Right_Hegelians

Another place we see a good deal of ambiguity is in argumentation.  A
compelling argument actually has `six components`_, but we'll just look at the
first two here:  the *claim*, which is the truth statement being asserted, and
the *grounds*, which is the information given in support of the claim.  More
often than not, the grounds are simply omitted.  Someone will levy an
accusation against you, but then when no support for the claim is given, the
implied grounds are "because I said so."  Even if grounds happen to be present,
much of the time it's also difficult to ascertain what the claim actually is.
"You done me wrong!"  Okay, what do you mean by that?  "I mean you're being
overly negative."  Okay, how so?  When you find yourself in such a scenario,
try to get definitive answers to the following questions:

.. _six components:  https://newmexicoforensicsclub.readthedocs.io/en/latest/training/crafting-an-argument.html

* What have I done wrong?  (Can the actual infraction be stated specifically?)
* Where and when did I do it?  (Can you point to a particular sentence,
  phrasing, tone, look, etc., and say, "This was where you messed up"?)
* Why do you think it was wrong?  (What is your ethic, and does it line up with
  the three Rs?)
* What would have been an appropriate action in its place?  (Are you willing to
  try to correct my behavior, or are you just here to yell at me?)

A specific situation you may run into is being accused of having said or done
something that caused a negative emotional response in someone.  You're already
well aware of the problems underlying such an accusation, but when dealing with
it one of the first things you need to do is determine if someone has actually
been hurt or offended, or if that person is only hypothetical.  For instance,
does the use of the term "master branch" in a software engineering context
actually cause emotional grief in someone whose ancestors were brought to this
continent as slaves?  Or is such a person just a possibility?  If the one
offended is real, then deal with them directly and work through whatever the
issue is; if hypothetical, then require the accuser to either produce an
example or retract the accusation.  As long as claims remain vague and
unsubstantiated, it's too easy to be bullied into submission---don't fall for
it.

Request Transparent Communication
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

As you read through the examples of how these worldviews can play out, you
might've noticed that oftentimes things take a turn for the worse when
communication happens one-on-one and in private.  That's not always the case,
and all-to-all communication in public isn't the right thing to do in all
scenarios, but the frequency with which these worldviews silence people
suggests that we should try to make communication transparent wherever
appropriate.

What I don't mean by this is the next time you have a disagreement with a
fellow believer you go air it before the entire congregation before trying to
work it out with them directly.  Don't do that (|MAT.18.15-MAT.18.17|).  What I
do mean is you should discern whether the conversation involves or impacts more
than just the two of you, and then bring others in as appropriate.  For
instance, concerns about the pastoral candidate impact the entire search
process, so the discussion should be with the whole search team.  Whether the
small group has been appropriate or off the rails in their online forum posts
impacts the entire group, so the discussion should involve more than just the
person raising the concern.

If the situation justifies a larger discussion, though, what do you do?
Politely say something along these lines:  "Given the nature of this issue and
the others involved with or impacted by it, I suspect a larger discussion would
be more beneficial than us talking one-on-one.  Can we find a time for [insert
people here] to sit down together and talk it through?"  Where you go from
there will depend on the response.  If the person you're talking with refuses,
politely ask them their reasons for thinking a private conversation is
appropriate and a more open one is not.  They may have valid reasons of which
you're unaware, but they should be able to relay those reasons to you.

If no rationale is given and a private conversation is still demanded, trust
your gut.  You may be able to request that your pastor accompany you in the
private conversation, because if there's a problem with you, they're the one
responsible for overseeing the correction of your behavior or attitude.
Alternatively, as wisdom dictates, you might consider agreeing to a private
conversation initially, if you can get the other person to agree ahead of time
to a broader discussion if the concerns aren't satisfactorily addressed.  If
either of these don't work, again ask for a justification, and if none is
given, retreat, regroup, and reconsider what your next move should be.

Rely on Your Community
~~~~~~~~~~~~~~~~~~~~~~

Last, but certainly not least, understand that you're not a maverick out doing
battle against the forces of evil on your own.  You're a part of the body of
Christ---both your local congregation, and the church universal---in which God
has uniquely gifted and outfitted each of us to serve in different roles.
Learn to rely on one another in our various areas of expertise.  Rely on the
scouts, who can see the enemy coming from a long way off.  Rely on the
swordmasters, who can help you improve your proficiency.  Rely on your
squadmaster, who can marshal a group of you for a particular mission.

What do I mean by this?  For the individual, take a candid look at yourself and
assess your strengths and weaknesses.  Where you are weak, seek out others who
are strong and develop those relationships.  Where you are strong, ask God to
put you in relationship with those you could support and bless with the gifts
he's given you.  For the body of believers, figure out what each other's
strengths are and learn to rely on each other in those areas.  For instance,
when one gifted in administration sees potential logistical problems, see what
they can contribute to help shore things up.  When one gifted in discernment
perceives something awry, tap the brakes, and see if there's anything to the
intuition.  The work of the church is meant to be done by the body of
believers, not only by its leadership.

Such thinking means rebelling against the radical individualism that has
characterized our age.  It means devoting your time and skills to something
other than your own pleasure.  It means intentionally investing in deep
relationships in community, though there's potential for pain and heartache.
It requires vulnerability, as it means keeping yourself open to correction from
your brothers and sisters, because---believe it or not---you might be fooled by
the next incarnation of one of these ungodly ideologies too, as I was for so
many years.  Don't worry; we've got your back, and we need you to have ours.
We can fight this fight together.

Conclusion
==========

So why on earth did God put us through the last few years of pain and heartache
within the church?  To wake us up, both to the reality of what's going on in
the world around us, and to what's required of us as believers in the midst of
it.  It's easy to look around today and think that the world has gone
absolutely crazy in only a few short years, but to think that is to
misunderstand the situation completely.  We are now simply reaping the fruit of
the ideas sown over the last many centuries.  It's also tempting to think we
can turn things around relatively quickly (e.g., with the next election cycle,
by speaking up at school board meetings, etc.).  That we would do so is
actually one of my major concerns, as that would allow us to kick back and
relax, thinking we'd avoided impending disaster.  What was built over the
course of generations cannot be undone overnight.

In a sense, the problems we face today are no different than the ones we've
been facing since the fall.  One way or another it boils down to us trying to
remove God from his proper place and insert ourselves in it.  The good news,
then, is the solution's not new either.  We must allow God's truth to permeate
and direct every thought we think, every word we speak, and everything we do.
One day we'll stand before God and be held accountable for how we conducted
ourselves.  We can thank him in advance for his grace and mercy.

Fellow believers, we have lost our ability to think.  If we don't regain it, we
can't hope to stand firm for truth.  Let me conclude with this:

.. raw:: html

   <div style="border: 2px solid ForestGreen;
               border-radius: 5px;
               padding: 10px;
               background-color: HoneyDew;">
       <div data-gb-widget="passage" data-passage="EPH.6.10-EPH.6.20">
       </div>
   </div>
   <br>

Stand firm, brothers and sisters.  Never give up; never surrender.  `Live not
by lies`_.

.. _Live not by lies:  https://www.solzhenitsyncenter.org/live-not-by-lies

.. raw:: html

   <script src="https://bibles.org/static/widget/v2/widget.js"></script>
   <script>
       GLOBALBIBLE.init({
           url: "https://bibles.org",
           bible: "f421fe261da7624f-01",
           autolink: false,
       });
   </script>

