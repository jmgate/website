Where Are We Today?
-------------------

.. raw:: html

   <p style="text-align:left;">
       <i>January 29, 2021</i>
       <span style="float:right;">
           🕑 11 min.
       </span>
   </p>

.. note::

   The following was written to update a few friends on how God has been
   leading my family and me over the past year.  By request, it has been
   adapted for wider distribution.

I was concerned in the summer of 2020 when I started hearing critical
theory—though I had not yet heard the term—coming from various church
leaders in my local community, across our country, and around the world.  At
that time, I knew something was wrong at a gut level, but didn’t yet have the
words to express, or research to back up, my gut reaction, other than, “A war
is being waged over the hearts and minds of men.”  (I’m pretty sure
that’s Gandalf, though I can’t find the exact quote at the moment.)

It’s tempting to think that culture has changed radically in the last decade
or so, but what we’re seeing in the world today is really the logical
conclusion of the last few centuries of philosophical progression.  Indeed,
some thoughts even trace their lineage back to antiquity, and others, of
course, back to the garden of Eden.  Critical theory, which really woke me up
to the problem, is only one component of the cultural transformation.  Others
include pandemic response, economic planning activities, election interference,
media communications, and so on.  Regardless of the application space, the
characteristic that unites these is the inability for dissenting opinions to be
voiced, let alone heard.  Most of the time, you’re not even able to ask the
question.  Where are we today?  I’ll borrow and augment a phrase from Michael
O’Fallon and say the war for epistemology is well underway, what’s at stake
is the future of the Christian church and western civilization as we know it,
and much of the church unknowingly finds itself on the wrong side.

That’s a bold statement, I know, so before I go on, let me clarify some of
what I mean.  I’m not saying God’s not in control.  I am supremely
confident that God will accomplish all his holy will.  That said, I am fully
aware that at no time has he promised to preserve for us any particular way of
life, either for the church or for our country.  Am I afraid of what’s going
on in the world today?  Concerned, but not afraid, for history tells us that
the church experiences the most substantial growth, both in terms of new souls
justified and lives further sanctified, in times of persecution.  I eagerly
look forward to how God grows his people in the rest of my lifetime, but I know
it won’t necessarily be pleasant.

What do we need to do?  I’ve heard a number of people saying things like,
“We just need to have faith and focus on the gospel.”  The mental picture
that comes to mind when I hear that is of a squad of swordsmen in the midst of
battle.  Beleaguered by enemy forces (though they don’t realize it), their
squadmaster encourages them, shouting, “Hold on to your swords, men!”  You
look at the squad and realize that’s not the most useful advice.  One guy has
his hand on the hilt, but hasn’t drawn it yet.  Another’s resting his hand
on the pommel, using it as a walking stick.  A third, the most eager of the
group, is holding it like a baseball bat, but he’s holding it backwards,
grasping the blade near the point, as blood runs down his hands from
self-inflicted wounds.  A fourth has faith in the countless hours he’s logged
as a swordsman in a video game, though he doesn’t even know how to grip his
weapon correctly yet.  None of these men have been trained in swordsmanship,
let alone battle, and their squadmaster doesn’t realize the precarious spot
they’re in.

So what do we need to do?  I want to be really careful here, because I never
want to be misunderstood for arguing that the world needs God plus something,
the Bible plus something, the gospel plus something.  We need the Bible, yes,
but we need the truths of scripture to permeate and direct every nook and
cranny of our thoughts, words, and actions:  we need a biblical worldview.
Various polls over the last few decades have warned of the decreasing
percentages of Christians holding a biblical worldview.  One from ~5 years ago
indicated only ~40% of evangelicals held to it; one from last year said that
number was down to ~20%.  Restrict the respondents to church leaders and you
don’t get numbers that are too much higher.  We need to realize that all
truth is God’s truth, and that God’s truth is total truth impacting all
areas of our lives.  We can no longer allow the secular/spiritual,
public/private, fact/value split to exist in the minds of believers.

How did we get here?  That’s a rather long story that I’ll save for another
time, though I’ll happily point you to a number of resources to find out.  My
wife says I have a tendency to give too much homework, so I’ll try (mostly
unsuccessfully) to restrain myself and only ask that you do two things in the
near term:  Read |live_not_by_lies|_, by Rod Dreher, and
|expository_apologetics|_, by Voddie Baucham Jr.  The first will give you an
indication of where we are today and where we’re headed, while the second
answers, “What do we do about it?”  In addition to the two books, set aside
an hour every other day over the next three weeks to listen to the following
lectures:

.. _live_not_by_lies:  https://smile.amazon.com/Live-Not-Lies-Christian-Dissidents/dp/0593087399/ref=sr_1_1?dchild=1&keywords=live+not+by+lies&qid=1608493936&sr=8-1
.. |live_not_by_lies| replace::  *Live Not By Lies*
.. _expository_apologetics:  https://smile.amazon.com/Expository-Apologetics-Answering-Objections-Power/dp/1433533790/ref=sr_1_1?crid=D9912ZLYPD18&dchild=1&keywords=expository+apologetics+voddie+baucham&qid=1608494186&sprefix=expository+apol%2Caps%2C216&sr=8-1
.. |expository_apologetics| replace::  *Expository Apologetics*

* `Reflexivity`_ | Michael O’Fallon
* `Diversity, Inclusion, Equity`_ | James Lindsay
* `Critical Race Theory and Christianity`_ | Tom Ascol
* `The Grievance Gospel`_ | Josh Buice
* `The Great Reformation`_ | Michael O’Fallon
* `The Problem is Enmity, Not Ethnicity`_ | Darrel B. Harrison
* `The Road to Serfdom`_ | Rod Martin
* `Woke Hermeneutics`_ | Tom Buck
* `Overcoming the Sins of Our Fathers`_ | John Connell
* `Social Justice vs Authentic Biblical Justice`_ | Phil Johnson

.. _Reflexivity:  https://youtu.be/0rNXyd5A4ho
.. _Diversity, Inclusion, Equity:  https://youtu.be/3jLNgLABuTw
.. _Critical Race Theory and Christianity:  https://youtu.be/jO7wlm25uzU
.. _The Grievance Gospel:  https://youtu.be/5YyubxG8OFI
.. _The Great Reformation:  https://youtu.be/POdML5RLI_U
.. _The Problem is Enmity, Not Ethnicity:  https://youtu.be/xBNNC6y_ojY
.. _The Road to Serfdom:  https://youtu.be/CFBcTc37j8I
.. _Woke Hermeneutics:  https://youtu.be/Mf_GfIZI46w
.. _Overcoming the Sins of Our Fathers:  https://youtu.be/8PZVWm-tc_M
.. _Social Justice vs Authentic Biblical Justice:  https://youtu.be/i-5CkaqCHkU

These fairly concisely summarize much of what I’ve been learning over the
last many months.  The focus is on the critical theory side of things, but
occasionally they branch out into some of the history and broader context.  I
was glad to have stumbled upon these lectures late in the game, after having
come to the conclusions myself before hearing them.  How did I come to these
same conclusions?  That’s what the rest of the reading list is for:

* |the_law|_, by Frédéric Bastiat (1850):  An examination of the meaning and
  implications of the phrase “law is justice”.
* |on_liberty|_, by John Stuart Mill (1859):  An examination of the
  relationship between authority and liberty.
* |the_communist_manifesto|_, by Karl Marx and Friedrich Engels (1888):  “The
  history of all hitherto existing society is the history of class struggles.”
* |the_road_to_serfdom|_, by F.A. Hayek (1944):  A clarion call to the
  socialists of all parties in England that the cultural trends the author was
  seeing there closely paralleled what he’d observed in Germany and Austria
  before the rise of the National Socialist (Nazi) party.  76 years later, it
  reads like it was written to the world yesterday.
* |the_twelve_days_of_the_aspen_executive_seminar|_, by Mortimer Adler (1972):
  A brief history of the political and economic questions facing the world
  today, which have been hotly debated since well before this country was
  founded.
* |a_conflict_of_visions|_, by Thomas Sowell (1987):  The inability for people
  to effectively dialogue on the issues of the day stems from the paradigmatic
  disconnect between diametrically opposed visions of human nature.
* |the_consequences_of_ideas|_, by R.C. Sproul (2000):  A whirlwind overview of
  western political thought from antiquity to today.
* |political_visions_and_illusions|_, by David Koyzis (2003):  A walk through
  the counterfeit redemptive narratives of the most prevalent ideologies of our
  day, supporting the author’s thesis that “ideology is idolatry”.
* |total_truth|_, by Nancy Pearcey (2004):  A rich historical examination of
  how the church inadvertently pushed itself to where it is today, combined
  with exhortations on what to do about it.
* |expository_apologetics_2|_, by Voddie Baucham Jr (2015):  A framework for
  approaching the defense of the faith by identifying, analyzing, and then
  refuting counterfeit worldviews.
* |live_not_by_lies_2|_, by Rod Dreher (2020): A warning that the cultural
  circumstances in the world today bear remarkable similarities to various
  societies over the last century before their embrace of totalitarianism, and
  recommended practices from Christians who weathered the storm and kept the
  faith alive.

.. _the_law:  http://gutenberg.org/ebooks/44800
.. |the_law| replace::  *The Law*
.. _on_liberty:  https://smile.amazon.com/Liberty-Dover-Thrift-Editions/dp/0486421309/ref=sr_1_1?crid=168FAIGKUIF4T&dchild=1&keywords=on+liberty+john+stuart+mill&qid=1608494166&sprefix=on+liberty+john%2Caps%2C309&sr=8-1
.. |on_liberty| replace::  *On Liberty*
.. _the_communist_manifesto:  https://smile.amazon.com/Communist-Manifesto-Karl-Marx/dp/0717802418/ref=sr_1_1?crid=33O2JVHU31BLS&dchild=1&keywords=the+communist+manifesto&qid=1608494137&sprefix=the+communist+%2Caps%2C216&sr=8-1
.. |the_communist_manifesto| replace::  *The Communist Manifesto*
.. _the_road_to_serfdom:  https://smile.amazon.com/Road-Serfdom-Documents-Definitive-Collected/dp/0226320553/ref=sr_1_1?dchild=1&keywords=the+road+to+serfdom&qid=1608494062&sr=8-1
.. |the_road_to_serfdom| replace::  *The Road to Serfdom*
.. _the_twelve_days_of_the_aspen_executive_seminar:  http://cyberspacei.com/greatbooks/h2/speak_listen/sp_listen_016.htm
.. |the_twelve_days_of_the_aspen_executive_seminar| replace::  *The Twelve Days of the Aspen Executive Seminar*
.. _a_conflict_of_visions:  https://smile.amazon.com/Conflict-Visions-Ideological-Political-Struggles/dp/0465002056/ref=sr_1_2?crid=1EHM3X1GVM8ZJ&dchild=1&keywords=a+conflict+of+visions+thomas+sowell&qid=1608494089&sprefix=a+conflict+of+visions%2Caps%2C206&sr=8-2
.. |a_conflict_of_visions| replace::  *A Conflict of Visions:  Ideological Origins of Political Struggles*
.. _the_consequences_of_ideas:  https://smile.amazon.com/Consequences-Ideas-Redesign-Understanding-Concepts/dp/1433563770/ref=sr_1_2?dchild=1&keywords=the+consequences+of+ideas+rc+sproul&qid=1608494211&sr=8-2
.. |the_consequences_of_ideas| replace::  *The Consequences of Ideas:  Understanding the Concepts that Shaped Our World*
.. _political_visions_and_illusions:  https://smile.amazon.com/Political-Visions-Illusions-Contemporary-Ideologies/dp/0830852425/ref=sr_1_1?crid=387ZNX45ZYQTZ&dchild=1&keywords=political+visions+and+illusions+david+koyzis&qid=1608493977&sprefix=political+visions+and+i%2Caps%2C291&sr=8-1
.. |political_visions_and_illusions| replace::  *Political Visions and Illusions:  A Survey & Christian Critique of Contemporary Ideologies*
.. _total_truth:  https://www.amazon.com/Total-Truth-Study-Guide-Christianity/dp/1433502208/ref=sr_1_1?crid=I7JJWRY1GM7C&dchild=1&keywords=total+truth+nancy+pearcey&qid=1606515393&sprefix=total+truth+na%2Caps%2C203&sr=8-1
.. |total_truth| replace::  *Total Truth:  Liberating Christianity from Its Cultural Captivity*
.. _expository_apologetics_2:  https://smile.amazon.com/Expository-Apologetics-Answering-Objections-Power/dp/1433533790/ref=sr_1_1?crid=D9912ZLYPD18&dchild=1&keywords=expository+apologetics+voddie+baucham&qid=1608494186&sprefix=expository+apol%2Caps%2C216&sr=8-1
.. |expository_apologetics_2| replace::  *Expository Apologetics:  Answering Objections with the Power of the Word*
.. _live_not_by_lies_2:  https://smile.amazon.com/Live-Not-Lies-Christian-Dissidents/dp/0593087399/ref=sr_1_1?dchild=1&keywords=live+not+by+lies&qid=1608493936&sr=8-1
.. |live_not_by_lies_2| replace::  *Live Not By Lies:  A Manual for Christian Dissidents*

And then these two are on deck for me:

* |law_legislation_and_liberty|_, by F.A. Hayek (1973-1979)
* |strong_and_courageous|_, by Jared Longshore and Tom Ascol (2021)

.. _law_legislation_and_liberty:  https://smile.amazon.com/Law-Legislation-Liberty-statement-principles/dp/0415522293/ref=sr_1_2?crid=17AWNN5ZC08DU&dchild=1&keywords=law+legislation+and+liberty+hayek&qid=1611853480&sprefix=law+legis%2Caps%2C234&sr=8-2
.. |law_legislation_and_liberty| replace::  *Law, Legislation and Liberty*
.. _strong_and_courageous:  https://press.founders.org/shop/strong-and-courageous/
.. |strong_and_courageous| replace::  *Strong and Courageous:  Following Jesus Amid the Rise of America's New Religion*

I’m ashamed it’s taken me so long to wake up to where we are in the world
today.  From a young age, growing up in a multicultural environment, I was
trained well in apologetics, particularly with respect to how Christianity
relates to other world religions.  That informed much of my growth,
particularly from middle school through college.  I even applied to some
seminaries for postgraduate studies, but wound up setting that aside for fear
that such a path would just give me a bigger hammer with which to bludgeon my
opponents, who were usually within the church.  My relative lack of maturity
meant I approached the defense of the faith from the wrong perspective.

In response to that, God started taking us on a journey emphasizing the
importance of how he’s designed us for intimate community.  Some of our
thinking here often paralleled questions a friend has asked numerous times:
“How does our head knowledge translate into heart/hand
knowledge-in-action?”  I had plenty of experience, both in myself and in the
church, with people intellectually assenting to the correct things, but then
not seeing the impact the kingdom of God should be having in our lives and on
the world around us.  The pendulum started swinging in the direction of
“it’s less important what you believe than how you live that out.”

Gracefully, in the past few years, God has pulled us back to center, uniting
the two sides of the journey that were seemingly at odds with each other.  What
you believe is of the utmost importance.  How you live that out is of the
utmost importance.  The connection that weds the two is your worldview.  To
whatever extent we see believers not living out every aspect of their lives in
accordance with the truths of scripture, the disconnect is due to holding one
or more logically inconsistent ideologies, behind which lie evil spiritual
strongholds, in tension with biblical truth.

Through the circumstances of the past few years, and a small mountain of books
to go along with them, God has revealed to us his calling on my life:  To
prepare his people for the current and coming spiritual warfare by training
them in the “martial” arts of worldview analysis, logical argumentation,
and effective communication.  I’ve been working this calling out in building
up a local club geared toward just that.  It’s encouraging to see people
analyzing the worldview implications of things like The Eagles’ “Hotel
California” or concepts like the depravity of man.  How exactly this develops
in the future remains to be seen; we’re just taking one step of faith at a
time.

Can one be held accountable for what one did not know?  That’s a fun
philosophical question to ponder.  Regardless, this letter is my attempt to
open the door for you and point you in the right direction such that you can
know what’s going on and what our responsibility is in the midst of it.  One
day we’ll stand before God and be held accountable for how we led those he
gave us to lead in this time.  We can thank him ahead of time for his grace and
mercy.  Do I expect everyone to have the same response we did?  No.  How
exactly God wants to use you in this time is up to him.  Should I have written
this months ago?  Possibly, but I wanted to make sure my thoughts were
well-measured and I wasn’t speaking out of a charged emotional state.  Months
ago there was too much frustration; today there’s hope for a painful, but
ultimately bright, future.

