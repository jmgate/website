Jason M. Gates
--------------

* Husband
* Father
* :doc:`Reader <books>`
* :doc:`Thinker <thoughts>`
* :doc:`DevOps Engineer <resume>`
* `Homeschool Speech & Debate Coach <https://newmexicoforensicsclub.readthedocs.io>`_

.. toctree::
   :hidden:

   books
   thoughts
   resume
   notes

