import sys
from pathlib import Path

sys.path.append(str(Path("_ext").resolve()))

master_doc = "index"
project = "Jason M. Gates"
copyright = "2023–2024, Jason M. Gates"
author = "Jason M. Gates"
version = release = ""
html_logo = "../images/headshot.png"
extensions = [
    "sphinx.ext.todo",
    "sphinx_rtd_theme",
    "sphinx_tabs.tabs",
    "odysee",
    "piped",
]
html_theme = "sphinx_rtd_theme"
#html_theme = "classic"
html_theme_options = {
    "navigation_depth": -1,
}
todo_include_todos = True

