A Practical Guide to Culture
----------------------------

*Helping the Next Generation Navigate Today's World*

* Authors:  John Stonestreet and Brett Kunkle
* Publication Year:  2019
* Link:  https://smile.amazon.com/Practical-Guide-Culture-Generation-Navigate/dp/0830781242/ref=sr_1_1?keywords=a+practical+guide+to+culture&qid=1640624413&s=books&sprefix=a+practical+guide+%2Cstripbooks%2C175&sr=1-1
* Started on:  2021-12-17
* Finished on:  2021-12-27

Questions
=========

1. John's most recent book is intended for Christian leaders and parents as
   they equip the next generation for cultural engagement.  The book is neatly
   organized.  Read through the contents and write out the titles of the four
   main sections.  Note the subsection titles and how they relate to the
   section theme.  Why do you think Part Four was included?

   Part One:  Why Culture Matters

     1. What Culture Is and What It Does to Us
     2. Keeping the Moment and the Story Straight
     3. A Vision of Success

   Part Two:  A Read of the Cultural Waters

     4. The Information Age
     5. Identify After Christianity
     6. Being Alone Together
     7. Castrated Geldings and Perpetual Adolescence

   Part Three:  Pounding Cultural Waves

     8. Pornography
     9. The Hookup Culture
     10. Sexual Orientation
     11. Gender Identity
     12. Affluence and Consumerism
     13. Addiction
     14. Entertainment
     15. Racial Tension

   Part Four:  Christian Worldview Essentials

     16. How to Read the Bible
     17. How the Trust the Bible
     18. The Right Kind of Pluralism
     19. Taking the Gospel to the Culture.

   If Parts One through Three introduce you to the problem and convince you of
   its significance, Part Four is there to answer the, "Now what do I do about
   it?" question.

2. Part One:  Summarize the concept "culture" and what impact it has on the
   individual.

   Culture is the environment humans make for themselves to live in; it's what
   we think is normal.  We (mostly passively) shape it, and it actively shapes
   us.

3. Part Two:  In a sentence or two, summarize how each of the "undercurrents"
   of culture in the subsections impact the individual.

   The Information Age
     Kids these days are inundated with information, but aren't trained with
     how to evaluate the ideas that bombard them.  The media through which
     information is delivered are not neutral, but shape what and how we think.
   Identity After Christianity
     When we jettisoned Christianity, we lost what it means to be human.  In
     the ensuing search for meaning, we seek to place self, sex, science,
     stuff, or the state in the place of God.
   Being Alone Together
     The ever-presence of information technology in our lives gives us the
     false impression that we don't need physical community in our lives, that
     digital community is sufficient; however, buying this lie tends to
     convince us that (1) we're the center of our own universe, (2) we deserve
     to be happy all the time, (3) we must have choices, (4) we are our own
     authorities, and (5) information is all we need, not teachers.
   Castrated Geldings and Perpetual Adolescence
     There was a time when there were two groups in society:  children and
     adults.  In the middle of the 20th century, teenagers emerged as a
     distinct group, and in the intervening decades this middle group of those
     who haven't matured has expanded to the age of 30 or so.  The antidote for
     this perpetual adolescence is to cultivate virtue in young people and
     actively raise them into adulthood.

4. Part Three:  Which ones among the cultural issues discussed really cought
   your attention?  Are the "Action Steps" particularly helpful for you?

   Pornography
     Having been rescued from addiction to pornography myself, I was surprised
     that none of the action steps involved growing up to be the man or woman
     God's made you to be.  That was the turning point for me.
   Addiction
     As one who's been saved from certain addictions in the past, and still
     struggles with some in the present, I was disappointed that this chapter
     was so narrowly focused on drugs and alcohol.  It'd be worthwhile to
     broaden it and ask people if they're addicted to coffee, sugar,
     carbohydrates, etc., plus a number of the other chapters in this part of
     the book.
   Racial Tension
     It feels like there's an unspoken assumption that if your circle of
     friends isn't ethnically diverse, then you're racist (I'm exaggerating it
     to make a point).  When I lived in the United Arab Emirates, my friends
     came from here, there, and everywhere across the globe.  As I live now in
     Albuquerque, NM, the people I regularly interact with are largely white
     and New Mexican.  The diversity of the groups of people I interact with is
     representative of the diversity of the local population.  Such a
     possibility was overlooked in the treatment of this chapter.
