Mere Christianity
-----------------

* Author:  C.S. Lewis
* Publication Year:  1952
* Link:  https://smile.amazon.com/Mere-Christianity-C-S-Lewis/dp/0060652926/ref=sr_1_1?dchild=1&keywords=mere+christianity&qid=1622568911&sr=8-1
* Started on:  2021-06-27
* Finished on:  2021-07-22

..
        What Kind of Book is This?
        ==========================

        What is the Book About?
        =======================

How is the Book Organized?
==========================

Book 1.  Right and Wrong as a Clue to the Meaning of the Universe

  The existence of the Law of Human Nature, which all men fundamentally agree on,
  implies there is a mind beyond the physical universe, which called the universe
  into being and demands we behave in a certain way.  We know we are unable to
  keep the law fully, so we have put ourselves at odds with that mind, which is a
  serious problem.

  1. The Law of Human Nature

     We all operate as if we agree on the Law of Human Nature, and none of us are
     really keeping it.

  2. Some Objections

     The Moral Law is different from mere instinct and is not mere social
     convention but real truth.

  3. The Reality of the Law

     When considering inaminate objects, you have only the facts; however, with
     humans you have both the facts (how men behave) and something else (how they
     ought to behave).  That something else is a real law, which we haven't made,
     pressing in on us, and our inability to keep it yields consequences.

  4. What Lies Behind the Law

     The universe can be viewed through either a materialist lens, in which case
     matter is all that existed and what we observe is essentially a fluke, or
     through a religious lens, in which case there is something behind the
     universe that is more like a mind than anything we know.  If such a mind
     existed, it would be beyond the powers of observation alone, but we could
     expect it to show itself inside ourselves as an influence trying to get us
     to behave in a certain way.

  5. We Have Cause to be Uneasy

     If the universe is not governed by an absolute goodness, then all our
     efforts are in the long run hopeless.  But if it is, then we are making
     ourselves enemies to that goodness every day, and are not in the least
     likely to do any better tomorrow, and so our case is hopeless again.

Book 2.  What Christians Believe

  God made the universe and it was good.  He gave us free will to choose good,
  which came with the freedom to choose evil, and we chose that instead.  That
  fundamentally broke creation, but it somehow remembers what it was meant to be.
  Christ came to be the human life run fully on God as intended, such that his
  death could somehow make right what we messed up, and our call is to spread his
  life to others.

  1. The Rival Conceptions of God

     Pantheists believe God animates the universe, while Christians believe God
     made it.

  2. The Invasion

     We have a universe that contains much that is obviously bad and apparently
     meaningless, but contains creatures like ourselves who know that it is bad
     and meaningless.  Christians believe this is a good world that's gone wrong,
     but still remembers what it's supposed to be.  Dualists believe there are
     two independent and opposite powers warring it out here, but the
     "independent and opposite" notions falls apart on further examination.
     Rather this is a civil war or rebellion, and we are living in enemy-occupied
     territory.

  3. The Shocking Alternative

     The moment you have a self at all, there is a possibility of putting
     yourself first---wanting to be the center---wanting to be God, in fact.  God
     designed the human machine to run on himself, but when we try to run without
     him, the machine breaks down.  Jesus arrives in history claiming to be the
     mind beyond the universe.

  4. The Perfect Penitent

     The central Christian belief is that Christ's death has somehow put us right
     with God and given us a fresh start, and we can accept this without knowing
     how it works.  Fallen man is not simply an imperfect creature who needs
     improvement; he is a rebel who must lay down his arms.  Jesus could
     surrender his will, and suffer and die, because he was man; and he could do
     it perfectly because he was God.

  5. The Practical Conclusion

     Baptism, believe, and the Lord's supper somehow spread the Christ-life to
     us.

Book 3.  Christian Behavior

  Questions of morality ultimately boil down to the question of the purpose of
  man's existence, which is why they wind up being so contentious.  If we life
  the way we were designed to function, it will go better both for us as
  individuals, and for society as a whole.

  1. The Three Parts of Morality

     Every moral rule is there to prevent a breakdown in the running of the
     machine, and every moral failure is going to cause trouble to both you and
     others.  Morality is concerned with (1) fair play and harmony between
     individuals, (2) tidying up or harmonising the things inside each
     individual, and (3) the general purpose of human life as a whole.  We can
     all cooperate in the first, disagreements begin with the second, and become
     more serious with the third.

  2. The 'Cardinal Virtues'

     Prudence
       Practical common sense; taking the trouble to think out what you are doing
       and what is likely to come of it.
     Temperence
       Not abstaining, but going the right length and no further.
     Justice
       The old name for everything we should now call "fairness," which includes
       honesty, give and take, truthfulness, keeping promises, and all that side
       of life.
     Fortitude
       Both kinds of courage---the kind that faces danger as well as the kind
       that "sticks it" under pain.

  3. Social Morality

     Scripture was never meant to replace or supersede human arts and sciences;
     rather it is a director that will set them to the right jobs, and energizer
     that will give them new life.  We won't have a Christian society until most
     of us really want it.  We can't carry out the golden rule until we love our
     neighbors as ourselves, which we can't do until we love God, which means
     obeying him.

  4. Morality and Psychoanalysis

     Psychoanalysis is concerned with giving man better raw materials for making
     his choices; morality is concerned with the choices themselves---the free
     choice of man, on the material presented to him.  With every choice you are
     changing the central part of you into either a more heavenly or more hellish
     creature.  As you get better, you better understand the evil that is still
     left in you; as you get worse, you understand it less.

  5. Sexual Morality

     The choice of marriage, with total faithfulness, or complete abstinence is
     so difficult and against our natures that either Christianity is wrong or
     our sexual instinct has gone wrong.  It's neither the case, as we're led to
     believe, that the sexual desires we experience are natural and healthy, or
     that the Christian path is impossible.

  6. Christian Marriage

     The monstrosity of sexual intercourse outside marriage is that those who
     indulge in it are trying to isolate one kind of union (the sexual) from all
     the other kinds of union which were intended to go along with it and make up
     the total union.  Those who are in love have a natural inclination to bind
     themselves by promises, so the Christian law is not forcing upon the passion
     of love something which is foreign to that passion's own nature; it is
     demanding that lovers should take seriously something which their passion of
     itself impels them to do.  Love, as opposed to "being in love," is a deep
     unity, maintained by the will and deliberately strengthened by habit.

  7. Forgiveness

     Loving my neighbor (or enemy) as myself doesn't mean liking them or thinking
     them nice, but it does mean forgiving them as we do ourselves, choosing to
     look past their prior mistakes and assume the best of them.  The something
     inside us that feels resentment must be killed, and we do so with God's help
     through practice, forgiving first in small things, and building up to bigger
     things.

  8. The Great Sin

     Pride leads to every other vice; it is the complete anti-God state of mind.
     It means enmity, both between you and someone else, and between you and God.
     It is spiritual cancer.

  9. Charity

     Charity, meaning "love in the Christian sense," is a state of will, rather
     than feelings, which we naturally have about ourselves, and must learn to
     have about others.  Good and evil both increase at compound interest, so the
     little decisions we make every day are of the utmost importance.  Ask
     youself, "What would it look like if I loved [my neighbor, or God]," and go
     and do it.

  10. Hope

      I must keep alive in myself the desire for my true country, which I shall
      not find till after death; I must never let it get snowed under or turned
      aside; I must make it the main object of life to press on to that other
      country and to help others to do the same.

  11. Faith

      Faith, in our context, is accepting and regarding as true the doctrines of
      Christianity.  A battle is underway within each of us between faith and
      reason on one side and emotion and imagination on the other.  We must train
      the habit of faith and continually remind ourselves of what we believe.

  12. Faith

      "Work out your own faith with fear and trembling, for it is God who worketh
      in you."  If you are right with him, you will inevitably be right with all
      your fellow creatures.

Book 4.  Beyond Personality:  or First Steps in the Doctrine of the Trinity

  God is three persons in one being.  He wants us to share in the life he has had
  in himself since before time began.  To do so is to deny our own desires and
  wills, and instead take on his.  It's hard, but he helps us to do it, and
  intends for us to help others do the same.

  1. Making and Begetting

     Theology is practical, especially now.  A great many of the ideas about God
     which are trotted out as novelties today are simply the ones which real
     theologians tried centuries ago and rejected.  To beget is to become the
     father of; to create is to make.  When you beget, you beget something of the
     same kind as yourself.

  2. The Three-Personal God

     The whole purpose for which we exist is to be taken into the life of God.
     Theology is, in a sense, an experimental science.

  3. Time and Beyond Time

     God is not in time.  In that case, what we call "tomorrow" is visible to him
     in just the same way s what we call "today".  In a sense, he does not know
     your action till you have done it, but then the moment at which you have
     done it is already "now" for him.

  4. Good Infection

     I think it important to make clear how one thing can be the source, or
     cause, or origin, of another without being there before it.  The Son exists
     because the Father exists, but there never was a time before the Father
     produced the Son.  What grows out of the joint life of the Father and the
     Son is a real person, is in fact the third of the three persons who are God.
     Now the whole offer which Christianity makes is this:  that we can, if we
     let God have his way, come to share in the life of Christ.  If we do, we
     shall then be sharing a life which was begotten, not made, which always has
     existed and always will exist.  Christ is the Son of God.  If we share in
     this kind of life we also shall be sons of God.  We shall love the Father as
     he does and the Holy Ghost will arise in us.  He came to this world and
     became man in order to spread to other men the kind of life he has---by what
     I call "good infection".  Every Christian is to become a little Christ.  The
     whole purpose of becoming a Christian is simply nothing else.

  5. The Obstinate Toy Soldiers

     The natural life in each of us is something self-centered, something that
     wants to be petted and admired, to take advantage of other lives, to exploit
     the whole universe.  And especially it wants to be left to itself, to keep
     well away from anything better or stronger or higher than it, anything that
     might make it feel small.

  6. Two Notes

     God gave us free will because a world of mere automata could never love and
     therefore never know infinite happiness.  When you're talking about
     God---i.e., about the rock bottom, irreducible fact on which all other facts
     depend---it is nonsensical to ask if it could have been otherwise.  It is
     what it is, and there is an end of the matter.

  7. Let's Pretend

     Very often the only way to get a quality in reality is to start behaving as
     if you had it already.  When you are "dressing up as Christ," you will
     quickly see some way in which the charade is most apparent, and then you
     know something that you and God must work on.  You are no longer thinking
     simply about right and wrong; you are trying to catch the good infection
     from a person.  Men are mirrors, or "carriers", of Christ to other men.
     Sometimes unconscious carriers.  This "good infection" can be carried by
     those who have not got it themselves.  God looks at you as if you were a
     little Christ; Christ stands beside you to turn you into one.

  8. Is Christianity Hard or Easy?

     If you are really going to try to meet all the demands made on the natural
     self, it will not have enough left over to live on.  Laziness means more
     work in the long run.  The church exists for nothing else but to draw men
     into Christ, to make them little Christs.

  9. Counting the Cost

     God is easy to please, but hard to satisfy.  No possible degree of holiness
     or heroism which has ever been recorded of the greatest saints is beyond
     what he is determined to produce in every one of us in the end.  The job
     will not be completed in this life, but he mans to get us as far as possible
     before death.

  10. Nice People or New Men

      If Christianity is true, why are not all Christians obviously nicer than
      all non-Christians?  Our careless lives set the outer world talking, and we
      give them grounds for talking ina  way that throws doubt on the truth of
      Christianity itself.  There are many people (a great many of them) who are
      slowly ceasing to be Christians but who still call themselves by that name.
      And always, of course, there are a great many people who are just confused
      in mind and have a lot of inconsistent beliefs all jumbled up together.
      Consequently, it is not much use trying to make judgments about Christians
      and non-Christians in the mass.  It costs God nothing, so far as we know,
      to create nice things, but to convert rebellious wills cost his
      crucifixion.  A world of nice people, content in their own niceness,
      looking no further, turned away from God, would be just as desperately in
      need of salvation as a miserable world---and might even be more difficult
      to save.  What can you ever really know of other people's souls---of their
      temptations, their opportunities, their struggles?  One soul in the whole
      creation you do know, and it is the only one whose fate is place in your
      hands.

  11. The New Men

      Becoming a Christian is not mere improvement but transformation.

..
        What Problems has the Author Tried to Solve?
        ============================================

        What are the Author's Key Terms?
        ================================

        What are the Author's Leading Propositions?
        ===========================================

        What are the Author's Arguments?
        ================================

        Of the Problems Identified, Which has the Author Solved?
        ========================================================

Questions to Answer
===================

1. *This book is really a collection of four books.  What are they?*

   Book 1.  Right and Wrong as a Clue to the Meaning of the Universe
     The existence of the Law of Human Nature, which all men fundamentally agree on,
     implies there is a mind beyond the physical universe, which called the universe
     into being and demands we behave in a certain way.  We know we are unable to
     keep the law fully, so we have put ourselves at odds with that mind, which is a
     serious problem.
   Book 2.  What Christians Believe
     God made the universe and it was good.  He gave us free will to choose good,
     which came with the freedom to choose evil, and we chose that instead.  That
     fundamentally broke creation, but it somehow remembers what it was meant to be.
     Christ came to be the human life run fully on God as intended, such that his
     death could somehow make right what we messed up, and our call is to spread his
     life to others.
   Book 3.  Christian Behavior
     Questions of morality ultimately boil down to the question of the purpose of
     man's existence, which is why they wind up being so contentious.  If we life
     the way we were designed to function, it will go better both for us as
     individuals, and for society as a whole.
   Book 4.  Beyond Personality:  or First Steps in the Doctrine of the Trinity
     God is three persons in one being.  He wants us to share in the life he has had
     in himself since before time began.  To do so is to deny our own desires and
     wills, and instead take on his.  It's hard, but he helps us to do it, and
     intends for us to help others do the same.
2. *What makes this book so accessible to so many people is the origin of the
   messages.  When and for what was this content first communicated?*

   It was originally a series of lectures given over BBC radio in the midst of
   World War II.  In a time when people were naturally asking, "What is wrong
   with the world?" on a regular basis, Lewis was asked to explain to the
   people of Great Britain what it is that Christians believe.  His original
   audience was about as broad as it gets, and that, combined with his
   conversational tone (as is fitting for a radio broadcast), makes it such
   that the work has a minimal (perhaps nonexistent) barrier of entry.
3. *As you read, notice the flow of Lewis's arguments.  Highlight 10--15 key
   ideas/quotes from each of the four major sections of the book.  Share some
   of these here and with your cohort.*

   See the outline above.

4. *How has* Mere Christianity *helped you understand and articulate the
   Christian worldview?  Would you recommend the book to others?*

   I definitely appreciated Book 1 the most, as Lewis starts with no
   Christianity---no religion even---only human nature, and methodically works
   his way from there to the logical conclusion that there must be something
   outside of the natural world that caused it to come into being, that acts as
   the source of the human nature within us, and that we have fundamentally and
   irreparably offended (and we know it), and that is a very big deal.  I would
   hope non-believers would be willing to engage his logic and argumentation
   and try to figure out where he goes wrong (if he does).

   I would heartily recommend Book 1 to others; the remaining three I would
   recommend with some caveats.  I was surprised by how much Lewis was
   persuaded to view evolution as incontrovertible fact, and was disappointed
   by the extent to which that informed some of his views.  I'm also curious to
   learn how exactly he understood that one could lose one's salvation.  I
   wouldn't necessarily argue against it, but I was surprised to see it pop up
   a few times in a book on the bare-bones basics of Christianity.
