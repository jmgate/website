Refuting the New Atheists
-------------------------

*A Christian Response to Sam Harris, Christopher Hitchens, and Richard Dawkins*

* Author:  Douglas Wilson
* Publication Year:  2021
* Link:  https://www.amazon.com/Refuting-New-Atheists-Christian-Christopher/dp/1952410924/ref=sr_1_1?keywords=refuting+the+new+atheists+doug+wilson&sr=8-1

What is the Book About?
=======================

