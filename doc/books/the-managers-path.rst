The Manager's Path
------------------

*A Guide for Tech Leaders Navigating Growth & Change*

* Author:  Camille Fournier
* Publication Year:  2017
* Link:  https://smile.amazon.com/Managers-Path-Leaders-Navigating-Growth/dp/1491973897/ref=sr_1_1?dchild=1&keywords=the+manager%27s+path&qid=1622571752&sr=8-1
* Started on:  2021-06-17
* Finished on:  2021-08-13
