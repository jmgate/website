Making Sense of Your World
--------------------------

*A Biblical Worldview*

* Authors:  W. Gary Phillips, William E. Brown, and John Stonestreet
* Publication Year:  2008
* Link:  https://smile.amazon.com/Making-Sense-Your-World-Worldview/dp/1879215519/ref=sr_1_2?crid=NSMVRSO6ETN8&dchild=1&keywords=making+sense+of+your+world+a+biblical+worldview&qid=1628889565&sprefix=making+sense+of+your+world%2Caps%2C245&sr=8-2
* Started on:  2021-08-11
* Finished on:  TBD

How is the Book Organized?
==========================

Introduction

..
  * Consistent thinking on the part of Christians does not exist.  While some
    lament the nonexistence of the Christian mind, American Christianity
    charges full speed ahead in its course of incessan activity, indomitable
    individualism, and irrepressible pragmatism.  Comprehensive and coherent
    Christian thinking has never been a major part of religious life in
    America.
  * Without a well-developed philosophy of life, students are not prepared for
    the ethical and existential dilemmas they will face.  How do people become
    good people?
  * Developing a biblical worldview involves both a *mindset* and a *willset*.
    The former includes the essentials of a biblical view *of* the world.  The
    latter aspect of a biblical worldview presents the challenge to put this
    view into practice.
..

Part One:  The Difference a Worldview Makes

..
  * The tragedy of modern man is not that he knows less and less about the
    meaning of his own life, but that it bothers him less and less.
  * Despite our attempts to ignore these sorts of issues, there exists a "quiet
    desperation" that drives humanity to think about the question, "Does life
    have meaning?"
  * I just sort of accept the way the world is and then don't think about it a
    whole lot.
  * Raising the basic questions addressed by worldview inquiry is imperative in
    the face of New Age irrationality, Islamic extremism, scientific dogmatism,
    and existential sensuality.  Christianity must present an alternative that
    sweeps away stereotypes and speaks to the central longings of man's
    existence.
  * Many Christians brandish a commitment to a biblical worldview that goes
    little beyond a "moralistic therapeutic deism", believing in a God who only
    exists to enhance their personal behavior and well-being.
..

  1. When Worldviews Collide

     Major events in life tend to force us to wrestle with the ultimate
     questions of origins, meaning, morality, destiny, and identity.  Our
     answers to those questions reveal our worldview, which is a framework of
     our most basic beliefs that shapes our view *of* and *for* the world and
     is the basis for our decisions and actions.  There are many worldviews to
     choose from, and we must keep in mind that it doesn't matter whether or
     not a particular one *suits us*, but whether or not it *suits the world*;
     that is, does it match reality.  All worldviews confront the three major
     concepts of human existence---God, humanity, and nature---and it can be
     helpful to compare and contrast them in those three arenas.  If a
     worldview doesn't match reality, then feel free to throw it out, but if it
     does, the only reasonable response is to search out its implications and
     consequences.

..
     * When we are confronted with the great events of life, we tend to become
       armchair philosophers, wrestling with the deeper reasons and meaning of
       the events and life itself.
     * Our worldview puts the world in focus and shapes how we make sense out
       of what we see; and, like glasses, it will either help us or prevent us
       from seeing the world as it really is.
     * Where did we come from?  What is our purpose?  Is there anything after
       death to look forward to or to fear?  How should we live while we are
       alive?  Who are we?
     * Even the nonreligious recognize that man has a "crying need" to make
       sense out of his life.
     * Which worldview is the "correct" one?  Which view actually represents
       reality?  Which one adequately explains the events in the universe and
       the experiences of a person's mind and emotions?  We must always keep in
       mind that it does not matter whether or not a particular worldview suits
       *us*; the question is, Does it suit *the world*?
     * He who with his whole heart believes in Jesus as the Son of God is
       thereby committed to much else besides.  He is committed to a view of
       God, to a view of man, to a view of sin, to a view of redemption, to a
       view of the purpose of God in creation and history, to a view of human
       destiny, found only in Christianity.
     * We all have a "story," a description of what life is about, why we are
       here and where we are going.
     * Two key aspects of any worldview are first, it is a particular
       perception of reality, and second, it is the basis for one's decisions.
     * A worldview is the framework of our most basic beliefs that shapes our
       view *of* and *for* the world and is the basis of our decisions and
       actions.
     * The "ultimate questions" include questions of origins, meaning,
       morality, destiny, and identity.
     * The answers we embrace to these ultimate questions, whether consciously
       or subconsciously, shape our assumptions about the three major concepts
       of human existence:  God (up), humanity (in), and nature (out).
     * A belief about God is really a belief about everything else.
     * In confronting humanity I am laying the foundation for my worldview,
       because whatever I decide about humanity's place in the world affects
       me.
     * Unlike ancient cultures, Western societies do not generally confront
       nature with the same sense of respect.
     * The modern mind, which drinks heavily at the well of naturalism, has no
       room for the existence of spirits.
     * The scientific enterprise has not eradicated belief in another realm
       beyond the physical.
     * One's culture, then, is "the integrated system of learned behavior
       patterns which are characteristic of the members of a society and which
       are not the result of biological inheritance."
     * *Culture* suggests the way a group of people may appear to an
       anthropologist; *worldview* suggests how the universe looks to the
       group.
     * Does a biblical worldview fit the actual world?  If it does not, then it
       may be discarded onto the heap of misguided philosophies.  If it does,
       then a careful search into its implications and consequences is the only
       reasonable response.
     * The biblical worldview starts with (1) God exists, and (2) God has
       uniquely revealed his character and will in the Bible.
..

  2. The World of Worldviews

..
     * Those who do not consciously evaluate their worldview beliefs end up
       "catching" their worldview the same way they might "catch" a cold---they
       absorb it from the culture around them.
     * A worldview of this sort is quite *volatile*, susceptible to great
       inconsistency.  It is also *vulnerable*, subject to deep confusion in
       the face of the dissonance caused by life's deepest existential
       experiences.
     * The crucial feature of any worldview is what it says about ultimate
       reality; that is, how does it answer the ultimate questions of origin,
       meaning, morality, destiny and identity?
     * *Naturalism* includes those worldviews that suggest ultimate reality is
       limited to the physical matter of the universe; *transcendentalism*
       includes those that see ultimate reality as being only spiritual or
       psychic (mental energy); and *theism* refers to those worldviews that
       posit a personal God as ultimate reality who created the material and
       spiritual universe.
     * Postmodernism differs from naturalism, transcendentalism and theism by
       suggesting that it is impossible to have the sort of knowledge about
       ultimate reality claimed by the worldviews.
     * Naturalism projects the view that ultimate reality is material.  The
       physical universe is all there is.  There is nothing beyond or separate
       from that which we can see, touch, and measure.
     * So many of the earliest scientists were strong Christians.
     * Naturalism, as a full-blown atheistic worldview, is a relatively new
       perspective.
     * For those who have reasoned themselves into a naturalistic position,
       there still remains a substratum of allegiance to the supernatural.
     * The orderly Newtonian universe served as a constant proof of an orderly
       God.
     * As late as the nineteenth century, most scientists gave the purpose of
       scientific investigation being to shore up religious belief.
     * A practical atheism slowly began to be a part of a scientific worldview
       that no longer found the "God-hypothesis" necessary.
     * The design of the universe was not the handiwork of an intelligent and
       purposeful God, but the result of natural selection.
     * Huxley coined the term *agnosticism* to describe a tentative, uncertain
       attitude toward any question about God and the meaning of life.
     * Agnosticism says it is wrong for a man to say that he is certain of the
       objective truth of any proposition unless he can produce evidence which
       logically justifies that certainty.
     * "Truth" became synonymous with "scientific fact," and the great sin
       became holding to certainty in metaphysical questions.
     * Man is merely a machine that works for a while and then eventually runs
       down and quits.
     * The ethical system of naturalism is utilitarian for either
       individualistic or societal ends.
     * Whatever promotes happiness, self-consciousness and self-identity in my
       life and in the lives of others is "good"; whatever hinders them is
       "bad".
     * The result, of course, is ethical relativism, the belief that morality
       depends upon the individual or situation.
     * Naturalists, because of their view of ultimate reality, see no
       overarching meaning for the universe or humanity.
     * It is up to mankind to understand how nature works so that he will be
       able to harness its power for the improvement of human life.
     * From where did the universe come?  Why is the universe organized as it
       is?
     * The naturalist will answer the question of design by appealing to
       natural selection.
     * It is no longer entirely absurd to imagine that the universe came into
       existence spontaneously from nothing as a result of a quantum process.
       Davies sees the universe as self-creative and self-organizing.
     * Some naturalists believe their worldview ultimately makes life absurd.
       Such an approach is called *nihilism*.
     * *Existentialism* is an approach to life that ventures to overcome the
       hopelessness of naturalism by creating one's own meaning for life.
     * Each person must find their own meaning.
     * Another approach is *hedonism*, which views the goal of life as
       pleasure.
     * Certain naturalists choose to focus their energies on making the world a
       better place to live.
     * This approach, known as *humanism*, discards any ultimate meaning for
       life and places the needs of humanity as a whole at the center of all
       universal concerns.
     * The currents of the Universal Being circulate through me; I am part or
       parcel of God.
     * Unhappy with what they considered the stale and rational theology of
       Christianity and the spiritual bankruptcy of deism, the
       transcendentalists sought to return God to the experience of everyday
       life.
     * God was not "out there," he was "right here," all around us, in
       everything we see and do.
     * Among them are Hinduism, Buddhism, Taoism, Confucianism, Hare Krishna,
       Baha'ism, Christian Science, Scientology, Unity School of Christianity,
       and the many New Age organizations and groups.
     * Much transcendental thought is based on the fundamental idea that all
       things are a unified whole, a concept known as *monism*.
     * This ultimate reality is spiritual or psychic in nature.
     * The transcendental God and the universe are inseparable.
     * The transcendental God is not a personal being.
     * Man's nature is an extension of the divine oneness.  Therefore, each
       person is divine.
     * A person could be separated into five elements:  form, feeling,
       perceptions, impulses, and consciousness.
     * Much of transcendentalism has little to do with ethics, at least as far
       as the Western mind perceives moral systems.
     * Moral behavior, although not based upon absolute norms given from
       heaven, still allows a person to attain oneness.
     * Mankind possesses a fragmented vision of the oneness of all things.
     * Reincarnation is a fundamental feature of transcendental philosophy.
     * Man's solution to his problems is not to be found outside himself.
       Truth, salvation, peace, and enlightenment are all found within.
     * A person's ideal goal should be some form of enlightenment by which he
       moves beyond his individualness and "becomes one" with ultimate reality.
     * In general, the physical realm is at best a part of reality and at
       worst a bad dream.  The focus of transcendentalism is upon the fabric of
       reality that underlies the physical world.
     * There has been little emphasis on scientific inquiry in cultures where a
       transcendental worldview is dominant.
     * The ecological gospel of saving the whales, rescuing seals, preserving
       redwoods, and so forth, promises our own salvation from mutual
       destruction and the achievement of universal harmony.
     * Transcendentalism promises a progression toward unity.  Modern society
       is enamored with the concept of progression.
     * The most positive aspect of transcendentalism is the promise of a "New
       Age" of global harmony and peace.  As mankind progresses toward this
       unity, the shackles of theistic religions and atheistic naturalism must
       be removed.
     * Transcendentalism replaces the theistic view of man's depravity with a
       positive acclamation of man's divinity.
     * Theism beings with the assumption that God exists.
     * Theism holds that real things do exist beyond the physical realm:  God,
       angels, the human soul, immortality, and the like.
     * Theism sees the created world as a work of art from the hand of the
       Creator.  Theism also delivers an indictment against man because of his
       personal rebellion against the truth revealed by God.
     * Theism sees God as a personal being who is separate from the natural
       world.  Therefore, God exists in a different way than does the creation
       he brought into existence.
     * We can derive from the evidence around us that God is a powerful,
       orderly, moral being, but little about his character traits and his plan
       for the world and humanity.
     * God is a person without a body (i.e., a spirit), present everywhere, the
       creator and sustainer of the universe, a free agent, able to do
       everything (i.e., omnipotent), knowing all things, perfectly good, a
       source of moral obligation, immutable, eternal, a necessary being, holy,
       and worthy of worship.
     * To claim that man is *dependent* means that he is not autonomous.
     * To claim that man has *dignity* is to make a strong assertion about the
       nature of man's being.
     * Our abilities to seek that which is true, good, and beautiful are
       profound indicators of God's imprint in the soul of each person.
     * The three categories of human reason, human volition, and aesthetic
       sensitivity are celebrated faculties that are distinctly human.
     * Humanity is capable of the greatest acts of kindness and selflessness,
       the greatest discoveries of the universe, the greatest thoughts of
       creativity, and the greatest acts of courage; yet he is also capable of
       the grossest acts of decadence and violence against himself and others.
     * The concept of *dominion* is difficult to articulate, yet there is a
       compelling notion that mankind is, in a small way, ruler over his
       environment.
     * While reflecting some of the most positive features of naturalism and
       transcendentalism, theism solves the mysteries concerning man's purpose
       and meaning, his individual personality and destiny.  From a personal
       point of view, the naturalist would claim, "There is no God"; the
       transcendentalist would affirm, "I am god"; but the theist would insist,
       "I am God's."
     * God alone is the ultimate reality and everything else is derived from
       him.
     * The deistic God created the physical universe and instilled within it
       natural laws to ensure its orderly operation.  Now the world runs
       according to God's precise initial design without any need for his
       intrusion.
     * *Finitism* is the belief that God is limited in certain aspects of his
       nature and power.
     * How can an all-good, all-powerful God allow evil to persist in a
       universe he created?  Finitism attempts to solve the problem by claiming
       that God is limited and incapable of destroying evil by himself.
     * The strict legalistic lifestyle of the *Muslim* ("one who surrenders")
       is a result of the Islamic view of God as one who exacts justice
       according to a stern ethical and ritual code.
     * Postmodernism can resemble the more pessimistic expressions of
       naturalism:  there are no transcendent categories of truth or morality;
       life is largely meaningless; there is no overarching design or direction
       to history.
     * During the Enlightenment, authority shifted from traditional
       institutions to human reason.  A scientific approach to the world
       yielded tremendous advances in medicine, technology, and communications
       and challenged the centrality of theology and religious belief as the
       paradigm for learning.
     * The "Big Story" of the world was not *given* by revelation; rather, it
       was to be *discovered* and perhaps even *determined* by science, reason
       and technology.
     * Postmoderns are skeptical of any and all claims to an authoritative
       comprehensive worldview, absolute truth about reality, and an
       overarching purpose to the human story.  Postmoderns embrace local
       narratives, not metanarratives; a multitude of stories, not a "Big
       Story."
     * We all come from a perspective, or bias, that is shaped by the culture
       we inhabit.  According to postmodernism, we cannot claim objectivity for
       our views.
     * Whereas the central concern of naturalism, transcendentalism and theism
       is on what the real world actually is, the focus of postmodernism is on
       *how we perceive* and *how we describe* what the world is.
     * Reality is ultimately unknowable.
     * We never really have the facts; there is only interpretation.
     * Truth and knowledge are constructions of language.
     * The postmodern worldview says there is no absolute truth; there are only
       "truths".
     * Since we simply cannot escape language in our attempts to describe
       reality, all objectivity is jettisoned.
     * This view of language is at the root of the practice of "deconstruction"
       in literature, which was first espoused by Jacque Derrida.
     * Progress is an illusion.
     * *Pragmatism* says as ideas, expressions, and concepts clash, one will
       emerge as a better working option; and something that works in a
       particular culture (like monogamy) or situation (like an appropriate age
       of sexual consent) may not in another culture or situation.
     * Foucault, a Frenchman who was heavily influenced by Nietzsche and Marx,
       suggested that every society was made up of the powerful and the
       oppressed.  The powerful decided what was normal in terms of societal
       behavior, laws, and sexuality and imposed these views on the oppressed.
       By showing how standards of normalcy changed from one society to
       another, Foucault concluded all cultural standards were arbitrary and
       only a way for one group to exert its power over another.
     * According to postmodernism, we are social constructs situated in groups.
       We are identified by our networks, not our souls.
     * The *pluralism* of Western societies increases our tendency to see
       ourselves and others as part of groups.
     * There is a great *fluidity* to contemporary life.
     * Because scientific naturalism is deeply entrenched in the university,
       the university has become a principal disseminator of atheism.
..

  3. Putting Worldviews to the Test

..
     * It's not popular today to even suggest that beliefs *can* or *should* be
       evaluated.  In fact, attempts to do so often earn the accusation of
       intolerance.
     * **The Test of Reason:**  Does the worldview affirm views that are
       self-contradictory?
     * **The Test of the Outer World:**  If a worldview is true, we can expect
       to find at least some external corroborating evidence to support it.
     * It is important, as we evaluate ideas, to be careful not to limit
       evidence due to preconceived notions.
     * Evidence is always subject to interpretation, it can be manipulated, and
       just because evidence has not been found yet does not imply that
       something does not exist.
     * **The Test of the Inner World:**  A worldview may be reasonable and
       supportable, but it is not much use if it cannot adequately address the
       victories, disappointments, blessings, crises, and relationships of our
       everyday world.
     * The experiences we have should force us toward intentional reflection and
       evaluation of our worldview.
     * **The Test of the Real World:**  This test evaluates what a worldview
       looks like in its *appication*.
     * Ideas are not isolated cognitive concepts.  They have consequences.
     * Worldviews exert their greatest influence in offering definitions, and
       faulty worldviews define words---and consequently ideas and persons---in
       destructive ways.
     * The major problem for naturalism is that it cannot offer proper
       foundations for its conclusions.
     * From this point of view, our beliefs would be dependent on
       neurophysiology, and a belief would just be a neurological structure of
       some complex kind...  But why think for a moment that the beliefs
       dependent on or caused by that neurophysiology will be mostly true? ...
       From a naturalist point of view the thought that our cognitive faculties
       are reliable would be at best naive hope...  In fact, he'd (the
       naturalist) have to hold that it is unlikely, given unguided evolution,
       that our cognitive faculties are reliable...  If this is so, the
       naturalist has a *defeater* for the natural assumption that his cognitive
       faculties are reliable---a reason for rejecting that belief, for no
       longer holding it.  And if he has a defeater for that belief, he also has
       a defeater for any belief that is a product of his cognitive faculties.
       But of course that would be *all* of his beliefs---including naturalism
       itself.  So the naturalist has a defeater for naturalism; naturalism,
       therefore, is self-defeating and cannot be rationally believed.
     * At some point in every explanation of phenomena, the question 'Why?' can
       no longer be answerable within science.  Science cannot even give final
       truth about matters of science, let alone about matters of morals or
       religion.
     * To believe that science is valid does not mean that one must adopt a
       naturalistic worldview.
     * Another hesitation in accepting naturalism as an adequate worldview is
       the *problem of human thought and meaning*.
     * How is it that man has the ability to reason abstractly and transcend his
       own existence?
     * How did unconscious forces give rise to minds and sound reasoning?
     * If the universe is meaningless, then so is the statement "The universe is
       meaningless."
     * *Unbelief* was at the heart of most of the devastating philosophies of
       the twentieth century.
     * The "might makes right" mentality that characterized these views is
       *consistent* with a naturalistic view of the world in which any progress
       is the result of natural selection.
     * The most glaring difficulty is the inability to evaluate
       transcendentalism.
     * Transcendentalism is not only beyond proof, it is also beyond disproof.
     * For one cannot reach the conclusion that the intellect should be
       dismissed without using the intellect in the process of dismissing it.
     * If all things are one, why are so many persons not aware of this oneness?
     * Transcendentalism also fails to adequately address the reality of evil.
     * Transcendentalism leaves us no place to find healing, direction, or peace
       but inside ourselves.
     * If truth is found only in looking within ourselves, we have no basis from
       which to condemn the actions of anyone else.
     * New Age religion infantalizes our relationship with God.  It would take
       us back to the premoral stage of religion, asking for something, trying
       to manipulate God to get what we want, without our having to meet any
       standards at our end of the relationship.
     * The very fact that we recognize evil as something that *ought not be*
       suggests that we know what *ught to be*.
     * *Proof* may not be an appropriate term to describe the evidence for the
       existence of God.
     * To search scientifically for the presence of God in the physical universe
       is like searching for the artist in the canvas of a painting.
     * The *cosmological argument* points to the existence of the cosmos as
       evidence of God's existence.
     * Something outside the universe is required to explain its existence.
     * It is argued that God is the only existence that *must* exist, all other
       things are not necessary.
     * A second argument for the existence of God is the *teleological*
       argument, which looks beyond the mere existence of the cosmos and extends
       the scope of inquiry to the apparent design within the creation.
     * The concept of disorder implies an understanding of order.  From where
       does this idea arise except from a divine orderer?
     * All creation is the most beautiful book of the Bible; in it God has
       described and portrayed himself.
     * The *moral argument* sees the reality of man's moral nature as evidence
       for a "moral lawgiver".
     * Other arguments include the argument from religious experience (If there
       is no God, how is it that millions of people have religious
       experiences?), the argument from consciousness (How can a material
       object give rise to conscious thought?), and the ontological argument (Is
       not the very idea of God proof of his existence?).
     * There is something profoundly good and profoundly twisted about our world
       and the people in it, and naturalism and transcendentalism fail to offer
       adequate explanations for this.
     * Postmodernism is proving rather successful at undermining the hubris of
       modernism, and no thoughtful Christian can be entirely sad about that.
     * Although the dethroning of humanistic scientific reason is attractive to
       battle-weary Christian intellectuals, the postmodern denial of all
       objective truth is unacceptable.
     * Postmodernism offers a metanarrative that there are no metanarratives.
     * The statement "there is no absolute truth, there are only truths" is an
       objective, absolute propositional statement.
     * Postmodernism is guilty at times of intellectual cowardice, at other
       times of intellectual bullying, and at other times of intellectual
       laziness.
     * While the biases of a community certainly shape the perspectives of the
       members of that community, it does not follow that reality itself is
       socially constructed and that we never have access to objective reality,
       as postmodernists also claim.
     * While modernism placed the hubris of authority with the autonomous self,
       postmodernism attempts to place it with the community.  However,
       postmodernism ironically tends to *increase* our isolation from others.
     * Postmodernism is, in its most despairing form, a rehash of classic
       nihilism.  In its most positive form, it cannot elevate itself beyond a
       more corporate existentialism.  In either case, all ultimate values are
       eliminated.
     * Perhaps the ultimate proof that postmodernism cannot adequately explain
       reality is that it can never ultimately be lived out.
     * In all of its rebellion against the evils of modernism and Western
       civilizations, postmodernism ironically exists only in the context of
       modernism and Western civilization!
     * Unlike the other worldviews we have evaluated, the biblical worldview can
       withstand all challenges and still speak to the dominant culture.
..

Part Two:  A *Biblical* Worldview

..
  * A biblical worldview holds to the following:

    1. Absolute moral truth exists.
    2. The Bible is the source of that truth.
    3. Jesus lived a sinless life.
    4. God created the universe and continues to rule it.
    5. Salvation is a gift from God.
    6. Satan is a real living entity.
    7. Christians have a personal obligation to share the Gospel.
    8. The Bible is accurate in all of its teaching.

  * Leslie Newbigin relays a stunning statement said to him by a Hindu leader
    and friend:

      As I read the Bible I find in it a quite unique interpretation of
      universal history and, therefore, a unique understanding of the human
      person as a responsible actor in history.  You Christian missionaries
      have talked of the Bible as if it were simply another book of religion.
      We have plenty of these already in India and we do not need another to
      add to our supply.
..

  4. The Essentials of a Biblical Worldview

     FOO

..
     * Once the existence of God is established, the most important issue
       becomes the nature of God.
     * If God exists, then what he has revealed is of utmost importance, since
       it is our only means of discovering both *who God is* and *what God
       requires*.
     * It is through God's *Word* that we may properly understand God's *world*.
     * Human beings and the universe in which they reside are the creation of
       God who has revealed himself in scripture.
     * *All* worldviews begin with certain assumptions and metaphysical claims
       that cannot be proved.
     * The source of the orderliness of the universe and of man's rational
       abilities is God.
     * *Stopped underlining at this point.*
..

  5. Why a Biblical Worldview?

     The Bible is God's word---his truth---accurately received and written, and
     faithfully transmitted to us through the ages.  Its authorship was
     supernaturally inspired by the Holy Spirit, and the product of that
     inspiration is inerrant and infallible in all things.  Jesus assumed and
     relied on this view of inerrancy in his teachings, particularly in
     asserting his claim to be God incarnate.  The inspiration of scripture
     took place through the prophets in the old testament and through the
     apostles in the new, and then stopped.  The Bible we have today is the
     best historically attested ancient document we have by orders of
     magnitude; nobody does textual criticism like biblical scholars do.  Many
     claim there are errors and contradictions within scripture, but closer
     examination of things like context and the meanings of words reveals these
     claims to have no foundation.

  6. What's Wrong?  The Problem of Evil and Suffering
  7. Who's Right?  The Problem of Pluralism

Part Three:  So What?  A View *Of* and a View *For*

  8. A View *of* and *for* the Self
  9. A View *of* and *for* the Family
  10. A View *of* and *for* the Church
  11. A View *of* and *for* the World

Questions
=========

1. *Before you read, write out the titles of the major sections and chapters to
   see how the book is organized.*

   Part One:  The Difference a Worldview Makes

     1. When Worldviews Collide
     2. The World of Worldviews
     3. Putting Worldviews to the Test

   Part Two:  A *Biblical* Worldview

     4. The Essentials of a Biblical Worldview
     5. Why a Biblical Worldview?
     6. What's Wrong?  The Problem of Evil and Suffering
     7. Who's Right?  The Problem of Pluralism

   Part Three:  So What?  A View *Of* and a View *For*

     8. A View *of* and *for* the Self
     9. A View *of* and *for* the Family
     10. A View *of* and *for* the Church
     11. A View *of* and *for* the World

2. *According to the authors, why does everyone have a worldview?*

   We all have a "story," a description of what life is about, why we are here
   and where we are going.  A worldview is the framework of our most basic
   beliefs that shapes our view *of* and *for* the world and is the basis of
   our decisions and actions.

3. *Give a summary statement for each of the worldviews.*

   Naturalism
     The natural world is all that is, or ever was, or ever will be.
   Theism
     There is some being, which we call God, outside the natural world that
     brought it into being.
   Transcendentalism
     Everything in the natural world is a part of the supreme being known as
     God.

4. *Write out several meaningful quotes from the book you want to remember.*

   * Consistent thinking on the part of Christians does not exist.  While some
     lament the nonexistence of the Christian mind, American Christianity
     charges full speed ahead in its course of incessan activity, indomitable
     individualism, and irrepressible pragmatism.  Comprehensive and coherent
     Christian thinking has never been a major part of religious life in
     America.
   * The tragedy of modern man is not that he knows less and less about the
     meaning of his own life, but that it bothers him less and less.
   * Many Christians brandish a commitment to a biblical worldview that goes
     little beyond a "moralistic therapeutic deism", believing in a God who
     only exists to enhance their personal behavior and well-being.
   * A belief about God is really a belief about everything else.
   * Does a biblical worldview fit the actual world?  If it does not, then it
     may be discarded onto the heap of misguided philosophies.  If it does,
     then a careful search into its implications and consequences is the only
     reasonable response.
   * Those who do not consciously evaluate their worldview beliefs end up
     "catching" their worldview the same way they might "catch" a cold---they
     absorb it from the culture around them.
   * As late as the nineteenth century, most scientists gave the purpose of
     scientific investigation being to shore up religious belief.

5. *How does thinking "worldviewishly" help us to "make sense of the world?"*

   Because our worldviews are at the root of all of our thoughts and actions,
   we need to think in terms of worldviews to understand what's really going on
   behind the scenes.  If we don't, we'll constantly be talking past one
   another, as we're working from completely different paradigms.  What makes
   complete sense in one worldview simply won't compute in another.
