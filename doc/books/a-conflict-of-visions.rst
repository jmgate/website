A Conflict of Visions
---------------------

*Ideological Origins of Political Struggles*

* Author:  Thomas Sowell
* Publication Year:  1987
* Link:  https://smile.amazon.com/Conflict-Visions-Ideological-Political-Struggles/dp/0465002056/ref=sr_1_1?crid=3TK5FIBBMTRC1&dchild=1&keywords=a+conflict+of+visions+thomas+sowell&qid=1600831487&sprefix=a+conflict+of+visions%2Caps%2C231&sr=8-1
* Started on:  ?
* Finished on:  2020-11-24

What is the Book About?
=======================

The inability for people to effectively dialogue on the issues of the day stems
from the paradigmatic disconnect between diametrically opposed visions of human
nature.
