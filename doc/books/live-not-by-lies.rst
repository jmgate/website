Live Not by Lies
----------------

*A Manual for Christian Dissidents*

* Author:  Rod Dreher
* Publication Year:  2020
* Link:  https://smile.amazon.com/Live-Not-Lies-Christian-Dissidents/dp/0593087399/ref=sr_1_1?dchild=1&keywords=live+not+by+lies&qid=1608493936&sr=8-1
* Started on:  ?
* Finished on:  2020-11-14

What is the Book About?
=======================

A warning that the cultural circumstances in the world today bear remarkable
similarities to various societies over the last century before their embrace of
totalitarianism, and recommended practices from Christians who weathered the
storm and kept the faith alive.
