Why You Think the Way You Do
----------------------------

*The Story of Western Worldviews from Rome to Home*

* Author:  Glenn Sunshine
* Publication Year:  2009
* Link:  https://smile.amazon.com/Why-You-Think-Way-Worldviews/dp/0310292301/ref=sr_1_2?dchild=1&keywords=why+you+think+the+way+you+do&qid=1629734405&sr=8-2
* Started on:  2021-08-22
* Finished on:  2021-08-31

How is the Book Organized?
==========================

1. What Is a Worldview and Why Should I Care?

   A worldview is the framework you use to interpret the world and your place
   in it.  You should care because worldviews are the foundation for culture,
   and they change over time, either for better or worse.

2. The Worldview of Ancient Rome

   Though the ancient Roman world permitted a wide variety of pagan and mystery
   religions, it was neoplatonism that really laid the foundation for its
   epistemology.  The heierarchy of being gave the necessary justification for
   practices such as human sacrifice, slavery, contraception, abortion, and
   infanticide.

3. Christianity and the Transformation of the Pagan World

   The life of those in the early church displayed a vastly different
   underlying worldview from the ancient Roman world around them, specifically
   in terms of valuing the individual as made in the image of God.  As
   Christianity grew, some of the gnostic and neoplatonic philosophies of the
   day started to make inroads into the church.  With the legalization of
   Christianity under Constantine, the dominant worldview within the Roman
   empire began to shift.

4. The Emergence of the Medieval Worldview

   As the empire started to collapse, the clergy largely picked up the pieces
   because they were the ones educated and trained in things like
   administration.  Irish Christianity largely preserved education, and as the
   Germanic and Roman cultures intermingled, they produced feudalism and
   manorialism.  Urban populations began to grow.  The inquisitorial and
   adversarial legal systems developed.  Platonic humanism developed---meaning
   God created the world, and thus it can tell us about him---along with
   scholasticism---which consists of a five-part method of investigation.
   Science developed, in terms of both inventions to improve every-day life,
   and broader concepts of understanding the cosmos.  Similar developments were
   squashed under non-Christian worldviews.

5. Medieval Economics and Politics

   The idea of work being good led to monasteries reinvesting profits to
   increase productivity.  That mindset, along with the concept of private
   property, laid the foundation for capitalism.  The guild system developed to
   regulate the production standards.  Technological innovation progressed to
   do away with drudgery.  Merchants arose as middlemen to transport goods from
   here to there.  Augustine's *City of God* set the stage for political
   thought.  Tensions between the church and the state, and between the church
   and the people, grew.  Aristotle's breakdown of forms of government
   (monarchy/tyranny, aristocracy/oligarchy, and republic/democracy) was
   helpful in assessing how sin plays into rule.  The concepts of limited
   government, natural law, and inalienable rights developed.

6. The Breakdown of the Medieval Model

   The Italian Renaissance rediscovered ancient Rome as the epitome of
   civilization.  They shifted the emphasis in education from logic (from
   scholasticism) to rhetoric (from ancient Rome).  They sought to study past
   authors to achieve a grand synthesis of all human knowledge, but the systems
   of thought that went into that synthesis were incompatible.  Scholars had
   previously assumed everyone was speaking the same truth from different
   directions, but this raised the question of who was right and wrong.
   Protestantism hit the scene, and then Catholicism rebounded, adding more
   questions to the "who's right and wrong" pile.  Resistance theory developed,
   as well as the notion of the divine right of kings.  The discovery of the
   New World raised questions about the truthfulness of biblical history.
   Pyhhronical skepticism laid the early groundwork for postmodernism.

7. A New Paradigm of Knowledge

   The Copernican Revolution changed our mental model of the universe.  A
   heliocentric universe had been proposed a number of times, gaining little
   traction and causing minimal religious consternation; Galileo just happened
   to be the first person to be a jerk about it, an calling the pope a
   simpleton in a treatise he commissioned is bound to create problems for you.
   The question of the appropriate model of the universe wasn't science on one
   side and Bible on the other, but science and Bible on both sides of the
   debate.  Descartes sought to answer the Pyhhronists, and came up with "I
   think, therefore I am," followed by "I exist, therefore God exists," and
   finally "God exists, therefore clear and distinct ideas must be true."
   Pascal developed probabilism, and believed truth could be found through
   experimentation and observation, setting the stage for Newton to "stand on
   the shoulders of giants."  Though we think of Newton as a scientist, he
   didn't distinguish between his theological and scientific investigations.

8. Enlightenment and Revolutions

   In the age of the Enlightenment, rationalism started prevailing in both
   science and religion.  In Christianity, God is infinite and personal,
   creator and sustainer, and transcendent and immanent.  Deism arose holding
   to just the first characteristic in each of these pairs.  Adam Smith's *The
   Wealth of Nations* laid the foundation for both classical (free market)
   economics and classical liberalism.  Edmund Burke became the father of
   classical conservatism, agreeing with Smith that government regulation was
   bad for the economy.  The philosophes prized intellectual independence and
   original thinking, and paved the way for philosophical naturalism.
   Worldview began to shift in the direction of materialism.  Religious
   revivals happened all over trying to bring more of the scriptures to bear on
   daily life.  The revolutions in England, America, and France showed how the
   worldviews of Locke and Rousseau played out.  The results were dictated by a
   combination of those ideas and the histories of the countries involved.

9. Modernity and Its Discontents

   Deism had set the stage for a transition to materialistic naturalism, just
   as soon as a mechanism could be discovered that could explain how everything
   came to be.  Darwin's natural selection was that mechanism.  *Science* no
   longer meant knowledge, but rather only that which is knowable via the
   scientific method.  Darwinian evolution, ironically, isn't subject to the
   scientific method.  It becomes the basis for both racism and eugenics.
   Utilitarianism arose as an alternative ethic, along with the notion that
   ethics are situational and relative.  The survival of the fittest mentality
   also found its way into economics and industrialization, which then laid the
   groundwork for Marxism and all it entails.  Materialism led to nihilism,
   meaning there is no meaning, but when that wasn't palatable, it bred
   existentialism as a sort of nihilism-lite.

10. The Decay of Modernity

    In the wake of the two world wars, most Western nations adopted some
    version of Keynesian economics, along with expanded state powers to
    intervene on behalf of society.  Deconstructionism led to reinterpretations
    of anything previously known.  Postmodernism, with its rejection of
    objective truth, and its insistence on cultural and moral relativity, is
    essentially deconstructionism-lite, not following through on all its
    implications.  This gave rise to feminism and the sexual revolution, with
    their attempts to undermine the traditional family structure.  While all
    this was going on, transcendental worldviews started having a greater
    impact, with the advent of New Age and Neo-Paganism.  The postmodern and
    transcendental thinking merge in movement like eco-spirituality, which lies
    behind things like the Gaia hypothesis (e.g., Pandora in *Avatar*) and the
    global warming movement functioning as a religion.

11. Trajectories

    Basically we've come full circle back to ancient Rome.  Abortion,
    infanticide, human rights violations, slavery...  Whatever you believe in
    is totally fine, as long as you pay homage to Caesar.

Questions
=========

1. *Read Chuck Colson’s Foreword and Glenn’s Acknowledgments to get some
   background of Glenn’s involvement in the Colson Fellows (formerly
   Centurions) Program.  Now look over the Contents page.  What can you tell
   from the book by how the contents are organized?*

   First you need to start with the pre-evangelism of getting people to
   understand what worldview is and why it's important.  Once that's
   established, you can start walking through the various thought patterns in
   any given age, analyzing them from a worldview perspective.

2. *The best way to grasp the flow of Glenn’s explanations and arguments is to
   summarize each of the chapters and note any key ideas that strike you as
   important.  A brief paragraph for each is sufficient.*

   See **How is the Book Organized?** above.

3. *The closing section of the book, The Image of God, is a call for Christians
   to recover the ideas and values of the biblical worldview.  List some of the
   key ones.*

   * When Western civilization embraced a robust vision of the image of God, we
     had our greatest successes; when we ignored it, we had our greatest
     failures.
   * In terms of human rights, the image of God is the source of human worth.
   * We must resist calls to consider other creatures on par with humanity.
   * Christians should be at the forefront of environmental concerns and
     creation care.
   * In science and technology, we need to be as concerned about the ethical
     dimensions of our research as we are about the results---particularly with
     regard to biotechnologies.
   * God gave humanity both intellectual and physical work to do in the garden
     of Eden, and thus we should view work as a positive good, not a necessary
     evil.
   * We must provide for the needs of others, honoring them as individuals and
     serving them rather than sub-contracting our responsibilities to care for
     our neighbors to the government or to any other entity.
   * In political life, we must keep the importance of the image of God shared
     by people of all nationalities front and center in our thinking.
   * We also need to insist on the right to dissent.  Everyone has the right to
     freedom of conscience and the right to express themselves without fear of
     repercussion, whether or not they agree with us.

4. *What are three things you will do differently as a result of reading this
   book?*

   #. For those who don't understand worldview or its implications, I'll ask
      them to read through this book with me.
   #. I'll see if Glen has references to back up all the claims he's made,
      particularly the ones of the form, "What you usually hear about this
      period is history isn't quite accurate."
   #. I'll be on the lookout for other ways our current cultural moment is
      beginning to mimic ancient Rome, such that I can call attention to it.
