Knowing God
-----------

* Author:  J.I. Packer
* Publication Year:  1973
* Link:  https://smile.amazon.com/Knowing-God-J-I-Packer/dp/0340863544/ref=sr_1_4?dchild=1&keywords=knowing+god&qid=1628565182&sr=8-4
* Started on:  2021-08-08
* Finished on:  TBD

How is the Book Organized?
==========================

Foreword

Modern skepticism, combined with the sacred/secular, fact/value split, has led
to the church's significant weakening due to an overwhelming ignorance of God.
Those who can see what's happening are tempted to withdraw from the
institutions that don't seem to recognize their problems or be willing to
address them.  This current confusion is worse than anything the church has
experienced since Gnosticism in the first few centuries AD.

..
  * Ignorance of God---ignorance both of his ways and of the practice of
    communion with him---lies at the root of much of the church's weakness
    today.
  * Christian minds have been conformed to the modern spirit:  the spirit, that
    is, that spawns great thoughts of man and leaves room for only small
    thoughts of God.  The modern way with God is to set him at a distance, if
    not to deny him altogether; and the irony is that modern Christians,
    preoccupied with maintaining religious practices in an irreligious world,
    have themselves allowed God to become remote.  Clear-sighted persons,
    seeing this, are tempted to withdraw from the churches in something like
    disgust to pursue a quest for God on their own.
  * Christian minds have been confused by the modern skepticism.  Skepticism
    about both divine revelation and Christian origins has bred a wider
    skepticism which abandons all idea of a unity of truth, and with it any
    hope of unified human knowledge:  so that it is now commonly assumed that
    my religious apprehensions have nothing to do wtih my scientific knowledge
    of things external to myself, since God is not 'out there' in the world,
    but only 'down here' in the psyche.  The uncertainty and confusion about
    God which marks our day is worse than anything since Gnostic theosophy
    tried to swallow Christianity in the second century.
..

Part I.  Know the Lord

  INSERT PART SUMMARY HERE.

  1. The Study of God

     Given that we live in a world God created, we must study the one who
     created it, that we might live in it as he intends us to.  But beware:
     pursuit of knowledge of God and his truths as an end in itself is
     problematic, leading to self-satisfied self-deception.  Knowledge *about*
     God should transition to meditation *before* God, leading to prayer and
     praise *to* God.  This is the charge given to all mankind; abandon it and
     you can waste your life and lose your soul.

..
     * The proper study of God's elect is God; the proper study of a Christian
       is the Godhead.
     * There is something exceedingly *improving to the mind* in a
       contemplation of the Divinity.
     * No subject of contemplation will tend more to humble the mind, than
       thoughts of God.
     * He who often thinks of God, will have a larger mind than the man who
       simply plods around this narrow globe.  The most excellent study for
       expanding the soul, is the science of Christ, and Him crucified, and the
       knowledge of the Godhead in the glorious Trinity.
     * Knowing about God is crucially important for the living of our lives.
     * We are cruel to ourselves if we try to live in this world without
       knowing about the God whose world it is and who runs it.
     * Disregard the study of God, and you sentence yourself to stumble and
       blunder through life blindfolded, as it were, with no sense of direction
       and no understanding of what surrounds you.  This way you can waste your
       life and lose your soul.
     * Christians have five basics truths about God:

       1. God has spoken to man, and the Bible is His Word, given to us to make
          us wise unto salvation.
       2. God is Lord and King over His world; He rules all things for His own
          glory, displaying His perfections in all that He does, in order that
          men and angels may worship and adore Him.
       3. God is Saviour, active in sovereign love through the Lord Jesus
          Christ to rescue believers from the guilt and power of sin, to adopt
          them as His sons, and to bless them accordingly.
       4. God is Triune; there are within the Godhead three persons, the
          Father, the Son, and the Holy Ghost; and the work of salvation is one
          in which all three act together, the Father purposing redemption, the
          Son securing it, and the Spirit applying it.
       5. Godliness means responding to God's revelation in trust and
          obedience, faith and worship, prayer and praise, submission and
          service.  Life must be seen and lived in the light of God's Word.
          This, and nothing else, is true religion.

     * There was a time when the subject of God's attributes (as it was called)
       was thought so important as to be included in the Catechism which all
       children in the churches were taught and all adult members were expected
       to know.
     * What is God?  God is a Spirit, infinite, eternal, and unchangeable in
       His being, wisdom, power, holiness, justice, goodness, and truth.
     * What is my ultimate aim and object in occupying my mind with these
       things?  What do I intend to *do* with my knowledge about God, once I
       have got it?
     * To be preoccupied with getting theological knowledge as an end in
       itself, to approach Bible study with no higher a motive than a desire to
       know all the answers, is the direct route to a state of self-satisfied
       self-deception.
     * There can be no spiritual health without doctrinal knowledge; but it is
       equally true that there can be no spiritual health with it, if it is
       sought for the wrong purpose and valued by the wrong standard.
     * The psalmist wanted to understand God's truth in order that his heart
       might respond to it and his life be conformed to it.
     * He was interested in truth and orthodoxy, in biblical teaching and
       theology, not as ends in themselves, but as means to the further ends of
       life and godliness.
     * The rule for doing this is demanding, but simple.  It is that we turn
       each truth that we learn *about* God into matter for meditation *before*
       God, leading to prayer and praise *to* God.
     * And it is as we enter more and more deeply into this experience of being
       humbled and exalted that our knowledge of God increases, and with it our
       peace, our strength, and our joy.  God help us, then, to put our
       knowledge about God to this use, that we all may in truth 'know the
       Lord'.
..

  2. The People who Know their God

     The question isn't whether we're good at theology, or tend to live
     "Christianly", but rather whether or not we have known God.  Those who
     know God

     * have great energy for God,
     * have great thoughts of God,
     * show great boldness for God, and
     * have great contentment in God.

     They are sensitive to God's honor and truth being jeopardized, and the
     defiance spurs them to action, even at personal risk.  The gravity of the
     situation forces them to prayer.  The understanding of the Lordship of
     Christ in all things, both now and in the future, permeates every aspect
     of their lives.  They stand for what's right, regardless of the
     consequences, and they boldly face antagonism with the utmost peace and
     serenity, confident in their relationship with the one in control.

..
     * When Paul says he counts the things he lost 'dung', he means not merely
       that he does not think of them as having any value, but also that he
       does not live with them constantly in his mind:  what normal person
       spends his time nostalgically dreaming of manure?
     * One can know a great deal about God without much knowledge of Him.
     * One can know a great deal about godliness without much knowledge of God.
     * The question is not whether we are good at theology, or 'balanced'
       (horrible, self-conscious word!) in our approach to problems of
       Christian living; the question is, can we say, simply, honestly, not
       because we feel that as evangelicals we ought to, but because it is
       plain matter of fact, that we have known God, and that because we have
       known God the unpleasantness we have had, or the pleasantness we have
       not had, through being Christians does not matter to us?
     * Those who know God have great energy for God.
     * This shows us that the action taken by those who know God is their
       *reaction* to the anti-God trends which they see operating around them.
       While their God is being defied or disregarded, they cannot rest; they
       feel they must do something; the dishonor done to God's name goads them
       into action.
     * It is simply that those who know their God are sensitive to situations
       in which God's truth and honour are being directly or tacitly
       jeopardized, and rather than let the matter go by default will force the
       issue on men's attention and seek thereby to compel a change of heart
       about it---even at personal risk.
     * Men who know their God are before anything else men who pray, and the
       first point where their zeal and energy for God's glory come to
       expression is in their prayers.
     * If, however, there is in us little energy for such prayer, and little
       consequent practice of it, this is a sure sign that as yet we scarcely
       know our God.
     * Those who know God have great thoughts of God.
     * He knows, and foreknows, all things, and His foreknowledge is
       foreordination; He, therefore, will have the last word, both in world
       history and in the destiny of every man; His kingdom and righteousness
       will triumph in the end, for neither men nor angels shall be able to
       thwart Him.
     * Is this how we think of God?  Is this the view of God which our own
       praying expresses?  Does this tremendous sense of His holy majesty, His
       moral perfection, and His gracious faithfulness keep us humble and
       dependent, awed and obedient?
     * Those who know God show great boldness for God.
     * Once they were convinced that their stand was *right*, and that loyalty
       to their God required them to take it, then they 'smilingly washed their
       hands of the consequences'.
     * Those who know God have great contentment in God.
     * There is no peace like the peace of those whose minds are possessed with
       full assurance that they have known God, and God has known them, and
       that this relationship guarantees God's favour to them in life, through
       death, and on for ever.
     * First we must recognise how much we lack knowledge of God.  We must
       learn to measure ourselves, not by our knowledge about God, not by our
       gifts and responsibilities in the church, but by how we pray and what
       goes on in our hearts.  Second, we must seek the Saviour.
     * It is those who have sought the Lord Jesus till they have found
       Him---for the promise is that when we seek Him with all our hearts, we
       shall surely find Him---who can stand before the world to testify that
       they have known God.
..

  3. Knowing and Being Known
  4. The Only True God
  5. God Incarnate
  6. He Shall Testify

Part II.  Behold Your God!

  7. God Unchanging
  8. The Majesty of God
  9. God Only Wise
  10. God's Wisdom and Ours
  11. Thy Word is Truth
  12. The Love of God
  13. The Grace of God
  14. God the Judge
  15. The Wrath of God
  16. Goodness and Severity
  17. The Jealous God

Part III.  If God be For Us...

  18. The Heart of the Gospel
  19. Sons of God
  20. Thou our Guide
  21. These Inward Trials
  22. The Adequacy of God

Questions
=========

Chapters 1--2:

  1. *Why does Packer describe his readers as "travelers?"  How are you a
     traveler?*

     Travelers are those walking the road of life, asking practical questions
     like which way to go, or how to go about whatever's next.  Packer
     contrasts these with the "balconeers", who approach things only at the
     theoretical level, never actually getting out on the road and going
     anywhere.  I'm a traveler in that I've been asking God what I'm supposed
     to be doing, and how I'm supposed to go about doing it.

  2. *Many people think there is "no road to knowledge about God."  How do you
     answer that?*

     This statement seems foolish on two fronts.  First, if God exists, and
     we're going with a standard definition of God, then he is the source of
     the created order.  We have the ability to observe the world around us,
     and also have the ability to think critically and rationally, so we can
     use those abilities to make inferences about the creator.  It seems
     straightforward that we can draw a handful of easy conclusions:  God is
     creative; God is somehow both harsh and good; God is powerful; etc.
     Second, many religions hold that God has taken the initiative of revealing
     himself to mankind in various ways, often through the written word.  Such
     writings can teach us a good deal about the one claiming to be their
     author, and it's up to us to evaluate whether or not they are trustworthy.
     I suspect what's actually at play behind this phrase is an epistemological
     assumption that "there is no road to knowledge," period.

  3. *What are the five basic truths that the book will follow?*

     1. God has spoken to man, and the Bible is His Word, given to us to make
        us wise unto salvation.
     2. God is Lord and King over His world; He rules all things for His own
        glory, displaying His perfections in all that He does, in order that
        men and angels may worship and adore Him.
     3. God is Saviour, active in sovereign love through the Lord Jesus Christ
        to rescue believers from the guilt and power of sin, to adopt them as
        His sons, and to bless them accordingly.
     4. God is Triune; there are within the Godhead three persons, the Father,
        the Son, and the Holy Ghost; and the work of salvation is one in which
        all three act together, the Father purposing redemption, the Son
        securing it, and the Spirit applying it.
     5. Godliness means responding to God's revelation in trust and obedience,
        faith and worship, prayer and praise, submission and service.  Life
        must be seen and lived in the light of God's Word.  This, and nothing
        else, is true religion.

  4. *What are the four characteristics of people who know God?  Do you agree
     with these?*

     Those who know God

     * have great energy for God,
     * have great thoughts of God,
     * show great boldness for God, and
     * have great contentment in God.

     This seems pot on, and the lack of such people does much to explain the
     emaciated state of the church today.

  5. *How are your prayers a key to knowing your view of God?*

     "Men who know their God are before anything else men who pray, and the
     first point where their zeal and energy for God's glory come to expression
     is in their prayers[...]  If, however, there is in us little energy for
     such prayer, and little consequent practice of it, this is a sure sign
     that as yet we scarcely know our God."

     Prayer is something I've always struggled with.  I suspect it's because
     I'm very much a results motivated person, and the "results" of prayer are
     hard to observe and quantify.  If Packer's right here, my prayer life
     indicates I scarcely know God.  It sounds like I need to spend more time
     meditating on who he is, at least as a start.

Chapters 3--4:

  1. People frequently ask questions about their meaning and purpose in life.
     How does Packer answer that question?

     Knowing God, which consists of

     1. listening to God's word and receiving it as the Holy Spirit interprets
        it, in application to oneself;
     2. noting God's nature and character, as his word and works reveal it;
     3. accepting his invitations, and doing what he commands; and
     4. recognizing, and rejoicing in, the love that he has shown in thus
        approaching one and drawing one into this divine fellowship.

  2. In what ways is knowing God like knowing a person?  How is it different?

     Similar
       God is personal.  You get to know him by spending quality time with him.
       You can know him for a lifetime and still have more to learn.  You get
       to know him by him allowing you to.
     Different
       God isn't physically present with us.  Instead we learn of him through
       his written word and his revelation in creation.  He's not a peer, but
       is infinitely "over" us in majesty and glory.

  3. Do you really believe when you pray that you are talking to God
     personally?  How does this impact the way you pray?

     Sometimes.  In principle, yes, but not really, no.  I feel like I've
     always been bad at prayer, but no matter what I've tried, it doesn't feel
     like I've improved.  It feels like a very one-sided conversation.

  4. What two reasons does Packer give for not using images to worship God?

     1. Images dishonor God, because they obscure his glory.  They inevitably
        conceal most, if not all, of the truth about the personal nature and
        character of the divine being whom they represent.
     2. Images mislead man.  The very inadequacy with which they represent him
        perverts our thoughts of him, and plants in our minds errors of all
        sorts about his character and will.

  5. Is your God the God who revealed himself in Scripture?  How does Packer's
     "test" help you answer this?

     Pretty darn sure.  I don't know that the test is all that useful.  Is your
     knowledge of God focused on Jesus who reveals him?  Yes, but who would
     answer "no" to that?

Chapters 5--6:

  1. What is "the supreme mystery with which the gospel confronts us?"  Why is
     this such a mystery?  (Chapter 5)

     The incarnation.

  2. Why should the Incarnation move us to praise God for his humility?
     (Chapter 5)

     Without it we are at a complete loss to fix our completely broken
     situation.

  3. What does it mean for "the Son of God to empty himself and become poor?"
     (Chapter 5)

     He restrained his divine powers to live his life in complete and total
     submission to the will of God the Father.

  4. How does the Gospel of John teach us about the Trinity?  (Chapter 6)

     He establishes in the opening chapter that Jesus is God the Son, such that
     you can make sense of everything else he's to relate to you.  Toward the
     end, he tells you how Jesus promised to send *another* comforter,
     indicating the Holy Spirit is to carry on his work in the lives of
     believers from now on.  The relationships within the Trinity are as
     follows:

     * The Son is subject to the Father, for the Son is sent by the Father in
       His (the Father's) name.
     * The Spirit is subject to the Father, for the Spirit is sent by the
       Father in the Son's name.
     * The Spirit is subject to the Son as well as to the Father, for the
       Spirit is sent by the Son as well as by the Father.

  5. How does the Holy Spirit's work make possible the Gospel and the New
     Testament?  (Chapter 6)

     Without the Holy Spirit the disciples went back to their prior jobs
     (fishing, etc.), not knowing what to do.  When the Holy Spirit indwelled
     them at Pentecost, that's what set the stage for the missionary nature of
     the church.  The Spirit inspired people to preach the good news, to teach
     doctrine, and to write the books and letters that would become the New
     Testament.  Without the Holy Spirit, Christianity wouldn't have happened.

Chapters 7--10:

  1. State six aspects of God which do not change.  (Chapter 7)

     1. His life.
     2. His character.
     3. His truth.
     4. His ways.
     5. His purposes.
     6. His Son.

  2. Why does God never need to "repent"?  (Chapter 7)

     Repenting means revising one's judgment and changing one's plan of action.
     God never does this; He never needs to, for His plans are made on the
     basis of complete knowledge and control which extend to all things past,
     present, and future, so there can be no sudden emergencies or unlooked-for
     developments to take Him by surprise.

  3. What should our response be to the majesty of God?  (Chapter 8)

     Rebuke our wrong thoughts about God, our wrong thoughts about ourselves,
     and our slowness to believe in God's majesty.

  4. How are God's wisdom and human wisdom different?  (Chapter 9)

     Wisdom is the power to see, and the inclination to choose, the best and
     highest goal, together with the surest means of attaining it.  God alone
     is naturally and entirely and invariably wise.  Man is limited in his
     ability to do any of these things.

  5. How is human wisdom like driving a car?  (Chapter 10)

     It's trying to see and do the right things in the situation that presents
     itself.

  6. What effect does God's gift of wisdom have on us?  (Chapter 10)

     The effect of His gift of wisdom is to make us more humble, more joyful,
     more godly, more quick-sighted as to His will, more resolute in the doing
     of it and less troubled (not less sensitive, but less bewildered) than we
     were at the dark and painful things of which our life in this fallen world
     is full.

Chapters 11--13:

  1. What two facts are assumed in every biblical passage?  (Chapter 11)

     God is king and he speaks.

  2. In the "Believe and Obey" section, how does Packer describe a Christian?
     How well do you think this describes you?  (Chapter 11)

     He is a man who acknowledges and lives under the word of God.  He submits
     without reserve to the word of God written in 'the Scripture of truth'
     (Dan. 10:21), believing the teaching, trusting the promises, following the
     commands.  His eyes are to the God of the Bible as his Father, and the
     Christ of the Bible as his Saviour.  He will tell you, if you ask him,
     that the word of God has both convinced him of sin and assured him of
     forgiveness.  His conscience, like Luther's, is captive to the word of
     God, and he aspires, like the psalmist, to have his whole life brought
     into line with it[...]  The promises are before him as he prays, and the
     precepts are before him as he moves among men.  He knows that in addition
     to the word of God spoken directly to him in the Scriptures, God's word
     has also gone forth to create, and control, and order things around him;
     but since the Scriptures tell him that all things work together for his
     good, the thought of God ordering his circumstances brings him only joy.
     He is an independent fellow, for he uses the word of God as a touchstone
     by which to test the various views that are put to him, and he will not
     touch anything which he is not sure that Scripture sanctions.

     I'd say that's a decent description of me.

  3. What three points about God's love does Packer highlight from Romans 5:5?
     (Chapter 12)

     1. Paul is not talking about faint and fitful impressions, but of deep and
        overwhelming ones.
     2. The knowledge of the love of God, having once flooded our hearts, still
        fills them now.
     3. The instilling of this knowledge is described as part of the regular
        ministry of the Spirit to those who receive Him.

  4. Read through the final section of Chapter 12 ("Amazing Love").  Are there
     areas of your life that you need to open up more to God's love?

     I could stand to trust more that God has placed me in our current
     circumstances for a purpose.

  5. What does Packer mean when he says, "Those who suppose that the doctrine
     of God's grace tends to encourage moral laxity... do not know what they
     are talking about"?  (Chapter 13)

     If you are indeed a recipient of God's grace, you cannot help but respond
     accordingly, and moral laxity becomes an impossibility.

Chapters 14--16

  1. What four ideas are involved in God being the judge?  (Chapter 14)

     ANSWER

  2. What is the relation of Jesus Christ to God's judgment?  (Chapter 14)

     ANSWER

  3. Can you explain God's wrath?  (Chapter 15)

     ANSWER

  4. According to Romans, what is God's wrath?  (Chapter 15)

     ANSWER

  5. What three lessons does Packer draw from his consideration of the goodness
     and severity of God?  How can we apply each one?  (Chapter 16)

     ANSWER

Chapters 17--18

  1. Packer describes two types of jealousy.  How is the second type a positive
     virtue?  (Chapter 17)

     ANSWER

  2. What are the practical consequences of God's jealousy for those who
     profess to be God's people?  (Chapter 17)

     ANSWER

  3. What does the word "propitiation" mean?  (Chapter 18)

     ANSWER

  4. Explain in a sentence for each how the following help us understand
     propitiation.  (Chapter 18)

     * The driving force in Jesus' life.

       ANSWER

     * What of those who reject God?

       ANSWER

     * What is peace?

       ANSWER

     * The dimensions of God's love.

       ANSWER

     * The glory of God.

       ANSWER

Chapters 19--20

  1. Is God the Father of all people?  What sort of "sonship" can a human being
     have?  (Chapter 19)

     ANSWER

  2. Why is the "Fatherhood" of God such an important concept?  (Chapter 19)

     ANSWER

  3. What are the four aspects of God as Father Packer describes?  (Chapter 19)

     ANSWER

  4. How does our adoption of the Father show us the richness of God's grace? 
     (Chapter 19)

     ANSWER

  5. In what ways do you struggle with God's guidance in your life?  (Chapter
     20)

     ANSWER

  6. How does God guide you?  (Chapter 20)

     ANSWER

  7. What are the six common pitfalls we have about God's guidance?  (Chapter
     20)

     ANSWER
