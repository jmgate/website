Seeking Allah, Finding Jesus
----------------------------

*A Devout Muslim Encounters Christianity*

* Author:  Nabeel Qureshi
* Publication Year:  2014
* Link:  https://smile.amazon.com/Seeking-Allah-Finding-Jesus-Christianity/dp/0310515025/ref=sr_1_2?keywords=seeking+allah+finding+jesus&qid=1641086663&sprefix=seeking+allah+find%2Caps%2C341&sr=8-2
* Started on:  2022-01-01
* Finished on:  TBD

Questions
=========

1. Read the *Introduction* from Nabeel.  What are the three purposes of the
   book?  What does he add in the *Introduction* that reveals something of the
   kind of person he is?

   1. Give non-Muslim readers an inside perspective of a Muslim's heart and
      mind.
   2. Contrast the strength of the case for the gospel and Jesus against that
      of the Quran and Muhammad.
   3. Portray the immense inner struggle of Muslims grappling with the gospel.

   The benediction the author includes at the end of the introduction showcases
   how he's motivated by his love for and the Lordship of Christ in his life.

2. Now read the *Prologue*.  How does the closing request in his prayer give us
   an understanding of why he even started his spiritual journey?

   ANSWER

3. Since this is an autobiography, I would like for you to focus on several
   aspects of Nabeel's journey and just jot them down as you read.

   a. List the points of contrast between Islam and Christianity.

      ANSWER

   b. Point out key truths that made Nabeel seriously consider Christianity.

      ANSWER

   c. Point out the role friends and others played in his life.  How would you
      describe their approach and attitude?

      ANSWER
