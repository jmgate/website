How Now Shall We Live?
----------------------

* Authors:  Charles Colson and Nancy Pearcey
* Publication Year:  1999
* Link:  https://smile.amazon.com/How-Now-Shall-We-Live/dp/084235588X/ref=sr_1_1?dchild=1&keywords=how+now+shall+we+live&qid=1622568879&sr=8-1
* Started on:  2021-07-27
* Finished on:  2021-12-12

..
        What Kind of Book is This?
        ==========================

        What is the Book About?
        =======================

How is the Book Organized?
==========================

Introduction---How Now Shall We Live?

  Our culture today is falling apart in its rebellion against God.  The church
  generally doesn't realize that Christianity is a worldview, a framework that
  speaks to *absolutely everything* in existence.  As such, it is ill-equipped
  to carry out the cultural mandate of being God's redemptive agent of blessing
  in the world.  Our culture is showing signs that it is starting to wake up to
  the lies it has been operating under for centuries.  The time to reach the
  world with the message that all truth is God's truth is now.

..
  * We live in a culture that is at best morally indifferent.  Judeo-Christian
    values are mocked; immorality in high placs is not only ignored but even
    rewarded; violence, banality, meanness, and disintegrating personal
    behavior are destroying civility and endangering the very life of our
    communities.
  * Battle weary, we are tempted to withdraw into the safety of our
    sanctuaries, to keep busy by plugging into every program offered by our
    megachurches, hoping to keep ourselves and our children safe from the
    coming desolation.
  * Churches known for their biblical preaching think that the church's mission
    is to prepare for Jesus' return through prayer, Bible study, worship,
    fellowship, and witnessing.  If in the process we ignore our responsibility
    to redeem the surrounding culture, our Christianity will remain privatized
    and marginalized.
  * Turning our backs on the culture is a betrayal of our biblical mandate and
    our own heritage because it denies God's sovereignty over all of life.
    Nothing could be deadlier for the church---or more ill-timed.
  * The process of secularization begun in the Enlightenment is grinding to a
    halt.  Moral discourse is reviving.  For the first time in years, many
    people are actually willing to admit that private immorality has public
    consequences.  All the ideologies, all the utopian promises that have
    marked this century have proven utterly bankrupt.  We have finally achieved
    individual autonomy, yet this has not produced the promised freedom.  We
    have discovered that we cannot live with the chaos that inevitably results
    from choice divorced from morality.
  * We must show that Christianity is a comprehensive life system that answers
    all of humanity's age-old questions:  Where did I come from?  Why am I
    here?  Where am I going?  Does life have any meaning and purpose?
    Christianity offers the only viable, rationally defensible answers to these
    questions.
  * We must understand that God's revelation is the source of *all truth*, a
    comprehensive framework for all of reality.  The church's singular failure
    in recent decades has been the failure to see Christianity as a life
    system, or worldview, that governs every area of existence.  Our failure to
    see Christianity as a comprehensive framework of truth has crippled our
    efforts to have a redemptive effect on the surrounding culture.
  * Saving grace is the means by which God's power calls people who are dead in
    their trespasses and sins to new life in Christ.  Common grace is the means
    by which God's power sustains creation, holding back the sin and evil that
    result from the Fall and that would otherwise overwhelm his creation like a
    great flood.
..

Part One.  Worldview:  Why It Matters

  Christianity is not merely a collection of doctrines and practices, but a
  comprehensive framework for understanding all of reality---it is a worldview.
  Living our lives in accordance with what God says is true about life, the
  universe, and everything will transform the world---its cultures, social and
  economic institutions, etc.---from what it is into more of what God intends
  it to be.  To do so we must understand that the truths of Christianity are
  attacked (often subtley) on all sides by ideas stemming from the assumption
  that God does not exist.  We must familiarize ourselves with such ideas so we
  know how to identify how they differ from a Christian worldview, and how they
  fail to adequately answer the fundamental questions of life:  Who am I?  Why
  am I here?  What's wrong with the world?  How can we fix it?

  1. A New Creation

     Understanding Christianity as a worldview leads to substantial change in
     the world around us.

..
     * Jorge Crespo's faith was not just a personal matter but a framework for
       all of life.  Everything he did had to be motivated by God's truth.
     * By nurturing the flower of justice in what was once the most evil of
       gardens, by living out the reality of being a new creation in Christ,
       Jorge Crespo has helped to create a whole new world for others.  And the
       forces of hell are being conquered by the power of heaven.
..

  2. Christianity Is a Worldview

     A worldview is the sum total of your beliefs about the world, and it
     shapes how you live your life, the decisions you make.  A Christian's
     worldview should be based on the truths revealed to us in Scripture, but
     many believers don't realize that those truths are meant to inform *all of
     life*.  Understanding that they are enables us to live more rationally, as
     we'll be living in accordance with the laws and ordinances God built in to
     his creation.  Only when we do so can we live the best life possible;
     defying the natural or moral law leads to consequences.  The culture war
     is a clash of worldviews.  We must understand the ideas that seduce people
     away from the truth.

..
     * In every action we take, we are doing one of two things:  we are either
       helping to create a hell on earth or helping to bring down a foretaste
       of heaven.
     * Renewal can occur when Christians are committed to living out their
       faith, seeing the world as God sees it, viewing reality through the lens
       of divine revelation.
     * Our choices are shaped by our worldview.  Worldview is intensely
       practical.  It is simply the sum total of our beliefs about the world,
       the "big picture" that directs our daily decisions and actions.
     * Our major task in life is to discover what is true and to live in step
       with that truth.
     * The basis for the Christian worldview, of course, is God's revelation in
       Scripture.  Yet sadly, many believers fail to understand that Scripture
       is intended to be the basis for all of life.  Evangelicals have been
       particularly vulnerable to this narrow view of the sacred/secular divide
       because of our emphasis on personal commitment.  On the one hand, this
       has been the movement's greatest strength, bringing millions to
       relationship with Christ.  But this emphasis on a personal relationship
       can also be evangelicalism's greatest weakness because it may prevent us
       from seeing God's plan for us beyond personal salvation.
     * Genuine Christianity is a way of seeing and comprehending *all* reality.
       It is a worldview.  In every area of life, genuine knowledge means
       discerning the laws and ordinances by which God has structured creation,
       and then allowing those laws to shape the way we should live.  All truth
       is God's truth.  We are compelled to see Christianity as the
       all-encompassing truth, the root of everything else.  It is ultimate
       reality.
     * Understanding Christianity as a total life system enables us to make
       sense of the world we live in and thus order our lives more rationally.
       It also enables us to understand forces hostile to our faith, equipping
       us to evangelize and to defend Christian truth as God's instruments for
       tranforming culture.
     * To live in defiance of known physical laws is the height of folly.  But
       it is no different with the moral laws prescribing human behavior.  No
       transgression of moral law is without painful consequences.  If we want
       to live healthy, well-balanced lives, we had better know the laws and
       ordinances by which God hs structured creation.
     * Wisdom in Scripture is, broadly speaking, the knowledge of God's world
       and the knack of fitting oneself into it.  To be wise is to know reality
       and then accomodate yourself to it.  Folly is a stubborn swimming
       against the stream of the universe, spitting into the wind, coloring
       outside the lines.  Serious Christians actually live happier, more
       fulfilled, more productive lives by almost every measure.
     * We are commanded both to preach the Good News and to bring all things
       into submission to God's order, by defending and living out God's truth
       in the unique historical and cultural conditions of our age.  To engage
       the world, however, requires that we understand the great ideas that
       compete for people's minds and hearts.  Ideas have consequences.
     * We have not identified the worldviews that lie at the root of cultural
       conflict---and this ignorance dooms our best efforts.  The real war is a
       cosmic struggle between worldviews---between the Christian worldview and
       the various secular and spiritual worldviews arrayed against it.  This
       is what we must understand if we are going to be effective both in
       evangelizing our world today and in transforming it to reflect the
       wisdom of the Creator.
..

  3. Worldviews in Conflict

     Broadly speaking, belief systems fall into the two categories of the
     *naturalistic*, stating that nature is all that there is, and the
     *theistic*, holding that there is something beyond nature.  The
     naturalistic perspective gives rise to a number of philosophical
     frameworks that are rife with logical flaws and internal inconsistencies.
     Tolerance of multiple perspectives has been redefined and elevated to such
     a status that questioning the implicit assumption that all perspectives
     are equally valid, or morally equivalent, is no longer tolerated.  This
     climate of apathy, in which no idea is worth fighting for, means we can't
     pursuade people with rational arguments, which makes it that much harder
     to convince people of the truth of Christianity.

..
     * America's culture war is increasingly spilling over into other nations.
       Our own effectiveness in defending and contending for truth has
       repercussions across the entire globe.
     * Theism
         The belief that there is a transcendent God who created the universe.
       Naturalism
         The belief that natural causes alone are sufficient to explain
         everything that exists.
       Moral Relativism
         If nature is all there is, then there is no transcendent source of
         moral truth, and we are left to construct morality on our own.
       Multiculturalism
         The naturalist treats all cultures as morally equivalent, each merely
         reflecting its own history and experience.  If there is no
         transcendent source of truth or morality, then we find our identity
         only in our race, gender, or ethnicity.
       Pragmatism
         Whatever works best is right.  Actions and policies are judge on
         utilitarian grounds alone.
       Utopianism
         If only we create the right social and economic structures, we can
         usher in an age of harmony and prosperity.
       This-World Perspective
         Naturalists consider only what happens in this world, this age, this
         life.
       Post-Christian
         Americans, along with most other Western cultures, no longer rely on
         Judeo-Christian truths as the basis of their public philosophy or
         their moral consensus.
       Postmodernism
         Rejects any notion of a universal, overarching truth and reduces all
         ideas to social constructions shaped by class, gender, and ethnicity.
       Existentialism
         Life is absurd, meaningless, and the individual self must create his
         own meaning by his own choices.
     * Tolerance has become so important that no exception is tolerated.  But
       if all ideas are equally valid, as postmodernism insists, then no idea
       is really worth our allegiance; nothing is worth living or dying
       for---or even arguing about.  And this climate of apathy can actually
       make it harder than ever to witness to the truth of Christianity.
     * There can be no basis for law without a Christian consensus, or at least
       a recognition of natural law.
     * Debate can be unpleasant at times, but at least it presupposes that
       there are truths worth defending, ideas worth fighting for.  If there is
       no truth, then we cannot persuade one another by rational arguments.
       All that's left is sheer power---which opens the door to a new form of
       fascism.
..

  4. Christian Truth in an Age of Unbelief

     We will often come up against people with false ideas, but before we can
     address the ideas themselves, we first need to address the worldview gap
     between us.  If we don't, we're effectively speaking a different language
     and nothing will even begin to make sense.  Start by listening for their
     answers to the four fundamental questions, paying attention to where their
     thoughts differ from those of Christianity.  You can't get to why
     Christianity matters unless you first have an understanding of who God is
     and why we owe him our obedience.  Our commission is not merely to save
     souls, but also minds, and to do so in a way that brings the truths of
     scripture to bear on all of life, such that we revive the culture around
     us and awaken it to what God desires it to be.

..
     * False ideas are the greatest obstacles to the reception of the gospel.
       We may preach with all the fervor of a reformer and yet succeed only in
       winning a straggler here or there, if we permit the whole collective
       thought of a nation or of the world to be controlled by ideas which by
       the resistless force of logic, prevent Christianity from being regarded
       as anything more than a harmless delusion.
     * In Marxism, human beings are merely a complex form of matter, and their
       identity lies in the way they relate to other forms of matter---that is,
       how they shape and make material things, or the means of production.
     * Pre-evangelism is addressing the huge gap between his worldview and
       mine.
     * Only love can overcome our sinful self-centeredness.  At the core,
       people are spiritual beings, not pawns of economic forces.  Sin begins
       in the heart, where it battles for control of our very being.
     * Only after establishing who God is and why we are morally responsible to
       him did Paul talk about repentance and Christ's resurrection.  Today,
       many people are completely unfamiliar with even basic biblical teaching,
       and we must find ways to engage those who think more like Greeks than
       Jews.  Today, more than ever, we are aliens in our own land, worldview
       missionaries to our own post-Christian, postmodernist culture.
     * The world can accept that we love Jesus---they can even acknowledge the
       social benefits of religion---and yet they can still think that he is
       merely a human or mythical figure.
     * We have a strong case to make, and people will listen if we cast it in
       terms of the questions they have.  God has created each of us with a
       mind, with the capacity to study, think, and ask questions.  If our
       culture is to be transformed, it will happen from the bottom up---from
       ordinary believers practicing apologetics over the backyard fence or
       around the barbecue grill.  Most of those who object to Christianity are
       simply spiritually blind, and our job is lovingly to help bring them
       into the light.
     * The cultural commission is the call to create a culture under the
       lordship of Christ.  He calls us to be agents not only of his saving
       grace but also of his common grace.  All citizens live better in a world
       that more closely conforms to reality, to the order God created.
     * We ought to interpret biblical truth in ways that appeal to the common
       good.  We do not have to derive all arguments directly from Scripture.
     * The Christian calling is not only to save souls but also to save minds.
       Sadly, many Christians have been misled into believing there is a
       dichotomy between faith and reason, and as a result they have actually
       shunned intellectual pursuits.  Pastors must begin to redefine their
       task to include intellectual evangelism, for if they do not preach to
       issues of the mind, they will find themselves increasingly alienated
       from their own flock.
     * Every day we face attempts to seduce us into worshipping the idols of
       modern life, sometimes cleverly disguised.
     * You can avoid the debate [over contested issues of the faith] if you
       choose.  You need only drift with the current.  Preach every Sunday
       during your seminary course, study as you studied in college---and these
       questions will probably never trouble you.  The great questions may be
       easily avoided.  Many preachers are avoiding them.  And many preachers
       are preaching to the air.  The church is waiting for men of another
       type.
     * When the church is truly the church, a community living in biblical
       obedience and contending for faith in every area of life, it will surely
       revive the surrounding culture or create a new one.  The material order
       rests on the spiritual order.
..

Part Two.  Creation:  Where Did We Come From, and Who Are We?

  All worldviews start with the question of origins, and the answers generally
  fall into the categories of the naturalistic and the theistic.  Naturalism is
  a philosophy that goes back centuries that claims that nature is all that is,
  or was, or ever will be.  As it was gaining in popularity, Darwin's
  hypothesis of natural selection and its implications for the evolution of all
  life from base ingredients gave naturalism the pseudo-scientific rationale it
  was looking for.  Scientists have tried to show you could create life in a
  test tube, but their efforts fall far short and only support that if such a
  thing were possible, it would only happen under the careful direction of an
  intelligent agent.  What we now know from breeding, mutation, and the fossil
  record indicates that natural selection is not the engine for unbounded
  change required by evolution.  Yet despite the evidence, the world today is
  saturated with naturalism, and attempting to question it isn't a fair fight,
  because what's at stake isn't a scientific theory, but a worldview with
  implications in every aspect of public and private life.  If naturalism is
  wrong, and if the scientific evidence actually supports the idea that the
  only thing that could've created the world was an intelligent being outside
  of the physical universe, then we need to go home and rethink our lives, and
  we don't want to.

  5. Dave and Katy's Metaphysical Adventure

     The world is currently *saturated* with the idea that science can explain
     everything about our existence, and your religious beliefs are fine if you
     want to have them to make you feel good.  However, honest questions about
     theories of naturalistic origins are not tolerated, and there's no
     applying scientific methodology to "scientific" theories of origins.  For
     young people growing up in this environment, it can be *excruciating* to
     go against the flow of what they're being inundated with day in and day
     out.  Naturalism is a philosophy, a worldview, bent on eradicating the
     idea of God as creator.  If God isn't the creator, all of Christianity
     goes up in smoke, but if he is, then it's not a matter of "my beliefs" or
     "your beliefs," it's a matter of the ultimate truth of reality.

..
     * Somewhere in the endless reaches of the universe, on the outer edge of
       the galaxy of a hundred-thousand-million suns, deep within the cluster
       of slowly forming planets, is a small sphere of just the right size, a
       sphere just the right distance from its mother star.
     * And then the clouds of gas and steam condense and rain upon the planet.
       From the play of chemicals in the primeval ocean arose tiny,
       single-celled plants that captured the energy of the sun, producing
       oxygen required for the more advanced organisms to evolve.
     * The universe was about to come into existence through the big bang.
       What was there before the big bang?
     * It's just that I don't want to be so different.  And I don't have to be.
       I can be a good person without believing the things you believe.  I
       don't think it's a question of what anyone believes.  It's a question of
       what's true.  How does anyone know what's really true?  Science is
       things that are proved.  Most of it was more like philosophy.
     * By chance the universe came into existence, by chance Earth was just
       right for life to exist, by chance life developed into birds and bees
       and butterflies, by chance human beings came along, and by chance human
       beings turned out to be so smart that all the world's problems will
       someday succumb to our technological prowess.  End of story.
       Hallelujah, amen.
     * People want to believe they're important, so they invent religion.  They
       invent the idea of a God who created them so they'll feel better.
     * If you dismiss God as the Creator, then the whole foundation of faith
       dissolves.
     * Everything is at stake.  If Christianity is true, then it's not my
       belief or your mother's belief.  It's the truth about *reality*, about
       what is ultimately real.
..

  6. Shattering the Grid

     Whatever you take as the starting point of your worldview functions as
     your religion.  Naturalism begins with assumptions (e.g., that nature is
     all that was, or is, or ever will be) that can't be tested with scientific
     methodology, and as such those starting premises are taken on faith.  The
     naturalist worldview is proclaimed all around us as absolute
     truth---hilarious, as naturalist philosophy leads to postmodernism, in
     which there is no absolute truth---while other religious beliefs are
     claimed to be simply matters of personal, subjective opinion.

..
     * The chief aim of all investigations of the external world should be to
       discover the rational order and harmony which has been imposed on it by
       God.
     * She had absorbed the idea that science is the source of truth, while
       religion is merely subjective opinion, something we tolerate for those
       weak enough to need that kind of comfort.
     * The dominant view in our culture today is radically one-dimensional:
       that this life is all there is, and nature is all we need to explain
       everything that exists.  This is, at heart, the philosophy of
       naturalism, and not only has it permeated the classroom curriculum, but
       it has also been expressed widely in popular culture, from Disney World
       to television nature shows to children's books.
     * Naturalistic scientists try to give the impression that they are
       fair-minded and objective, implying that religious people are subjective
       and biased in favor of their personal beliefs.  But this is a ruse, for
       naturalism is as much a philosophy, a worldview, a personal belief
       system as any religion is.  Naturalism begins with premises that cannot
       be tested empirically, such as the assumption that nature is "all that
       is or ever was or ever will be."  This is not a scientific statement,
       for there is no conceivable way it could be tested.  It is a philosophy.
     * Whatever you take as the starting point of your worldview does function,
       in effect, as your religion.
     * Even unbelievers know somewhere deep within that God must exist.  Paul
       teaches that those who look honestly at the world around them should be
       able to conclude that it was created by and intelligent Being.
..

  7. Let's Start at the Very Beginning

     Most ancient cultures have believed that the universe was more or less
     eternal, and science also held this viewpoint until relatively recently.
     As our scientific investigations into the cosmos has progressed, however,
     we've determined that the universe did, in fact, have a beginning---what's
     been called "the big bang".  The first two laws of thermodynamics
     (conservation of matter, and entropy) indicate that there must have been a
     beginning, and that beginning must have been triggered by something
     outside the physical universe itself.  Determining what triggered the
     universe into being is outside the purview of science, as there is no way
     for us to scientifically observe something outside the physical universe.
     Unfortunately, many scientists, seeking to avoid the theological
     implications of the origin of the natural world, have started proposing
     ever more ludicrous hypotheses (many universes, self generation, imaginary
     time, etc.) to explain our origin without resorting to an intelligent
     transcendent mind as the agent behind it.  Additionally, the natural world
     we observe seems eerily well-suited to both support life itself, and
     support our observation of it.  If the fundamental constants of physics
     were slightly different, or if Earth's situation were slightly different,
     either the universe as we know it wouldn't exist, or we wouldn't.  The
     denial of the straightforward implications of our scientific discoveries
     demonstrates the committment to the atheistic agenda of many in the
     scientific community.

..
     * [Design] is the most empirical of the arguments for God [based on]
       observational premises about the kind of order we discover in nature.
     * The first question any worldview must answer is how it all started.
     * After maintaining for centuries that the physical universe is eternal
       and therefore needs no creator, science today has uncovered dramatic new
       evidence that the universe did have an ultimate origin, that it began at
       a finite time in the past---just as the Bible teaches.
     * Most ancient cultures believed that the universe is eternal---or, more
       precisely, that it was formed from some kind of primordial material that
       is eternal.
     * Today the indestructibility or permanence of matter is a scientific
       fact.  Those who talk about an independent or supernatural creative
       force that created the universe out of nothing are in antagonism with
       the first and simplest axiom of a philosophical view of nature.  The
       idea that the universe had a beginning was reduced to a bare article of
       religious faith, standing in lonely opposition to firmly established
       science.
     * The second law of thermodynamics, the law of decay, implies that the
       universe is in a process of gradual disintegration.  The inescapable
       inference is that everything had a *beginning:*  somehow and sometime
       the cosmic processes were started, the steller fires ignited and the
       whole vast pageant of the universe brought into being.
     * The first law of thermodynamics (the conservation of matter) implies
       that matter cannot just pop into existence or create itself.  And
       therefore, if the universe had a beginning, then something *external* to
       the universe must have caused it to come into existence.
     * As a result, the idea of creation is no longer merely a matter of
       religious faith; it is a conclusion based on the most straightforward
       reading of the scientific evidence.  The big bang is the one place in
       the universe where there is room, even for the most hard-nosed
       materialist, to admit God.
     * Big bang theory delivers a near fatal blow to naturalistic philosophy,
       for the naturalistic credo regards reality as an unbroken sequence of
       cause and effect that can be traced back endlessly.  But the big bang
       represents a sudden discontinuity in the chain of cause and effect.
       Science has reached its limit; it will never be able to discover whether
       the agent of creation was the personal God of the Old Testament or one
       of the familiar forces of physics.
     * Perhaps the most common strategy among scientists and educators today is
       simply to ignore the startling implications of the big bang, labeling
       them "philosophy" or "religion" and shunting them aside.  Discussion of
       the ultimate cause *behind* the big bang is dismissed as philosophy and
       is given no place in the science classroom.
     * Other scientists face the facts of an ultimate beginning, but in an
       effort to avoid the idea of a creator, they craft notions that are,
       frankly, illogical:  the self-generation of the universe, the early
       universe existing in "imaginary time", the universe simply popped into
       existence out of nothing.
     * Science has begun to sound eerily like Genesis 1:  "And God said, 'Let
       there be light'" (1:3).
     * What we need to avoid in such situations is giving the mistaken idea
       that Christianity is opposed to science.  If we are too quick to quote
       the Bible, we will never break out of the stereotype spread by *Inherit
       the Wind*.  We should not oppose science with religion; we should oppose
       *bad* science with *better* science.
     * What came before the big bang?  What caused it?  If the big bang was the
       origin of the universe itself, then its cause must be something
       *outside* the universe.
     * What a message for kids to hear.  It tells them they're nothing more
       than a cosmic accident.  Small wonder that, over time, language about a
       loving God who created them and loves them sounds more and more like a
       fairy tale.
     * Not only are scientists acknowledging an ultimate beginning, but they
       are also recognizing that the physical structure of the universe gives
       striking evidence of purpose and design.  They have proposed what is
       known as the *anthropic principle*, which states that the physical
       structure of the universe is exactly what it must be in order to support
       life.
     * From the perspective of the space age, it has become clearer than ever
       that Earth is unique.  It boasts a wealth of characteristics that make
       it capable of supporting life---a nearly endless list of preconditions
       that have been exquisitely met only, as far as we know, on our planet.
     * If Earth were even slightly closer to the sun, all its water would boil
       away, and life would be impossible.  On the other hand, if Earth were
       only slightly farther away from the sun, all its water would freeze, and
       the terrestrial landscape would be nothing but barren deserts.
     * The chemical reactions necessary for life to function occur within a
       narrow temperature range, and Earth is exactly the right distance from
       the sun to fall within that range.  What's more, for this to happen,
       Earth must remain about the same distance from the sun in its orbit;
       that is, its orbit must be nearly circular---which it is.
     * From a molecular standpoint, the various properties of water are nothing
       short of miraculous.  No other compound even comes close to duplicating
       its many life-supporting properties.
     * The fact that the force of gravity just happens to be the right number
       with such stunning accuracy, is surely one of the greatest mysteries of
       cosmology.
     * There is no known physical reason, no natural explanation, for the
       precise balance in the electrical charges of the proton and the
       election---especially when you consider that the two particles differ
       from one another in all other respects:  in size, weight, magnetic
       properties, and so on.
     * It turns out that the slightest tinkering with the values of the
       fundamental forces of physics---gravity, electromagnetism, the strong
       and weak nuclear forces---would have resulted in a universe where life
       was utterly impossible.
     * One of the widely held versions of the anthropic principle is the "many
       worlds" hypothesis.  According to this theory, an infinite number of
       universes exist, all with different laws and different values for
       fundamental nubmers.  Most of these universes are dark, lifeless places.
       But by sheer probability, some will have just the right structure to
       support life.  The "fit" universes survive, while the "unfit" are weeded
       out.  Our own, of course, happens to be a universe "fit" for life.
     * The idea is purely a product of scientific imagination.  Candid
       scientists admit that the whole idea is motivated by a desire to avoid
       the theological implications of the anthropic principle.  Physicist
       Heinz Pagels says that if the universe appears to be tailor-made for
       life, the most straightforward conclusion is that is *was* tailor-made,
       created by a transcendent God; it is only because many scientists find
       that conclusion "unattractive" that they adopt the theory of multiple
       universes.
     * Another version is the *participatory anthropic principle*, which holds
       that the universe did not fully exist until human beings emerged to
       observe it.  And so, in order to become fully real, the universe decided
       to evolve human consciousness.  The universe wants to be known.  It is
       astonishing that scientists will dismiss the idea of a Creator as
       unscientific, yet turn around and embrace the bizarre, almost mystical
       concept of a conscious universe.
     * The fact that so many scientists are willing to accept wild speculations
       about unseens universes for which not a shred of observational evidence
       exists suggests something about both the power of the modern atheistic
       ideology and the cultural agenda of many in the scientific profession.
       The mainstream scientific community has in effect shown its attachment
       to the atheistic ideology of the random universe to be in some respects
       more powerful than its commitment to the scientific method itself.
     * We intuitively recognize the products of design versus the products of
       natural forces.  When we try to explain any natural phenomenon, there
       are three possibilities:  chance, law, or design.  Irregular, erratic,
       and unspecified:  random.  Regular, repeatable, and predictable:  law.
       unpredictable and yet highly specified:  design.
..

  8. Life in a Test Tube?

     Scientists have attempted to show that life could spontaneously arise from
     the conditions of a supposed primordial earth by replicating those
     conditions in the laboratory.  However, what such experiments actually
     show is a far cry from what media reporting on them claims.  What they
     have shown is if you want to get even the most basic building blocks for
     life, you need an intelligent agent diligently controlling the conditions
     of the experiment, the reactions taking place, etc., and with all our
     technological prowess, we haven't come anywhere close to producing life.
     The thought, though, is that over millions of years the improbable becomes
     inevitable; however, simulations indicate that the probability of life
     arising spontaneously is essentially zero, regardless of the timeframe you
     examine.  The key problem for the naturalist is the difference between
     order with low information content (as in the lattice structure of
     crystals) versus order with high information content (as in the structure
     of DNA).  Empirical evidence makes it clear that natural forces do not
     produce structures with high information content, and that's not to say we
     don't know how it works, but rather everything we've observed says it
     doesn't work.  There are times when it is more rational to accept a
     supernatural explanation, and this is one of those times.

..
     * A little science estranges a man from God.  A lot of science brings him
       back.
     * The way scientists try to prove that life arose in the primitive seas is
       to re-create the same conditions in the laboratory and see what happens.
     * One of the best-known experiments occurred in 1953.  Stanley Miller, of
       the University of Chicago, had mixed simple chemicals and gases in a
       glass tube, then zapped them with an electical charge to induce chemical
       reactions.  What emerged at the other end of the laboratory apartus were
       amino acids.
     * It also set off a domino series of similar experiments, some using heat
       as an energy source, others using ultraviolet light to simulate light
       from the sun.  Most of these experiments have succeeded in producing
       amino acids, and the amino acids have even linked up in chains
       resembling proteins.
     * Living things are highly selective:  They use only the left-handed amino
       acids.  But when Miller and his colleagues mixed chemicals in the
       laboratory, they got both kinds---an even fifty-fifty mix of left- and
       right-handed.  There is no natural process that produces only
       left-handed amino acids, the kind required by living things.  All of
       this means that the amino acids formed in the test tube are useless for
       life.
     * The proteins in living things are comprised of amino acids hooked
       together in a very particular chemical bond called a peptide bond.  But
       amino acids are capable of hooking together in all sorts of different
       ways, forming several different chemical bonds.  And in the test tube,
       they hook up in a variety of ways, never producing a genuine protein
       capable of functioning in a living cell.  For a protein to be
       functional, the amino acids must link up in a particular sequence.  Yet
       in laboratory experiments, all we get are scrambled, random sequences.
     * To get even useless, nonfunctional amino acids and proteins, researchers
       have to control the experiment in various ways.  They use only pure
       isolated ingredients.  They start each step with fresh ingredients,
       rather than the products of the prior step.  They screen out the longer
       wavelengths of ultraviolet light, which would destroy the very amino
       acids that scientists are hoping to get, and use shorter wavelengths
       instead.  They build traps to remove the amino acids from the reaction
       as soon as they're formed to protect them from disintegrating.
     * Even the most successful origin-of-life experiments tell us next to
       nothing about what could have happened under natural conditions.  They
       do suggest, however, that life can be created only by an intelligent
       agent directing, controlling, and manipulating the process.
     * Over millions of years, the unlikely becomes likely, the improbable is
       transformed into the inevitable.  The computers showed that the
       probability of evolution by chance processes is essentially zero, no
       matter how long the time scale.  As a result, today it is common to hear
       prominent scientists scoff at the idea that life arose by chance.
     * For the naturalist who assumes life evolved spontaneously, there is only
       one other logical possibility:  If life did not arise by random
       processes, then it must have arisen under the compulsion of forces in
       the matter itself, the notion of biochemical predestination.
     * If you look at the experiments, one thing that stands out is that you do
       not get ordered sequences of amino acids.  If we thought we were going
       to see a lot of spontaneous ordering, something must have been wrong
       with our theory.  It is becoming ever clearer that the experiments fails
       to support any naturalistic theory of life's origin.  What they do
       support is the idea of intelligent design.
     * Scientists committed to naturalism must try to construct an explanation
       of life based solely on physical-chemical laws.  They must explain the
       information in DNA as a product of natural processes at work in the
       chemicals that comprise living things.  But what makes DNA function as a
       message is not the chemicals themselves but rather their sequence, their
       pattern.  If the letters (nucleotides) are scrambled, the result is
       nonsense.
     * Several branches of science (forensic science, archaeology,
       cryptography, astronomy, etc.) already use the concept of intelligence
       and have even devised tests for detecting the work of an intelligent
       agent.  In everyday life, we weigh natural versus intelligent causes all
       the time without thinking much about it.  Ripples on a beach vs writing
       in the sand.  Clouds in the sky vs sky-writing.  The discovery of DNA
       provides powerful new evidence that life is the product of intelligent
       design.
     * Information theory is a field of research that investigates the ways
       information is transmitted.  Both chance and law lead to structures with
       low information content, whereas DNA has a very high information
       content.
     * In nature, both random patterns and regular patterns have low
       information content.  By contrast, DNA has a very high information
       content.  It would be impossible to produce a simple set of instructions
       telling a chemist how to synthesize the DNA of even the simplest
       bacterium.
     * If the DNA molecule were really analogous to a crystal, it would consist
       of a simple pattern repeating again and again, so crystal formation
       gives us no clue whatsoever to the origin of DNA.
     * Complexity theory is a new field of research that thinks it might
       finally uncover a law that can account for the spontaneous origin of
       life itself.  However, the ferns and swirls constructed by complexity
       theorists on their computer screens represent the same kind of order as
       a crystal.  There are no known physical laws capable of creating a
       structure like DNA with high informational content.
     * There are times when it is more rational to accept a supernatural
       explanation and when it is actually irrational to hold out for a natural
       one.
     * Empirical evidence makes it clear that natural forces do not produce
       structures with high information content.  This is not a statement about
       our *ignorance*---a "gap" in knowledge that will be filled in later by a
       natural explanation.  Rather, it is a statement about what we
       *know*---about our consistent experience of the character of natural
       processes.  Today, holding on to the hope that some natural process will
       be found to explain DNA is supremely irrational.  The elusive process
       that naturalists hope to find would have to be completely unprecedented,
       different in kind from any we currently know.  Surely *this* is an
       argument from ignorance.
..

  9. Darwin in the Dock

     Darwin's hypothesis was that behind the cyclical variations he observed in
     animal traits was an engine for unlimited change that, given enough time,
     could take nothing and produce the diversity he saw around him.  It was
     conjecture, nothing more---an extrapolation into the distant past, with
     the feeblest of backings---and it turns out he was simply mistaken.
     First, we have centuries of breeding to show that the best efforts of
     shuffling the genes in a population lead to specializations *of the same
     animal*; that is, organisms stay true to type, and variation away from the
     mean leads to a decrease in "fitness" (specifically the abilities to stay
     healthy and reproduce).  Second, random mutations, which are the only
     natural way of introducing new genetic information into a population,
     alter the details in existing structures, but don't lead to the creation
     of new structures, and the mutations again lead to a less fit organism.
     Finally, the fossil record bears out that the intermediary forms predicted
     by Darwin simply don't exist.  An organism is a highly complex integrated
     system.  If a single subsystem were to evolve on its own, that would
     likely lead to the catastrophic failure of the system as a whole.  To
     transition from one complex integrated system into another one, a whole
     host of corresponding changes need to be made immediately, and this is
     something natural selection can't do.  Darwin made his conjecture with far
     less information available to him than we have today, but the question is
     why the scientific establishment sticks with it so dogmatically in the
     face of the last 150 years of evidence.  The reason, of course, is that
     this isn't about science, but about a conflict of worldviews, and if the
     universe is governed by an intelligent personal being, then that's
     something I need to reckon with.

..
     * As long as Darwinists control the definitions of key terms [such as
       science], their system is unbeatable, regardless of the evidence.
     * Oh, I don't have any problem with God being at the start of it.  Maybe
       he did kick everything off, you know, back at the very beginning.  But
       everyone knows that once life was here, it evolved just as Darwin said
       it did.  I saw it in my textbook at school.
     * By coupling undirected, purposeless variation to the blind, uncaring
       process of natural selection, Darwin made theological or spiritual
       explanations of the life processes superfluous.
     * The best argument against Darwinism has been known for centuries by
       farmers and breeders, and it can be stated in a simple principle:
       Natural change in living things is limited.  Or, stated positively:
       Organisms stay true to type.
     * None of the changes have created a novel kind of organism.  Dog breeding
       has given rise to varieties ranging from the lumbering Great Dane to the
       tiny Chihuahua, but no variety shows any tendency to leave the canine
       family.  None of the examples cited in biology textbooks are evolving to
       a new level of complexity; they all simply illustrate variation around a
       mean.  Despite the spectacular variation in tails and feathers, all the
       pigeons Darwin observed remained pigeons.  They represent cyclical
       change in gene frequencies but no new genetic information.
     * He took the changes he had observed and *extrapolated* them back into
       the ancient past.  Given enough time, change would be virtually
       unlimited, and the pigeon might even be transformed into a completely
       different kind of bird.  It was a bold speculation, but no one should be
       misled into thinking it was more than that.  Neither Darwin nor anyone
       else has ever actually witnessed evolution occurring.  It is a
       conjecture, an extrapolation going far beyond any observed fact.
     * Centuries of experiments show that the change produced by breeding does
       *not* continue at a steady rate from generation to generation.  Instead,
       change is rapid at first, then levels off, and eventually reaches a
       limit that breeders cannot cross.  Why does progress halt?  Because once
       all the genes for a particular trait have been selected, breeding can go
       no further.  Breeding shuffles and selects among existing genes in the
       gene pool, combining and recombining them, much as you might shuffle and
       deal cards from a deck.  But breeding does not create new genes, any
       more than shuffling cards creates new cards.
     * What's more, as breeders keep up the selection pressure, the organism
       grows weaker until it finally becomes sterile and dies out.  Our highly
       bred cows and chickens produce more milk and eggs, but they are also
       much more prone to disease and sterility.  Moreover, when an organism is
       no longer subject to selective preessure, it tends to revert to its
       original type.  Darwin was simply mistaken in his extrapolation.  The
       minor change produced by shuffling genes is not an engine for the
       ultimited change required by evolution.  The natural tendency in living
       things is not to continue changing indefinitely but to stay close to the
       original type.
     * The only natural source of new genetic material in nature is mutations.
       In today's neo-Darwinism, the central mechanism for evolution is random
       mutation and natural selection.  Since a gene is like a coded set of
       instructions, a mutation is akin to a typing error.  If you introduce a
       typing error into a report you are writing, it is not likely to improve
       the report.  Most mutations are harmful, often lethal, to the organism,
       so that if mutations were to accumulate, the result would more likely be
       *de*-volution than evolution.
     * Mutations alter the details in *existing* structures---like eye color or
       wing size---but they do not lead to the creation of *new* structures.
       Furthermore, the minor changes observed do not accumulate to create
       major change---the principle at the heart of Darwinism.  Hence,
       mutations are not the source of the endless, limitless change required
       by evolutionary theory.
     * The same pattern holds throughout the past, as we see in the fossil
       record.  The overwhelming pattern is that organisms appear fully formed
       with variations clustered around a mean, and without transitional stages
       leading up to them.
     * The change in finch beaks is a cyclical fluctuation that allows the
       finches to adapt and survive.  It's a minor adjustment that allows
       finches to... stay finches.
     * Even more disturbing, some of the most famous examples have been exposed
       as hoaxes, e.g., peppered moths.
     * The tendency for organisms to stay true to type is so constant that it
       can be considered a natural law that keeps all living things within some
       more or less fixed limitations.
     * After more than 150 years, it has become clear that Darwin's speculation
       flies in the face of all the results of breeding and laboratory
       experimentation, as well as the pattern in the fossil record.
     * An organism is an integrated system, and any isolated change in the
       system is more likely to be harmful than helpful.  The only way to turn
       a fish into a land-dwelling animal is to transform it all at once, with
       a host of interrelated changes happening at the same time---not only
       lungs but also coadapted changes in the skeleton, the circulatory
       system, and so on.  Anything that is irreducibly complex cannot evolve
       in gradual steps.
     * If one part were to evolve in isolation, the entire system of
       interacting parts would stop functioning, and since, according to
       Darwinism, natural selection preserves the forms that function better
       than their rivals, the nonfunctioning system would be eliinated by
       natural selection.
     * If it could be demonstrated that any complex organ existed which could
       not possibly have been formed by numerous, successive, slight
       modifications, my theory would absolutely break down.
     * It's as if we asked how a stero system is made, and someone answered,
       "By plugging a set of speakers into an amplifier and adding a CD player
       and a tape deck."  Right.  The real question is how to make those
       speakers and amplifiers in the first place.
     * The most rational explanation of irreducibly complex structures in
       nature is that they are products of the creative mind of an intelligent
       being.
     * Since the scientific evidence is so pursuasive, why does the scientific
       establishment cling so tenaciously to Darwinian evolution?  Why is
       Darwinism still the official creed in our public schools?  Because the
       real issue is not what we see through the microscope or the telescope;
       it's what we adhere to in our hearts and minds.  Darwinism functions as
       the cornerstone propping up a naturalistic worldview, and therefore the
       scientist who is committed to naturalism before he or she even walks
       into the laboratory is primed to accept even the flimsiest evidence
       supporting the theory.
     * The core of the controversy is not science; it is a titanic struggle
       between opposing worldviews---between naturalism and theism.  Is the
       universe governed by blind material forces or by a loving personal
       being?
..

  10. Darwin's Dangerous Idea

      When Darwin came on the stage, it was in the midst of the naturalistic
      philosophy fighting for purchase against the Christian worldview in the
      western world.  Though the evolution hypothesis was admittedly weak
      scientifically, natural selection became the linchpin that finally gave
      naturalism its credibility.  As a worldview, naturalism shapes our
      understanding of *everything*.  For instance, ethics are merely ideas we
      get once we've evolved to a certain level, so there is no objective
      standard for morality, and we can create and recreate our own standards
      as we continue to progress; law is nothing more than a collection of
      policies deemed to be socially advantageous, not based on any concept of
      right and wrong; etc.  With no transcendent truth, we default to the
      perspectives of our affinity groups (ethnicity, gender, etc.), which vary
      across time and space.  Though Darwinism is put forth as science, it is
      philosophy---one that fails to reflect reality accurately.  Only
      Christianity consistently stands up to the test of practical living.

..
      * Do evolution and religion really conflict?  Science and religion cannot
        conflict, because they deal with different things:  Science is about
        facts, while religion struggles with human morality.
      * Darwinism is not just about mutations and fossils; it is a
        comprehensive philosophy stating that all life can be explained by
        natural causes acting randomly---which implies that there is no need
        for the Creator.  And if God did not create the world, then the entire
        body of Christian belief collapses.
      * No life after death; no ultimate foundation for ethics; no ultimate
        meaning for life; no free will.  The only reason anyone still believes
        in such things, is that people have not yet grasped the full
        implications of Darwinism.
      * I have found that any discussion with modernists about the weaknesses
        of the theory of evolution quickly turns into a discussion of politics,
        particularly sexual politics.  Why?  Because modernists typically fear
        that any discrediting of naturalistic evolution will end in women being
        sent to the kitchen, gays to the closet, and abortionists to jail.
        Most people sense instinctively that there is much more at stake here
        than a scientific theory---that a link exists between the material
        order and the moral order.
      * Our view of origins shapes our understanding of ethics, law,
        education---and yes, even sexuality.

        * **Ethics:**  Christian morality is not subjective, based on our
          personal feelings; it is objective, based on the way God created
          human nature.  Naturalism claims that morality is nothing more than
          an idea that appears in our minds when we have evolved to a certain
          level.  Consequently, there is no ultimate objective basis for
          morality; humans create their own standards.  The result is radical
          ethical relativism.
        * **Law:**  Traditionally, a nation's laws were understood to be based
          on a transcendent moral order.  Men do not make laws.  They do but
          discover them.  Laws must rest on the eternal foundation of
          righteousness.  Yet if Darwinism is true, there is no final,
          authoritative basis for law.  Laws are merely a codification of
          political policies judged to be socially and econommically
          advantageous.
        * **Education:**  Ideas are merely hypotheses about what will get the
          results we want, and their validity depends on whether they work.

      * Because Darwinism eliminates the transcendent, postmodernism draws the
        inevitable conclusion that there is no transcendent truth.  Each of us
        is locked in the limited perspective of our race, gender, and ethnic
        group.  Despite its flamboyant skepticism toward objective truth,
        ironically, postmodernism rests on an assumption that *something* is
        objectively true---namely Darwinism.
      * Darwinism thus forms the linchpin to the fundamental debate between
        Christianity and naturalism in virtually every subjecct area.  Since
        modern culture has given science authority to define the way the world
        "really is," Darwinism provides the scientific justification for a
        naturalistic approach in every field.
      * Many Christians hope to combine Darwin's biological theory with belief
        in God, yet Darwin himself insisted that the two are mutually
        exclusive.  Darwin is typically portrayed as a man forced to the theory
        of natural selection by the weight of the facts.  But today historians
        recognize that he was first committed to the philosophy of naturalism
        and then sought a theory to justify it scientifically.  Many of
        Darwin's earliest and most ardent supporters were quick to spot the
        scientific weaknesses in his theory, yet they chose to champion it
        because they saw it as a useful means of promoting naturalistic
        philosophy.
      * The historical data makes it clear that the contest over evolution in
        the nineteenth century was philosophically "rigged."  Darwinism won not
        so much because it fit the evidence but because it provided a
        scientific rationale for naturalism.
      * We are forced by our *a priori* adherence to material causes to create
        an apparatus of investigation and a set of concepts that produce
        material explanations.
      * The truth is that much of Darwinism is not science but naturalistic
        philosophy masquerading as science.  So an honest debate between
        Darwinism and Christianity is not fact versus faith but philosophy
        versus philosophy, worldview versus worldview.
      * If God created all of finite reality, then every aspect of that reality
        must be subject to him and his truth.  Everything finds its meaning and
        interpretation in relation to God.  No part of life can be autonomous
        or neutral, no part can be sliced off and made independent from
        Christian truth.  Because creation includes the whole scope of finite
        reality, the Christian worldview must be equally comprehensive,
        covering every aspect of our lives, our thinking, our choices.
      * If you live according to a certain worldview but keep bumping up
        against reality in painful ways, you can be sure something is wrong
        with the worldview.  It fails to reflect reality accurately.
      * Christianity is not merely a religion, defined narrowly as personal
        piety and corporate worship.  It is also an objective perspective on
        all reality, a complete worldview.  Only Christianity consistently
        stands up to the test of practical living.  Only Christianity gives us
        an accurate road map.  Only Christianity matches the way we must act if
        we are to live humanely and rationally in the real world.
..

  11. A Matter of Life

      The story of a helicopter gunner shot down in Vietnam, and the Army
      surgeon who saved his life.  Though no one would have questioned allowing
      the private to die, having lost both legs, shattered both arms, lost one
      eye and erreparably damaged the other, and sustained shrapnel in the
      brain, the surgeon knew he had to do whatever he could to save the man
      *because he was human*.  The image of God is not to be taken lightly.

  12. Whatever Happened to Human Life?

      We used to think that human life was sacred, as we humans were created in
      the image of God, but since naturalism tells us we are ultimately no more
      than the highest-evolved primates, we shouldn't let a concept like the
      sanctity of human life get in the way of our further progression.  Where
      natural selection says we got to where we are by the least fit in the
      population dying out, the logical extension asks, "Why don't we help the
      population along by weeding out the weaker members?"  Thus we have the
      three horrors of abortion, infanticide, and euthanasia backed by the cold
      (il)logic of natural selection, all in service to further propagating the
      superior genes that got us here.  When naturalistic philosophy eliminated
      God from the public consciousness, it necessarily jettisoned all concrete
      notions of morality and meaning, but the image of God within us
      ceaselessly searches for the meaning and purpose we know we were created
      for.  We find the answer in creation.

..
      * Christians believe that God created human beings in his own image.  And
        because human life bears this divine stamp, life is sacred, a gift from
        the Creator.
      * The question of where life comes from is not some academic argument for
        scientists to debate.  Our understanding of the origin of life is
        intensely personal.  It determines what we believe about human
        identity, what we value, and what we believe is our very reason for
        living.
      * The beginning point might be fixed in the seventeenth century, when
        French mathematician René Descartes resolved to doubt everything that
        could possibly be doubted.  With this, Descartes unleashed the
        revolutionary idea that the human mind, not God, is the source of
        certainty; human experience is the fixed point around which everything
        else revolves.
      * The death of God means the death of morality.  If you give up belief in
        God, you must also give up biblical ideas of morality and meaning.
        This is exactly what the twentieth century has done.  If we were not
        created by God---and therefore are not bound by his laws---if we are
        simply the most advanced of the primates, why shouldn't we do whatever
        we choose?
      * We have a society today in which the one intrinsic good,
        self-justifying end, self-evident value, meaning of life, and
        non-negotiable absolute is sex.  What makes this view possible is a
        radical dualism betweeen body and soul, a dualism that can also be
        traced back to Descartes, who reduced the body to little more than a
        machine operated by the mind.  Carried to its logical conclusion, this
        view implies that sexual acts between unmarried people or partners of
        the same sex or even complete strangers have no moral significance.
      * *Roe v. Wade* was the leading edge of a powerful social movement,
        fueled by sexual politics, to free the individual from the yoke of
        allegedly repressive moral restraints.  "Choice" over what to do with
        one's own body became the defining value of the 1970s and 1980s---all
        the while ignoring the fact that choice in itself cannot possibly be a
        value and that value depends on *what* is chosen.
      * Baby Doe was born with a deformed esophagus, making it impossible for
        him to digest food.  Infant Doe was also born with Down's syndrome.
        Two Indiana courts declined to intervene, and six days later Baby Doe
        had starved to death.  The baby was killed because it was retarded.
        Handicapped infants were quite routinely being allowed to die.
      * Abortion is one sensible method of dealing with such problems as
        over-population, illegitimacy, and possible birth defects.  Abortion
        has an important and positive public health effect, reducing the number
        of children afflicted with severe defects.
      * The number of Down's syndrome children born in Washington State in 1976
        was sixty-four percent lower than it would have been without legal
        abortion.  Most people with Down's syndrome are only moderately
        retarded and grow into adults who are capable of holding a job and
        living independently.  And if the birth parents cannot cope, there is a
        waiting list of couples eager to adopt these children.  Yet today, they
        are being targeted for elimination.
      * For any "unwanted" or "defective" baby who may manage to slip through
        the front line of defense, there is always the ultimate solution.  All
        who fail to reach a certain level on the Apgar test, used to determine
        the health of newborns, would be euthanized.
      * Evolutionary psychology is the latest verions of sociobiology, and
        reduces living things to products of their genes.  The newborn is
        basically a gene carrier, and before bonding with their newborn
        children, parents have always coolly assessed the biological value of a
        child (the chance that it will live to produce grandchildren) based on
        its health and the parents' own resources.
      * Rights belong only to persons, so if someone can be reduced to a
        nonperson, then he or she has no rights.  You can therefore kill
        disabled babies on the basis that they are "nonpersons" until they are
        rational and self-conscious.
      * Clearly, anyone who threatens our cherished right to do wahtever we
        please with our bodies must be stopped, by whatever means necessary.
        And yet many well-meaning Americans, including Christians, have bought
        into the "choice" argument.  They don't see that abortion, infanticide,
        and euthanasia are all part of the same package.
      * Why to pro-choicers oppose even modest limits?  Because they understand
        that abortion represents a worldview conflict:  God and the sanctity of
        life versus the individual's moral autonomy.  They can give no quarter.
      * If life is simply the result of a chance naturalistic
        process---molecules colliding and combining in a primordial soup---why
        shouldn't we control our own genes or create new life forms?  There is
        almost no stopping the technological imperative:  If something *can* be
        done, it *will* be done.  Truly our capabilities have exceeded our
        ethical and moral grasp.
      * Something within us stirs ceaselessly in search of meaning and purpose
        and connection.  Christians know this something as the soul, as the
        *imago Dei*---the image of God within us.  Because of the doctrine of
        creation, we know life has worth.  We know it is rooted in something
        beyond the test tube or colliding atoms, even as many voices around us
        say otherwise.
..

  13. In Whose Image?

      The Christian worldview

      * corresponds with the scientific evidence,
      * provides the strongest basis for human dignity,
      * gives a sense of meaning and purpose,
      * provides a sense of assurance about our ultimate destiny, and
      * provides the most certain motive for service and care of others.

      The naturalistic worldview falls short in all those areas.  Humans long
      for fulfillment, dignity, meaning, and purpose.  The naturalist has no
      real hope here, but the Christian has the assurance of the creation
      narrative.

..
      * Which worldview corresponds with the scientific evidence?  Respect for
        human life at all stages is supported by growing scientific data
        showing that even before birth, the fetus is fully human.
      * "Professor George is right, and he is right to correct me.  Today the
        scientific evidence favors the pro-life position."  The audience sat in
        stunned silence.
      * Which worldview provides the strongest basis for human dignity?
        Scripture tells us that "God created man in his own image... male and
        female he created them."  How could anyone even theoretically conceive
        of any more secure basis for human dignity?
      * In that naturalistic worldview it is only logical to place the goal of
        population control above the dignity of human life and to resort to any
        means available to reduce the human population in order to preservee
        Mother Nature from being depleted and despoiled.
      * Which worldview gives a sence of meaning and purpose?  We constantly
        hear that personal choice is the only thing that will produce
        "happiness"---the most sacred goal of American life.  People long for a
        sense of fulfillment and dignity that no amount of pleasure can
        provide.  Men and women cannot live without purpose.  What is the chief
        end of man?  To glorify God and enjoy him forever.
      * Which worldview provides a sense of assurance about our ultimate
        destiny?  The existentialists pointed out that if there is nothing
        beyond the grave, then death makes a mockery of everything we have
        lived for; death reduces human projects and dreams to a temporary
        diversion, with no ultimate significance.  But if our souls survive
        beyond the grave, as the Bible teaches, then this life is invested with
        profound meaning.  Everything we do here has a significance for all
        eternity.  The life of each person, whether in the womb or out, whether
        healthy or infirm, takes on an enormous dignity.
      * Which view of life provides the most certain motive for service and
        care of others?  If we know we are created by God, then we should live
        in a state of continuous gratitude to God.  Compelled by this
        gratitude, we desire to love him and life as he commands.  Of course,
        Christians often fail to follow their own convictions.  But when
        believers are selfish, they are acting contrary to their own beliefs.
        By contrast, when secularists are compassionate, they are acting
        contrary to the internal logic of their own worldview.
      * The high view of human life offered by Christianity is not a veneration
        of mere biological life.  The Christian understands that our real hope
        is in the spiritual realm, so that some things are more important than
        biological life.  Obedience to God is one of those things.
..

  14. God Makes No Mistakes

      Chuck's story of his autistic grandson is a reminder that those the
      naturalists think we should simply get rid of, for the betterment of the
      species, are often the ones who most naturally live a life of
      unadulterated joy, wonder, and compassion.

Part Three.  The Fall:  What has Gone Wrong with the World?

  15. The Trouble with Us
  16. A Better Way of Living?
  17. Synanon and Sin
  18. We're All Utopians Now
  19. The Face of Evil
  20. A Snake in the Garden
  21. Does Suffering Make Sense?

Part Four.  Redemption:  What Can We Do to Fix It?

  22. Good Intentions
  23. In Search of Redemption
  24. Does It Liberate?
  25. Salvation through Sex?
  26. Is Science Our Savior?
  27. The Drama of Despair
  28. That New Age Religion
  29. Real Redemption

Part Five.  Restoration:  How Now Shall We Live?

  30. The Knockout Punch
  31. Saved to What?
  32. Don't Worry, Be Religious
  33. God's Training Ground
  34. Still at Risk
  35. Anything Can Happen Here
  36. There Goes the Neighborhood
  37. Creating the Good Society
  38. The Work of Our Hands
  39. The Ultimate Appeal
  40. The Basis for True Science
  41. Blessed Is the Man
  42. Soli Deo Gloria
  43. Touched by a Miracle
  44. Does the Devil Have All the Good Music?
  45. How Now Shall We Live?

..
        What Problems has the Author Tried to Solve?
        ============================================

        What are the Author's Key Terms?
        ================================

        What are the Author's Leading Propositions?
        ===========================================

        What are the Author's Arguments?
        ================================

        Of the Problems Identified, Which has the Author Solved?
        ========================================================

Questions to Answer
===================

Part 1

  1. *Read Chuck's introduction.  Why did he write this book?  How has our
     culture changed from what he describes?*

     He wrote the book to remind the church that its mission is to act as God's
     redemptive agent of blessing within the world.  We can only do that if we
     bring God's truth to bear in all areas of life.

     While Chuck had an optimistic perspective on where things were headed 20
     years ago, it seems what he saw as a course change was perhaps more just
     shifting gears before continuing on our course "to hell in a hand basket."
     Some of his encouraging statistics haven't quite panned out.  E.g.,
     divorce rates are decreasing, but so are marriage rates.  Instead we see
     the significant rise in cohabitation.  Birth rates for unwed teen moms are
     decreasing, but birth rates for unwed moms are increasing.  We're just
     waiting longer before having kids.  Welfare spending topped $1 trillion in
     2016.

     Chuck thought all the ideologies marking the 20th century had proven
     themselves bankrupt (and they have), but they're now rearing their ugly
     heads again with a shiny new mask, and our inability to be aware of, let
     alone learn anything from, history means we're going to give them another
     shot, thinking this time we'll get it right.

  2. *How has the book been organized?*

     * Part 1:  What is worldview and why is it important?
     * Part 2:  Who am I?
     * Part 3:  What's wrong with the world?
     * Part 4:  How can what's wrong be made right?
     * Part 5:  Why am I here?

  3. *How does Chuck define and describe a worldview?*

     Worldview is intensely practical.  It is simply the sum total of our
     beliefs about the world, the "big picture" that directs our daily
     decisions and actions.

  4. *What is the real "culture war" according to Chuck?*

     The real war is a cosmic struggle between worldviews---between the
     Christian worldview and the various secular and spiritual worldviews
     arrayed against it.  This is what we must understand if we are going to be
     effective both in evangelizing our world today and in transforming it to
     reflect the wisdom of the creator.

  5. *What does Chuck mean by "discipleship of the mind?"*

     The Christian calling is not only to save souls but also to save minds.
     Sadly, many Christians have been misled into believing there is a
     dichotomy between faith and reason, and as a result they have actually
     shunned intellectual pursuits.  Pastors must begin to redefine their task
     to include intellectual evangelism, for if they do not preach to issues of
     the mind, they will find themselves increasingly alienated from their own
     flock.

Part 2

  1. *Why is the view of creation such an important part of any worldview?*

     Our view of creation answers the question, "Who am I?" and how we answer
     that impacts our answer to the next question, "Why am I here?"  What we
     believe about origins necessarily impacts what we believe about God, man,
     truth, knowledge, and ethics; that is, it impacts every other aspect of
     our worldview.
  2. *Chuck discusses the implications of atheistic Darwinism (chapter 10).
     List a few of these.*

     * **Ethics:**  These are merely ideas we get once we've evolved to a
       certain level, so there's no objective standard for morality.
     * **Law:**  This is nothing more than a collection of policies deemed to
       be socially advantageous.  It's not based on any concept of right and
       wrong.  We make it up as we go and can change it whenever a change
       becomes more advantageous that what we have currently.
     * **Education:**  "Good" ideas are just the ones that do the best job of
       getting us what we want.  There are no right or wrong ideas, just
       whatever works best for you.
     * **Truth:**  Since there is no transcendent truth, we're necessarily held
       captive by the perspectives of our affinity groups (ethnicity, gender,
       etc.).  All perspectives are equally valid, as they're all just social
       constructs.

  3. *How do the practices of abortion and assisted suicide reveal worldview
     commitments (chapter 12)?*

     We used to think that human life was sacred, as we humans were created in
     the image of God, but since naturalism tells us we are ultimately no more
     than the highest-evolved primates, we shouldn't let a concept like the
     sanctity of human life get in the way of our further progression.  Where
     natural selection says we got to where we are by the least fit in the
     population dying out, the logical extension asks, "Why don't we help the
     population along by weeding out the weaker members?"  Thus we have the
     three horrors of abortion, infanticide, and euthanasia backed by the cold
     (il)logic of natural selection, all in service to further propagating the
     superior genes that got us here.  When naturalistic philosophy eliminated
     God from the public consciousness, it necessarily jettisoned all concrete
     notions of morality and meaning, but the image of God within us
     ceaselessly searches for the meaning and purpose we know we were created
     for.  We find the answer in creation.

  4. *What other key ideas and truths did you find particularly helpful?*

     The Christian worldview

     * corresponds with the scientific evidence,
     * provides the strongest basis for human dignity,
     * gives a sense of meaning and purpose,
     * provides a sense of assurance about our ultimate destiny, and
     * provides the most certain motive for service and care of others.

     The naturalistic worldview falls short in all those areas.  Humans long
     for fulfillment, dignity, meaning, and purpose.  The naturalist has no
     real hope here, but the Christian has the assurance of the creation
     narrative.

Part 3

  1. What is the "human dilemma" (Chapter 15)?

     It's the question of what's wrong with the world.  Why are we plagued
     with war, suffering, disease, and death?

  2. Why does Chuck call this the most formidable stumbling block to the
     Christian faith (Chapter 15)?

     First, it's a very difficult question to answer, but beyond that, it's
     significantly harder for us to truly believe whatever answer we might
     intellectually assent to in the midst of the suffering and pain.  We
     believe the universe was created by a being who is infinitely wise and
     infinitely good.  How, then, do we explain the presence of evil?  Could
     not an all-loving, all-powerful God instantly put a stop to the suffering
     and injustice in the world?

     Though he could, he chooses not to because, in his infinite foreknowledge,
     he knew that making us free moral agents, with the ability to choose good
     or evil at any moment, would be *better than any alternative*.  Why?  We
     won't know until the end of time when we can ask him face to face.  In the
     meantime, we can trust his judgment, given everything else we know of him.

  3. What are the four common consequences of the Fall and sin that Chuck
     mentions (Chapter 20)?

     1. Sin disrupts our relationship with God.
     2. Sin alienates us from each other.
     3. The fall affects all of nature.
     4. Death and its preliminaries---sickness and suffering---would become
        part of the human experience.

  4. What are the five false solutions to the problem of evil (Chapter 21)?

     1. Deny that God exists at all.
     2. Deny that suffering exists.
     3. Place God beyond good and evil.
     4. God's power is limited.
     5. God has created evil to achieve a greater good.

  5. What other key ideas and truths did you find particularly helpful?

     The fall being a historical physical reality is necessary to making sense
     of the world.  When searching for what's wrong with the world, if you
     don't identify the root problem as sin in the human heart, any solutions
     you'll come up with will miss the mark (and might make things worse).

Part 4

  1. How is the conversion of Dr. Bernard Nathanson an example of the
     redemption offered in the Biblical worldview?

     It's an example of God taking something that was once good and had gone
     bad and restoring it back into something good again.

  2. Chuck lists a number of false answers to the question, "How can we fix
     what's wrong with the world."  List them:

     * Chapter 24:  We need to overturn the systemic oppression in society to
       usher in an earthly utopia.
     * Chapter 25:  We need to liberate ourselves from oppressive notions of
       sexual purity.
     * Chapter 26:  We need to discover scientific/technological solutions to
       the problems of the world.
     * Chapter 27:  We need to understand that ultimately everything in life is
       meaningless, so there's no sense fixing anything.
     * Chapter 28:  We need to realize we're part of the eternal oneness such
       that we can return to a state of peace with the universe.

  3. Why does Chuck call Christian redemption "a restoration" (chapter 29)?

     It restores us to our created condition in which we have a right
     relationship with God, man, self, and creation.

  4. What other ideas and truths did you find particularly helpful?

     I'm skeptical of Colson's conflating of the concepts of
     consummation---Christ's making all things new at the end of time---and
     restoration as described in this part of the book.  I worry there's too
     much emphasis on ushering in the eternal kingdom here and now through the
     efforts of the church, though we know that's not possible until Christ's
     return.

Part 5

  1. What is "Cultural Commission" (p. 295)?

     Be fruitful and multiply; fill the earth and subdue it.

  2. Read the last paragraph of chapter 31 (p. 305).  How does this perspective
     give you insight into how you should live for Christ?

     The great commission and the cultural commission are inseparable.

  3. What is "real shalom" (p. 365)?  How is this a goal of the work of
     restoration God has given us?

     Right relationship with God, man, self, and creation.  This was the state
     mankind was created in.  Sin destroys those four relationships.  Christ's
     work of redemption makes possible the righting of those wronged
     relationships.

  4. What other key ideas and truths did you find particularly helpful?

     I found it interested that *Touched by an Angel* was held up as a shining
     example of a Christian being a Christian in whatever their field is, as I
     remember that show pretty consistently being called out when it was on the
     air for its misrepresentation of God and Christianity.

  5. What are three things you are going to do or think about differently after
     reading this section?

     1. Remember the concept of shalom.
     2. Try to figure out what God wants me doing right now.
     3. Try to encourage my fellow Colson Fellows in training to think more
        critically about the content we're ingesting.
