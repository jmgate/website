Meditations on First Philosophy
-------------------------------

* Author:  René Descartes
* Publication Year:  1641
* Link:  https://smile.amazon.com/Meditations-First-Philosophy-Ren%C3%A9-Descartes/dp/B08GVJ6K2N/ref=sr_1_2_sspa?dchild=1&keywords=meditations+on+first+philosophy&sr=8-2-spons&psc=1&spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUFSSFVUSEdMODFUNkkmZW5jcnlwdGVkSWQ9QTA0Njk1NTcyNUY1VjJJNERIM1dZJmVuY3J5cHRlZEFkSWQ9QTA0MzIzNTUxVUtDTTcxQVozVkhUJndpZGdldE5hbWU9c3BfYXRmJmFjdGlvbj1jbGlja1JlZGlyZWN0JmRvTm90TG9nQ2xpY2s9dHJ1ZQ==
* Started on:  2021-10-05
* Finished on:  2021-10-09
