The Vision of the Anointed
--------------------------

*Self-Congratulation as a Basis for Social Policy*

* Author:  Thomas Sowell
* Publication Year:  1996
* Link:  https://www.amazon.com/Vision-Anointed-Self-Congratulation-Social-Policy/dp/046508995X/ref=tmm_pap_swatch_0?_encoding=UTF8&sr=8-1

What is the Book About?
=======================

