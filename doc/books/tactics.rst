Tactics
-------

*A Game Plan for Discussing Your Christian Convictions*

* Author:  Greg Koukl
* Publication Year:  2009
* Link:  https://smile.amazon.com/Tactics-10th-Anniversary-Discussing-Convictions/dp/0310101468/ref=sr_1_1?keywords=tactics+greg+koukl&qid=1640624310&s=books&sprefix=tactics+%2Cstripbooks%2C150&sr=1-1
* Started on:  2021-11-14
* Finished on:  2021-12-12

How is the Book Organized?
==========================

Part 1:  The Game Plan

  INSERT DESCRIPTION HERE.

  1. Diplomacy or D-Day?

     * Tactics help you (1) get in the driver’s seat and stay there, and (2)
       maneuver effectively in the midst of disagreement.
     * Strategy involves the big picture of why someone should believe
       Christianity is true, but tactics involve the details of engagement and
       navigating the conversation.
     * Tactics aren’t clever tricks, but are used to gain a footing,
       maneuver, and expose someone’s bad thinking so you can guide them to
       the truth.

  2. Reservations

     * Arguments, which are distinct from fights, are a virtue, because they
       advance clear thinking and help to refine our understanding of truth.
     * When Christians avoid conflict out of fear of disunity, the church is
       crippled in three ways:  (1) without argumentation, errors abounds, (2)
       believers don’t learn how to argue well among themselves, and (3) we
       get a contrived unanimity in place of the actual oneness we seek.
     * Only the Holy Spirit can change a sinner’s heart, but God happily uses
       many things to bring that about, including argumentation.
     * You do your part, and let God to his.  Be on the lookout for those God
       is working on, and don’t waste time trying to force a horse to drink
       the water.
     * Have as your goal to place a pebble in their shoe to give them something
       to think about.

  3. Getting in the Driver’s Seat:  The Columbo Tactic

     * Questions (1) are great conversation starters, (2) are interactive, (3)
       are neutral, protecting you from sounding preachy, (4) buy time, and (5)
       keep you in the driver’s seat.
     * The purposes of the Columbo tactic are (1) to gain information, (2) to
       reverse the burden of proof, and (3) to make a point.
     * At the beginning of a conversation, the only thing to focus on is
       gathering information.

  4. Columbo Step 1:  Gathering Information

     * “What do you mean by that?”
     * Forces someone to think carefully about what they mean when they toss
       out a challenge.  Ask them to clearly spell out their objection/concern.
     * Use this to study others’ views.

  5. Columbo Step 2:  Reversing the Burden of Proof

     * People should offer good reasons for the important things they think are
       true.  Make them defend their beliefs rather than requiring you to
       refute them.
     * “How did you come to that conclusion?”
     * Helps you understand their reasons and gets them thinking about the
       legitimacy of their view.
     * Opinions are not proof, and a real argument requires more than just a
       statement of belief.
     * It’s not enough for someone to contradict your view by simply telling
       a story.  An alternate explanation is not a refutation.  The new option
       must not only be possible, or even plausible, but also more likely (all
       things considered) than the idea you are offering.

  6. Two Reliable Rescues

     * Deal with the Professor’s Ploy by (1) never making a frontal assault
       on a superior force in an entrenched position, and (2) refusing to
       shoulder the burden of proof for a claim you haven’t made.
     * When in an intense conversation with a fast talker, switch to
       fact-finding mode, ask a bunch of questions to understand their
       viewpoint, and then say, “Let me think about it,” so you can process
       everything at your leisure.
     * When your interlocutor does something fishy (e.g., dodges questions,
       changes topics, etc.), take a moment to “narrate the debate” so
       everyone’s clear what just happened, and ask if it’s possible to
       stay on the topic at hand and get some answers.

  7. Columbo Step 3:  Using Questions to Make a Point

     * Questions can be an effective means in leading someone where we want
       them to go.  Using questions in this way rather than statements makes it
       easier for us to make our point and softens our challenge to the
       other’s view.
     * When we know what we want to accomplish, we can use leading questions to
       make that happen.
     * Ask yourself if their conclusions follow from their evidence, and if
       not, use questions to point out the disconnect.  E.g., “Have you
       considered…?”  “Can you clear this up for me…?”
     * Remember that you don’t have to follow a conversation through to
       completion.  Do what you need to in the moment and trust God with the
       rest.

  8. Perfecting Columbo

     * Try to anticipate objections you might face, plan questions in advance,
       and then formulate responses ahead of time.
     * Assess your performance after each encounter to see where you could
       possibly improve next time around.
     * Practice by writing out dialogues ahead of time and role-play them with
       a friend.

  9. Turnabout:  Defending Against Columbo

     * Politely refuse to answer leading questions, and instead ask him to
       state his point and reasons so you can think about them.
     * If you suspect an ambush, slow down and ask for clarification about what
       they’re getting at.  Don’t feel obligated to answer.
     * Be on the lookout for assertions disguised as questions and ask them to
       rephrase or revert to Columbo Part 1.

Part 2:  Finding the Flaws

  INSERT DESCRIPTION HERE.

  10. Suicide:  Views that Self-Destruct

      * Sometimes views are self-defeating because they contain contradictory
        concepts.
      * The law of noncontradiction:  "One cannot say of something that it is
        and that it is not in the same respect and at the same time."  ~
        Aristotle
      * If a claim applies to itself, see if it satisfies its own criteria, or
        if there's some internal contradiction present.
      * Most people, no matter how smart they are, hold contradictory views
        without realizing it, so your task is to help them realize the
        contradictions that aren't immediately obvious.

  11. Practical Suicide

      * Some views are not internally contradictory, but are self-defeating in
        practice, because they can be believed but not acted out.
      * Be on the lookout for, "You shouldn't do [insert thing I'm doing right
        now]."
      * Moral relativists are particularly susceptible to this problem.

  12. Sibling Rivalry and Infanticide

      * Sibling rivalry:  Sometimes objections come in pairs that are logically
        inconsistent with each other, meaning at least one is illegitimate.
      * Infanticide:  When a child claim depends upon a parent claim, but the
        child claim denies the parent claim (e.g., the existence of evil
        "proving" God doesn't exist).

  13. Taking the Roof Off

      * Also known as *reductio ad absurdum*, this tactic shows that some
        views, taken to their logical conclusion, are either counterintuitive
        or downright absurd.
      * To execute the tactic:  (1) reduce the viewpoint to its basic argument,
        (2) consistently apply the logic of the view to see if you arrive at
        any ridiculous conclusions, and (3) have the person consider the
        unexpected implications of their view.
      * When people try to live at odds with revelation, reason, and reality,
        that creates tension, which they then shield themselves from through
        self-deception.  Removing the self-deception exposes them once again to
        the way God has ordered creation, such that they have to deal with it
        in some way.

  14. Steamroller

      * There may be a variety of reasons why someone would reject a sound
        logical argument.
      * Oftentimes people are simply reaction emotionally to something from
        their past, other times their minds are simply closed off to other
        viewpoints, and sometimes it's just plain rebellion against God.
      * To manage a steamroller (one who attempts to overpower you with a
        strong personality or constant interruption):  (1) First stop the
        interruption gracefully but firmly, and briefly negotiate an agreement.
        If that doesn't work, (2) shame them by making a direct request for
        courtesy.  If that doesn't work, (3) just leave.

  15. Rhodes Scholar

      * The "Rhodes Scholar" is one who invokes scholarly opinion against our
        view, which can often be an instance of the fallacy of the expert
        witness.
      * Expert opinion can be good grounds for an argument, but experts aren't
        always right, so beware of appeals to scholarship that are misapplied.
      * Sometimes experts speak outside their realm of expertise, other times
        they get the facts wrong, and other times philosophical bias distorts
        their judgment.
      * How did the expert come to his conclusions?  What were the facts he
        considered?  Are there any biases that may be distorting the
        assessment?
      * What an expert believes is not as important as why he believes it.
        What matters most are the reasons, not the opinions.

  16. Just the Facts, Ma’am

      * Often bad viewpoints are based on misinformation or error, meaning at
        the root, they've simply gotten the facts wrong.
      * When a challenge is based on an alleged fact, first determine what the
        precise claim is and separate it from the rest of the rhetoric.
      * Next, determine if the precise claim is accurate by consulting
        reference books, informed friends, the internet, etc.
      * Alternatively, before researching, see if anything about the claim
        seems unlikely or implausible on its face (e.g., if the math just
        doesn't add up).
      * Now you can get back to your friend with the facts, but be sure to be
        precise, cite sources, discuss their credibility, etc.
      * First tell the truth, and then give your opinion.

  17. Inside Out

      * Every human is made in God's image, and this fact is known by every
        human, though they might not realize it.
      * We're aware that we have profound value and worth, that we were
        designed for a valuable purpose, that we're beautiful yet broken and
        guilty, and that our restless souls hunger for rescue.
      * Be on the lookout for when someone's speech or actions agree with these
        truths, even while their worldview lies to them, then ask about the
        disconnect.

  18. Mini-Tactics

      * **What a Friend We Have in Jesus:**  Most people have a high regard for
        Jesus as an authority, so frame the disagreement with Jesus on your
        side and pit them against him.
      * **Sticks and Stones:**  When someone tries to blunt your attack by
        calling you a name, as for a definition.  This stops their momentum and
        forces them to realize that ridicule is not an argument.
      * **Moving toward the Objection:**  It may be advantageous to agree with
        a charge rather than oppose it, by casting the complaint in a different
        light.
      * **Watch Your Language:**  Don't use Christianese.  Use appropriate
        synonyms for religious jargon, and communicate your convictions in a
        down-to-earth way.
      * **The Power of "So?":**  When someone makes a claim that's actually
        irrelevant to your argument, simply ask what that has to do with
        anything, and see if you can get them to focus back on the argument at
        hand.

  19. More Sweat, Less Blood

      * **Eight Quick Tips:**

        1. Be ready.
        2. Keep it simple.
        3. Avoid religious language and spiritual pretense.
        4. Focus on the truth of Christianity, not merely its personal
           benefits.
        5. Give reasons.
        6. Stay calm.
        7. If they want to go, let them leave.
        8. Don't let them leave empty-handed.

      * An ambassador is ready, patient, reasonable, tactical, clear, fair,
        honest, humble, attractive, and dependent.

Questions
=========

1. How does Greg distinguish between the terms "strategy" and "tactics"?

   Strategy is the high-level plan for getting from where you are to where you
   want to go.  Tactics are the maneuvers you use to react/respond to what
   you're faced with in the moment.

2. Why does Greg believe that "tactics" is the missing piece of the puzzle in
   apologetics?

   To the extent that the church has been training Christian in apologetics,
   we've largely been giving people claims to reiterate.  We haven't been
   training them to respond to critique.  Tactics, then, help you to think, and
   to force your interlocutor to think as well.

3. What is the "Columbo Tactic"?  Give an example of how to use it.

   What do you mean by that?

4. What is the second application of the "Columbo Tactic?"  What is the "burden
   of proof"?

   Why do you think that?  When someone makes a claim, they need to support it
   with grounds; that is, they have the burden of proving their claim.

5. The third step in the Columbo Tactic is to use questions to make a point.
   Why does he believe asking questions is important?

   Rather than making a statement, you ask a question to force the other person
   to come to the conclusion you would've made as a statement.  If you make the
   statement, they can easily write you off; if they come to the same
   conclusion internally, it's much harder to write off their own reasoning
   abilities.

6. Describe the "Suicide Tactic" and give an example.

   Show how the claim self-destructs, e.g., if someone attempts to argue that
   there are no absolute truths, ask how they know that notion is true.

7. Describe the "Taking-the-Roof-Off Tactic" and give an example.  How
   effective do you think this tactic is?

   It's *reductio ad absurdum*, assuming the other person's view, and then
   following it to its logical conclusions to see if they're sensical or
   absurd.  E.g., you ask "Given all the evils perpetrated in the world, how
   could a good God exist?"  But your own viewpoint is there is no god, only
   naturalism.  If your view is correct then there is no basis for morality;
   ultimately all actions are caused by prior location and momenta of subatomic
   particles.  So why do you believe there is evil in the world?

8. Describe the "Steamroller Tactic".  What kind of people does this work best
   with?

   It's making bad behavior blatantly obvious when someone is being rude and
   not actually allowing you to engage in the dialogue.
