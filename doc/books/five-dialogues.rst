Five Dialogues
--------------

* Author:  Plato
* Publication Year:  ca. 350 BC
* Links:

  * http://gutenberg.org/ebooks/1642
  * http://gutenberg.org/ebooks/1656
  * http://gutenberg.org/ebooks/1657
  * http://gutenberg.org/ebooks/1643
  * http://gutenberg.org/ebooks/1658

* Started on:  2021-10-10
* Finished on:  2021-11-13
