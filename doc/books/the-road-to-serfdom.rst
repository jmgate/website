The Road to Serfdom
-------------------

* Author:  F.A. Hayek
* Publication Year:  1944
* Link:  https://smile.amazon.com/Road-Serfdom-Documents-Definitive-Collected/dp/0226320553/ref=sr_1_2?dchild=1&keywords=road+to+serfdom&sr=8-2
* Started on:  ?
* Finished on:  2020-11-28

What is the Book About?
=======================

A clarion call to the socialists of all parties in England that the cultural
trends the author was seeing there closely paralleled what he'd observed in
Germany and Austria before the rise of the National Socialist (Nazi) party.
Seventy six years later, it reads like it was written to the world yesterday.
