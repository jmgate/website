Total Truth
-----------

*Liberating Christianity from Its Cultural Captivity*

* Author:  Nancy Pearcey
* Publication Year:  2005
* Link:  https://www.amazon.com/Total-Truth-Study-Guide-Christianity/dp/1433502208/ref=sr_1_1?crid=I7JJWRY1GM7C&dchild=1&keywords=total+truth+nancy+pearcey&qid=1606515393&sprefix=total+truth+na%2Caps%2C203&sr=8-1
* Started on:  ?
* Finished on:  2021-01-12

What is the Book About?
=======================

A rich historical examination of how the church inadvertently pushed itself to
where it is today, combined with exhortations on what to do about it.
