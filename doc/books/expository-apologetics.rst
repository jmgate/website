Expository Apologetics
----------------------

*Answering Objections with the Power of the Word*

* Author:  Voddie Baucham Jr
* Publication Year:  2015
* Link:  https://smile.amazon.com/Expository-Apologetics-Answering-Objections-Power/dp/1433533790/ref=sr_1_1?crid=NBF4U5SDL8L&dchild=1&keywords=expository+apologetics+voddie+baucham&qid=1600779698&sprefix=expository+apolo%2Caps%2C313&sr=8-1
* Started on:  ?
* Finished on:  2020-09-28

What is the Book About?
=======================

A framework for approaching the defense of the faith by identifying, analyzing,
and then refuting counterfeit worldviews.
