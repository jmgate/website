The Twelve Days of the Aspen Executive Seminar
----------------------------------------------

* Author:  Mortimer J. Adler
* Publication Year:  1972
* Link:  http://cyberspacei.com/greatbooks/h2/speak_listen/sp_listen_016.htm
* Started on:  2020-08-21
* Finished on:  2020-08-28

What is the Book About?
=======================

A brief history of the political and economic questions facing the world today,
which have been hotly debated since well before this country was founded.
