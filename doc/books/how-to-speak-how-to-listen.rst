How to Speak, How to Listen
---------------------------

* Author:  Mortimer J. Adler
* Publication Year:  1983
* Link:  https://smile.amazon.com/How-Speak-Listen-Mortimer-Adler/dp/0684846470/ref=sr_1_1?dchild=1&keywords=how+to+speak+how+to+listen&qid=1622571465&sr=8-1
* Started on:  ?
* Finished on:  2020-08-28
