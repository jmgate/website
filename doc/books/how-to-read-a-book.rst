How to Read a Book
------------------

*The Classic Guide to Intelligent Reading*

* Authors:  Mortimer J. Adler and Charles van Doren
* Publication Year:  1940
* Link:  https://smile.amazon.com/How-Read-Book-Classic-Intelligent/dp/0671212095/ref=sr_1_1?dchild=1&keywords=how+to+read+a+book&sr=8-1
* Started on:  ?
* Finished on:  TBD

Notes
=====

The Rules of Analytical Reading
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

I. The first stage of analytical reading:  Rules for finding what a book is about

  1. Classify the book according to the kind and subject matter.
  2. State what the whole book is about with the utmost brevity.
  3. Enumerate its major parts in their order and relation, and outline these parts as you have outlined the whole.
  4. Define the problem or problems the author has tried to solve.

II. The second stage of analytical reading:  Rules for interpreting a book's contents

  5. Come to terms with the author by interpreting his key words.
  6. Grasp the author's leading propositions by dealing with his most important sentences.
  7. Know the author's arguments, by finding them in, or constructing them out of, sequences of sentences.
  8. Determine which of his problems the author has solved, and which he has not; and of the latter, decide which the author knew he had failed to solve.

III. The third stage of analytical reading:  Rules for criticizing a book as a communication of knowledge

  A. General maxims of intellectual etiquette

    9. Do not begin criticism until you have completed your outline and your interpretation of the book.  (Do not say you agree, disagree, or suspend judgment, until you can say "I understand.")
    10. Do not disagree disputatiously or contentiously.
    11. Demonstrate that you recognize the difference between knowledge and mere personal opinion by presenting good reasons for any critical judgment you make.

  B. Special criteria for points of criticism

    12. Show wherein the author is uninformed.
    13. Show wherein the author is misinformed.
    14. Show wherein the author is illogical.
    15. Show wherein the author's analysis or account is incomplete.
