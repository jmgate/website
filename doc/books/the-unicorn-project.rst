The Unicorn Project
-------------------

*A Novel about Developers, Digital Disruption, and Thriving in the Age of Data*

* Author:  Gene Kim
* Publication Year:  2019
* Link:  https://smile.amazon.com/Unicorn-Project-Developers-Disruption-Thriving/dp/1942788762/ref=sr_1_1?dchild=1&keywords=the+unicorn+project&qid=1622569473&sr=8-1
* Started on:  ?
* Finished on:  2021-05-27
