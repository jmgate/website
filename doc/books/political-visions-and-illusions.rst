Political Visions and Illusions
-------------------------------

*A Survey & Christian Critique of Contemporary Ideologies*

* Author:  David Koyzis
* Publication Year:  2003
* Link:  https://smile.amazon.com/Political-Visions-Illusions-Contemporary-Ideologies/dp/0830852425/ref=sr_1_1?crid=387ZNX45ZYQTZ&dchild=1&keywords=political+visions+and+illusions+david+koyzis&qid=1608493977&sprefix=political+visions+and+i%2Caps%2C291&sr=8-1
* Started on:  ?
* Finished on:  2021-02-26

What is the Book About?
=======================

A walk through the counterfeit redemptive narratives of the most prevalent
ideologies of our day, supporting the author's thesis that "ideology is
idolatry".
