Confronting Injustice without Compromising Truth
------------------------------------------------

*12 Questions Christians Should Ask*

* Author:  Thaddeus Williams
* Publication Year:  2020
* Link:  https://smile.amazon.com/Confronting-Injustice-without-Compromising-Truth/dp/0310119480/ref=sr_1_1?crid=2ASDNDID058G&dchild=1&keywords=confronting+injustice+without+compromising+truth&qid=1631018369&sprefix=confronting+injustice%2Caps%2C265&sr=8-1
* Started on:  2021-08-30
* Finished on:  2021-09-12

How is the Book Organized?
==========================

What is "Social Justice"?

  Scripture commands us to seek justice, but the term "social justice" has two
  different meanings, depending on who you're talking to.  Social Justice A is
  biblical justice---rescuing infants left out to die, dismantling the
  trans-Atlantic slave trade, opposing the Third Reich---while Social Justice B
  conflicts with a biblical view of reality---the oppressor vs oppressed
  narrative of Marx, Engels, Gramsci, and the Frankfurt school; the
  deconstructionism of Foucault and Derrida; gender and queer theory; etc.
  Everybody is pro-justice, but our worldviews determine what we mean by the
  term.

Part 1:  Jehovah or Jezebel?  Three Questions about Social Justice and Worship

  Justice is giving others what is due them, so ultimately justice is about
  worship and giving God his due.

  1. The God Question:  Does our vision of social justice take seriously the
     godhood of God?

     The opening of Romans describes humanity when we're at our worst, from the
     horrors of the Aztec empire and its conquest, to Hitler, Stalin, and Mao.
     We inherently know God and what he requires of us, but we continually put
     something else in his place in our lives.  Any attempts to fix things via
     political or economic control will ultimately fail due to the failure to
     address the problem of sin in each of us.  The first commandment is first
     for a reason, and if we try to do justice without first giving God his
     due, we're not really doing justice.

  2. The *Imago* Question:  Does our vision of social justice acknowledge the
     image of God in everyone, regardless of size, shade, sex, or status?

     Idolatry is the first injustice, and the carcinogenic source of all other
     injustices.  When we reduce people to inside-the-box (meaning a
     naturalistic worldview) categories, we become oblivious to the
     beyond-the-box fact that every human being is a divine image-bearer.

  3. The Idolatry Question:  Does our vision of social justice make a false god
     out of the self, the state, or social acceptance?

     * Idols of the right:  material prosperity, rugged individualism,
       Christianity that is only heavenly-minded, status quo, skin tone.
     * Idols of the left:  political correctness, critical theories,
       postmodernism, intersectionality.
     * The idol of self:  We, not God, are the author of human *telos*
       (meaning), but we buckle under the impossible weight of that
       self-assigned responsibility.
     * The idol of state:  "Once we abolish God, the government becomes God"
       (Chesterton), so we turn there for validation of our self-defined selves
       that we absolutely *must* have.
     * The idol of social acceptance:  We want to somehow hold to our Christian
       beliefs and practices, while at the same time allowing culture to trump
       the truth of God's word.  Basically we don't want our Christianity to
       cost us anything.

Part 2:  Unity or Uproar?  Three Questions about Social Justice and Community

  Tribalism is the idea that we should divide people into group identities,
  then assign undesirable or evil traits to each group in such a way that we
  don't see the image-bearers of God before us.  It's been the idea behind It
  inspired racism in America, genocide in Germany, the gulags in Siberia, the
  killing fields of the Khmer Rouge, the killings in Rwanda, Darfur, the Congo,
  etc.  The idea has such staying power because humans have a need to belong in
  a community, but we need to fill that need without our groups becoming
  self-righteous and resulting in tribal warfare.

  4. The Collective Question:  Does our vision of social justice take any
     group-identity more seriously than our identities "in Adam" and "in
     Christ"?

     Extreme groups on all sides of the spectrum tend to attract and keep
     adherents not necessarily because of their ideology, but because of the
     community they provide.  Though we all belong to the single group of the
     human race, what we believe about human nature differentiates us.
     Christianity believes that the human nature is sinful and in need of
     redemption, while ideologies influenced by Rousseau believe that mankind
     is naturally good, but that our institutions make us wicked.  It's
     interesting to note that if you tackle the evil in the individual, you
     have the possibility of solving the problem of evil both in the individual
     and in our social institutions, but if you only tackle the social
     institutions and the evil really lies in the individual, you don't have a
     hope of solving either.  In Christ, all our divisions no longer matter.
     Unfortunately there are those, even within the church, who insist that
     something or other trumps our identity in Christ.

  5. The Splintering Question:  Does our vision of social justice embrace
     divisive propaganda?

  6. The Fruit Question:  Does our vision of social justice replace love,
     peace, and patience with suspicion, division, and rage?

Part 3:  Sinners or Systems?  Three Questions about Social Justice and
Salvation

  7. The Disparity Question:  Does our vision of social justice prefer damning
     stories to undamning facts?

  8. The Color Question:  Does our vision of social justice promote racial
     strife?

  9. The Gospel Question:  Does our vision of social justice distort the best
     news in history?

Part 4:  Truth or Tribes Thinking?  Three Questions about Social Justice and
Knowledge

  10. The Tunnel Vision Question:  Does our vision of social justice make one
      way of seeing something the only way of seeing something?

  11. The Suffering Question:  Does our vision of social justice turn the
      "lived experience" of hurting people into more pain?

  12. The Standpoint Question:  Does our vision of social justice turn the
      quest for truth into an identity game?

Epilogue:  12 Differences between Social Justice A and B

Questions
=========

What is "Social Justice"?

  1. How does Williams define/describe Social Justice A and Social Justice B?

     A is biblical justice and B is in conflict with the biblical view of
     reality.

Part 1:  Jehovah or Jezebel?

  1. What are the three questions Williams says we should ask about Social
     Justice and Worship?

     #. Does our vision of social justice take seriously the godhood of God?
     #. Does our vision of social justice acknowledge the image of God in
        everyone, regardless of size, shade, sex, or status?
     #. Does our vision of social justice make a false god out of the self, the
        state, or social acceptance?

  2. Give a summary sentence or two explaining why each question is important?

     #. Justice is giving each his due, so any attempts at justice without
        first giving God his due are ultimately not doing justice.
     #. We can't pursue justice (giving each his due) without first
        acknowledging that those we're pursuing justice for are image-bearers
        of God.
     #. Pursuing idols of self, the state, social acceptance, etc., will
        inevitably lead to injustice.

Part 2:  Unity or Uproar?

  1. What are the three questions Williams says we should ask about Social
     Justice and Community?

     #. Does our vision of social justice take any group-identity more
        seriously than our identities "in Adam" and "in Christ"?
     #. Does our vision of social justice embrace divisive propaganda?
     #. Does our vision of social justice replace love, peace, and patience
        with suspicion, division, and rage?

  2. Why does Williams say "Tribalism" is the worst and most destructive idea
     in the twentieth century?

     Tribalism is the idea that we should divide people into group identities,
     then assign undesirable or evil traits to each group in such a way that we
     don't see the image-bearers of God before us.  It's been the idea behind
     It inspired racism in America, genocide in Germany, the gulags in Siberia,
     the killing fields of the Khmer Rouge, the killings in Rwanda, Darfur, the
     Congo, etc.

  3. What does he say are three fundamental human needs (chapter 4)?  How do
     these needs manifest in our cultural moment?

     The three fundamental human needs are for identity, community, and
     purpose.  The question of identity is answered via identity politics,
     critical theories, and intersectionality.  The answer to the identity
     question automatically provides you with a community of everyone else in
     the same group.  Each group, then, has its own sense of what the highest
     purpose is, whether that's achieving political victories over the rival
     party, campaigning for [insert qualifier here] justice, etc.

  4. In what ways does he say James Cone inverts the Apostle Paul's three
     truths that bring the Church to unity?

     +---------------------------+---------------------------------+----------------------------------+
     | Issue                     | Paul                            | Cone                             |
     +===========================+=================================+==================================+
     | sin                       | universal human affliction      | oppression, a white man's game   |
     +---------------------------+---------------------------------+----------------------------------+
     | "in Christ"               | transcends all group identities | subordinate to racial identities |
     +---------------------------+---------------------------------+----------------------------------+
     | who decides justification | God                             | the black community              |
     +---------------------------+---------------------------------+----------------------------------+

Part 3 : Sinners or Systems?

  1. What are the three questions Williams asks about Social Justice and
     Salvation?

     #. Does our vision of social justice prefer damning stories to undamning
        facts?
     #. Does our vision of social justice promote racial strife?
     #. Does our vision of social justice distort the best news in history?

  2. William says, "We build our sins into our systems."  What examples does he
     give?

     Antisemitism in Nazi Germany, anti-black racism in early American
     history, the caste system in India, Apartheid in South Africa, and the
     one-child policy in China.

  3. How is the equation "Disparity = Discrimination" not biblically or
     practically true?

     It's true that disparity can be caused by discrimination, but it can also
     be caused by a whole host of other factors, some nefarious, many benign.
     The Bible does speak against discrimination, but not against disparity.
     It calls out people for sinning in their pursuit of wealth (e.g.,
     Solomon), but never rebukes those who earned their wealth righteously and
     steward it well (e.g., Job).  Indeed, Jesus even reminds us that "the poor
     you'll always have with you" (Matthew 26:11).

  4. What examples do you find helpful to understand Williams's argument?

     * Speeding violations on the New Jersey Turnpike:  Twice as many blacks
       speeding as whites.  Black population is younger, and younger drivers
       tend to speed more.
     * Bank lenders reject twice as many black applicants for home loans:
       Whites turned down at higher rate than Asians or Hawaiians.  Black
       banks reject black applicants at an even higher rate.
     * Average age within a group can explain differences (e.g., more maturity
       leading to better outcomes).
     * Month of birth leading to different outcomes in sports.

Part Four:  Truth or Tribes Thinking?

  1. What are Williams's three questions about Social Justice and Knowledge?

     #. Does our vision of social justice make one way of seeing something the
        only way of seeing something?
     #. Does our vision of social justice turn the "lived experience" of
        hurting people into more pain?
     #. Does our vision of social justice turn the quest for truth into an
        identity game?

  2. How does he use the concept of "tunnel vision?"

     If you're only able to process the world in a single way---that is, if the
     problem, no matter what it is, must necessarily be explained by racism,
     sexism, heteronormativity, or whatever else your tunnel is---then you wind
     up blinded to all other explanations, many of which may better explain the
     data than your own viewpoint.  You see this same tunnel vision not only in
     the critical social justice issue, but in most of the divisive issues of
     our current cultural moment:  pandemic response (vaccines, masks,
     lockdowns), election integrity, economic planning activities, media
     communications, climate change, etc.  If you're unable to view an issue
     from multiple perspectives, dialogue is effectively dead before it ever
     begins, which means your ability to solve the problem is predicated on you
     having assumed the correct answer before encountering any evidence.
     There's some hubris for you.

  3. Explain what Williams means when he says, "The Bible is as anti-fear as it
     is anti-oppression."

     The Bible clearly demonstrates God's concern for the oppressed, with
     numerous admonitions to care specifically for them.  It also commands you
     over 100 times to "fear not," but instead to trust and hope in the Lord.

Epilogue

  1. Is the chart of the twelve differences between Social Justice A & B
     helpful?  Does it seem forced at points?  Does it faithfully represent a
     Christian Worldview, in your opinion?

     It's a nice concise summary, so it should be helpful to reference later.
     I didn't notice any points that seemed forced.  I don't think it's a
     comprehensive representation of a Christian worldview, but it seems to be
     faithful in what it presents.  Something that stood out as missing was
     what exactly the church is and what role it is to play in God's redemptive
     work in the world.
