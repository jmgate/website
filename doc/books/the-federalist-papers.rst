The Federalist Papers
---------------------

* Authors:  Alexander Hamilton, James Madison, and John Jay
* Publication Year:  1787--1788
* Link:  http://gutenberg.org/ebooks/1404
* Started on:  ?
* Finished on:  2021-04-09
