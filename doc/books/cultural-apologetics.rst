Cultural Apologetics
--------------------

* Author:  Paul Gould
* Publication Year:  2019
* Link:  https://smile.amazon.com/Cultural-Apologetics-Conscience-Imagination-Disenchanted/dp/0310530490/ref=sr_1_1?crid=3DMNK9ITSKHWM&dchild=1&keywords=cultural+apologetics+paul+gould&qid=1633270663&sprefix=cultural+apologetics%2Caps%2C363&sr=8-1
* Started on:  2021-10-04
* Finished on:  2021-10-17

Questions
=========

1. As you read the opening chapters, try to distinguish between traditional and
   cultural apologetics.  Provide a sentence description of each.  Remember,
   these are not competing but complementary approaches to explaining the
   Christian faith.

   * **Traditional Apologetics:**  Having all the evidence on hand to back up
     why Christianity is true.
   * **Cultural Apologetics:**  In addition to the aforementioned evidence,
     it's showing Christianity to be not only plausible but also desirable.

   **Note:**  I don't actually agree with Gould's conception of cultural
   apologetics.  I think it misses something substantial in the ability to take
   non-biblical worldviews and pick them apart, such that you can identify that
   which is good and show where it's been pilfered from God, and that which is
   bad, and show how it attempts to run counter to reality.

2. Gould describes the state of the world as "disenchanted."  What does he mean
   by this?

   If there's one thing that drives me absolutely nuts about this author, it's
   that he so rarely uses concrete language and defines his terms.  This was
   perhaps the worst one in terms of undefined terms.  It sounds like he uses
   it to mean a world that no longer believes in the supernatural, but he also
   seems to use it to mean more than that, attaching undefined mystical
   connotations to the term.

3. Our most fundamental orientation to life and the world is love.  We are
   driven and directed by our longings and desires.  How does cultural
   apologetics address these yearnings?

   First I'll note that I don't think I agree with the underlying premise here,
   but again it depends on *how you define* love.  Regardless, cultural
   apologetics is about identifying where a particular individual's longing
   line up with what God has wired us to desire, and then making known to them
   why they love what they love.

4. In an age of disenchantment, "self-expression and satisfaction of desires"
   are the highest good.  The chief sins are failures to be true to yourself
   (hypocrisy) and failure to be tolerant (judgementalism).  Describe some ways
   this is evident in our current cultural moment.

   * The "be true to your heart" line is what Disney's been selling for
     decades, heard most readily in Mulan's *True to Your Heart*, by 98 Degrees
     and Stevie Wonder.  The problem there is the heart is fickle, so it isn't
     a solid basis by which to evaluate anything.
   * Diversity, equity, and inclusion programs exist all over the corporate
     landscape to promote a diverse workforce, as long as your beliefs allow
     you to consider all others' beliefs as equally valid.  If you say
     something like "there is only one truth," that kind of thinking won't be
     tolerated, so the workforce is diverse and inclusive as long as you agree
     with the mainline narrative.

5. Gould argues that Christianity is good because (1) it is true; and (2)
   Christianity has been good for the world.  In what ways has Christianity
   been good for the world?

   All you need is to look at the way the concept of all humans being made in
   the image of God has transformed cultures throughout the ages:

   * Castaway babies rescued from trash heaps in ancient Rome.
   * The ingenuity of the middle ages freeing humans from the drudgery of
     manual labor, freeing them to more creative uses of their talents.
   * The biblical notion of private property laying the foundation for
     capitalism, and the millions of people that's lifted out of poverty over
     the centuries.
   * Combatting slavery, both hundreds of years ago and today.
   * Etc.

