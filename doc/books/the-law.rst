The Law
-------

* Author:  Frédéric Bastiat
* Publication Year:  1850
* Link:  http://gutenberg.org/ebooks/44800
* Started on:  ?
* Finished on:  2021-01-22

What is the Book About?
=======================

An examination of the meaning and implications of the phrase "law is justice".
