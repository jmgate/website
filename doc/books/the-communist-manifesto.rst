The Communist Manifesto
-----------------------

* Authors:  Karl Marx and Friedrich Engels
* Publication Year:  1888
* Link:  http://gutenberg.org/ebooks/61
* Started on:  ?
* Finished on:  2021-01-02

What is the Book About?
=======================

"The history of all hitherto existing society is the history of class
struggles."
