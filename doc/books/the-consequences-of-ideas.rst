The Consequences of Ideas
-------------------------

*Understanding the Concepts that Shaped Our World*

* Author:  R.C. Sproul
* Publication Year:  2000
* Link:  https://smile.amazon.com/Consequences-Ideas-Redesign-Understanding-Concepts/dp/1433563770/ref=sr_1_1?dchild=1&keywords=the+consequences+of+ideas+sproul&sr=8-1
* Started on:  ?
* Finished on:  2020-12-15

What is the Book About?
=======================

A whirlwind overview of western political thought from antiquity to today.
