Strong and Courageous
---------------------

*Following Jesus Amid the Rise of America's New Religion*

* Authors:  Jared Longshore and Tom Ascol
* Publication Year:  2021
* Link:  https://press.founders.org/shop/strong-and-courageous/
* Started on:  ?
* Finished on:  2021-03-06
