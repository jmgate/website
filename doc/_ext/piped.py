from __future__ import annotations

from docutils import nodes

from sphinx.application import Sphinx
from sphinx.util.docutils import SphinxDirective
from sphinx.util.typing import ExtensionMetadata


class PipedDirective(SphinxDirective):
    """A directive to embed a video from Piped."""

    required_arguments = 1

    def run(self) -> list[nodes.Node]:
        html = f"""
        <div style="position: relative;
                      padding-bottom: 56.25%;
                      height: 0;
                      overflow: hidden;
                      max-width: 100%;
                      height: auto;">
              <iframe
               src="https://piped.video/embed/{self.arguments[0]}"
               frameborder="0"
               allowfullscreen
               style="position: absolute;
                      top: 0;
                      left: 0;
                      width: 100%;
                      height: 100%;
                      padding: 0px;">
              </iframe>
          </div>
        """
        return [nodes.raw(rawsource="", text=html, format="html")]


def setup(app: Sphinx) -> ExtensionMetadata:
    app.add_directive('piped', PipedDirective)

    return {
        'version': '0.0.1',
        'parallel_read_safe': True,
        'parallel_write_safe': True,
    }

