from __future__ import annotations

from docutils import nodes

from sphinx.application import Sphinx
from sphinx.util.docutils import SphinxDirective
from sphinx.util.typing import ExtensionMetadata


class OdyseeDirective(SphinxDirective):
    """A directive to embed a video from Odysee."""

    required_arguments = 1

    def run(self) -> list[nodes.Node]:
        html = f"""
        <div style="position: relative;
                    height: 0;
                    overflow: hidden;
                    max-width: 100%;
                    height: auto;">
            <iframe
             id="odysee-iframe"
             style="width:100%; aspect-ratio:16 / 9;"
             src="https://odysee.com/$/embed/{self.arguments[0]}"
             allowfullscreen>
            </iframe>
        </div>
        """
        return [nodes.raw(rawsource="", text=html, format="html")]


def setup(app: Sphinx) -> ExtensionMetadata:
    app.add_directive('odysee', OdyseeDirective)

    return {
        'version': '0.0.1',
        'parallel_read_safe': True,
        'parallel_write_safe': True,
    }

