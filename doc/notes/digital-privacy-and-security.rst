Digital Privacy and Security
----------------------------

The following pages are for me to collect my notes as I dive into attempting to
preserve the level of digital convenience we have today, while at the same time
ensuring all our information stays private and secure.  Any sections marked
**Work in Progress** are areas I'm still actively investigating to figure out
the best way for us to go.  Feel free to check back often as I update things.

.. toctree::
   :titlesonly:

   digital-privacy-and-security/device-independent
   digital-privacy-and-security/smart-phones
   digital-privacy-and-security/burner-phones
   digital-privacy-and-security/computers
   digital-privacy-and-security/personal-cloud
   digital-privacy-and-security/cars
   digital-privacy-and-security/cryptocurrency

