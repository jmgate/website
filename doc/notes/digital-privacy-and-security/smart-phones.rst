Smart Phones
------------

When it comes to smart phones, there are two ways to go.  The lower barrier to
entry is to first see about :doc:`securing your current phone
<smart-phones/securing-your-current-phone>`.  If you want to take things to the
next level, you should :doc:`start with a secure operating system
<smart-phones/starting-with-a-secure-operating-system>`.

.. toctree::
   :titlesonly:
   :hidden:

   smart-phones/securing-your-current-phone
   smart-phones/starting-with-a-secure-operating-system

