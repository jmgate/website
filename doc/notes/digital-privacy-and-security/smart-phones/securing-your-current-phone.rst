Securing your Current Phone
---------------------------

.. todo::

   Insert introductory text.

.. todo::

   * https://youtu.be/wg00QkcpOOM?si=sEGvZHLOkBN813gW
   * https://odysee.com/@NaomiBrockwell:4/Fingerprint-unlock:e

Disabling Sensors
=================

.. tabs::

   .. tab:: Recommendations

      * Open your **Settings** app.
      * Scroll to the bottom and tap **About phone**.
      * Scroll to the bottom and tap **Build number** seven times to turn on
        developer mode.
      * Go back to **Settings** and tap **System**.
      * Open **Advanced** and then tap **Developer options**.
      * At the bottom of the first section, tap **Quick settings developer
        tiles**.
      * Turn on the **Sensors Off** tile and close the **Settings** app.
      * Now when you swipe down from the top of the screen, tap the widget on
        the left to turn off all sensors (microphone, camera, GPS,
        accelerometer, etc.).
      * Leave these off unless you actually need them.  (E.g., if you want to
        take a photo, enable the sensors and open your camera app; if you need
        to answer a phone call, enable the sensors and then answer.)

   .. tab:: Notes

      .. todo::

         Need to jot down notes.

   .. tab:: Video

      .. piped:: 7sZVdoQ0NN0

Keyboard
========

.. tabs::

   .. tab:: Recommendations

      * If you're using a custom operating system focused on privacy (see
        :doc:`starting-with-a-secure-operating-system`), the built-in system
        keyboard is a good choice.
      * If you desire more customizability, or don't want to use a privacy-focused
        OS, use `AnySoftKeyboard`_.

   .. tab:: Notes

      .. todo::

         Need to jot down notes.

   .. tab:: Video

      .. odysee:: @NaomiBrockwell:4/Keyboard-privacy:1

.. _LineageOS:  https://lineageos.org/
.. _GrapheneOS:  https://grapheneos.org/
.. _AnySoftKeyboard:  https://anysoftkeyboard.github.io/

App Store
=========

.. tabs::

   .. tab:: Recommendations

      * Download and install the `F-Droid app store`_, which is a catalogue of free
        and open-source software (FOSS).
      * Generally prefer to get apps from this source.
      * Use F-Droid to install the `Aurora app store`_, which is a different front
        end to the Google Play store without all the tracking.
      * For apps that are unavailabe via F-Droid, use Aurora.

   .. tab:: Notes

      .. todo::

         Need to jot down notes.

   .. tab:: Video

      .. odysee:: @NaomiBrockwell:4/Alt-app-store:f

.. _F-Droid app store:  https://f-droid.org/
.. _Aurora app store:  https://aurorastore.org/

Location Tracking
=================

.. tabs::

   .. tab:: Recommendations

      * Put your phone in airplane mode whenever you're not using it.
      * Disable WiFi when you're not using it.
      * Disable WiFi scanning.
      * Disable Bluetooth when you're not using it.
      * Disable Bluetooth scanning.
      * Either disable location services when not using them, or ensure the
        only apps that have access to location services are those that
        absolutely must have access, and even then, only while they're in use.
      * You might consider getting a Faraday bag/pouch for your phone, but
        after researching a number of brands and models, it looks like the only
        sure-fire way to block all signals to and from your phone is to simply
        wrap it in aluminum foil.  These are also useful for protecting
        RFID-enabled credit cards, key fobs, and the like.  You could always
        purchase a bag, test it out, and if it doesn't sufficiently shield
        whatever you put inside, simply return it.

   .. tab:: Notes

      .. todo::

         Need to jot down notes.

   .. tab:: Video

      .. odysee:: @NaomiBrockwell:4/location-tracking:7
      .. odysee:: @NaomiBrockwell:4/faraday-bags:0

WiFi
====

.. todo::

   * https://youtu.be/KXEe2kqiYIM?si=vWEk8YaxDT13O3JU
   * https://odysee.com/@NaomiBrockwell:4/snowden-says-don-t-use-wifi-i-explain:b
   * https://odysee.com/@NaomiBrockwell:4/Snowden-says-dont-use-wifi-RESPONSE-VIDEO:e

.. tabs::

   .. tab:: Recommendations

      In addition to the recommendations above:

      * Don't allow WiFi to automatically turn back on near saved networks.
      * Generally remove saved networks after you're done using them.
      * For saved networks you don't remove, disable auto-connect.
      * Don't allow apps to control WiFi.

      Alternatively, keep WiFi off all the time and use a USB-C tether to a
      mobile hotspot for internet connectivity (see **SIM Card** below).

   .. tab:: Notes

      .. todo::

         Need to jot down notes.

   .. tab:: Video

      .. odysee:: @NaomiBrockwell:4/How-Your-WiFi-is-Betraying-You:e

Maps
====

.. tabs::

   .. tab:: Recommendations

      * Try either `OsmAnd`_ or `Organic Maps`_.
      * Consider a dedicated device (e.g., Garmin, TomTom, etc.) that's not tied to
        you personally.

   .. tab:: Notes

      .. todo::

         Need to jot down notes.

   .. tab:: Video

      .. odysee:: @NaomiBrockwell:4/MOST-private-MAP-apps:e

.. _OsmAnd:  https://osmand.net/
.. _Organic Maps:  https://organicmaps.app/

Messaging App
=============

.. todo::

   https://odysee.com/@NaomiBrockwell:4/Telegram:68

.. tabs::

   .. tab:: Recommendations

      * Use `Signal`_.
      * `Create a username`_ instead of sharing your phone number, or consider
        setting up a Voice over IP (VoIP) number just for Signal (see below).
      * Consider using `disappearing messages`_.
      * For even better privacy, though a reduced feature set, use `Session`_.

   .. tab:: Notes

      .. todo::

         Need to jot down notes.

   .. tab:: Video

      .. odysee:: @NaomiBrockwell:4/MOST-Private-Messaging-Apps:3
      .. odysee:: @NaomiBrockwell:4/ditch-sms:1
      .. odysee:: @NaomiBrockwell:4/disappearing-messages:5

.. _Signal:  https://www.signal.org/
.. _Create a username:  https://support.signal.org/hc/en-us/articles/6712070553754-Phone-Number-Privacy-and-Usernames
.. _disappearing messages:  https://support.signal.org/hc/en-us/articles/360007320771-Set-and-manage-disappearing-messages
.. _Session:  https://getsession.org/

Team Communication App
======================

.. tabs::

   .. tab:: Recommendations

      * If you're looking for a secure alternative to Slack, Teams, What'sApp,
        etc., there aren't many options.
      * `Mattermost`_ is more secure than most.
      * A better option would be `Element`_.
      * Both are only free for small teams.  Once the number of users grows
        beyond a threshold, you'll need to pay per user per month.

   .. tab:: Notes

      .. todo::

         Need to jot down notes.

   .. tab:: Video

      .. todo::

         Need to find a video.

.. _Mattermost:  https://mattermost.com
.. _Element:  https://element.io

SIM Card
========

.. todo::

   https://youtu.be/0Em-J_3QYu4?si=tULL4ALuNWF-KyiX

.. tabs::

   .. tab:: Recommendations

      * Don't use a SIM card in your phone.
      * Instead use a mobile hotspot, such as those provided by the `Calyx
        Institute`_.
      * Pay for it with a privacy-focused cryptocurrency.
      * Keep it switched off unless you need to use it.
      * Run your VPN of choice on the hotspot rather than on the phone.
      * Prefer to connect your phone to the hotspot via a USB-C tether, rather
        than WiFi.
      * Use a VoIP number for phone functionality (see below).

   .. tab:: Notes

      .. todo::

         Need to jot down notes.

   .. tab:: Video

      .. odysee:: @NaomiBrockwell:4/Calyx:7

.. _Calyx Institute:  https://calyxinstitute.org/membership/internet

Phone Numbers
=============

.. todo::

   https://youtu.be/m_xtR6n94ro?si=i2gXz_mpXHz__aGC

.. admonition:: Work in Progress

   Still working through the process here.

.. tabs::

   .. tab:: Recommendations

      .. todo::

         Need to jot down recommendations.

   .. tab:: Notes

      .. todo::

         Need to jot down notes.

   .. tab:: Video

      .. odysee:: @NaomiBrockwell:4/Getting-MULTIPLE-PRIVATE-Phone-Numbers:5

Secondary Profiles
==================

.. tabs::

   .. tab:: Recommendations

      .. todo::

         Need to jot down recommendatons.

   .. tab:: Notes

      .. todo::

         Need to jot down notes.

   .. tab:: Video

      .. odysee::  @NaomiBrockwell:4/secondary-profiles:a

