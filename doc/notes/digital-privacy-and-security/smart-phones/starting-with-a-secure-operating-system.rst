Starting with a Secure Operating System
---------------------------------------

.. todo::

   Insert introductory text.

CalyxOS
=======

.. tabs::

   .. tab:: Recommendations

      If you want an operating system focused on privacy and security, but
      don't want to go through the hassle of installing and configuring it
      yourself from scratch, just get a `CalyxOS Membership`_ from the `Calyx
      Institute`_.

   .. tab:: Notes

      .. todo::

         Need to jot down notes.

   .. tab:: Video

      .. todo::

         Need to find a video.

.. _CalyxOS Membership:  https://calyxinstitute.org/membership/calyxos
.. _Calyx Institute:  https://calyxinstitute.org/

GrapheneOS
==========

.. tabs::

   .. tab:: Recommendations

      The Google Pixel phones are regarded as being the most secure smart phones on
      the market.  If you have one, or you don't mind buying one, then you'll want to
      install `GrapheneOS`_, which can only be installed on Pixel devices.

   .. tab:: Notes

      .. todo::

         Need to jot down notes.

   .. tab:: Video

      .. odysee:: @NaomiBrockwell:4/de-googled-phone:3

.. _GrapheneOS:  https://grapheneos.org/

LineageOS
=========

.. tabs::

   .. tab:: Recommendations

      If you don't want to give Google another dime, then `LineageOS`_ is the
      way to go.

   .. tab:: Notes

      .. admonition::  Work in Progress

         Still working through the details here.

      1. Check the list of their `supported devices`_ to ensure your phone is
         supported, or to ensure you purchase one that is.  In my case, I went
         with a `Motorola Edge 20`_.
      2. Start going through the `installation instructions`_.

         .. note::

            I was unable to get this to work on Fedora, but the documentation
            suggests trying another machine/OS.  I tried running an Ubuntu
            virtual machine (VM) on top of my Fedora host, but no luck there
            either.  I was finally able to get it to work on a different
            machine running Ubuntu as the host OS.

         a. Follow the linked instructions for
            |installing_adb_and_fastboot_on_linux|_.
         b. Then |set_up_adb|_.
         c. And |set_up_fastboot|_.

      3. Return to the `installation instructions`_, and ensure you've met all
         the `basic requirements`_.
      4. Move on to `ensuring you have the correct firmware version`_.
      5. Next `unlock the bootloader`_.

         a. Go to the phone's **Settings** → **System** → **Developer
            options** and turn on **OEM unlocking**.  (This option may not
            become available for a few days after enabling developer mode.)
         b. Pull up the instructions at `Motorola Support`_.
         c. Read through everything and click **Proceed Anyway**.
         d. Follow the instructions to get the Device ID.  When you paste it
            into the field on the page, make sure there aren't any extraneous
            characters (e.g., whitespace), and then click **Can my device be
            unlocked?**
         e. Scroll down, agree to the terms, and click **Request Unlock Key**,
            and you should receive an email with the key.

            .. note::

               If the Motorola Support site is being flaky in step (d) when you
               try to paste your Device ID to see if it's unlockable, here's a
               workaround: The Device ID is separated into four parts with
               ``#`` characters.  Navigate to the following URL, pasting in the
               corresponding parts (without the ``#`` characters).

               .. code::

                  https://en-us.support.motorola.com/cc/productRegistration/unlockPhone/<part 1>/<part 4>/<part 3>

               That will trigger the email with your unlock key.

         f. Follow the intructions to use the key to unlock the bootloader.
         g. After the bootloader is unlocked, start the phone and it'll be
            wiped.  That means you'll need to set it up again from scratch
            (e.g., connect to WiFi and enable developer mode).

   .. tab:: Video

      .. todo::

         Need to find a video.

.. _LineageOS:  https://lineageos.org/
.. _supported devices:  https://wiki.lineageos.org/devices/
.. _Motorola Edge 20:  https://wiki.lineageos.org/devices/berlin/
.. _installation instructions:  https://wiki.lineageos.org/devices/berlin/install/
.. |installing_adb_and_fastboot_on_linux| replace:: installing ``adb`` and ``fastboot`` on Linux
.. _installing_adb_and_fastboot_on_linux:  https://wiki.lineageos.org/adb_fastboot_guide#on-linux
.. |set_up_adb| replace:: set up ``adb``
.. _set_up_adb:  https://wiki.lineageos.org/adb_fastboot_guide#setting-up-adb
.. |set_up_fastboot| replace:: set up ``fastboot``
.. _set_up_fastboot:  https://wiki.lineageos.org/adb_fastboot_guide#setting-up-fastboot
.. _basic requirements:  https://wiki.lineageos.org/devices/berlin/install/#basic-requirements
.. _ensuring you have the correct firmware version:  https://wiki.lineageos.org/devices/berlin/install/#checking-the-correct-firmware
.. _unlock the bootloader:  https://wiki.lineageos.org/devices/berlin/install/#unlocking-the-bootloader
.. _Motorola support:  https://en-us.support.motorola.com/app/standalone/bootloader/unlock-your-device-a

