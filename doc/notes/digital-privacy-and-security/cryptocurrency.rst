Cryptocurrency
--------------

.. todo::

   Insert introductory text.

   * https://odysee.com/@NaomiBrockwell:4/does-your-wallet-leak-your-information:1

Reasons to Use Cryptocurrency
=============================

.. tabs::

   .. tab:: Recommendations

      * If you're concerned about keeping your financial transactions private,
        investigating cryptocurrency will be worthhile.

   .. tab:: Notes

      * There are four primary reasons to use cryptocurrency:

        1. It can serve as an economic hedge against inflation when governments
           decide to just print more fiat currency.
        2. It can help with privacy in transactions, because your purchases
           aren't tied to your personal information, as they are with, e.g.,
           credit and debit cards.
        3. You have access to, and control over, your money at all times,
           because governments can't, e.g., pressure financial institutions to
           freeze your assets or restrict where you can spend your funds.
        4. You can save a good deal of money, as a variety of retailers offer
           discounts when paying in cryptocurrency.

      * Cryptocurrency is decentralized, borderless, permissionless, and unable
        to be debased.

   .. tab:: Video

      .. odysee:: @NaomiBrockwell:4/4-reasons-to-use-crypto:b

Different Types of Cryptocurrency
=================================

.. tabs::

   .. tab:: Recommendations

      * `Bitcoin`_ is good as a store of value.
      * `Dash`_ and `BitcoinCash`_ are good for everyday purchases.
      * `Monero`_ and `Zcash`_ are good for privacy.
      * `Etherium`_ is good for decentralized finance.
      * Don't use cryptocurrency for speculative investing.

   .. tab:: Notes

      * Different cryptocurrencies are optimized for different purposes:

        * **Store of Value:**

          * `BitCoin`_ has a fixed supply, so it can't be debased.  It's not
            the fastest or cheapest, so it's not generally used as a currency
            for everyday purposes.  It has an open ledger, which shows clear
            transaction histories, so it's not the most private option.
            Rather, it tends to be used as a store of value, like gold.

        * **Everyday Purchases:**

          * `Dash`_ (short for "digital cash") is intended to be used for
            everyday purchases (groceries, gas, etc.).  Transaction cost less
            than a penny, and clear in under a second, so it's ideal for its
            intended use.  While not the most private option, it does
            incorporate a coin mixing feature (`CoinJoin`_), which provides
            consumer grade privacy so an observer has difficulty tracing
            transaction histories.
          * `BitcoinCash`_ is also good for everyday use.  There's no built-in
            privacy mixing service, but `CashFusion`_ provides this on top.

        * **Privacy:**

          * `Monero`_ is the current leader in the private cryptocurrency
            space.
          * `Zcash`_ is more widely praised by cryptographers, but privacy is
            opt-in.  This allows Zcash to remain on many exchanges where other
            privacy coins are unallowed.

        * |Decentralized Finance|_ **(DeFi):**

          * `Etherium`_ allows access to various financial tools (earning
            interest, borrowing, lending, buying insurance, trading assets and
            derivatives, etc.) without the gatekeepers, middlemen, or censors
            of the traditional banking system.
          * Other coins are `starting to support DeFi as well`_.

   .. tab:: Video

      .. odysee:: @NaomiBrockwell:4/Crypto-for-everyday:1

.. _Bitcoin:  https://bitcoin.org/en/
.. _Dash:  https://www.dash.org/
.. _CoinJoin:  https://docs.dash.org/en/stable/docs/user/wallets/dashcore/coinjoin-instantsend.html
.. _BitcoinCash:  https://bitcoincash.org/
.. _CashFusion:  https://cashfusion.org/
.. _Monero:  https://www.getmonero.org/
.. _Zcash:  https://z.cash/
.. _Decentralized Finance:  https://www.coinbase.com/learn/crypto-basics/what-is-defi
.. |Decentralized Finance| replace::  **Decentralized Finance**
.. _Etherium:  https://ethereum.org/en/
.. _starting to support DeFi as well:  https://coinmarketcap.com/view/defi/

Accessing Your Cryptocurrency
=============================

.. tabs::

   .. tab:: Recommendations

      .. todo::

         Need to finalize recommendations.

      * Make sure you back up your crypto wallet keys.
      * Phone wallets:

        * Edge wallet
        * Bitcoin.com wallet:  only Bitcoin and Bitcoin Cash; non-custodial;
          map to show nearby merchants

      * Desktop wallets:

        * Wasabi:  privacy focused for Bitcoin
        * Electron Cash:  added privacy for Bitcoin Cash

      * Cold wallets:

        * KeepKey
        * Trezor
        * Ledger

   .. tab:: Notes

      .. todo::

         Need to finalize notes.

      * *Crypto wallets* are used to access your cryptocurrency.  The currency
        isn't stored there, but rather a private key that allows you to access
        your funds on the blockchain.
      * Custodial wallets:  Work more like a crypto bank, which holds your key
        for you.  Any time you want to use your funds, you need to go through
        the bank to get access.  If you'll likely forget your key, this might
        be the route for you.
      * Non-custodial wallets:  Stores the private key directly on the device.
        The wallet company can't access the key---only you can.  If you lose
        the key, you lose access to the cryptocurrency.  There is no password
        reset button.
      * Hot wallets:  Connected to the internet (app on phone, browser plugin,
        etc.).  Easier to hack.  Only keep money in these wallets that you
        access regularly.  Easiest is probably a phone wallet.
      * Cold wallets:  Disconnected from the internet.  Small hardware devices
        that you plug into a computer when you want to access the crypto on
        them.  More resistant to hacking attempts.  Best for crypto you don't
        expect to spend day-to-day.  Buy directly from the company itself; not
        second-hand.
      * When you first set up your wallet, you'll receive a recovery seed.
        Back this up.  Never share it.  Store it in a safe place.  Write it
        down by hand.  Store two copies in different locations.  See "key
        keepers" (metal).

   .. tab:: Video

      .. odysee:: @NaomiBrockwell:4/crypto-wallets-explained:1

Purchasing Cryptocurrency
=========================

.. tabs::

   .. tab:: Recommendations

      .. todo::

         Need to finalize recommendations.

      * CEX:  Centralized Exchange

        * Coinbase:  FDIC insured
        * Kraken
        * Gemini:  FDIC insured
        * Binance

      * DEX:

        * ThorChain
        * ShapeShift
        * UniSwap
        * Banxa
        * Bisq
        * Haveno:  uses Monero
        * localbitcoins.com:  needs phone number and identification
        * local.bitcoin.com:  Bitcoin Cash, non-custodial
        * localcryptos.com:  non-custodial
        * localmonery.com:  custodial

   .. tab:: Notes

      .. todo::

         Need to finalize notes.

      * You send fiat currency from your financial institution to a centralized
        exchance, and when the money arrives, you can use it to buy crypto.
      * "Not your keys, not your coins."  CEX issues IOU for your crypto, and
        you trust they'll pay it back.
      * Keep as little funds on a CEX as possible.
      * CEX fees can differ dramatically, be hidden, etc.
      * There may also be hold times on your account, where you can't withdraw
        the funds to fiat currency for some time.
      * CEX also require identification as part of government KYC (Know Your
        Customer) regulations.
      * Foreign exchanges may not require this, but usually for smaller
        amounts.
      * Most fiat to crypto bridges are embroiled in all the beuracracy of the
        legacy financial system.
      * DEX:  Decentralized Exchange.  No middle man.  No KYC.
      * Find someone willing to pay crypto for services and perform work for
        them.

   .. tab:: Video

      .. odysee:: @NaomiBrockwell:4/how-to-buy-crypto:c

Earning Cryptocurrency
======================

.. tabs::

   .. tab:: Recommendations

      .. todo::

         Need to jot down recommendations.

      * Foo

   .. tab:: Notes

      .. todo::

         Need to finalize notes.

      * The best way to acquire crypto is to earn it.
      * People can live entirely on crypto.
      * Earning means you don't need to go through a CEX.
      * LBRY:  Peer-to-peer video hosting service, with Odysee frontend.  You
        can earn LBC (LBRY Credits), no KYC, no minimum withdrawals, you have
        access to your funds, traded on exchanges (CoinEx, Bittrex).  Can use
        LBC to boost content you approve of, but that doesn't actually spend
        your tokens.  You can also tip content creators, or put content behind
        a paywall such that you purchase it with LBC.  You can earn LBC just by
        watching content.
      * Hive:  Facebook meets Reddit.  Earn Hive Tokens based on how your
        content is upvoted.  Various interfaces to the Hive ecosystem.  PeakD
        for blogging.  3Speak for Twitter status update.  D.Buzz for UNKNOWN.
        Can convert to other coins via exchanges.  Can "stake" your Hive Tokens
        and earn interest.
      * Flote:  Facebook alternative that allows people to send you recurring
        Bitcoin donations.  Kind of like Patreon.
      * Minds:  Like Facebook with YouTube sync feature.  Not decentralized on
        the back-end, but Minds Tokens are.  Earn them through engaging on the
        platform.  Needs a Minds+ membership to withdraw.
      * Read.Cash is great for blogging.  Read articles.  Post status updates.
        Send someone a tip.  Uses BitcoinCash.  Sponsorships allow people to
        bid on add placement on your sponsor block.
      * Crypto shines in microtransactions.
      * Memo.Cash:  Crypto version of Twitter.  Also uses BitcoinCash.
        Reaction tokens are tradeable on a marketplace.
      * Brave Basic Attention Token (BAT):  Can be donated to as a creator on a
        variety of platforms.  Don't need to be a creator.  Can also earn by
        watching ads that Brave normally blocks.  Can spend them on gift cards,
        trade them on exchanges, etc.
      * Cointr.ee:  Tipping platform.  Handy link page for all your crypto
        addresses and social media platforms.
      * BitcoinCash:  Read.cash:  get tipped for your blog.  Memo.cash:  get
        tipped for your tweets.  Noise.cash: get tipped for your photos.
        Flipstarter:  crowd-fund projects.

   .. tab:: Video

      .. odysee:: @NaomiBrockwell:4/Earning-Crypto-on-social-media:2

Spending Cryptocurrency
=======================

.. tabs::

   .. tab:: Recommendations

      .. todo::

         Need to jot down recommendations.

      * Foo

   .. tab:: Notes

      .. todo::

         Need to jot down notes.

      * Savings are a great use for cryptocurrencies.
      * It can be part of your diversification strategy.
      * It's possible to live entirely off of cryptocurrency, depending on
        where you live and what your expenses are.
      * Dash
      * If a business doesn't accept crypto yet, have a conversation with the
        owner to see if they'd be open to it.
      * If consumers want it, businesses won't want to miss out.
      * Bitrefill:  Gift cards, etc.  Accepts several different coins.  Earns
        rewards.
      * Purse.io:  Purchase from major chains with crypto, and you'll get
        discounts for doing so.
      * Irving gas station gift cards.
      * DashDirect:  155,000+ merchants.  Discounts on everything.  Pay in
        Dash.  Map of nearby merchants.  You're buying a gift code in the exact
        amount of your purchase, in the fly.
      * Flexa:  Digital payments network.  Allows any app to spend any asset at
        merchants all over the world.  You pay in whatever you want, the
        merchant receives in whatever currency they want.  Used behind the
        scenes in Gemini Wallet (and others).
      * Using crypto can be easier than using a credit card.  No need to be 18.
        No credit check.  No bank account.
      * Paying bills is the hardest part with crypto.  US regulations make it
        difficult.
      * Given crypto volatility, when things are high, that's when you make
        your major purchases; when things are low, you live leaner.
      * Crypto debit and credit cards allow you to pay with crypto pretty much
        anywhere.  Some don't require KYC.
      * DashDirect:  See above.  Also offer a MasterCard.  Just-in-time
        funding, on-the-fly for each purchase.  MasterCard limit is $1000/day,
        to keep it KYC free.  Limit on the gift card is $10K/day, also
        KYC-free.  MasterCard has no fees.
      * BitPay MasterCard:  Supports more coins.  Load balance on the card when
        in line at the store.  No fees.  $10K/day limit, but does require KYC.
        Can purchase crypto with credit/debit cards in the app.
      * Volatility will level out as more people adopt crypto for everyday
        transactions.

   .. tab:: Video

      .. odysee:: @NaomiBrockwell:4/How-to-Live-ENTIRELY-on-Crypto:2
      .. odysee:: @NaomiBrockwell:4/spend-cryptoEVERYWHERE:9

