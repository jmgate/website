Burner Phones
-------------

In the event that you need a means of communication that is in no way tied to
you as an individual, it is still possible to buy a burner phone, but you need
to know what to look and ask for.

.. tabs::

   .. tab:: Recommendations

      .. admonition:: Work in Progress

         I still haven't been able to do this yet after multiple attempts.  It
         apparently is strongly dependent on the WalMart phone representative
         you speak with.

      * First stop by an ATM and grab $50 in cash.
      * Head to the electronics section in Walmart.
      * Grab a `Tracfone flip phone`_ for $20.
      * Grab a voucher for `60 minutes`_ of talk, text, and web, for another
        $20.
      * Head to the electronics desk and ask for the phone representative.
      * Ask them to activate the phone in the store for you.  At no point
        should you be required to provide personally identifiable information.
      * When prompted, pick an area code that's not associated with where you
        live.
      * Pay in cash.

   .. tab:: Notes

      .. todo::

         Need to jot down notes.

      * The 60 minutes for talk, text, and web is a 90-day plan.  The minutes
        roll over from one period to the next, as long as you continue to
        purchase another 60 minutes before the 90 days is up.  Of course you
        could always let the minutes expire, and then when you need them again,
        just spend another $20.  In that case, you may need to go through the
        process of registering the phone again, because the phone company may
        have recycled the phone number used previously.

   .. tab:: Video

      .. piped:: rJmvWdXO8Y0

.. _Tracfone flip phone:  https://www.walmart.com/ip/Tracfone-Nokia-2760-Flip-4GB-Black-Prepaid-Feature-Phone-Locked-to-Tracfone-Wireless/721170509?athbdg=L1103
.. _60 minutes:  https://www.walmart.com/ip/Tracfone-19-99-Basic-Phone-60-minutes-90-Day-Prepaid-Plan-Direct-Top-Up/694425047?athbdg=L1600

