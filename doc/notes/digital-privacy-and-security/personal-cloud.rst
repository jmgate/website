Personal Cloud
--------------

.. todo::

   Insert introductory text.

.. admonition::  Work in Progress

   Still need to work through all of this.

Hardware
========

.. tabs::

   .. tab:: Recommendations

      .. admonition::  Work in Progress

         Might be worth investigating other options (e.g., used Dell tower,
         etc.).

      * `TrueNAS Mini X+`_
      * `Western Digital WD Red Pro NAS`_ internal hard drive

   .. tab:: Notes

      .. todo::

         Need to jot down notes.

   .. tab:: Video

      .. todo::

         Need to find a video.

.. _TrueNAS Mini X+:  https://www.truenas.com/truenas-mini/
.. _Western Digital WD Red Pro NAS:  https://www.westerndigital.com/products/internal-drives/wd-red-pro-sata-hdd?sku=WD142KFGX

Productivity Suite
==================

.. tabs::

   .. tab:: Recommendations

      * Use `NextCloud`_ for:

        * File-sharing
        * Audio/video conferencing and text chat
        * Email client
        * Calendar
        * Contacts
        * Task management
        * Documents
        * Spreadsheets
        * Slides
        * Notetaking/sketching

   .. tab:: Notes

      .. todo::

         Need to jot down notes.

   .. tab:: Video

      .. todo::

         Need to find a video.

.. _NextCloud:  https://nextcloud.com/

Media
=====

.. tabs::

   .. tab:: Recommendations

      * Use the `Plex media server`_ for the home movie library.
      * Use `PlexAmp`_ for the home music library.

   .. tab:: Notes

      .. todo::

         Need to jot down notes.

   .. tab:: Video

      .. todo::

         Need to find a video.

.. _Plex media server:  https://www.plex.tv/register/
.. _PlexAmp:  https://www.plex.tv/plexamp/

Team Communication
==================

.. todo::

   Figure out how to self-host either Element or Mattermost.

