Device-Independent
------------------

.. todo::

   Insert introductory text.

Browser
=======

.. tabs::

   .. tab:: Recommendations

      * For everyday usage, use the `Brave browser`_.
      * For improved privacy, use the `Tor browser`_.  Note that this will be a
        much slower experience, will likely require multiple confirmations that
        you are human and not a bot, and not everything will work (e.g.,
        streaming services).  It'll probably be more frustrating than it's
        worth to set Tor as your default browser; instead use Brave as your
        default, and use Tor for operations that require the utmost anonymity.
      * As a back-up, for when a website simply won't function with either of
        the above, use `Firefox`_.

   .. tab:: Notes

      * Browsers are effectively spies for the companies that make them.
      * Cookies enable the convenience you're used to from modern internet
        usage, but allow companies to track your behavior across websites.
      * Turning tracking off in the most popular browsers can be difficult.

   .. tab:: Video

      .. odysee:: @NaomiBrockwell:4/Most-private-browsers:8

.. _Brave browser:  https://brave.com/
.. _Tor browser:  https://www.torproject.org/
.. _Firefox:  https://www.mozilla.org/en-US/firefox/new/

Search Engine
=============

.. tabs::

   .. tab:: Recommendations

      * Use `Brave Search`_.
      * If you're not satisfied with the results, try out `Qwant`_ or
        `Mojeek`_.
      * If you absolutely must have Google results, but without all the
        tracking, use `Startpage`_.

   .. tab:: Notes

      * *Pure* search engines crawl the web themselves to generate their own
        indices.  *Meta* search engines rely on the indices built by other
        search engines.  Some search engines are a hybrid of the two.
      * `Brave Search`_, `Qwant`_, and `Mojeek`_ are all pure search engines.
      * `DuckDuckGo`_, which is a hybrid search engine, used to be the
        preferred solution, but it uses Bing results under the hood, and
        Microsoft started censoring Bing results a few years ago.

   .. tab:: Video

      .. odysee:: @techlore:3/i%27m-leaving-duckduckgo%2C-and-here%27s-what:f
      .. odysee:: @NaomiBrockwell:4/The-Most-PRIVATE-Search-Engine:a

.. _Brave Search:  https://search.brave.com/
.. _Qwant:  https://www.qwant.com/
.. _Mojeek:  https://www.mojeek.com/
.. _Startpage:  https://www.startpage.com/
.. _DuckDuckGo:  https://duckduckgo.com/

Email
=====

.. tabs::

   .. tab:: Recommendations

      * Use `ProtonMail`_.
      * Once you've logged in on a computer, click the button in the address
        bar to install the app and access it via the app instead of a browser.
      * Consider `improving on the default security settings`_.
      * Use `ProtonPass`_ (see :ref:`Password Vault <password_vault>`) to allow
        the creation of email aliases.
      * Sign up for the **Pass Plus** subscription to allow for unlimited email
        aliases.  Create aliases for each service you log in to.
      * If you're going to be using more than one of Proton's paid services,
        it's worthwhile to pay for their `family membership`_.

   .. tab:: Notes

      * Email *subaddressing*, where you use myrealemail+subaddress@domain.com,
        is not private because it reveals your true email address, and allows
        data brokers to correlate your identity across services.
      * Proton purchased `SimpleLogin`_ and integrated it into `ProtonPass`_.
      * If you want, you can `host the code yourself`_ on your own server for
        free.

   .. tab:: Video

      .. odysee:: @NaomiBrockwell:4/most-private-email:4
      .. odysee:: @NaomiBrockwell:4/Email-aliases:2

.. _ProtonMail:  https://proton.me/mail
.. _improving on the default security settings:  https://freedom.press/training/protonmail-pro/
.. _SimpleLogin:  https://simplelogin.io/
.. _host the code yourself:  https://github.com/simple-login/app

.. _password_vault:

Password Vault
==============

.. tabs::

   .. tab:: Recommendations

      * Use `ProtonPass`_.
      * Once you've logged in on a computer, click the button in the address
        bar to install the app and access it via the app instead of a browser.
      * Install the Brave browser extension.

   .. tab:: Notes

      .. todo::

         Need to jot down notes.

   .. tab:: Video

      .. todo::

         Need to find a video.

.. _ProtonPass:  https://proton.me/pass

VPN
===

.. tabs::

   .. tab:: Recommendations

      * If you're just looking for an off-the-shelf VPN service, use
        `ProtonVPN`_.
      * If you're going to be using more than one of Proton's paid services,
        it's worthwhile to pay for their `family membership`_.
      * If you don't want to use a centralized VPN provider, but would prefer a
        distributed VPN where no one entity owns all the servers you'll be
        connecting to, use `MysteriumVPN`_.
      * For ultimate privacy and control, you can run your own VPN server
        (e.g., `Algo VPN`_, `OpenVPN`_, etc.), but that doesn't afford you the
        flexibility of hopping around locations whenever you like.
      * Depending on your privacy needs, a combination of all three options
        above might be worthwhile for different types of internet traffic.

   .. tab:: Notes

      * VPNs were never designed for anonymity---they hide your IP address, but
        that's all.
      * Most VPNs out there, including paid ones, are really just fronts for
        data brokerage (meaning they're selling your data).
      * While there's hundreds of VPNs available, they're all owned by a
        relatively small handful of companies.

   .. tab:: Video

      .. odysee:: @NaomiBrockwell:4/vpn-scandals:5
      .. odysee:: @AllThingsSecured:9/zero-trust-vpn-mysterium-review-2021:7
      .. piped:: gxpX_mubz2A

.. _ProtonVPN:  https://protonvpn.com/
.. _family membership:  https://proton.me/family
.. _MysteriumVPN:  https://www.mysteriumvpn.com/
.. _Algo VPN:  https://github.com/trailofbits/algo
.. _OpenVPN:  https://openvpn.net/

YouTube Alternatives
====================

.. tabs::

   .. tab:: Recommendations

      * Use `Odysee`_ for the content creators that are hosting their videos
        there.  An Android app is also available for your smart phone.  If you
        want to see if a particular YouTube video is available, use the **Try
        LBRY** app from the F-Droid app store.
      * For videos that aren't hosted on Odysee, you can use `Piped`_ instead,
        which is a front-end to YouTube without all the tracking.  On your
        smart phone, you can use `LibreTube`_, which uses Piped behind the
        scenes.
      * Ultimately it's best to watch videos on a computer rather than on your
        phone.

   .. tab:: Notes

      * YouTube's purpose is to track you as you search, scroll, and watch, for
        the sake of selling that data to thousands of companies.
      * It's also intended to influence you (and the world at large) via what
        content is promoted vs censored.
      * LBRY (which `Odysee`_ is a front-end for) is a blockchain-based,
        decentralized storage network, which means greater censorship
        resistance and improved freedom of publication.

   .. tab:: Video

      .. odysee:: @NaomiBrockwell:4/Youtube-alternative-apps:3

.. _Odysee:  https://odysee.com/
.. _Piped:  https://piped.video/
.. _LibreTube:  https://libretube.dev/

Authentication
==============

.. tabs::

   .. tab:: Recommendations

      .. todo::

         Need to jot down recommendations.

   .. tab:: Notes

      .. todo::

         Need to jot down notes.

   .. tab:: Video

      .. odysee:: @NaomiBrockwell:4/Security-keys:3
      .. odysee:: @NaomiBrockwell:4/most-private-2FA:5
      .. odysee:: @NaomiBrockwell:4/DARK-SIDE-of-2FA-Apps:c
      .. odysee:: @NaomiBrockwell:4/most-secure-2fa:7

Privacy Screens
===============

.. tabs::

   .. tab:: Recommendations

      .. todo::

         Need to jot down recommendations.

   .. tab:: Notes

      .. todo::

         Need to jot down notes.

   .. tab:: Video

      .. odysee:: @NaomiBrockwell:4/privacy-screen:c

