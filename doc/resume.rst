Résumé
------

.. role:: raw-html(raw)
   :format: html

..
  .. figure:: ../images/headshot.png
     :height: 200px
     :align: right

     jason.m.gates@pm.me :raw-html:`<br>`
     `LinkedIn Profile <https://www.linkedin.com/in/jason-gates-96196726a/>`_

`LinkedIn Profile <https://www.linkedin.com/in/jason-gates-96196726a/>`_

My career has taken me from being an engineering physicist to a computational
mathematician to a software engineer and finally to a DevOps evangelist.
During my undergraduate research into magnetically confined high-temperature
plasmas, I discovered I was more interested in the numerical methods working
behind the scenes, and decided to pursue that in graduate school.  In the midst
of that, I realized the majority of the problems you run into aren't so much in
the algorithms themselves, but in their implementation in the software, so I
made the switch over to software engineering.  My experience in that arena
showed me it's less about the implementation details and more about the team
culture you have and how that contributes to the overall success of the
project, and thus the switch to what I'm calling a "DevOps evangelist" role,
where my purpose is to help you do the amazing work you already do, but do it
better.

I excel in getting plugged into a team, determining what its various pain
points are that prevent it from being as successful as it can be, prioritizing
which of those need to be addressed first, and then iterating with the team to
make incremental improvements to help them move in the right direction.  Though
many view DevOps as a collection of tools or tasks that enable the "real work"
to get done, it's actually a paradigm shift in how we view both the work we do
and how we go about doing it.  Helping teams realize this paradigm shift, and
the resulting productivity boost, is one of my passions.

Throughout my career, I've enjoyed the opportunities afforded for continual
learning and improvement, whether that's developing my skills as a leader, or
honing my competencies with an ever-evolving tech stack.  I excel in
self-directed study, come up-to-speed with new technologies quickly, and then
often find myself the most proficient person around in whatever language,
library, or tool is in question.  I pride myself in being not just a developer,
but a software *engineer*, who is able to assess a project's needs, determine
the optimal technologies to address them, and then design, build, deliver, and
maintain solutions with their long-term sustainability and resiliency to the
ever-shifting landscape in mind.

I function best as a generalist, leading or interfacing with a team of
specialists.  I excel at keeping the high-level and long-term plan in mind,
breaking it down into manageable pieces, and finding the right people to work
those tasks.  When needed, I have the experience and attention to detail to dig
down into the weeds with the specialists to ensure the solutions we're
designing and building will meet the overall project needs.  I do all this best
through asynchronous communication via project management and team
collaboration tools, with synchronous meetings scheduled only as needed.

:raw-html:`<div style="page-break-before: always;"></div>`

Education
=========

| **Colorado School of Mines**, Golden, Colorado
| Ph.D. in Mathematical and Computer Sciences
| GPA: 4.0; Qualifying Exams: Passed
| *Left incomplete due to family responsibilities*

| **University of Tulsa**, Tulsa, Oklahoma
| M.S. in Applied Mathematics
| Graduation: May, 2011; GPA: 3.917

| **University of Tulsa**, Tulsa, Oklahoma
| B.S. in Engineering Physics, concentration in Robotics
| B.S. in Applied Mathematics
| B.A. in German
| Graduation: May, 2009; GPA: 3.916

Experience
==========

**Sandia National Laboratories**, Albuquerque, New Mexico
:raw-html:`<br>`
April, 2023 -- Present
:raw-html:`<br>`
*Senior Member of the Technical Staff:  Radar Intelligence, Surveillance, &
Reconnaissance*
:raw-html:`<br>`
Automating adherence to software engineering best practices; assisting teams in
improving their project management practices; streamlining a process for
open-sourcing Python infrastructure.

**Sandia National Laboratories**, Albuquerque, New Mexico
:raw-html:`<br>`
December, 2022 -- April, 2023
:raw-html:`<br>`
*Senior Member of the Technical Staff:  Systems Engineering & Integrated
Solutions*
:raw-html:`<br>`
Driving the adoption of software engineering best practices; establishing
coding/testing standards for Python package development; developing a process
for modularizing and open-sourcing Python infrastructure.

**Sandia National Laboratories**, Albuquerque, New Mexico
:raw-html:`<br>`
May, 2021 -- December, 2022
:raw-html:`<br>`
*Member of the Technical Staff:  Systems Engineering & Integrated Solutions*
:raw-html:`<br>`
Leading an initiative to automate manual integration and release testing;
encouraging fuller adoption of GitLab as an all-in-one DevOps and project
management platform to improve developer productivity and overall team success.

**Sandia National Laboratories**, Albuquerque, New Mexico
:raw-html:`<br>`
October, 2018 -- May, 2021
:raw-html:`<br>`
*Member of the Technical Staff:  Software Engineering & Research*
:raw-html:`<br>`
Developing continuous integration/deployment workflows and infrastructure;
advising teams on achieving a DevOps transformation; leading teams to design
and develop tools to improve sustainability and reproducibility.

**Sandia National Laboratories**, Albuquerque, New Mexico
:raw-html:`<br>`
September, 2017 -- October, 2018
:raw-html:`<br>`
*Member of the Technical Staff:  Computational Mathematics*
:raw-html:`<br>`
Introducing software engineering best practices into team workflows; developing
code integration workflows and infrastructure.

:raw-html:`<div style="page-break-before: always;"></div>`

**Sandia National Laboratories**, Albuquerque, New Mexico
:raw-html:`<br>`
June, 2016 -- September, 2017
:raw-html:`<br>`
*Limited Term Employee:  Computational Mathematics*
:raw-html:`<br>`
Software engineering, development, maintenance, testing; version control
instruction.

**Northrop Grumman Corporation**, Aurora, Colorado
:raw-html:`<br>`
June, 2014 -- June, 2016
:raw-html:`<br>`
*Engineer Systems II*
:raw-html:`<br>`
Extending software capabilities; developing and testing algorithms; addressing
data quality.

**Colorado School of Mines**, Golden, Colorado
:raw-html:`<br>`
August, 2012 -- May, 2014
:raw-html:`<br>`
*Graduate Teaching Fellow*
:raw-html:`<br>`
Advanced Engineering Mathematics and Calculus 3; "Problem Solving with Matlab"
tutorial series.

**Front Range Community College**, Westminster, Colorado
:raw-html:`<br>`
May, 2013 -- August, 2013
:raw-html:`<br>`
*Math Instructor*
:raw-html:`<br>`
College Algebra; online education certified.

**Sandia National Laboratories**, Albuquerque, New Mexico
:raw-html:`<br>`
May, 2012 -- July, 2012
:raw-html:`<br>`
*SIP Graduate Professional Technical Summer Intern*
:raw-html:`<br>`
Code validation via manufactured solutions to partial differential equations.

**Colorado School of Mines**, Golden, Colorado
:raw-html:`<br>`
August, 2011 -- May, 2012
:raw-html:`<br>`
*Graduate Teaching Assistant*
:raw-html:`<br>`
Recitation sections of Calculus 3.

**University of Tulsa**, Tulsa, Oklahoma
:raw-html:`<br>`
August, 2009 -- May, 2011
:raw-html:`<br>`
*Graduate Teaching Assistant*
:raw-html:`<br>`
Quiz sections of Calculus 1 & 2.

**University of Tulsa**, Tulsa, Oklahoma
:raw-html:`<br>`
May, 2007 -- May, 2009
:raw-html:`<br>`
*Plasma Physics Research Assistant*
:raw-html:`<br>`
Computationally solved nonlinear magnetohydrodynamic (MHD) equations.

:raw-html:`<div style="page-break-before: always;"></div>`

Skills
======

**Software Engineering:**

* DevOps:  Well-versed in the *three ways* and *five ideals*.  Extensive
  experience serving as DevOps lead on computational science teams.
* git:  Extensive experience developing, using, and teaching simple to
  sophisticated workflows, along with managing GitLab/GitHub projects.  Prefer
  GitLab for an all-in-one DevOps solution.
* GitLab CI/CD:  Extensive experience establishing GitLab CI/CD pipelines,
  along with coupling them to Jenkins for more complex workflows when needed.
* GitHub Actions:  Significant experience standing up GitHub Actions workflows
  and interfacing external services with GitHub for additional CI testing.
* Jenkins Pipelines:  Extensive experience crafting complex pipelines and
  maintaining hundreds of jobs via Pipeline scripts.  Modest experience
  administering Jenkins instances.
* Cloud:  Experience with OpenStack (similar to AWS, GCP, Azure), Kubernetes,
  and Helm managing and deploying applications containerized with Docker to an
  internal cloud.
* Automation:  Extensive experience writing custom scripting to ensure all
  users, developers, and CI services interact with a codebase in the same,
  replicable way.  Basic experience with Ansible and Terraform.
* Quality:  Experience with SonarQube, Fortify, BlackDuck, etc.
* Agile:  Extensive experience with Scrum, Kanban, and the Scaled Agile
  Framework (SAFe).  Specialize in tailoring methodologies to the team to get
  the most out of them.
* Project Management:  Experience with requirements elicitation, design,
  execution, monitoring, and stakeholder interaction.  Flexible within the
  plan, but will work hard to protect scope and team from external
  interference.

**Programming:**

* Python:  Extensive experience writing tools to unify build processes.
  Substantial experience with Sphinx, pytest, and building modular ecosystems
  of reusable packages.  Some experience with Django and the SciPy stack.
  Referred to as the "Python ninja".  Current language of choice.
* Groovy:  Extensive experience using advanced features to build complex
  Jenkins Pipeline suites.  A close second in language of choice.
* C++:  Extensive experience developing and maintaining large object-oriented
  codes.  Experience creating and utilizing templated classes, including
  template metaprogramming.  Proficient with the Standard Template Library and
  RogueWave containers.  Some experience with Boost libraries.
* bash/tcsh:  Extensive scripting experience.
* Fortran 77/95/2003:  Experience developing large, parallel, object-oriented
  codes.
* OpenMPI:  Experience parallelizing Fortran FEM codes.
* Perl:  Some experience patching installation scripts.
* Julia:  Basic experience.
* Java:  Basic experience
* JavaScript/TypeScript:  Basic experience.
* OpenMP:  Some experience parallelizing Fortran FEM codes.  Prefer OpenMPI.

:raw-html:`<div style="page-break-before: always;"></div>`

**Mathematical Tools:**

* LaTeX:  Extensive experience typesetting a variety of works.  Prefer to use
  ``tikZ``, ``pgfplots``, and ``pgfplotstable`` to automate the generation of
  papers from code-generated data using only LaTeX.
* *Mathematica*:  Certified by Wolfram Research.  Extensive experience with
  symbolic manipulations, visualizations, creating dynamic user interfaces to
  codes, etc.
* Matlab:  Extensive experience implementing numerical methods and visualizing
  results.  Developed "Problem Solving with Matlab" tutorial series.  Some
  experience with computer vision packages.
* Trilinos:  Panzer, Teuchos, Thyra, Phalanx, E/Tpetra, NOX, LOCA, Piro, Teko.
* PETSc/LAPACK:  Experience implementing parallel FEM codes.

**Other:**

* German:  Once fluent in conversational and some technical.
* SketchUp:  Extensive experience utilizing for woodworking and carpentry
  design.
* Blackboard/Desire2Learn/MyMathLab:  Experience managing courses; online
  education certified.

Open-Source Work
================

**staged-script**
:raw-html:`<br>`
*Lead Architect / Lead Developer / User / Maintainer*
:raw-html:`<br>`
`GitHub <https://github.com/sandialabs/staged-script>`__
| `PyPI <https://pypi.org/project/staged-script/>`__
| `ReadTheDocs <https://staged-script.readthedocs.io/>`__
:raw-html:`<br>`
See :ref:`Transforming Hacky Scripts into a Modular Infrastructure
<gms-project>` below.

**reverse-argparse**
:raw-html:`<br>`
*Lead Architect / Lead Developer / User / Maintainer*
:raw-html:`<br>`
`GitHub <https://github.com/sandialabs/reverse_argparse>`__
| `PyPI <https://pypi.org/project/reverse-argparse/>`__
| `ReadTheDocs <https://reverse-argparse.readthedocs.io/>`__
:raw-html:`<br>`
See :ref:`Transforming Hacky Scripts into a Modular Infrastructure
<gms-project>` below.

**SetProgramOptions**
:raw-html:`<br>`
*Requirements Developer / Code Reviewer / User / Maintainer*
:raw-html:`<br>`
`GitHub <https://github.com/sandialabs/SetProgramOptions>`__
| `PyPI <https://pypi.org/project/setprogramoptions/>`__
| `ReadTheDocs <https://setprogramoptions.readthedocs.io/>`__
:raw-html:`<br>`
See :ref:`Unifying the DevOps Infrastructure Within Trilinos
<trilinos-devops-project>` below.

**ConfigParserEnhanced**
:raw-html:`<br>`
*Requirements Developer / Code Reviewer / User / Maintainer*
:raw-html:`<br>`
`GitHub <https://github.com/sandialabs/ConfigParserEnhanced>`__
| `PyPI <https://pypi.org/project/configparserenhanced/>`__
| `ReadTheDocs <https://configparserenhanced.readthedocs.io/>`__
:raw-html:`<br>`
See :ref:`Unifying the DevOps Infrastructure Within Trilinos
<trilinos-devops-project>` below.

**shell-logger**
:raw-html:`<br>`
*Project Lead / Developer / User / Maintainer*
:raw-html:`<br>`
`GitHub <https://github.com/sandialabs/shell-logger>`__
| `PyPI <https://pypi.org/project/shell-logger-sandialabs/>`__
| `ReadTheDocs <https://shell-logger.readthedocs.io/>`__
:raw-html:`<br>`
See :ref:`One Script to Rule Them All:  Unifying Build Processes Across
Platforms <one-script>` below.

**SPiFI**
:raw-html:`<br>`
*Requirements Developer / Code Reviewer / User / Maintainer*
:raw-html:`<br>`
`GitHub <https://github.com/sandialabs/spifi>`__
:raw-html:`<br>`
See :ref:`Developing the SPiFI Library and Associated Jenkins Pipelines
<spifi-project>` below.

Projects
========

**Open-Sourcing Sandia-Developed Software Packages**
:raw-html:`<br>`
*Sandia National Laboratories*, Fall, 2022 -- Present
:raw-html:`<br>`
In order to build Sandia's reputation in the broader software engineering and
scientific computing communities, and increase the likelihood we can attract
and retain top talent, I'm leading an effort to define and streamline our
process for open-sourcing internally-developed software.  We hope to encourage
the design and development of general-purpose, reusable components, and
establish a workflow for active co-development of both internal and external
repositories by Sandians.  This work has resulted in an estimated 4,000 hours
of labor savings per year going forward.

**Bringing Python Best Practices to a Radar Simulation Code**
:raw-html:`<br>`
*Sandia National Laboratories*, Spring, 2023 -- Present
:raw-html:`<br>`
Python was used as a wrapper language to tie together a MATLAB radar simulation
suite and a C++ visualization library for the sake of exploring the
possibilities of planning radar collections.  I was brought on to the project
to initially help with packaging and distribution, and then to start applying
Python development best practices to the code.  In the midst of the latter, I
brought the team up-to-speed with modern software project management practices
using GitLab.  This led to an effusively positive review from an internal
software quality audit, and the auditors recommended some of our practices as
worthy of imitation for software projects across the labs.  Refactoring the
application with a focus on modularity enabled the team to quickly respond to
customer requests for a variety of new features.

**Code Generation Webapp**
:raw-html:`<br>`
*Sandia National Laboratories*, Spring, 2023 -- Summer, 2023
:raw-html:`<br>`
Sandia's radar software group uses a Django-based webapp to store programmatic
representations of the interface control documents (ICDs) for the radars under
development.  The app can translate the ICDs to CSV, HTML, and JSON, and can
automatically generate the C++ or Java code needed to interface with the
devices.  I extended the webapp to allow a secondary C++ generator for a
specialized use case, and refactored the code to start bringing it into
alignment with Python best practices.

.. _gms-project:

**Transforming Hacky Scripts into a Modular Infrastructure**
:raw-html:`<br>`
*Sandia National Laboratories*, Spring, 2022 -- Spring, 2023
:raw-html:`<br>`
Many view scripting as something that just needs to be done to enable the real
work to take place.  With such a view, there's a strong tendency to think
there's no need to apply software engineering best practices to the development
of your scripts, which, over time, leads to an ever-growing mountain of
technical debt.  Over the course of a year, I refactored a collection of
hacked-together Python scripts into a modular infrastructure, the components of
which are focused on doing only one thing and doing it well, and are reusable
in multiple contexts.  They are fully documented with Google-style docstrings,
and fully unit tested with ``pytest``.  The project concluded with a series of
tech talks (6+ hours of recorded content) to give the high-level overview of
how the various pieces interact, and the design considerations that went into
building them.  The talks highlight that the infrastructure now serves as a
case study for adhering to the SOLID principles of object-oriented design, and
should set the stage for further infrastructure extensions in the future after
their operations and maintenance was handed over to other individuals on the
team.

**DevOps Book Club**
:raw-html:`<br>`
*Sandia National Laboratories*, Fall, 2021 -- Present
:raw-html:`<br>`
Since building shared understanding is the basis for cultural transformation, I
established a book club to help my team build their understanding of the
fundamental principles that underpin the DevOps paradigm shift.  This is
accomplished through reading and discussing books on DevOps proper, on
collaboration and leadership, and on software engineering best practices.  This
effort has been called out in multiple retrospectives as having significantly
impacted the competencies of team members and overall effectiveness of the
team.

**From Fragility to Flexibility**
:raw-html:`<br>`
*Sandia National Laboratories*, Spring, 2023
:raw-html:`<br>`
Everyone has experience with a piece of code that's absolutely critical, but
everyone's afraid to touch.  The original developers have moved on, if you
touch it, you might break it, and it's "working" now.  One such example was the
Python script used to orchestrate a series of ``helm`` and ``kubectl`` commands
in the shell to deploy containerized services to internal Kubernetes clusters.
Over time, though, infrastructure feature requests continued to pile up to
which the response was always, "We could do that, but that would require
modifying the code we're not supposed to touch."  I took the initiative to
completely rewrite the script, breaking it up into a series of modules for
reusability elsewhere.  The unit test coverage level was maintained at 94%, and
no existing behavior was broken.  We tested the new system alongside the old
one for a number of weeks to prove feature parity before merging.  After the
reimplementation was merged, the various feature requests were able to be
addressed with only a few lines each, and the new flexible architecture will
allow the module to continue to grow to meet evolving stakeholder needs in the
future.

**Automated Testing Improvement Initiative**
:raw-html:`<br>`
*Sandia National Laboratories*, Summer, 2021 -- Fall, 2022
:raw-html:`<br>`
The `Geophysical Monitoring System <https://github.com/SNL-GMS/GMS-PI21-OPEN>`_
is a Kubernetes-based application suite developed in Java and TypeScript that
historically has a substantial set of manual acceptance tests that take days to
complete for each quarterly release.  I led an initiative to establish the
infrastructure and associated team policies and best practices to automate
component-, integration-, and system-level tests in the GitLab CI pipelines.
After the initial phase was completed, the prototype infrastructure was matured
to production-ready, the pipelines were optimized, and the robustness of the
applications was improved.  Work to automate existing manual tests continues as
we slowly but surely move toward continuous delivery.  It's worth noting that I
had no prior experience with GitLab CI pipelines, but learned the new
technology in a matter of weeks and quickly earned the nickname of "pipeline
guru".

:raw-html:`<div style="page-break-before: always;"></div>`

.. _trilinos-devops-project:

**Unifying the DevOps Infrastructure Within Trilinos**
:raw-html:`<br>`
*Sandia National Laboratories*, Summer, 2020 -- Spring, 2021
:raw-html:`<br>`
Over the past few years, two distinct DevOps infrastructures have grown up
within the `Trilinos <https://trilinos.github.io>`_ project.  Understanding
that both solutions had their pros and cons, both were less flexible than
desirable, and ultimately the prospect of maintaining two separate solutions
long term would be fraught with error, it was determined a year-long effort
would be made to replace them with a single solution incorporating the lessons
learned from the past.  I conducted an initial investigation of the existing
solutions over a two-month period, and then followed that with a time of
gathering stakeholder requirements.  I then drafted a plan to cover two
general-purpose components for consistently loading environments across
machines, and consistently configuring a `CMake <https://cmake.org>`_-based
code, and then led the all-remote team, spread across four states in two time
zones, in the design and execution.  Modularity, flexibility, unit testing,
code coverage, and documentation were all hallmarks of the way we tackled the
problem.  The intent was to not only provide Trilinos with what it needed, but
to provide those in the greater scientific software community with general
tools they can use to improve the sustainability and replicability of the codes
they develop.

**DevOps Infrastructure Consultant**
:raw-html:`<br>`
*Sandia National Laboratories*, Summer, 2020
:raw-html:`<br>`
The `Dakota <https://dakota.sandia.gov/>`_ project provides a software suite
for optimization and uncertainty quantification.  Their build and test
infrastructure had grown organically over more than two decades to the point of
being both fragile and brittle.  They sought my services to determine what they
would need to do to get from where there were to where they wanted to be.  I
conducted a number of interviews with team members, and interfaced closely with
their newly hired DevOps engineer, to determine both their needs and what they
could realistically accomplish.  I then developed a 15-month plan to rebuild
their infrastructure from the ground up such that it would be easy to maintain
and extend for years into the future.  I presented the plan to a wide audience
largely via an extended metaphor, so the various pieces would be easy to grasp
by non-experts, and a secretary's reaction was, "I hardly ever know what you
all are talking about, but this presentation I understood!"

**Developing JOG-CI:  Connecting Jenkins, OpenStack, and GitLab CI/CD**
:raw-html:`<br>`
*Sandia National Laboratories*, Spring, 2020 -- Summer, 2020
:raw-html:`<br>`
`OpenStack <https://www.openstack.org>`_ is a collection of components that
allows you to maintain your own private cloud infrastructure.  The ability to
rapidly stand up cloud tenants, running on corporate hardware behind the
scenes, was desirable for lowering the barrier to entry for teams to get up and
running with continuous integration.  A lightweight tool for standing up such
tenants and connecting them to either Jenkins or GitLab (or both) was developed
under my direction by our department's year-round intern, and that tool has
been used by a handful of teams to stand up and tear down instances as needed,
depending on changing testing needs.  We both came up to speed with OpenStack
and the ins and outs of private cloud administration in a matter of weeks.

:raw-html:`<div style="page-break-before: always;"></div>`

**Faster Turnaround Improves Developer Productivity**
:raw-html:`<br>`
*Sandia National Laboratories*, Winter, 2019 -- Summer, 2020
:raw-html:`<br>`
A complete run of EMPIRE's pipelines used to take about 20 hours.  Running only
once per day, it was hard to determine where new bugs were introduced in a
codebase that would see dozens of requests merged daily.  As such, a merge from
develop to master would happen every few weeks, if we were lucky.  I led a
major refactor of our pipelines, restructuring them with modularity in mind,
such that they could fail and get actionable feedback to the team as soon as
possible.  We additionally achieved parallelizing the testing across a
collection of machines, again decreasing our time to notification of success or
failure.  The dozens of Jenkins jobs used by each top-level pipeline are
governed by a single Groovy Pipeline script, making maintainability and
extensibility a breeze.  The end result was a reduction down to about five
hours, such that the pipeline suite now runs multiple times a day.  With more
frequent feedback, we're kept clean more often, and developers spend less time
debugging and more time doing science.

.. _one-script:

**One Script to Rule Them All:  Unifying Build Processes Across Platforms**
:raw-html:`<br>`
*Sandia National Laboratories*, Summer, 2019 -- Spring, 2020
:raw-html:`<br>`
The BuildScripts repository for the EMPIRE codebase had grown organically over
time, with bash scripts for running on different platforms, with different
configurations, etc.  Developers also had their own scripts for setting up
their environment and configuring the code.  I led an effort to unify our build
process across platforms and create a "one build script to rule them all," so
to speak, to be used by users, developers, and automation services.  Python was
used for the sake of documentation (`Sphinx
<https://www.sphinx-doc.org/en/master/>`_), testing (`pytest
<https://docs.pytest.org/en/latest/>`_), and unified style guides.
Replicability was enhanced by building in both a comprehensive logging utility
and the ability to replay prior runs of the script.  The tool was designed with
modularity and flexibility in mind, such that it's easy extend existing pieces
or plug in new ones when future needs arise.  Investing the time, money, and
energy in developing such an infrastructure paid dividends in productivity,
both for the scientific developers and the DevOps engineers.

.. _spifi-project:

**Developing the SPiFI Library and Associated Jenkins Pipelines**
:raw-html:`<br>`
*Sandia National Laboratories*, Spring, 2018 -- Spring, 2019
:raw-html:`<br>`
In order to adequately test the git workflow mentioned directly below, a
flexible pipeline was needed, and the `Jenkins pipeline plugin suite
<https://www.jenkins.io/doc/book/pipeline/>`_ with the `Apache Groovy
<https://groovy-lang.org>`_ language under the hood provided the power
necessary.  The plugin suite has a high barrier to entry, so a colleague and I
worked closely together to develop the SEMS Pipeline Framework Infrastructure
(`SPiFI <https://github.com/sandialabs/SPiFI>`_) library, I developing the
pipeline itself and driving the requirements for the library, and he developing
the library to ease and automate routine pipeline tasks.  The library has since
been rolled out to half a dozen teams or so, and is used to drive hundreds of
jobs on a daily basis.

:raw-html:`<div style="page-break-before: always;"></div>`

**Stability with Respect to the Tip of Develop**
:raw-html:`<br>`
*Sandia National Laboratories*, Fall, 2017 -- Fall, 2018
:raw-html:`<br>`
`Trilinos <https://trilinos.github.io>`_ is a collection of math libraries for
large-scale, complex multi-physics problems on next generation high-performance
computing architectures.  Its development is largely driven by a handful of
physics application codes that are tightly coupled with it.  Because the
applications drive the algorithm development, they would like to be able to use
the latest commit on the develop branch, but at the same time they would like
to make sure commits to Trilinos never break them and stall application
development.  I developed a git workflow involving a fork of Trilinos and a
secondary approved version of the develop branch, which is updated
automatically via nightly testing.  In the event testing fails, the branch
isn't updated, and the application team can continue development unhindered.
They can file an issue against Trilinos that will be resolved through Trilinos'
usual process.  Flexibility is also afforded for the rare instances where
simultaneous changes must be made to both the application and Trilinos
codebases.  This approach has been used successfully by two separate
application teams for the last few years.

**Defining Policies to Turn a Team and Project Around**
:raw-html:`<br>`
*Sandia National Laboratories*, Summer, 2017 -- Fall, 2018
:raw-html:`<br>`
EMPIRE is a collection of next generation electromagnetic/electrostatic/fluid
dynamic codes.  Prior to the summer of 2017, there was confusion as to who was
on the team, what people were working on, what needed to be done, how one could
get started, etc.  Pushes happened directly to the master branch, and there was
minimal testing, code review, documentation, etc.  I played a large part in
driving the adoption of the following:  *GitLab issues*, description templates,
and Kanban boards were used to track work and capture design discussions.
*GitLab merge requests*, complete with code review and approval, were required
to get changes into the develop branch.  *Style guides* for both the code and
documentation were developed to move toward a common look and feel.  A *git
workflow* was developed to ensure no direct pushes to master or develop, and
master would be updated via nightly testing.  *Automated testing* was
established to test multiple machines and nfigurations to improve stability.  A
*monthly retrospective* was established to regularly check in on how well our
policies were working for us and allow us to tweak them as needed.

**Git Instruction**
:raw-html:`<br>`
*Sandia National Laboratories*, Spring, 2017 -- Fall, 2019
:raw-html:`<br>`
I led the Center for Computing Research University (CCR-U) group in teaching
courses introducing participants to version control via git, utilizing the
`Software Carpentry <https://carpentries.org>`_ instruction style.  We
developed both introductory and intermediate courses, which were very popular
and received excellent feedback, and helped hundreds of Sandians to level up
their software engineering competency over the course of a few short years.
It's worth noting that I went from having never used git to teaching courses in
it in under a year, quickly earning the tongue-in-cheek moniker of "git fu
master".

:raw-html:`<div style="page-break-before: always;"></div>`

**Panzer Memory Usage Refactor**
:raw-html:`<br>`
*Sandia National Laboratories*, September, 2016 -- July, 2017
:raw-html:`<br>`
Local to global communication in parallel finite element simulations occurs
through the use of *owned* vectors, containing all the information owned by a
given process, and *ghosted* vectors, containing the information from
neighboring processes.  The original implementation duplicated all the data in
the owned vector in the midst of the ghosting process, meaning more data was
being stored in memory than was necessary.  I refactored classes such that
ghosted vectors contain only the ghosted information, and any time a user wants
to grab an element of a vector given a local ID, the logic of whether it lives
in the owned or ghosted vector is hidden from the user.  Avoiding the data
duplication significantly reduces the run-time memory usage.

**Generalized Current Constraint Boundary Conditions in Charon**
:raw-html:`<br>`
*Sandia National Laboratories*, October, 2016 -- June, 2017
:raw-html:`<br>`
The Charon semiconductor device physics simulation code previously had the
ability to attach a constant current constraint to a terminal of a device
(diode, transistor, etc.).  I generalized this capability such that any number
of constraints can be added to a device (at most one per terminal).  A resistor
contact constraint type was added, corresponding to hooking up a resistor with
a voltage source on its far side.  A block LDU preconditioner was generalized
to work for any of these constraint scenarios.  This capability helps users
more readily simulate real-world configurations.

**LOCA and Charon Integration**
:raw-html:`<br>`
*Sandia National Laboratories*, July -- September, 2017
:raw-html:`<br>`
Previously if a Charon user wanted to sweep a voltage contact boundary
condition on a device, they would use a rather brute-force Python script to get
the job done.  I integrated the Library of Continuation Algorithms (LOCA) with
Charon to provide this capability natively, and with more flexibility.  LOCA is
able to intelligently ramp up the parameter step size, and, in the case of a
solver failure, backtrack, cut the step size, and proceed with the continuation
run.  This also provides the capability to track bifurcations in the future,
should we need to.

**Algorithm Development**
:raw-html:`<br>`
*Northrop Grumman Corporation*, September, 2014 -- June, 2016
:raw-html:`<br>`
Given real-time input data from multiple sources, how do we clean and
manipulate the data to yield the answer we seek?  Details of the algorithm and
its application are classified.  A colleague and I reviewed relevant
literature, determined most information was no longer applicable to our new
geometric configuration, and developed an elegant iterative algorithm to walk
its way intelligently through the solution space to the correct answer.  I also
developed a Matlab tool to read in pieces of data from in the midst of the
algorithm to generate a multi-page PDF detailing just how the algorithm is
working its way to the solution, which aids tremendously in discovering
scenarios for which the algorithm needs improvement.

:raw-html:`<div style="page-break-before: always;"></div>`

**Automating Large-Scale Distributed Software Installation**
:raw-html:`<br>`
*Northrop Grumman Corporation*, Summer, 2014
:raw-html:`<br>`
An installation and configuration of HP's Network Node Manager software suite
across multiple virtual machines (VMs) took an operator four days using a
series of manuals to guide them through the process.  Having no experience with
VMs or Linux sysadmin activities, I came up to speed in weeks.  I then
developed a series of scripts to be deployed and run on the VMs to update
various packages in Red Hat Enterprise Linux (RHEL) to the appropriate
versions, patch some of HP's Perl scripts used in the installation, and install
and configure the software suite.  Automating the process reduced the time
needed to about two hours with minimal human interaction.

**Adaptive Local-Global Multiscale Finite Element Methods**
:raw-html:`<br>`
*Colorado School of Mines*, August, 2012 -- May, 2014
:raw-html:`<br>`
When solving the classical uniformly elliptic boundary value problem in a
medium that is either highly oscillatory or has high contrast the standard
Galerkin finite element method (FEM) is insufficient and :math:`h`-,
:math:`p`-, and :math:`r`-refinement become prohibitively expensive for large
problems.  Multiscale FEMs consist of solving local homogeneous problems on the
course mesh elements to create multiscale basis functions that already have
some knowledge of the medium.  Determining the appropriate boundary conditions
for these local solves is an area of active research.  The adaptive
local-global multiscale FEM projects an initial global solve onto extended
course mesh elements, makes that projection nodal on the coarse mesh elements,
and then averages across the edges of the course mesh.  The resulting local
solves yield nodal basis functions with expanded support that satisfy the
partition of unity.  In theory there exist ideal basis functions that can
reconstruct the exact solution exactly---iterating this method allows us to
work toward those ideal basis functions.  This computational effort can be done
ahead of time such that the near-ideal basis functions can be used for any
source terms and time-evolution scenarios.  Effective parallelism was achieved
through the use of OpenMPI and PETSc.

**Automated Generation of Homework Assignments and Solution Procedures**
:raw-html:`<br>`
*Colorado School of Mines*, August, 2013 -- May, 2014
:raw-html:`<br>`
Problems in Advanced Engineering Mathematics are highly formulaic---given a
problem of a certain type, there are certain steps to follow to the solution.
As such the generation of such problems, *and their full solution procedures*,
is simply a matter of programming.  *Mathematica* was utilized to randomly
generate problem sets and solutions for the class.

**Manufacturing Solutions to Fluid Flow Problems**
:raw-html:`<br>`
*Sandia National Laboratories*, Summer, 2012
:raw-html:`<br>`
Assuming solutions of a certain form and working them through systems of
nonlinear coupled partial differential equations (PDEs) allows one to determine
the source terms necessary for the equations to be satisfied.  I developed a
*Mathematica* suite for manufacturing such solutions to incompressible Navier
Stokes, some of its turbulent extensions, and to MHD.  Solutions and source
terms were exportable to C for interfacing with a code being validated, and all
details were exportable to LaTeX for paper generation.

:raw-html:`<div style="page-break-before: always;"></div>`

**Boundary Integral Equation Methods for Solutions to Laplace's Equation**
:raw-html:`<br>`
*University of Tulsa*, Fall, 2010
:raw-html:`<br>`
This general solution method consists of transferring all the computation from
the domain to its boundary.  Both inner and outer Dirichlet, Neumann, and Robin
problems were considered.  Solvability was proven, and uniqueness was shown for
all but the inner Neumann problem, whose solutions differ only by a constant.
Solutions were determined in terms of harmonic potentials from Green's
representation formulas.

**Boundary Element Method and Visualization Tool**
:raw-html:`<br>`
*University of Tulsa*, Fall, 2010
:raw-html:`<br>`
The numerical equivalent to the project above, when attempting to solve a PDE
on a given domain, one can instead subdivide the boundary into a number of
boundary elements and do all the necessary integration there.  Determining the
solution somewhere in the domain is then just a matter of evaluating a function
at that point.  I developed an interactive *Mathematica* suite for solving
various PDEs.  Users have the ability to specify the boundary, various PDE and
boundary condition terms, where to evaluate the solution, etc.

**Nonlinear Evolution of Unstable MHD Equilibria**
:raw-html:`<br>`
*University of Tulsa*, May, 2007 -- May, 2009
:raw-html:`<br>`
I created a user interface between an eigenvalue code, an equilibrium code
(SCOTS), and a nonlinear MHD evolution code (NIMROD) allowing for an
exploration of parameter space to determine where modes were stable or
resistive- or ideal-unstable.  We then ran nonlinearly from a starting point
near the stability boundary and observed how the plasma evolved.

Clearance
=========

**DOE Q:**  July, 2018 -- Present

| **DOD TS/SCI:**  May, 2014 -- June, 2016\
| *Deactivated after leaving Northrop Grumman Corporation*

Certifications
==============

.. figure:: ../images/lfd121-developing-secure-software.png
   :align: right
   :target: https://ti-user-certificates.s3.amazonaws.com/e0df7fbf-a057-42af-8a1f-590912be5460/e2ed501a-70a9-4f81-b839-1138e7b5d7a1-jason-gates-8f5fb502-c893-4eb5-831f-158f69419699-certificate.pdf

* `The Linux Foundation's Devloping Secure Software Certification Course <https://www.credly.com/badges/35b9a6ba-b2d1-40ff-afda-fdbf900e5910/public_url>`_
* `GitKraken Foundations of Git---Certification Course <https://learn.gitkraken.com/certificates/h06f53lrnq>`_
* `Carpentries Instructor <https://carpentries.org/>`_
* `CompTIA Security+ <https://www.comptia.org/landing/certificationsecurityplus/index.html?utm_compid=cpc-bing-paid_search_certs-Security%2B-text_ad-na-security%2B-B2C&utm_source=bing&utm_medium=cpc&utm_campaign=Security%2B%20Brand%2BCert&utm_term=comptia%20security%2B%20certification&utm_content=Security%2B%20Brand%2BCertification>`_ (June 2014 -- May 2017)
* *Mathematica*---Advanced Foundations

:raw-html:`<div style="page-break-before: always;"></div>`

Continuing Education
====================

**Kubernetes:**

* Docker & Kubernetes Internals Workshop (internal)
* `Kubernetes Hands-On---Deploy Microservices to the AWS Cloud <https://www.udemy.com/course/kubernetes-microservices/>`_
* `Certified Kubernetes Administrator (CKA) with Practice Tests <https://www.udemy.com/course/certified-kubernetes-administrator-with-practice-tests/>`_

**Python:**

* `Python for Scientists and Engineers <https://www.enthought.com/course/python-for-scientists-and-engineers/>`_
* `Pandas Mastery Workshop <https://www.enthought.com/course/pandas-mastery-workshop/>`_
* `Machine Learning Mastery Workshop <https://www.enthought.com/course/machine-learning-mastery-workshop/>`_

**Project Management:**

* `Project Management Overview <https://dynamicsolutionsnm.com/course-offerings/>`_
* `Managing a Project from Start to Finish <https://dynamicsolutionsnm.com/course-offerings/>`_

**Leadership:**

* The Art of Being an Effective Mentor (internal)
* Preparing for Management (internal)
* `Crucial Conversations <https://www.amazon.com/Crucial-Conversations-Talking-Stakes-Second/dp/1469266822>`_
* Five Conflict Styles and How to Use Them (internal)
* `Social Styles <https://www.amazon.com/People-Styles-Work-Beyond-Relationships/dp/0814413420/ref=sr_1_1?crid=2Q9LMTVLZ5HQU&keywords=people+styles+at+work+and+beyond&qid=1663681991&sprefix=people+styles+at+work+and+beyon%2Caps%2C214&sr=8-1>`_

**Miscellaneous:**

* `The Software Engineering Body of Knowledge <https://www.computer.org/education/bodies-of-knowledge#swebok>`_
* Basic Instructor Training (internal)
* System Administrator Policies (internal)
* `Clean Code <https://cleancoders.com/>`_

Presentations
=============

#. Jason M. Gates and Joseph Valeriano.  "Best Practices for Remote Work."
   *Interview.*  GitLab Government User Group.  November 2023.
#. Jason M. Gates and Jonathan Silva.  `"When Workflows Don’t Feel Like Work:
   A Lesson in Simplifying Pull Requests"
   <https://www.youtube.com/live/qzYJfJtB81U?si=lT63wMKCrjLGGCcT&t=5092>`_.
   *Interview.*  GitKon.  October 2023.
#. Jason M. Gates.  `"The Complexities of Replicability."
   <https://zenodo.org/record/7754004>`_  *Presentation.*  The Department of
   Applied Mathematics @ CU Boulder.  April 2023.
#. Jason M. Gates, David Collins, and Josh Braun.  "ShellLogger:  Keeping Track
   of Python's Interactions with the Shell."  *Presentation.*  Tri-lab Advanced
   Simulation & Computing Sustainable Scientific Software Conference.  May
   2022.
#. Jason M. Gates and William McLendon.  "Enhancing Python's ConfigParser."
   *Lightning talk.*  US Research Software Engineering Association Community
   Call.  April 2022.
#. David Collins, Josh Braun, and Jason M. Gates.  `"Logger:  A Tool for
   Keeping Track of Python's Interactions with the Shell."
   <https://www.youtube.com/watch?v=P32RYY_2V7w&t=5985>`_  *Presentation.*
   US Research Software Engineering Conference.  May 2021.
#. Jason M. Gates, William Mclendon, Josh Braun, and Evan Harvey.  `"LoadEnv:
   Consistently Loading Supported Environments Across Machines."
   <https://www.youtube.com/watch?v=P32RYY_2V7w&t=6833>`_  *Presentation.*
   US Research Software Engineering Conference.  May 2021.
#. Jason M. Gates, David Collins, and Josh Braun.  `"CI Tools as Lego Blocks:
   Build Your Ideal Custom Solution."
   <https://figshare.com/articles/presentation/CI_Tools_as_Lego_Blocks_Build_Your_Ideal_Custom_Solution/14180096>`_
   *Presentation.*  Society of Industrial & Applied Mathematics Computational
   Science & Engineering Conference.  March 2021.
#. Vivek Sarkar, Jason Gates, Charles Ferenbaugh, Vadim Dyadechko, Anshu Dubey,
   Hartwig Anzt, and Pat Quillen.  `"Technical Approaches to Improve Developer
   Productivity for Scientific Software."
   <https://www.youtube.com/watch?v=zMmtUgEExZ8>`_  *Panel discussion.*
   Collegeville Workshop on Scientific Software.  July 2020.
#. Jim Willenbring, Ross Bartlett, and Jason Gates.  `"Git Solutions."
   <https://www.youtube.com/watch?v=OyJ0AnesaRw>`_  *Interview.*  Collegeville
   Workshop on Scientific Software.  July 2020.
#. Jason M. Gates.  "Training Best Practices."  *Tea time discussion.*
   Collegeville Workshop on Scientific Software.  July 2020.
#. Jason M. Gates.  "Introduction to GitDist."  *Presentation.*  Trilinos
   User-Developer Group Meeting.  October 2019.
#. Jason M. Gates.  "Intro to SPiFI."  *Presentation.*  Trilinos User-Developer
   Group Meeting.  October 2019.
#. Jason M. Gates.  "Stability w.r.t. the Tip of develop:  An Experience
   Report from Two Years In."  *Presentation.*  Trilinos User-Developer Group
   Meeting.  October 2019.
#. Jason M. Gates.  `"Training in Version Control and Project Management."
   <https://zenodo.org/record/7753354>`_  *Lightning talk.*  Collaborations
   Workshop.  March 2019.
#. Jason M. Gates.  "Stability w.r.t. the Tip of Develop."  *Presentation.*
   Trilinos User-Developer Group Meeting.  October 2017.
#. Jason Matthew Gates, Roger P. Pawlowski, and Eric Christopher Cyr.
   `"Panzer:  A Finite Element Assembly Engine within the Trilinos Framework."
   <https://zenodo.org/record/7753260>`_  *Presentation.*  Society of
   Industrial & Applied Mathematics Computational Science & Engineering
   Conference.  March 2017.

Publications
============

#. Jason Gates.  `"From Beginner to Pro: The Three Stages of Git Adoption."
   <https://www.gitkraken.com/blog/three-stages-git-adoption>`_  *Blog post.*
   Git Blog.  December 2023.
#. Miranda Mundt, Jon Bisila, Reed Milewicz, Joshua Teves, Michael Buche,
   Jonathan Compton, Jason Gates, Kirk Landin, and Jay Lofstead.  `"Challenges
   and Strategies for Testing Automation Practices at Sandia National
   Laboratories." <https://zenodo.org/records/10420497>`_  *Paper.*  US
   Research Software Engineering Conference.  October 2023.
#. Jason M. Gates.  `"Using GitLab Issues for Iterative, Asynchronous Software
   Design." <https://zenodo.org/record/7250115>`_  *Whitepaper.*  Collegeville
   Workshop on Scientific Software.  July 2023.
#. Reed Milewicz, Jonathan Bisila, Miranda Mundt, Sylvain Bernard, Michael
   Buche, Jason M. Gates, Samuel Andrew Grayson, Evan Harvey, Alexander Jaeger,
   Kirk Timothy Landin, Mitchell Negus, and Bethany L. Nicholson.  `"DevOps
   Pragmatic Practices and Potential Pitfalls in Scientific Software
   Development." <http://icict.co.uk/ICICT%202023%20Vol%201.pdf>`_  *Paper.*
   International Congress on Information and Communication Technology.
   February 2023.
#. Jason M. Gates, Josh Braun, and David Collins.  `"One Script to Rule Them
   All:  Unifying Build Processes Across Platforms."
   <https://zenodo.org/record/7753980>`_  *Whitepaper.*  Collegeville Workshop
   on Scientific Software.  July 2020.
#. Jason M. Gates, Joe Frye, Brent Perschbacher, and Dena Vigil.  `"Git
   Productive!" <https://zenodo.org/record/7753973>`_  *Whitepaper.*
   Collegeville Workshop on Scientific Software.  July 2020.
#. Jason M. Gates.  `"Faster Turnaround Improves Developer Productivity."
   <https://zenodo.org/record/7753371>`_  *Poster.*  Collegeville Workshop on
   Scientific Software.  July 2020.
#. Patrick McCann, Rachael Ainsworth, Jason M. Gates, Jakob S. Jørgensen,
   Diego Alonso-Álvarez, and Cerys Lewis.  `"How do you motivate researchers
   to adopt better software practices?"
   <https://software.ac.uk/blog/2019-07-03-how-do-you-motivate-researchers-adopt-better-software-practices>`_
   *Speed blog.*  Collaborations Workshop.  July 2019.
#. Jason M. Gates.  `"Defining Policies to Turn a Team and Project Around."
   <https://zenodo.org/record/7753335>`_  *Poster.*  Third Conference of
   Research Software Engineers.  September 2018.
#. D P Brennan, P K Browning, J Gates, and R A M Van der Linden.
   `"Helicity-injected current drive and open flux instabilities in spherical
   tokamaks." <https://iopscience.iop.org/article/10.1088/0741-3335/51/4/045004/pdf>`_
   *Plasma Physics and Controlled Fusion* 51.4 (2009):045004.

:raw-html:`<div style="page-break-before: always;"></div>`

Favorite Work-Related Books
===========================

**DevOps:**

* `The Phoenix Project
  <https://www.amazon.com/Phoenix-Project-DevOps-Helping-Business/dp/1942788290/ref=sr_1_2?dchild=1&keywords=gene+kim&qid=1616636863&sr=8-2>`_:
  A Novel About IT, DevOps, and Helping Your Business Win, by Gene Kim.
* `The DevOps Handbook
  <https://www.amazon.com/DevOps-Handbook-World-Class-Reliability-Organizations/dp/1942788002/ref=sr_1_3?dchild=1&keywords=gene+kim&qid=1616636863&sr=8-3>`_:
  How to Create World-Class Agility, Reliability, and Security in Technology
  Organizations, by Gene Kim, Jez Humble, Patrick DeBois, & John Willis.
* `The Unicorn Project
  <https://www.amazon.com/Unicorn-Project-Developers-Disruption-Thriving/dp/1942788762/ref=sr_1_1?dchild=1&keywords=gene+kim&qid=1616636863&sr=8-1>`_,
  by Gene Kim.

**Teaming & Leadership:**

* `Team Topologies <https://www.amazon.com/Team-Topologies-Organizing-Business-Technology/dp/1942788819/ref=sr_1_1?sr=8-1>`_:
  Organizing Business and Technology Teams for Fast Flow, by Matthew Skelton
  and Manuel Pais
* `The Five Dysfunctions of a Team
  <https://www.amazon.com/Five-Dysfunctions-Team-Leadership-Fable/dp/0787960756/ref=sr_1_1?crid=3I8FG8CLI6P3R&dchild=1&keywords=five+dysfunctions+of+a+team&qid=1616637023&sprefix=five+dysf%2Caps%2C270&sr=8-1>`_:
  A Leadership Fable, by Patrick Lencioni.
* `The Manager's Path
  <https://www.amazon.com/Managers-Path-Leaders-Navigating-Growth/dp/1491973897/ref=sr_1_1?sr=8-1>`_:
  A Guide for Tech Leaders Navigating Growth and Change, by Camille Fournier.
* `Dare to Lead
  <https://smile.amazon.com/Dare-Lead-Brave-Conversations-Hearts/dp/0399592520/ref=sr_1_1?dchild=1&keywords=dare+to+lead&qid=1620683460&sr=8-1>`_:
  Brave Work, Tough Conversations, Whole Hearts, by Brené Brown.
* `1501 Ways to Reward Employees
  <https://www.amazon.com/1501-Reward-Employees-Nelson-Ph-D/dp/0761168788/ref=sr_1_1?crid=2BGYF4F8U4317&dchild=1&keywords=1501+ways+to+reward+employees+by+bob+nelson&qid=1616637149&sprefix=1501+ways%2Caps%2C265&sr=8-1>`_,
  by Bob Nelson.

**Software Crafstmanship:**

* `Clean Code
  <https://www.amazon.com/Clean-Code-Handbook-Software-Craftsmanship/dp/0132350882/ref=sr_1_1?keywords=clean+code+by+robert+c.+martin&qid=1678312642&sprefix=clean+code+%2Caps%2C154&sr=8-1>`__:
  A Handbook of Agile Software Craftsmanship, and `Clean Architecture
  <https://www.amazon.com/Clean-Architecture-Craftsmans-Software-Structure/dp/0134494164/ref=sr_1_1?keywords=clean+architecture+by+robert+c.+martin&qid=1680053498&sprefix=clean+arch%2Caps%2C188&sr=8-1>`_:
  A Craftsman's Guide to Software Structure and Design, by Robert C. Martin.
* `The Pragmatic Programmer
  <https://www.amazon.com/Pragmatic-Programmer-journey-mastery-Anniversary/dp/0135957052/ref=tmm_hrd_swatch_0?_encoding=UTF8&sr=8-1>`_:
  Your Journey to Mastery, by David Thomas and Andrew Hunt.
* `The Mythical Man-Month
  <https://www.amazon.com/Mythical-Man-Month-Software-Engineering-Anniversary/dp/0201835959/ref=sr_1_1?sr=8-1>`_:
  Essays on Software Engineering, by Frederick P. Brooks, Jr.

Honors & Awards
===============

* Team Employee Recognition Award for EMPIRE
* Team Employee Recognition Award Nomination for Advanced Simulation and
  Computing DevOps Visionaries
* Spot Award for Git Training
* Department of Applied Mathematics and Statistics Graduate Student Teaching
  Award
* Graduate Teaching Fellowship \& Assistantships
* Outstanding Senior in German
* Academic Excellence Award
* Member of ΦΒΚ, ΦΣΙ, ΤΒΠ, ΣΠΣ
* University of Tulsa Presidential Scholarship
* Byrd Scholarship
* Oklahoma Academic All-State Scholarship
* ACT Perfect Score

